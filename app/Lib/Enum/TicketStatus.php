<?php
  App::uses('Enum', 'Lib');
  class TicketStatus extends Enum {
    const NEW_TICKET = 1;
    const IN_PROGRESS = 2;
    const FEEDBACK = 3;
    const REVIEW = 4;
    const RESOLVED = 5;
    const REJECTED = 6;

  protected static $_options = array(
    self::NEW_TICKET => 'New',
    self::IN_PROGRESS => 'In Progress',
    self::FEEDBACK => 'Feedback',
    self::REVIEW => 'Review',
    self::RESOLVED => 'Resolved',
    self::REJECTED => 'Rejected'
  );
}