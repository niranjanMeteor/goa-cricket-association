<?php
  App::uses('Enum', 'Lib');
  class UserProjectRole extends Enum {
    const ADMINISTRATOR = 1;
    const MANAGER = 2;
    const REPORTER = 3;

  protected static $_options = array(
    self::ADMINISTRATOR => 'Administrator',
    self::MANAGER => 'Manager',
    self::REPORTER => 'Reporter'
  );
}