<?php
  App::uses('Enum', 'Lib');
  class PlayerArm extends Enum {
    const RIGHT = 1;
    const LEFT = 2;

  protected static $_options = array(
    self::RIGHT => 'Right Arm',
    self::LEFT => 'Left Arm',
  );
}