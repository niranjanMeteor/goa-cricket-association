<?php
  App::uses('Enum', 'Lib');
  class TicketPriority extends Enum {
    const LOW = 1;
    const NORMAL = 2;
    const HIGH = 3;
    const URGENT = 4;
    const IMMEDIATE = 5;

  protected static $_options = array(
    self::LOW => 'Low',
    self::NORMAL => 'Normal',
    self::HIGH => 'High',
    self::URGENT => 'Urgent',
    self::IMMEDIATE => 'Immediate'
  );
}