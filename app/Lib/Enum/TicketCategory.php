<?php
  App::uses('Enum', 'Lib');
  class TicketCategory extends Enum {
    const TASK = 1;
    const SUPPORT = 2;
    const SUBTASK = 3;

  protected static $_options = array(
    self::TASK => 'Task',
    self::SUPPORT => 'Support',
    self::SUBTASK => 'Sub-Task'
  );
}