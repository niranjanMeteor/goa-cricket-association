<?php
  App::uses('Enum', 'Lib');
  class TeamPlayerStatus extends Enum {
    const ACTIVE = 1;
    const INACTIVE = 2;

  protected static $_options = array(
    self::ACTIVE => 'Active',
    self::INACTIVE => 'Inactive'
  );
}