<?php
  App::uses('Enum', 'Lib');
  class UserStatus extends Enum {
    const VERIFIED = 1;
    const UNVERIFIED = 1;

	protected static $_options = array(
		self::VERIFIED => 'Verified',
		self::UNVERIFIED => 'Unverified'
	);
}