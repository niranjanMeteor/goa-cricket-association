<?php
  App::uses('Enum', 'Lib');
  class UmpireGrade extends Enum {
    const BCCI = 1;
    const LEVEL_1 = 2;
    const STATE = 3;

  protected static $_options = array(
    self::BCCI => 'BCCI',
    self::LEVEL_1 => 'Level - I',
    self::STATE => 'GCA',
  );
}