<?php
  App::uses('Enum', 'Lib');
  class SupportStaffType extends Enum {
    const COACH = 1;
    const ASSISTANT_COACH = 2;
    const MANAGER = 3;
    const TRAINER = 4;
    const PHYSIO = 5;
    const VIDEO_ANALYST = 6;

  protected static $_options = array(
    self::COACH => 'Coach',
    self::ASSISTANT_COACH => 'Assistant Coach',
    self::MANAGER => 'Manager',
    self::TRAINER => 'Trainer',
    self::PHYSIO => 'Physio',
    self::VIDEO_ANALYST => 'Video Analyst',
  );
}