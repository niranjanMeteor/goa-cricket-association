<?php
  App::uses('Enum', 'Lib');
  class MatchType extends Enum {
    const LEAGUE = 1;
    const QUATERFINAL = 2;
    const SEMIFINAL = 3;
    const FINALS = 4;
    const QUALIFIER = 5;
    const ELIMINATOR = 6;

  protected static $_options = array(
    self::LEAGUE => 'League',
    self::QUATERFINAL => 'QuaterFinal',
    self::SEMIFINAL => 'SemiFinal',
    self::FINALS => 'Finals',
    self::QUALIFIER => 'Qualifier',
    self::ELIMINATOR => 'Eliminator'
  );
}