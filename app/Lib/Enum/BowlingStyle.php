<?php
  App::uses('Enum', 'Lib');
  class BowlingStyle extends Enum {
    const FAST = 1;
    const FAST_MEDIUM = 2;
    const MEDIUM_FAST = 3;
    const MEDIUM = 4;
    const OFF_BREAK = 5;
    const LEG_BREAK = 6;
    const SLOW_ORTHODOX = 7;
    const SLOW_CHINAMAN = 8;

  protected static $_options = array(
    self::FAST => 'Fast',
    self::FAST_MEDIUM => 'Fast Medium',
    self::MEDIUM_FAST => 'Medium Fast',
    self::MEDIUM => 'Medium',
    self::OFF_BREAK => 'Off-break',
    self::LEG_BREAK => 'Leg-break',
    self::SLOW_ORTHODOX => 'Slow Orthodox',
    self::SLOW_CHINAMAN => 'Slow Chinaman',
  );
}