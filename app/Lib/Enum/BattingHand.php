<?php
  App::uses('Enum', 'Lib');
  class BattingHand extends Enum {
    const RIGHT = 1;
    const LEFT = 2;

  protected static $_options = array(
    self::RIGHT => 'Right Hand',
    self::LEFT => 'Left Hand',
  );
}