<?php
  App::uses('Enum', 'Lib');
  class Designations extends Enum {
    const PRESIDENT = 1;
    const SECRETARY = 2;
    const TREASURER = 3;
    const VICE_PRESIDENT = 4;
    const JOINT_SECRETARY = 5;
    const MEMBER = 6;
    const OFFICE = 7;

  protected static $_options = array(
    self::PRESIDENT => 'President',
    self::SECRETARY => 'Secretary',
    self::TREASURER => 'Treasurer',
    self::VICE_PRESIDENT => 'Vice President',
    self::JOINT_SECRETARY => 'Joint Secretary',
    self::MEMBER => 'Member',
    self::OFFICE => 'GCA Office',
  );
}