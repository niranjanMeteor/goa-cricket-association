<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
  Router::connect('/', array('controller' => 'pages', 'action' => 'index'));
  Router::connect('/new_task', array('controller' => 'tickets', 'action' => 'add'));
  Router::connect('/managing_committee', array('controller' => 'pages', 'action' => 'managing_committee'));
  Router::connect('/stadium', array('controller' => 'pages', 'action' => 'stadium'));
  Router::connect('/jobs', array('controller' => 'pages', 'action' => 'jobs'));
  Router::connect('/press', array('controller' => 'press_releases', 'action' => 'index'));
  Router::connect('/gpl', array('controller' => 'pages', 'action' => 'gpl'));
  Router::connect('/bidding_schedule', array('controller' => 'pages', 'action' => 'bidding_schedule'));
  Router::connect('/gca_staffs', array('controller' => 'pages', 'action' => 'gca_staffs'));
  Router::connect('/gpl_stats', array('controller' => 'matches', 'action' => 'gpl_stats'));

  Router::redirect('/gpl_auction', '/', array('status' => 302));
  // Router::connect('/gpl_auction', array('controller' => 'players', 'action' => 'gpl_auction'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	// Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

  Router::parseExtensions('xml', 'json');
/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
