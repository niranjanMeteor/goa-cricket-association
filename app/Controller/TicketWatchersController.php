<?php
App::uses('AppController', 'Controller');
/**
 * TicketWatchers Controller
 *
 * @property TicketWatcher $TicketWatcher
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TicketWatchersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TicketWatcher->recursive = 0;
		$this->set('ticketWatchers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TicketWatcher->exists($id)) {
			throw new NotFoundException(__('Invalid ticket watcher'));
		}
		$options = array('conditions' => array('TicketWatcher.' . $this->TicketWatcher->primaryKey => $id));
		$this->set('ticketWatcher', $this->TicketWatcher->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TicketWatcher->create();
			if ($this->TicketWatcher->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket watcher has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket watcher could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$tickets = $this->TicketWatcher->Ticket->find('list');
		$users = $this->TicketWatcher->User->find('list');
		$this->set(compact('tickets', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TicketWatcher->exists($id)) {
			throw new NotFoundException(__('Invalid ticket watcher'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TicketWatcher->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket watcher has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket watcher could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('TicketWatcher.' . $this->TicketWatcher->primaryKey => $id));
			$this->request->data = $this->TicketWatcher->find('first', $options);
		}
		$tickets = $this->TicketWatcher->Ticket->find('list');
		$users = $this->TicketWatcher->User->find('list');
		$this->set(compact('tickets', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TicketWatcher->id = $id;
		if (!$this->TicketWatcher->exists()) {
			throw new NotFoundException(__('Invalid ticket watcher'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->TicketWatcher->delete()) {
			$this->Session->setFlash(__('The ticket watcher has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The ticket watcher could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
