<?php
App::uses('AppController', 'Controller');
/**
 * RemoteActions Controller
 *
 * @property RemoteAction $RemoteAction
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class RemoteActionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function admin_start_bidding() {
		$this->RemoteAction->clearAllActiveStatus();
		if ($this->RemoteAction->activateBidding()) {
			$this->redirect(array('controller' => 'rounds', 'action' => 'new_round', 'admin' => true));
		} else {
			$this->Session->setFlash(__('The remote action could not be triggered. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('controller' => 'rounds', 'action' => 'new_round', 'admin' => true));
		}
	}
	public function admin_display_results() {
		$this->RemoteAction->clearAllActiveStatus();
		if ($this->RemoteAction->displayResults()) {
			$this->redirect(array('controller' => 'rounds', 'action' => 'completed', 'admin' => true));
		} else {
			$this->Session->setFlash(__('The remote action could not be triggered. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('controller' => 'rounds', 'action' => 'completed', 'admin' => true));
		}
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->RemoteAction->recursive = 0;
		$this->set('remoteActions', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->RemoteAction->exists($id)) {
			throw new NotFoundException(__('Invalid remote action'));
		}
		$options = array('conditions' => array('RemoteAction.' . $this->RemoteAction->primaryKey => $id));
		$this->set('remoteAction', $this->RemoteAction->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->RemoteAction->create();
			if ($this->RemoteAction->save($this->request->data)) {
				$this->Session->setFlash(__('The remote action has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The remote action could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->RemoteAction->exists($id)) {
			throw new NotFoundException(__('Invalid remote action'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RemoteAction->save($this->request->data)) {
				$this->Session->setFlash(__('The remote action has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The remote action could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('RemoteAction.' . $this->RemoteAction->primaryKey => $id));
			$this->request->data = $this->RemoteAction->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->RemoteAction->id = $id;
		if (!$this->RemoteAction->exists()) {
			throw new NotFoundException(__('Invalid remote action'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->RemoteAction->delete()) {
			$this->Session->setFlash(__('The remote action has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The remote action could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
