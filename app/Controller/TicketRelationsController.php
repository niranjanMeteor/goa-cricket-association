<?php
App::uses('AppController', 'Controller');
/**
 * TicketRelations Controller
 *
 * @property TicketRelation $TicketRelation
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TicketRelationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TicketRelation->recursive = 0;
		$this->set('ticketRelations', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TicketRelation->exists($id)) {
			throw new NotFoundException(__('Invalid ticket relation'));
		}
		$options = array('conditions' => array('TicketRelation.' . $this->TicketRelation->primaryKey => $id));
		$this->set('ticketRelation', $this->TicketRelation->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TicketRelation->create();
			if ($this->TicketRelation->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket relation has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket relation could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$primaryIDs = $this->TicketRelation->PrimaryID->find('list');
		$relatedIDs = $this->TicketRelation->RelatedID->find('list');
		$this->set(compact('primaryIDs', 'relatedIDs'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TicketRelation->exists($id)) {
			throw new NotFoundException(__('Invalid ticket relation'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TicketRelation->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket relation has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket relation could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('TicketRelation.' . $this->TicketRelation->primaryKey => $id));
			$this->request->data = $this->TicketRelation->find('first', $options);
		}
		$primaryIDs = $this->TicketRelation->PrimaryID->find('list');
		$relatedIDs = $this->TicketRelation->RelatedID->find('list');
		$this->set(compact('primaryIDs', 'relatedIDs'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TicketRelation->id = $id;
		if (!$this->TicketRelation->exists()) {
			throw new NotFoundException(__('Invalid ticket relation'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->TicketRelation->delete()) {
			$this->Session->setFlash(__('The ticket relation has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The ticket relation could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
