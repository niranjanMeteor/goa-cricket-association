<?php
App::uses('AppController', 'Controller');
/**
 * Audios Controller
 *
 * @property Audio $Audio
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AudiosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Audio->recursive = 0;
		$this->set('audios', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Audio->exists($id)) {
			throw new NotFoundException(__('Invalid audio'));
		}
		$options = array('conditions' => array('Audio.' . $this->Audio->primaryKey => $id));
		$this->set('audio', $this->Audio->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Audio->create();
			if ($this->Audio->save($this->request->data)) {
				$this->Session->setFlash(__('The audio has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The audio could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Audio->exists($id)) {
			throw new NotFoundException(__('Invalid audio'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Audio->save($this->request->data)) {
				$this->Session->setFlash(__('The audio has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The audio could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Audio.' . $this->Audio->primaryKey => $id));
			$this->request->data = $this->Audio->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Audio->id = $id;
		if (!$this->Audio->exists()) {
			throw new NotFoundException(__('Invalid audio'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Audio->delete()) {
			$this->Session->setFlash(__('The audio has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The audio could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
