<?php
App::uses('AppController', 'Controller');
/**
 * PlayerAuctionGroups Controller
 *
 * @property PlayerAuctionGroup $PlayerAuctionGroup
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PlayerAuctionGroupsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PlayerAuctionGroup->recursive = 0;
		$this->set('playerAuctionGroups', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PlayerAuctionGroup->exists($id)) {
			throw new NotFoundException(__('Invalid player auction group'));
		}
		$options = array('conditions' => array('PlayerAuctionGroup.' . $this->PlayerAuctionGroup->primaryKey => $id));
		$this->set('playerAuctionGroup', $this->PlayerAuctionGroup->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PlayerAuctionGroup->create();
			if ($this->PlayerAuctionGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The player auction group has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The player auction group could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$players = $this->PlayerAuctionGroup->Player->find('list');
		$auctionGroups = $this->PlayerAuctionGroup->AuctionGroup->find('list');
		$this->set(compact('players', 'auctionGroups'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PlayerAuctionGroup->exists($id)) {
			throw new NotFoundException(__('Invalid player auction group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PlayerAuctionGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The player auction group has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The player auction group could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('PlayerAuctionGroup.' . $this->PlayerAuctionGroup->primaryKey => $id));
			$this->request->data = $this->PlayerAuctionGroup->find('first', $options);
		}
		$players = $this->PlayerAuctionGroup->Player->find('list');
		$auctionGroups = $this->PlayerAuctionGroup->AuctionGroup->find('list');
		$this->set(compact('players', 'auctionGroups'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PlayerAuctionGroup->id = $id;
		if (!$this->PlayerAuctionGroup->exists()) {
			throw new NotFoundException(__('Invalid player auction group'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PlayerAuctionGroup->delete()) {
			$this->Session->setFlash(__('The player auction group has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The player auction group could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
