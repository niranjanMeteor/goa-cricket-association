<?php
App::uses('Component', 'Controller');
App::uses('HttpSocket', 'Network/Http');

require_once APP . '../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class ScoringComponent extends Component {
  public function update_score($id) {
    $connection = new AMQPConnection('52.0.9.95', 5672, 'guest', 'guest');
    $channel = $connection->channel();

    $bucketName = 'update_score';

    $channel->queue_declare($bucketName, false, false, false, false);

    $scoreUrl = Configure::read('scoreUrl') . $id;
    $HttpSocket = new HttpSocket();
    $results = $HttpSocket->get($scoreUrl);
    $json = $results['body'];
    
    $msg = new AMQPMessage($json);
    $channel->basic_publish($msg, '', $bucketName);

    $channel->close();
    $connection->close();
  }
  public function update_mini_score() {
    $connection = new AMQPConnection('52.0.9.95', 5672, 'guest', 'guest');
    $channel = $connection->channel();

    $bucketName = 'update_mini_score';

    $channel->queue_declare($bucketName, false, false, false, false);

    $scoreUrl = Configure::read('miniScoreUrl');
    $HttpSocket = new HttpSocket();
    $results = $HttpSocket->get($scoreUrl);
    $json = $results['body'];
    
    $msg = new AMQPMessage($json);
    $channel->basic_publish($msg, '', $bucketName);

    $channel->close();
    $connection->close();
  }
}
