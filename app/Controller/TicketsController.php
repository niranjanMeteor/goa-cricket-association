<?php
App::uses('AppController', 'Controller');
App::uses('TicketCategory', 'Lib/Enum');
App::uses('TicketStatus', 'Lib/Enum');
App::uses('TicketPriority', 'Lib/Enum');
/**
 * Tickets Controller
 *
 * @property Ticket $Ticket
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TicketsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	public $helpers = array(
		'Form' => array('className' => 'Bs3Form'),
		'Time'
	);


/**
 * beforeFilter callback
 *
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function add() {
		if ($this->request->is('post')) {
			$ticket = $this->Ticket->saveTicket($this->request->data);
			if ( ! $ticket) {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			} else {
				$this->redirect(array('controller' => 'tickets', 'action' => 'view', $ticket));
			}
		};

		$categories = TicketCategory::options();
		$statuses = TicketStatus::options();
		$priorities = TicketPriority::options();
		$assignees = $this->Ticket->Assignee->find('list');
		$projects = $this->Ticket->Project->find('list');
		$doneOptions = array(
			'0' => '0%',
			'10' => '10%',
			'20' => '20%',
			'30' => '30%',
			'40' => '40%',
			'50' => '50%',
			'60' => '60%',
			'70' => '70%',
			'80' => '80%',
			'100' => '100%',
		);
		$this->set(compact('categories', 'statuses', 'priorities', 'assignees', 'projects', 'doneOptions'));
	}

	public function view($ticketId) {
		$ticket = $this->Ticket->find('first', array(
			'conditions' => array(
				'Ticket.id' => $ticketId
			),
			'contain' => array(
				'Creator' => array('Profile'),
				'Assignee' => array('Profile'), 
				'TicketWatcher', 
				'TicketAttachment',
				'TicketUpdate' => array('User' => array('Profile'))
			)
		));
		$categories = TicketCategory::options();
		$statuses = TicketStatus::options();
		$priorities = TicketPriority::options();
		$assignees = $this->Ticket->Assignee->find('list');
		$projects = $this->Ticket->Project->find('list');
		$doneOptions = array(
			'0' => '0%',
			'10' => '10%',
			'20' => '20%',
			'30' => '30%',
			'40' => '40%',
			'50' => '50%',
			'60' => '60%',
			'70' => '70%',
			'80' => '80%',
			'100' => '100%',
		);
		$this->set(compact('ticket','categories', 'statuses', 'priorities', 'assignees', 'projects', 'doneOptions'));
	}

	public function edit($id) {
		if ($this->request->is('post')) {
			$ticket = $this->Ticket->saveTicket($this->request->data, $id);
			if ( ! $ticket) {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
				$this->redirect(array('controller' => 'tickets', 'action' => 'view', $id));
			} else {
				$this->redirect(array('controller' => 'tickets', 'action' => 'view', $ticket));
			}
		}
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Ticket->recursive = 0;
		$this->set('tickets', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Ticket->exists($id)) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		$options = array('conditions' => array('Ticket.' . $this->Ticket->primaryKey => $id));
		$this->set('ticket', $this->Ticket->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Ticket->create();
			if ($this->Ticket->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$projects = $this->Ticket->Project->find('list');
		$creators = $this->Ticket->Creator->find('list');
		$assignees = $this->Ticket->Assignee->find('list');
		$this->set(compact('projects', 'creators', 'assignees'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Ticket->exists($id)) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Ticket']['description'] = nl2br($this->request->data['Ticket']['description']);
			if ($this->Ticket->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Ticket.' . $this->Ticket->primaryKey => $id));
			$this->request->data = $this->Ticket->find('first', $options);
		}
		$projects = $this->Ticket->Project->find('list');
		$creators = $this->Ticket->Creator->find('list');
		$assignees = $this->Ticket->Assignee->find('list');
		$this->set(compact('projects', 'creators', 'assignees'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Ticket->id = $id;
		if (!$this->Ticket->exists()) {
			throw new NotFoundException(__('Invalid ticket'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Ticket->delete()) {
			$this->Session->setFlash(__('The ticket has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The ticket could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
