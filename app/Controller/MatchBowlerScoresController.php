<?php
App::uses('AppController', 'Controller');
/**
 * MatchBowlerScores Controller
 *
 * @property MatchBowlerScore $MatchBowlerScore
 * @property PaginatorComponent $Paginator
 */
class MatchBowlerScoresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->MatchBowlerScore->recursive = 0;
		$this->set('matchBowlerScores', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->MatchBowlerScore->exists($id)) {
			throw new NotFoundException(__('Invalid match bowler score'));
		}
		$options = array('conditions' => array('MatchBowlerScore.' . $this->MatchBowlerScore->primaryKey => $id));
		$this->set('matchBowlerScore', $this->MatchBowlerScore->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->MatchBowlerScore->create();
			if ($this->MatchBowlerScore->save($this->request->data)) {
				return $this->flash(__('The match bowler score has been saved.'), array('action' => 'index'));
			}
		}
		$matchInningScores = $this->MatchBowlerScore->MatchInningScore->find('list');
		$players = $this->MatchBowlerScore->Player->find('list');
		$this->set(compact('matchInningScores', 'players'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->MatchBowlerScore->exists($id)) {
			throw new NotFoundException(__('Invalid match bowler score'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MatchBowlerScore->save($this->request->data)) {
				return $this->flash(__('The match bowler score has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('MatchBowlerScore.' . $this->MatchBowlerScore->primaryKey => $id));
			$this->request->data = $this->MatchBowlerScore->find('first', $options);
		}
		$matchInningScores = $this->MatchBowlerScore->MatchInningScore->find('list');
		$players = $this->MatchBowlerScore->Player->find('list');
		$this->set(compact('matchInningScores', 'players'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->MatchBowlerScore->id = $id;
		if (!$this->MatchBowlerScore->exists()) {
			throw new NotFoundException(__('Invalid match bowler score'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MatchBowlerScore->delete()) {
			return $this->flash(__('The match bowler score has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The match bowler score could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
}
