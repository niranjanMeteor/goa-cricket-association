<?php
App::uses('AppController', 'Controller');
App::uses('UserLevel', 'Lib/Enum');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * beforeFilter callback
 *
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('login', 'register');
	}
	
	public function login() {
		$this->layout = 'auth';
		if ($this->request->is('post')) {
      if ($this->Auth->login()) {
        return $this->redirect('/');
      } else {
        $this->Session->setFlash( __('Email or password incorrect'), 'default', array('class' => 'alert alert-danger'));
      }
    }
	}

	public function register() {
		$this->layout = 'auth';
		if ($this->request->is('post')) {
			if ( ! empty($this->request->data['User']['password']) && ! empty($this->request->data['User']['password_confirm']) && ($this->request->data['User']['password'] == $this->request->data['User']['password_confirm']) ) {
				$imageUrl = $this->__uploadProfilePic($this->request->data);
				if (! empty($imageUrl)) {
					$data = array(
						'User' => array(
							'email' => $this->request->data['User']['email'],
							'password' => $this->request->data['User']['password']
						),
						'Profile' => array(
							'first_name' => $this->request->data['User']['first_name'],
							'last_name' => $this->request->data['User']['last_name'],
							'phone' => $this->request->data['User']['phone'],
							'image_url' => $imageUrl
						),
						'Image' => array(
							array(
								'caption' => $this->request->data['User']['image']['name'],
								'url' => $imageUrl
							)
						)
					);
					if ($this->User->saveAssociated($data, array('deep' => true))) {
						if ($this->Auth->login()) {
							if (AuthComponent::user('level') == UserLevel::ADMINISTRATOR) {
			    			$this->redirect(array('action' => 'index', 'admin' => true));
			      	} else {
			        	$this->redirect('/');;
			      	}
						} else {
							$this->Session->setFlash(__('Could not log in.'), 'default', array('class' => 'alert alert-danger'));
						}
					} else {
						pr($this->User->validationErrors);
						$this->Session->setFlash(__('Something went wrong. Please try again'));
					}
				} else {
					$this->Session->setFlash(__('Photo could not be uploaded.'), 'default', array('class' => 'alert alert-danger'));
				}
			} else {
				$this->Session->setFlash(__('Passwords do not match.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

	public function logout() {
		$this->Auth->logout();
    return $this->redirect('/');
	}

	public function dashboard() {

	}

	private function __uploadProfilePic($data) {
		$imageUrl = null;
		$uploadDir = WWW_ROOT . 'files' . DS . 'users';
 		$imageName = null;
 		// pr($data); die;
 		if ( ! empty($data['User']['image'])) {
 			$imageName = $data['User']['image']['name'];
 			$imageName = preg_replace("/[^A-Z0-9._-]/i", "_", $imageName); //Ensure safe file name 
 			$uuId = uniqid();
 			$finalImageName = $uuId . '_' . $imageName; // Ensure unique file name
 			$imageNameWithPath = $uploadDir . DS . $finalImageName;
 			if( ! file_exists($uploadDir)) {
				mkdir($uploadDir, 0777, true);
			}
 			$success = move_uploaded_file($data['User']['image']["tmp_name"], $imageNameWithPath);
 			if ($success) {
 				$imageUrl = '/files/users/' . $finalImageName;
 			}
 		}
 		return $imageUrl;
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
