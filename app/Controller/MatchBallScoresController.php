<?php
App::uses('AppController', 'Controller');
App::uses('InningNumber' , 'Lib/Enum');
App::uses('OutType', 'Lib/Enum');
/**
 * MatchBallScores Controller
 *
 * @property MatchBallScore $MatchBallScore
 * @property PaginatorComponent $Paginator
 */
class MatchBallScoresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->MatchBallScore->recursive = 0;
		$this->set('matchBallScores', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->MatchBallScore->exists($id)) {
			throw new NotFoundException(__('Invalid match ball score'));
		}
		$options = array('conditions' => array('MatchBallScore.' . $this->MatchBallScore->primaryKey => $id));
		$this->set('matchBallScore', $this->MatchBallScore->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			if ( 	!empty($this->request->data['MatchBallScore']['match_id']) && !empty($this->request->data['MatchBallScore']['innings_id']) ) {
				$matchId = $this->request->data['MatchBallScore']['match_id'];
				$inningsId = $this->request->data['MatchBallScore']['innings_id'];
				if ($this->MatchBallScore->MatchInningScore->checkIfMatchIsComplete($matchId)) {
					$this->Session->setFlash(__('Match Finished. Start scoring for new Match'), 'default', array('class' => 'alert alert-success'));
					$this->redirect(array('controller' => 'match_ball_scores', 'action' => 'add'));
				}
				if ( !$this->MatchBallScore->MatchInningScore->isMatchInningsComplete($matchId,$inningsId) )  {
					if ($this->MatchBallScore->MatchInningScore->checksIfInningsToResume($matchId,$inningsId)) {
						$lastBallEntryId = $this->MatchBallScore->getlastBallEntryId($matchId,$inningsId);
						if (!empty($lastBallEntryId)) {
							$this->Session->setFlash(__('Innings '.$inningsId.' is Resuming'), 'default', array('class' => 'alert alert-success'));
							$this->redirect(array('controller' => 'match_ball_scores', 'action' => 'edit', $lastBallEntryId));							
						} else {
						$this->Session->setFlash(__('Innings '.$inningsId.' Could not be resumed'), 'default', array('class' => 'alert alert-danger'));
						}
					}
					if ($this->MatchBallScore->saveData($this->request->data)) {
						$this->Session->setFlash(__('The match ball score has been saved.'), 'default', array('class' => 'alert alert-success'));
						$this->redirect(array('controller' => 'match_ball_scores', 'action' => 'edit', $this->MatchBallScore->getLastInsertID()));
					} else {
						$this->Session->setFlash(__('The Match Ball Score was not saved'), 'default', array('class' => 'alert alert-danger'));
					}
				} else {
					$this->Session->setFlash(__('Innings  '.$this->request->data['MatchBallScore']['innings_id'].'  has already completed.'), 'default', array('class' => 'alert alert-danger'));
					$this->redirect(array('controller' => 'match_ball_scores', 'action' => 'add'));
				}
			} else {
				$this->Session->setFlash(__('Match ball Score : Invalid Input Arguments'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$matches = $this->MatchBallScore->MatchInningScore->Match->getListofMatchesEligibleForScoring();
		$innings = InningNumber::options();
		$this->set(compact('matches','innings'));

	}


/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->MatchBallScore->exists($id)) {
			throw new NotFoundException(__('Invalid match ball score'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$data = $this->request->data['MatchBallScore'];	
			if ($this->MatchBallScore->updateScorecard($data)) { 
				$inning = $this->MatchBallScore->MatchInningScore->findById($data['match_inning_score_id']);
				$inning = $inning['MatchInningScore'];
				if ($this->MatchBallScore->MatchInningScore->checkIfMatchIsComplete($inning['match_id'])) {
					$this->Session->setFlash(__('Match Finished. Start scoring for new Match'), 'default', array('class' => 'alert alert-success'));
					$this->Scoring->update_mini_score();
					$this->redirect(array('controller' => 'match_ball_scores', 'action' => 'add'));
				}
				else if ($this->MatchBallScore->MatchInningScore->isMatchInningsComplete($inning['match_id'],$inning['inning'])) {
					$this->Session->setFlash(__('Innings '.$inning['inning'].' Completed . Start New Innings'), 'default', array('class' => 'alert alert-success'));
					$this->Scoring->update_mini_score();
					$this->redirect(array('controller' => 'match_ball_scores', 'action' => 'add'));
				} else {
					$this->Session->setFlash(__('The match ball score has been Updated.'), 'default', array('class' => 'alert alert-success'));
					$this->redirect(array('controller' => 'match_ball_scores', 'action' => 'edit',$this->MatchBallScore->getLastInsertID()));
				}
			} else {
				$this->Session->setFlash(__('The Match Ball Score was not Updated'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('MatchBallScore.' . $this->MatchBallScore->primaryKey => $id));
			$this->request->data = $this->MatchBallScore->find('first', $options);
		}
		$matchBallData = $this->MatchBallScore->getMatchBallData($id);
		$match = $this->MatchBallScore->MatchInningScore->Match->findById($matchBallData['match_id']);
		$inning = $matchBallData['inning'];
		$batsmen = $this->MatchBallScore->MatchInningScore->Match->getBatsmenList($matchBallData['match_id'], $matchBallData['inning'], $matchBallData['match_inning_score_id']);
		$bowlers = $this->MatchBallScore->MatchInningScore->Match->getBowlerList($matchBallData['match_id'], $matchBallData['inning']);
		$outTypes = OutType::options();
		$bowlingTeamPlayers = $this->MatchBallScore->MatchInningScore->Match->getBowlingTeamPlayersList($matchBallData['match_id'], $matchBallData['inning']);
		$matchId = $match['Match']['id'];
		$this->Scoring->update_score($matchId);
		$this->Scoring->update_mini_score();
		// $this->MatchBallScore->clearCache($matchId);
		$matchInning = $this->MatchBallScore->MatchInningScore->findById($matchBallData['match_inning_score_id']);
		$matchInning = $matchInning['MatchInningScore'];
		$runs = !empty($matchInning['runs']) ? $matchInning['runs'] : 0;
		$overs = !empty($matchInning['overs']) ? $matchInning['overs'] : 0;
		$wickets = !empty($matchInning['wickets']) ? $matchInning['wickets'] : 0;
		if (empty($matchBallData['striker_id']) || empty($matchBallData['non_striker_id'])) {
			$this->Session->setFlash(__('A Wicket Fell Or a Batsman Retired Hurt. Add New Bataman To Crease'), 'default', array('class' => 'alert alert-danger'));
		}
		if ($this->MatchBallScore->isOverComplete($matchBallData['match_inning_score_id'],$overs)) {
			$this->Session->setFlash(__('Over Completed . Start New Over'), 'default', array('class' => 'alert alert-danger'));				
		}
		$this->set(compact('inning', 'match', 'batsmen', 'bowlers', 'outTypes','bowlingTeamPlayers','runs','overs','wickets'));
	}
	public function admin_resume_scoring($matchId) {
		$currentInning = $this->MatchBallScore->MatchInningScore->getCurrentInning($matchId);
		$currentInningId = $currentInning['MatchInningScore']['id'];
		$currentBall = $this->MatchBallScore->getCurrentBall($currentInningId);
		$this->redirect(array('action' => 'edit', $currentBall['MatchBallScore']['id']));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->MatchBallScore->id = $id;
		if (!$this->MatchBallScore->exists()) {
			throw new NotFoundException(__('Invalid match ball score'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MatchBallScore->delete()) {
			return $this->flash(__('The match ball score has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The match ball score could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
}
