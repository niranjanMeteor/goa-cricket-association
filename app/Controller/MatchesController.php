<?php
App::uses('AppController', 'Controller');
App::uses('MatchLevel', 'Lib/Enum');
App::uses('MatchType', 'Lib/Enum');
App::uses('TossDecision', 'Lib/Enum');
App::uses('ResultType', 'Lib/Enum');
App::uses('PlayerRole', 'Lib/Enum');
App::uses('UserLevel', 'Lib/Enum');
/**
 * Matches Controller
 *
 * @property Match $Match
 * @property PaginatorComponent $Paginator
 */
class MatchesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	/**
	 * beforeFilter callback
	 *
	 * @return void
	 */
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('gpl_stats');
		}
	
	public function gpl_stats() {
		$this->layout = 'general';
		$topBatsmen = $this->Match->MatchInningScore->MatchBatsmanScore->getTopBatsmen();
		$topBowlers = $this->Match->MatchInningScore->MatchBowlerScore->getTopBowlers();
		$teams = $this->Match->FirstTeam->getAllGplTeams();
		$teams = $this->Match->MatchInningScore->calculateTeamStats($teams);
		$this->set(compact('topBatsmen', 'topBowlers', 'teams'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Match->recursive = 0;
		$this->set('matches', $this->Paginator->paginate());
	}
	public function admin_toss($id) {
		if (!$this->Match->exists($id)) {
			throw new NotFoundException(__('Invalid match'));
		}
		if ($this->request->is('post')) {
			$this->request->data['Match']['id'] = $id;
			if ($this->Match->saveTossData($this->request->data)) {
				$this->Session->setFlash(__('Toss data is saved.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('controller' => 'matches', 'action' => 'index', 'admin' => true));
			} else {
				$this->Session->setFlash(__('Toss could not be saved.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$teams = $this->Match->FirstTeam->getTeamsForMatch($id);
		$tossDecisions = TossDecision::options();
		$this->set(compact('teams', 'tossDecisions'));	
	}
	public function admin_add_players($id) {
		if (!$this->Match->exists($id)) {
			throw new NotFoundException(__('Invalid match'));
		}
		if ($this->request->is('post')) {
			if (!empty($this->request->data)) {
				foreach ($this->request->data['MatchPlayer'] as $key => $matchPlayer) {
					$this->request->data['MatchPlayer'][$key]['match_id'] = $id;
					if (empty($matchPlayer['player_id'])) {
						unset($this->request->data['MatchPlayer'][$key]);
					}
				}
			}
			if ($this->Match->MatchPlayer->saveMany($this->request->data['MatchPlayer'])) {
				$this->Session->setFlash(__('Players for the match are saved.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('controller' => 'matches', 'action' => 'index', 'admin' => true));
			} else {
				$this->Session->setFlash(__('Players for the match could not be saved.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$match = $this->Match->findById($id);
		$firstTeamId = $match['Match']['first_team_id'];
		$secondTeamId = $match['Match']['second_team_id'];
		$firstTeam = $this->Match->FirstTeam->findById($firstTeamId);
		$secondTeam = $this->Match->SecondTeam->findById($secondTeamId);
		$firstTeamList = $this->Match->FirstTeam->TeamPlayer->getPlayerList($firstTeamId);
		$secondTeamList = $this->Match->SecondTeam->TeamPlayer->getPlayerList($secondTeamId);
		$roles = PlayerRole::options();
		$this->set(compact('firstTeamList', 'secondTeamList', 'firstTeam', 'secondTeam', 'roles'));
	}

	public function admin_abandon($matchId) {
		
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Match->exists($id)) {
			throw new NotFoundException(__('Invalid match'));
		}
		$options = array('conditions' => array('Match.' . $this->Match->primaryKey => $id));
		$this->set('match', $this->Match->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Match->create();
			if ($this->Match->save($this->request->data)) {
				$this->Session->setFlash(__('The match is saved.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('controller' => 'matches', 'action' => 'index', 'admin' => true));
			} else {
				$this->Session->setFlash(__('The match is not saved.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$teams = $this->Match->FirstTeam->find('list');
		$levels = MatchLevel::options();
		$types = MatchType::options();
		$tossDecisions = TossDecision::options();
		$resultTypes = ResultType::options();
		$this->set(compact('teams','levels','types', 'tossDecisions', 'resultTypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Match->exists($id)) {
			throw new NotFoundException(__('Invalid match'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Match->save($this->request->data)) {
				$this->Session->setFlash(__('The match is saved.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('controller' => 'matches', 'action' => 'index', 'admin' => true));
			} else {
				$this->Session->setFlash(__('The match is not saved.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Match.' . $this->Match->primaryKey => $id));
			$this->request->data = $this->Match->find('first', $options);
		}
		$teams = $this->Match->FirstTeam->find('list');
		$levels = MatchLevel::options();
		$types = MatchType::options();
		$tossDecisions = TossDecision::options();
		$resultTypes = ResultType::options();
		$this->set(compact('teams','levels','types', 'tossDecisions', 'resultTypes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Match->id = $id;
		if (!$this->Match->exists()) {
			throw new NotFoundException(__('Invalid match'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Match->delete()) {
			return $this->flash(__('The match has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The match could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
	public function getBatsmenAndBowlers ($matchId, $inningsNumber) {
		$match = $this->Match->findById($matchId);
		$matchInning = $this->Match->MatchInningScore->findByMatchIdAndInning($matchId, $inningsNumber);
		$battingTeamId = $matchInning['MatchInningScore']['team_id'];
		$matchTeams = array($match['Match']['first_team_id'], $match['Match']['second_team_id']);
		$bowlingTeamId = array_values(array_diff($matchTeams, array($battingTeamId)))[0];
		$batsmen = $this->Match->MatchPlayer->getBatsmen($matchId, $battingTeamId, $matchInning['MatchInningScore']['id']);
		$bowlers = $this->Match->MatchPlayer->getBowlers($matchId, $bowlingTeamId);
		if (!empty($batsmen) && !empty($bowlers)) {
			echo json_encode(array('s' => true, 'batsmen' => $batsmen, 'bowlers' => $bowlers)); die;
		} else {
			echo json_encode(array('s' => false, 'message' => 'Please add all players playing in this match.')); die;
		}
	}
}
