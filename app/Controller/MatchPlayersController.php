<?php
App::uses('AppController', 'Controller');
App::uses('PlayerRole' , 'Lib/Enum');
/**
 * MatchPlayers Controller
 *
 * @property MatchPlayer $MatchPlayer
 * @property PaginatorComponent $Paginator
 */
class MatchPlayersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->MatchPlayer->recursive = 0;
		$this->set('matchPlayers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->MatchPlayer->exists($id)) {
			throw new NotFoundException(__('Invalid match player'));
		}
		$options = array('conditions' => array('MatchPlayer.' . $this->MatchPlayer->primaryKey => $id));
		$this->set('matchPlayer', $this->MatchPlayer->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->MatchPlayer->create();
			if ($this->MatchPlayer->save($this->request->data)) {
				$this->Session->setFlash(__('The Match Player is saved.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('action' => 'index', 'admin' => true));
			} else {
				$this->Session->setFlash(__('The Match Player is saved.'), 'default', array('class' => 'alert alert-success'));
			}
		}
		$matches = $this->MatchPlayer->Match->find('list');
		$teams = $this->MatchPlayer->Team->find('list');
		$players = $this->MatchPlayer->Player->find('list');
		$roles = PlayerRole::options();
		$this->set(compact('matches', 'teams', 'players','roles'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->MatchPlayer->exists($id)) {
			throw new NotFoundException(__('Invalid match player'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MatchPlayer->save($this->request->data)) {
				$this->Session->setFlash(__('The Match Player is saved.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('action' => 'index', 'admin' => true));
			} else {
				$this->Session->setFlash(__('The Match Player is saved.'), 'default', array('class' => 'alert alert-success'));
			}
		} else {
			$options = array('conditions' => array('MatchPlayer.' . $this->MatchPlayer->primaryKey => $id));
			$this->request->data = $this->MatchPlayer->find('first', $options);
		}
		$matches = $this->MatchPlayer->Match->find('list');
		$teams = $this->MatchPlayer->Team->find('list');
		$players = $this->MatchPlayer->Player->find('list');
		$this->set(compact('matches', 'teams', 'players'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->MatchPlayer->id = $id;
		if (!$this->MatchPlayer->exists()) {
			throw new NotFoundException(__('Invalid match player'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MatchPlayer->delete()) {
			return $this->flash(__('The match player has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The match player could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
}
