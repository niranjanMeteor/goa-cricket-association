<?php
App::uses('AppController', 'Controller');
App::uses('MatchLevel', 'Lib/Enum');
/**
 * PlayerBattingRecords Controller
 *
 * @property PlayerBattingRecord $PlayerBattingRecord
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PlayerBattingRecordsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PlayerBattingRecord->recursive = 0;
		$this->set('playerBattingRecords', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PlayerBattingRecord->exists($id)) {
			throw new NotFoundException(__('Invalid player batting record'));
		}
		$options = array('conditions' => array('PlayerBattingRecord.' . $this->PlayerBattingRecord->primaryKey => $id));
		$this->set('playerBattingRecord', $this->PlayerBattingRecord->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PlayerBattingRecord->create();
			if ($this->PlayerBattingRecord->save($this->request->data)) {
				$this->Session->setFlash(__('The player batting record has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The player batting record could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$players = $this->PlayerBattingRecord->Player->find('list');
		$levels = MatchLevel::options();
		$this->set(compact('players', 'levels'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PlayerBattingRecord->exists($id)) {
			throw new NotFoundException(__('Invalid player batting record'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PlayerBattingRecord->save($this->request->data)) {
				$this->Session->setFlash(__('The player batting record has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The player batting record could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('PlayerBattingRecord.' . $this->PlayerBattingRecord->primaryKey => $id));
			$this->request->data = $this->PlayerBattingRecord->find('first', $options);
		}
		$players = $this->PlayerBattingRecord->Player->find('list');
		$levels = MatchLevel::options();
		$this->set(compact('players', 'levels'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PlayerBattingRecord->id = $id;
		if (!$this->PlayerBattingRecord->exists()) {
			throw new NotFoundException(__('Invalid player batting record'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PlayerBattingRecord->delete()) {
			$this->Session->setFlash(__('The player batting record has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The player batting record could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
