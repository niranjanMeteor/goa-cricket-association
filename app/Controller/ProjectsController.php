<?php
App::uses('AppController', 'Controller');
App::uses('TicketPriority', 'Lib/Enum');
App::uses('UserProjectRole', 'Lib/Enum');
/**
 * Projects Controller
 *
 * @property Project $Project
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ProjectsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public $helpers = array(
		'Form' => array('className' => 'Bs3Form')
	);

/**
 * beforeFilter callback
 *
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function index() {
		$tickets = array();
		$projects = $this->Project->UserProject->find('all', array(
			'conditions' => array(
				'UserProject.user_id' => AuthComponent::user('id')
			),
			'contain' => array(
				'Project'
			),
			'order' => array('Project.modified DESC')
		));
		if (!empty($projects)) {
			$defaultProject = $projects[0]['Project'];
			$this->Paginator->settings = [
			  'limit' => 10,
			  'order' => [
			    'id' => 'asc'
			  ],
			  'contain' => array('Assignee.Profile')
			];
			$tickets = $this->Paginator->paginate('Ticket', array('Ticket.project_id' => $defaultProject['id']));
		}
		$this->set(compact('projects', 'tickets'));
	}

	public function view($id) {
		$tickets = array();
		$projects = $this->Project->UserProject->find('all', array(
			'conditions' => array(
				'UserProject.user_id' => AuthComponent::user('id')
			),
			'contain' => array(
				'Project'
			),
			'order' => array('Project.modified DESC')
		));
		if (!empty($projects)) {
			$defaultProject = $projects[0]['Project'];
			$this->Paginator->settings = [
			  'limit' => 10,
			  'order' => [
			    'id' => 'asc'
			  ],
			  'contain' => array('Assignee.Profile')
			];
			$tickets = $this->Paginator->paginate('Ticket', array('Ticket.project_id' => $id));
		}
		$this->set(compact('projects', 'tickets'));
		$this->render('/Projects/index');
	}

	public function add() {
		if ($this->request->is('post')) {
			$this->Project->create();
			$this->request->data['UserProject'] = array(
				array(
					'user_id' => AuthComponent::user('id'),
					'role' => UserProjectRole::ADMINISTRATOR
				)
			);
			if ($this->Project->saveAssociated($this->request->data)) {
				$this->redirect(array('controller' => 'projects', 'action' => 'view', $this->Project->getInsertID()));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Project->recursive = 0;
		$this->set('projects', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}
		$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
		$this->set('project', $this->Project->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Project->create();
			if ($this->Project->save($this->request->data)) {
				$this->Session->setFlash(__('The project has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Project->save($this->request->data)) {
				$this->Session->setFlash(__('The project has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
			$this->request->data = $this->Project->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Project->id = $id;
		if (!$this->Project->exists()) {
			throw new NotFoundException(__('Invalid project'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Project->delete()) {
			$this->Session->setFlash(__('The project has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The project could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
