<?php
App::uses('AppController', 'Controller');
App::uses('UserLevel', 'Lib/Enum');
App::uses('Designations', 'Lib/Enum');
/**
 * PressReleases Controller
 *
 * @property PressRelease $PressRelease
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PressReleasesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	/**
	 * beforeFilter callback
	 *
	 * @return void
	 */
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('index', 'view');
		}
	
	public function index() {
		$this->layout = 'general';
		$options = array();
		$this->Paginator->settings = [
		  'limit' => 10,
		  'order' => [
		    'id' => 'desc'
		  ]
		];
		if ( ! empty($this->request->query['q'])) {
			$options = array(
				'OR' => array(
					'PressRelease.subject LIKE' => '%' . $this->request->query['q'] . '%',
					'PressRelease.body LIKE' => '%' . $this->request->query['q'] . '%',
					'PressRelease.authority_name LIKE' => '%' . $this->request->query['q'] . '%'
				)
			);
		}
		$pressReleases = $this->Paginator->paginate('PressRelease', $options);
		$this->set(compact('pressReleases'));
	}

	public function view($id) {
		$this->layout = 'general';
		if (!$this->PressRelease->exists($id)) {
			$this->Session->setFlash(__('The Press Release could not be found. Please, try searching from here.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('controller' => 'press_releases', 'action' => 'index'));
		} else {
			$pressRelease = $this->PressRelease->findById($id);
			$this->set(compact('pressRelease'));
		}
	}
	

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PressRelease->recursive = 0;
		$this->set('pressReleases', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PressRelease->exists($id)) {
			throw new NotFoundException(__('Invalid press release'));
		}
		$options = array('conditions' => array('PressRelease.' . $this->PressRelease->primaryKey => $id));
		$this->set('pressRelease', $this->PressRelease->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PressRelease->create();
			if ($this->PressRelease->save($this->request->data)) {
				$this->Session->setFlash(__('The press release has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The press release could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$authorityDesignations = Designations::options();
		$this->set(compact('authorityDesignations'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PressRelease->exists($id)) {
			throw new NotFoundException(__('Invalid press release'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PressRelease->save($this->request->data)) {
				$this->Session->setFlash(__('The press release has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The press release could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('PressRelease.' . $this->PressRelease->primaryKey => $id));
			$this->request->data = $this->PressRelease->find('first', $options);
		}
		$authorityDesignations = Designations::options();
		$this->set(compact('authorityDesignations'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PressRelease->id = $id;
		if (!$this->PressRelease->exists()) {
			throw new NotFoundException(__('Invalid press release'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PressRelease->delete()) {
			$this->Session->setFlash(__('The press release has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The press release could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
