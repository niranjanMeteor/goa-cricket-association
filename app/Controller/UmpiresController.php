<?php
App::uses('AppController', 'Controller');
App::uses('UserLevel', 'Lib/Enum');
App::uses('UmpireGrade', 'Lib/Enum');
/**
 * Umpires Controller
 *
 * @property Umpire $Umpire
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UmpiresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	/**
	 * beforeFilter callback
	 *
	 * @return void
	 */
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('search');
		}

		public function search() {
			$this->layout = 'general';
			$options = array();
			$this->Paginator->settings = [
			  'limit' => 10,
			  'order' => [
			    'id' => 'asc'
			  ]
			];
			if ( ! empty($this->request->query['q'])) {
				$options = array(
					'Umpire.name LIKE' => '%' . $this->request->query['q'] . '%'
				);
			}
			$umpires = $this->Paginator->paginate('Umpire', $options);
			$this->set(compact('umpires'));
		}
	

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Umpire->recursive = 0;
		$this->set('umpires', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Umpire->exists($id)) {
			throw new NotFoundException(__('Invalid umpire'));
		}
		$options = array('conditions' => array('Umpire.' . $this->Umpire->primaryKey => $id));
		$this->set('umpire', $this->Umpire->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Umpire->create();
			if ($this->Umpire->save($this->request->data)) {
				$this->Session->setFlash(__('The umpire has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The umpire could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$grades = UmpireGrade::options();
		$this->set(compact('grades'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Umpire->exists($id)) {
			throw new NotFoundException(__('Invalid umpire'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Umpire->save($this->request->data)) {
				$this->Session->setFlash(__('The umpire has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The umpire could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Umpire.' . $this->Umpire->primaryKey => $id));
			$this->request->data = $this->Umpire->find('first', $options);
		}
		$grades = UmpireGrade::options();
		$this->set(compact('grades'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Umpire->id = $id;
		if (!$this->Umpire->exists()) {
			throw new NotFoundException(__('Invalid umpire'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Umpire->delete()) {
			$this->Session->setFlash(__('The umpire has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The umpire could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
