<?php
App::uses('AppController', 'Controller');
App::uses('UserLevel', 'Lib/Enum');
App::uses('TeamPlayerStatus', 'Lib/Enum');
App::uses('PlayerArm', 'Lib/Enum');
App::uses('BowlingStyle', 'Lib/Enum');
App::uses('BattingHand', 'Lib/Enum');
App::uses('SupportStaffType', 'Lib/Enum');
/**
 * Teams Controller
 *
 * @property Team $Team
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TeamsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	/**
	 * beforeFilter callback
	 *
	 * @return void
	 */
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('view', 'search');
		}
	

	public function view($id) {
		$this->layout = 'general';
		if ($this->Team->exists($id)) {
			$team = $this->Team->getTeamById($id);
			$this->set(compact('team'));
		} else {
			$this->Session->setFlash(__('The team could not be found. Please, try searching from here.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('controller' => 'teams', 'action' => 'search'));
		}
	}

	public function search() {
		$this->layout = 'general';
		$options = array();
		$this->Paginator->settings = [
		  'limit' => 10,
		  'order' => [
		    'id' => 'asc'
		  ]
		];
		if ( ! empty($this->request->query['q'])) {
			$options = array(
				'Team.name LIKE' => '%' . $this->request->query['q'] . '%'
			);
		}
		$teams = $this->Paginator->paginate('Team', $options);
		$this->set(compact('teams'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Team->recursive = 0;
		$this->set('teams', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Team->exists($id)) {
			throw new NotFoundException(__('Invalid team'));
		}
		$options = array('conditions' => array('Team.' . $this->Team->primaryKey => $id));
		$this->set('team', $this->Team->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Team->create();
			if ($this->Team->save($this->request->data)) {
				$this->Session->setFlash(__('The team has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Team->exists($id)) {
			throw new NotFoundException(__('Invalid team'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Team->save($this->request->data)) {
				$this->Session->setFlash(__('The team has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Team.' . $this->Team->primaryKey => $id));
			$this->request->data = $this->Team->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Team->id = $id;
		if (!$this->Team->exists()) {
			throw new NotFoundException(__('Invalid team'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Team->delete()) {
			$this->Session->setFlash(__('The team has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The team could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
