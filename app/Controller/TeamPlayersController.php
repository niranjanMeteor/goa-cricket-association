<?php
App::uses('AppController', 'Controller');
App::uses('TeamPlayerStatus', 'Lib/Enum');
/**
 * TeamPlayers Controller
 *
 * @property TeamPlayer $TeamPlayer
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TeamPlayersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TeamPlayer->recursive = 0;
		$this->set('teamPlayers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TeamPlayer->exists($id)) {
			throw new NotFoundException(__('Invalid team player'));
		}
		$options = array('conditions' => array('TeamPlayer.' . $this->TeamPlayer->primaryKey => $id));
		$this->set('teamPlayer', $this->TeamPlayer->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TeamPlayer->create();
			if ($this->TeamPlayer->save($this->request->data)) {
				$this->Session->setFlash(__('The team player has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team player could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$teams = $this->TeamPlayer->Team->find('list');
		$players = $this->TeamPlayer->Player->find('list');
		$statuses = TeamPlayerStatus::options();
		$this->set(compact('teams', 'players', 'statuses'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TeamPlayer->exists($id)) {
			throw new NotFoundException(__('Invalid team player'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TeamPlayer->save($this->request->data)) {
				$this->Session->setFlash(__('The team player has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team player could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('TeamPlayer.' . $this->TeamPlayer->primaryKey => $id));
			$this->request->data = $this->TeamPlayer->find('first', $options);
		}
		$teams = $this->TeamPlayer->Team->find('list');
		$players = $this->TeamPlayer->Player->find('list');
		$this->set(compact('teams', 'players'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TeamPlayer->id = $id;
		if (!$this->TeamPlayer->exists()) {
			throw new NotFoundException(__('Invalid team player'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->TeamPlayer->delete()) {
			$this->Session->setFlash(__('The team player has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The team player could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
