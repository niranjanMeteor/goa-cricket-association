<?php
App::uses('AppController', 'Controller');
/**
 * AuctionGroups Controller
 *
 * @property AuctionGroup $AuctionGroup
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AuctionGroupsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->AuctionGroup->recursive = 0;
		$this->set('auctionGroups', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->AuctionGroup->exists($id)) {
			throw new NotFoundException(__('Invalid auction group'));
		}
		$options = array('conditions' => array('AuctionGroup.' . $this->AuctionGroup->primaryKey => $id));
		$this->set('auctionGroup', $this->AuctionGroup->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->AuctionGroup->create();
			if ($this->AuctionGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The auction group has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The auction group could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->AuctionGroup->exists($id)) {
			throw new NotFoundException(__('Invalid auction group'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AuctionGroup->save($this->request->data)) {
				$this->Session->setFlash(__('The auction group has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The auction group could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('AuctionGroup.' . $this->AuctionGroup->primaryKey => $id));
			$this->request->data = $this->AuctionGroup->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->AuctionGroup->id = $id;
		if (!$this->AuctionGroup->exists()) {
			throw new NotFoundException(__('Invalid auction group'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AuctionGroup->delete()) {
			$this->Session->setFlash(__('The auction group has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The auction group could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
