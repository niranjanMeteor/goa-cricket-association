<?php
App::uses('AppController', 'Controller');
App::uses('RemoteAction', 'Model');
/**
 * Bids Controller
 *
 * @property Bid $Bid
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BidsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');


	public function admin_add_bid($playerId, $teamId) {
		$bid = $this->Bid->findBidDetails($playerId);
		$minimiumAmount = $bid['Player']['PlayerAuctionGroup'][0]['AuctionGroup']['minimum_amount'];
		$raise = $bid['Player']['PlayerAuctionGroup'][0]['AuctionGroup']['raise'];
		$newBid = $this->Bid->BidHistory->addNewBid($bid['Bid']['id'], $teamId, $minimiumAmount, $raise);
		if ($newBid) {
			$this->redirect(array('controller' => 'rounds', 'action' => 'new_round', 'admin' => true));
		} else {
			$this->Session->setFlash(__('The bid could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('controller' => 'rounds', 'action' => 'new_round', 'admin' => true));
		}
	}

	public function admin_sold($playerId) {
		if ($this->Bid->sellPlayer($playerId)) {
			$this->redirect(array('controller' => 'rounds', 'action' => 'new_round', 'admin' => true));
		} else {
			$this->Session->setFlash(__('Player could not be sold. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('controller' => 'rounds', 'action' => 'new_round', 'admin' => true));
		}
	}

	public function admin_next_bid($playerId) {
		if ($this->Bid->canProceedToNextBid($playerId)) {
			$this->redirect(array('controller' => 'rounds', 'action' => 'new_round', 'admin' => true));
		} else {
			$this->Session->setFlash(__('Could not proceed to next bid. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('controller' => 'rounds', 'action' => 'new_round', 'admin' => true));
		}
	}
	public function admin_pause() {
		$RemoteAction = new RemoteAction();
		$remoteAction = $RemoteAction->pauseBidding();
	}

	public function admin_bid_and_pause($playerId) {
		if ($this->Bid->canProceedToNextBid($playerId)) {
			$this->redirect(array('controller' => 'bids', 'action' => 'pause', 'admin' => true));
		} else {
			$this->Session->setFlash(__('Could not pause. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('controller' => 'rounds', 'action' => 'new_round', 'admin' => true));
		}
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Bid->recursive = 0;
		$this->set('bids', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Bid->exists($id)) {
			throw new NotFoundException(__('Invalid bid'));
		}
		$options = array('conditions' => array('Bid.' . $this->Bid->primaryKey => $id));
		$this->set('bid', $this->Bid->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Bid->create();
			if ($this->Bid->save($this->request->data)) {
				$this->Session->setFlash(__('The bid has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bid could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$players = $this->Bid->Player->find('list');
		$teams = $this->Bid->Team->find('list');
		$this->set(compact('players', 'teams'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Bid->exists($id)) {
			throw new NotFoundException(__('Invalid bid'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Bid->save($this->request->data)) {
				$this->Session->setFlash(__('The bid has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bid could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Bid.' . $this->Bid->primaryKey => $id));
			$this->request->data = $this->Bid->find('first', $options);
		}
		$players = $this->Bid->Player->find('list');
		$teams = $this->Bid->Team->find('list');
		$this->set(compact('players', 'teams'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Bid->id = $id;
		if (!$this->Bid->exists()) {
			throw new NotFoundException(__('Invalid bid'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Bid->delete()) {
			$this->Session->setFlash(__('The bid has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The bid could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
