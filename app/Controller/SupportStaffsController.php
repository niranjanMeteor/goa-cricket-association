<?php
App::uses('AppController', 'Controller');
App::uses('SupportStaffType', 'Lib/Enum');
/**
 * SupportStaffs Controller
 *
 * @property SupportStaff $SupportStaff
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SupportStaffsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->SupportStaff->recursive = 0;
		$this->set('supportStaffs', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->SupportStaff->exists($id)) {
			throw new NotFoundException(__('Invalid support staff'));
		}
		$options = array('conditions' => array('SupportStaff.' . $this->SupportStaff->primaryKey => $id));
		$this->set('supportStaff', $this->SupportStaff->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->SupportStaff->create();
			if ($this->SupportStaff->save($this->request->data)) {
				$this->Session->setFlash(__('The support staff has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The support staff could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$roles = SupportStaffType::options();
		$this->set(compact('roles'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->SupportStaff->exists($id)) {
			throw new NotFoundException(__('Invalid support staff'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SupportStaff->save($this->request->data)) {
				$this->Session->setFlash(__('The support staff has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The support staff could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('SupportStaff.' . $this->SupportStaff->primaryKey => $id));
			$this->request->data = $this->SupportStaff->find('first', $options);
		}
		$roles = SupportStaffType::options();
		$this->set(compact('roles'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->SupportStaff->id = $id;
		if (!$this->SupportStaff->exists()) {
			throw new NotFoundException(__('Invalid support staff'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->SupportStaff->delete()) {
			$this->Session->setFlash(__('The support staff has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The support staff could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
