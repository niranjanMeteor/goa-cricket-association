<?php
App::uses('AppController', 'Controller');
/**
 * TeamSupportStaffs Controller
 *
 * @property TeamSupportStaff $TeamSupportStaff
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TeamSupportStaffsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TeamSupportStaff->recursive = 0;
		$this->set('teamSupportStaffs', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TeamSupportStaff->exists($id)) {
			throw new NotFoundException(__('Invalid team support staff'));
		}
		$options = array('conditions' => array('TeamSupportStaff.' . $this->TeamSupportStaff->primaryKey => $id));
		$this->set('teamSupportStaff', $this->TeamSupportStaff->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TeamSupportStaff->create();
			if ($this->TeamSupportStaff->save($this->request->data)) {
				$this->Session->setFlash(__('The team support staff has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team support staff could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$teams = $this->TeamSupportStaff->Team->find('list');
		$supportStaffs = $this->TeamSupportStaff->SupportStaff->find('list');
		$this->set(compact('teams', 'supportStaffs'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TeamSupportStaff->exists($id)) {
			throw new NotFoundException(__('Invalid team support staff'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TeamSupportStaff->save($this->request->data)) {
				$this->Session->setFlash(__('The team support staff has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team support staff could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('TeamSupportStaff.' . $this->TeamSupportStaff->primaryKey => $id));
			$this->request->data = $this->TeamSupportStaff->find('first', $options);
		}
		$teams = $this->TeamSupportStaff->Team->find('list');
		$supportStaffs = $this->TeamSupportStaff->SupportStaff->find('list');
		$this->set(compact('teams', 'supportStaffs'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TeamSupportStaff->id = $id;
		if (!$this->TeamSupportStaff->exists()) {
			throw new NotFoundException(__('Invalid team support staff'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->TeamSupportStaff->delete()) {
			$this->Session->setFlash(__('The team support staff has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The team support staff could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
