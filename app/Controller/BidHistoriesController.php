<?php
App::uses('AppController', 'Controller');
/**
 * BidHistories Controller
 *
 * @property BidHistory $BidHistory
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class BidHistoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->BidHistory->recursive = 0;
		$this->set('bidHistories', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->BidHistory->exists($id)) {
			throw new NotFoundException(__('Invalid bid history'));
		}
		$options = array('conditions' => array('BidHistory.' . $this->BidHistory->primaryKey => $id));
		$this->set('bidHistory', $this->BidHistory->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->BidHistory->create();
			if ($this->BidHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The bid history has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bid history could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$bids = $this->BidHistory->Bid->find('list');
		$teams = $this->BidHistory->Team->find('list');
		$this->set(compact('bids', 'teams'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->BidHistory->exists($id)) {
			throw new NotFoundException(__('Invalid bid history'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BidHistory->save($this->request->data)) {
				$this->Session->setFlash(__('The bid history has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The bid history could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('BidHistory.' . $this->BidHistory->primaryKey => $id));
			$this->request->data = $this->BidHistory->find('first', $options);
		}
		$bids = $this->BidHistory->Bid->find('list');
		$teams = $this->BidHistory->Team->find('list');
		$this->set(compact('bids', 'teams'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->BidHistory->id = $id;
		if (!$this->BidHistory->exists()) {
			throw new NotFoundException(__('Invalid bid history'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->BidHistory->delete()) {
			$this->Session->setFlash(__('The bid history has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The bid history could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
