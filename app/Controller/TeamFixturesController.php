<?php
App::uses('AppController', 'Controller');
/**
 * TeamFixtures Controller
 *
 * @property TeamFixture $TeamFixture
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TeamFixturesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TeamFixture->recursive = 0;
		$this->set('teamFixtures', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TeamFixture->exists($id)) {
			throw new NotFoundException(__('Invalid team fixture'));
		}
		$options = array('conditions' => array('TeamFixture.' . $this->TeamFixture->primaryKey => $id));
		$this->set('teamFixture', $this->TeamFixture->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TeamFixture->create();
			if ($this->TeamFixture->save($this->request->data)) {
				$this->Session->setFlash(__('The team fixture has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team fixture could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$teams = $this->TeamFixture->Team->find('list');
		$this->set(compact('teams'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TeamFixture->exists($id)) {
			throw new NotFoundException(__('Invalid team fixture'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TeamFixture->save($this->request->data)) {
				$this->Session->setFlash(__('The team fixture has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The team fixture could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('TeamFixture.' . $this->TeamFixture->primaryKey => $id));
			$this->request->data = $this->TeamFixture->find('first', $options);
		}
		$teams = $this->TeamFixture->Team->find('list');
		$this->set(compact('teams'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TeamFixture->id = $id;
		if (!$this->TeamFixture->exists()) {
			throw new NotFoundException(__('Invalid team fixture'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->TeamFixture->delete()) {
			$this->Session->setFlash(__('The team fixture has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The team fixture could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
