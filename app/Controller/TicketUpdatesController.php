<?php
App::uses('AppController', 'Controller');
/**
 * TicketUpdates Controller
 *
 * @property TicketUpdate $TicketUpdate
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TicketUpdatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TicketUpdate->recursive = 0;
		$this->set('ticketUpdates', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TicketUpdate->exists($id)) {
			throw new NotFoundException(__('Invalid ticket update'));
		}
		$options = array('conditions' => array('TicketUpdate.' . $this->TicketUpdate->primaryKey => $id));
		$this->set('ticketUpdate', $this->TicketUpdate->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TicketUpdate->create();
			if ($this->TicketUpdate->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket update has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket update could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$tickets = $this->TicketUpdate->Ticket->find('list');
		$users = $this->TicketUpdate->User->find('list');
		$this->set(compact('tickets', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TicketUpdate->exists($id)) {
			throw new NotFoundException(__('Invalid ticket update'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TicketUpdate->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket update has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket update could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('TicketUpdate.' . $this->TicketUpdate->primaryKey => $id));
			$this->request->data = $this->TicketUpdate->find('first', $options);
		}
		$tickets = $this->TicketUpdate->Ticket->find('list');
		$users = $this->TicketUpdate->User->find('list');
		$this->set(compact('tickets', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TicketUpdate->id = $id;
		if (!$this->TicketUpdate->exists()) {
			throw new NotFoundException(__('Invalid ticket update'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->TicketUpdate->delete()) {
			$this->Session->setFlash(__('The ticket update has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The ticket update could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
