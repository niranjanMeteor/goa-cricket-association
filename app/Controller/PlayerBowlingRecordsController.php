<?php
App::uses('AppController', 'Controller');
App::uses('MatchLevel', 'Lib/Enum');
/**
 * PlayerBowlingRecords Controller
 *
 * @property PlayerBowlingRecord $PlayerBowlingRecord
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PlayerBowlingRecordsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->PlayerBowlingRecord->recursive = 0;
		$this->set('playerBowlingRecords', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->PlayerBowlingRecord->exists($id)) {
			throw new NotFoundException(__('Invalid player bowling record'));
		}
		$options = array('conditions' => array('PlayerBowlingRecord.' . $this->PlayerBowlingRecord->primaryKey => $id));
		$this->set('playerBowlingRecord', $this->PlayerBowlingRecord->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->PlayerBowlingRecord->create();
			if ($this->PlayerBowlingRecord->save($this->request->data)) {
				$this->Session->setFlash(__('The player bowling record has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The player bowling record could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$players = $this->PlayerBowlingRecord->Player->find('list');
		$levels = MatchLevel::options();
		$this->set(compact('players', 'levels'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->PlayerBowlingRecord->exists($id)) {
			throw new NotFoundException(__('Invalid player bowling record'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->PlayerBowlingRecord->save($this->request->data)) {
				$this->Session->setFlash(__('The player bowling record has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The player bowling record could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('PlayerBowlingRecord.' . $this->PlayerBowlingRecord->primaryKey => $id));
			$this->request->data = $this->PlayerBowlingRecord->find('first', $options);
		}
		$players = $this->PlayerBowlingRecord->Player->find('list');
		$levels = MatchLevel::options();
		$this->set(compact('players', 'levels'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->PlayerBowlingRecord->id = $id;
		if (!$this->PlayerBowlingRecord->exists()) {
			throw new NotFoundException(__('Invalid player bowling record'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->PlayerBowlingRecord->delete()) {
			$this->Session->setFlash(__('The player bowling record has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The player bowling record could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
