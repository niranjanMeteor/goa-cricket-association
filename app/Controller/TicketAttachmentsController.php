<?php
App::uses('AppController', 'Controller');
/**
 * TicketAttachments Controller
 *
 * @property TicketAttachment $TicketAttachment
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TicketAttachmentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->TicketAttachment->recursive = 0;
		$this->set('ticketAttachments', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->TicketAttachment->exists($id)) {
			throw new NotFoundException(__('Invalid ticket attachment'));
		}
		$options = array('conditions' => array('TicketAttachment.' . $this->TicketAttachment->primaryKey => $id));
		$this->set('ticketAttachment', $this->TicketAttachment->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->TicketAttachment->create();
			if ($this->TicketAttachment->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket attachment has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket attachment could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$tickets = $this->TicketAttachment->Ticket->find('list');
		$this->set(compact('tickets'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->TicketAttachment->exists($id)) {
			throw new NotFoundException(__('Invalid ticket attachment'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->TicketAttachment->save($this->request->data)) {
				$this->Session->setFlash(__('The ticket attachment has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ticket attachment could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('TicketAttachment.' . $this->TicketAttachment->primaryKey => $id));
			$this->request->data = $this->TicketAttachment->find('first', $options);
		}
		$tickets = $this->TicketAttachment->Ticket->find('list');
		$this->set(compact('tickets'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->TicketAttachment->id = $id;
		if (!$this->TicketAttachment->exists()) {
			throw new NotFoundException(__('Invalid ticket attachment'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->TicketAttachment->delete()) {
			$this->Session->setFlash(__('The ticket attachment has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The ticket attachment could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
