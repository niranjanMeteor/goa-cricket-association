<?php
App::uses('AppController', 'Controller');
App::uses('PlayerRole', 'Lib/Enum');
App::uses('PlayerArm', 'Lib/Enum');
App::uses('BattingHand', 'Lib/Enum');
App::uses('BowlingStyle', 'Lib/Enum');
App::uses('RemoteAction', 'Model');
App::uses('Audio', 'Model');
/**
 * Rounds Controller
 *
 * @property Round $Round
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class RoundsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	/**
	 * beforeFilter callback
	 *
	 * @return void
	 */
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('prepare_auction_data');
		}

	public function prepare_auction_data() {
		$RemoteAction = new RemoteAction();
		$remoteAction = $RemoteAction->findByIsActive(true);
		$status = $remoteAction['RemoteAction']['code'];
		if ($status == 300) {
			$player = $this->Round->AuctionGroup->PlayerAuctionGroup->Player->Bid->getLastSoldPlayer();
			$playerImageUrl = 'http://' . Configure::read('webUrl') . '/files/player/image/' . $player['Player']['dir'] . '/' . 'xvga_' . $player['Player']['image'];
			// if (@getimagesize($playerImageUrl)) {
			// 	$playerImageUrl = $playerImageUrl;
			// } else {
			// 	$playerImageUrl = 'img/default-user.jpeg';
			// }
			$response['player']['url'] = 'http://' . Configure::read('webUrl') . '/players/view/' . $player['Player']['id'];
			$response['player']['image_url'] = $playerImageUrl;
			$response['player']['role'] = PlayerRole::stringValue($player['Player']['role']);
			$response['player']['name'] = $player['Player']['name'];
			$response['team']['name'] = $player['Team']['name'];
			$response['amount'] = $player['Bid']['buying_amount'];
			$Audio = new Audio();
			$lastAudio = $Audio->find('first', array(
				'order' => array('Audio.id DESC')
			));
			$response['audio'] = !empty($lastAudio['Audio']['name']) ? $lastAudio['Audio']['name'] : null;
			echo json_encode(array('status' => $status, 'data' => $response)); die;
		} else if ($status == 100) {
			echo json_encode(array('status' => $status)); die;
		} else if ($status == 600) {
			echo json_encode(array('status' => $status)); die;
		} else if ($status == 400) {
			$teams = $this->Round->AuctionGroup->PlayerAuctionGroup->Player->TeamPlayer->Team->getFinalTeamList();
			$teams = $this->Round->formatTeamsFinalList($teams);
			$response['teams'] = $teams;
			echo json_encode(array('status' => $status, 'data' => $response)); die;
		}
		$player = $this->Round->getNextRoundPlayerForClient();
		if (!empty($player)) {
			$playerImageUrl = 'http://' . Configure::read('webUrl') . '/files/player/image/' . $player['Player']['dir'] . '/' . 'xvga_' . $player['Player']['image'];
			// if (@getimagesize($playerImageUrl)) {
			// 	$playerImageUrl = $playerImageUrl;
			// } else {
			// 	$playerImageUrl = 'img/default-user.jpeg';
			// }
			$teams = $this->Round->AuctionGroup->PlayerAuctionGroup->Player->TeamPlayer->Team->findAuctionTeams();
			$teams = $this->Round->formatTeams($teams, $player['Player']['id']);
			$currentBid = $this->Round->getCurrentBidAmount($player['Player']['id']);
			$roundCount = $this->Round->findRoundCount($player['PlayerAuctionGroup']['auction_group_id']);
			$playersLeftInThisRound = $this->Round->findPlayersInThisRound();

			$response['bid_amount']['minimum_bid'] = $player['AuctionGroup']['minimum_amount'];
			$response['bid_amount']['current_bid'] = $currentBid;
			$response['bid_amount']['player_details']['name'] = $player['Player']['name'];
			$response['bid_amount']['player_details']['role'] = PlayerRole::stringValue($player['Player']['role']);

			$response['teams'] = $teams;

			$response['round_details']['group_name'] = $player['AuctionGroup']['name'];
			$response['round_details']['round_number'] = $roundCount;

			$response['player']['url'] = 'http://' . Configure::read('webUrl') . '/players/view/' . $player['Player']['id'];
			$response['player']['image_url'] = $playerImageUrl;
			$response['player']['matches'] = !empty($player['Player']['PlayerBattingRecord'][0]['match_count']) ? $player['Player']['PlayerBattingRecord'][0]['match_count'] : 0;
			$response['player']['wickets'] = !empty($player['Player']['PlayerBowlingRecord'][0]['wickets_taken']) ? $player['Player']['PlayerBowlingRecord'][0]['wickets_taken'] : 0;
			$response['player']['runs'] = !empty($player['Player']['PlayerBattingRecord'][0]['runs']) ?$player['Player']['PlayerBattingRecord'][0]['runs'] : 0;
			$response['player']['best_batting'] = !empty($player['Player']['PlayerBattingRecord'][0]['high_score']) ? $player['Player']['PlayerBattingRecord'][0]['high_score'] : 0;
			$response['player']['best_bowling'] = !empty($player['Player']['PlayerBowlingRecord'][0]['best_bowling_match']) ? $player['Player']['PlayerBowlingRecord'][0]['best_bowling_match'] : 0;
			$response['player']['batting_strike_rate'] = !empty($player['Player']['PlayerBattingRecord'][0]['strike_rate']) ? $player['Player']['PlayerBattingRecord'][0]['strike_rate'] : 0;
			$response['player']['bowling_economy'] = !empty($player['Player']['PlayerBowlingRecord'][0]['bowling_economy']) ? $player['Player']['PlayerBowlingRecord'][0]['bowling_economy'] : 0;
			$response['player']['role'] = PlayerRole::stringValue($player['Player']['role']);
			$response['player']['batting_hand'] = BattingHand::stringValue($player['Player']['batting_arm']);
			$response['player']['bowling_arm'] = PlayerArm::stringValue($player['Player']['bowling_arm']);
			$response['player']['bowling_style'] = BowlingStyle::stringValue($player['Player']['bowling_style']);
			$response['player']['name'] = $player['Player']['name'];

			$response['bidding_details'] = $this->Round->getBiddingDetails($player['Player']['id']);
			echo json_encode(array('status' => $status, 'data' => $response)); die;
		}
	}
	

	public function admin_start() {

	}

	public function admin_new_round() {
		$player = $this->Round->getNextRoundPlayer();
		if (!empty($player)) {
			$currentBid = $this->Round->getCurrentBidAmount($player['Player']['id']);
			$roundCount = $this->Round->findRoundCount($player['PlayerAuctionGroup']['auction_group_id']);
			$playersLeftInThisRound = $this->Round->findPlayersInThisRound();
			$actualBasePrice = $player['AuctionGroup']['minimum_amount'] + $player['AuctionGroup']['raise'];
			$nextBidAmount = $currentBid + $player['AuctionGroup']['raise'];
			$teams = $this->Round->AuctionGroup->PlayerAuctionGroup->Player->TeamPlayer->Team->updateTeamsEligibilityForNextBid($nextBidAmount, $actualBasePrice);
			$teams = $this->Round->formatTeamsForServer($teams, $player['Player']['id']);
			$this->set(compact('player', 'teams', 'currentBid', 'roundCount'));
		} else {
			$this->Session->setFlash(__('The auction is completed'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('controller' => 'rounds', 'action' => 'completed', 'admin' => true));
		}
	}

	public function admin_completed() {

	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Round->recursive = 0;
		$this->set('rounds', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Round->exists($id)) {
			throw new NotFoundException(__('Invalid round'));
		}
		$options = array('conditions' => array('Round.' . $this->Round->primaryKey => $id));
		$this->set('round', $this->Round->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Round->create();
			if ($this->Round->save($this->request->data)) {
				$this->Session->setFlash(__('The round has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The round could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$auctionGroups = $this->Round->AuctionGroup->find('list');
		$this->set(compact('auctionGroups'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Round->exists($id)) {
			throw new NotFoundException(__('Invalid round'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Round->save($this->request->data)) {
				$this->Session->setFlash(__('The round has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The round could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Round.' . $this->Round->primaryKey => $id));
			$this->request->data = $this->Round->find('first', $options);
		}
		$auctionGroups = $this->Round->AuctionGroup->find('list');
		$this->set(compact('auctionGroups'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Round->id = $id;
		if (!$this->Round->exists()) {
			throw new NotFoundException(__('Invalid round'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Round->delete()) {
			$this->Session->setFlash(__('The round has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The round could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
