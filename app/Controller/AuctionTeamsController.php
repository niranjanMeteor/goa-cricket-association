<?php
App::uses('AppController', 'Controller');
/**
 * AuctionTeams Controller
 *
 * @property AuctionTeam $AuctionTeam
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class AuctionTeamsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->AuctionTeam->recursive = 0;
		$this->set('auctionTeams', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->AuctionTeam->exists($id)) {
			throw new NotFoundException(__('Invalid auction team'));
		}
		$options = array('conditions' => array('AuctionTeam.' . $this->AuctionTeam->primaryKey => $id));
		$this->set('auctionTeam', $this->AuctionTeam->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->AuctionTeam->create();
			if ($this->AuctionTeam->save($this->request->data)) {
				$this->Session->setFlash(__('The auction team has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The auction team could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$teams = $this->AuctionTeam->Team->find('list');
		$this->set(compact('teams'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->AuctionTeam->exists($id)) {
			throw new NotFoundException(__('Invalid auction team'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AuctionTeam->save($this->request->data)) {
				$this->Session->setFlash(__('The auction team has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The auction team could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('AuctionTeam.' . $this->AuctionTeam->primaryKey => $id));
			$this->request->data = $this->AuctionTeam->find('first', $options);
		}
		$teams = $this->AuctionTeam->Team->find('list');
		$this->set(compact('teams'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->AuctionTeam->id = $id;
		if (!$this->AuctionTeam->exists()) {
			throw new NotFoundException(__('Invalid auction team'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AuctionTeam->delete()) {
			$this->Session->setFlash(__('The auction team has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The auction team could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
