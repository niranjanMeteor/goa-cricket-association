<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
  public $components = array(
    'Session',
    'Auth' => array(
      'logoutRedirect' => array('controller' => 'users', 'action' => 'login', 'admin' => false),
      'authenticate' => array(
        'Form' => array(
          'fields' => array('username' => 'email'),
          'passwordHasher' => 'Blowfish'
        )
      )
    ),
    'Scoring'
  );
  // public $helpers = array('Parser', 'Link');
  
  public function beforeFilter() {
    parent::beforeFilter();

    if (!empty($this->request->params['admin']) && $this->request->params['admin']) {
      App::uses('UserLevel', 'Enum');
      if ($this->Auth->user('level') != UserLevel::ADMINISTRATOR) {
        $this->redirect(array('controllers' => 'users', 'action' => 'logout', 'admin' => false));
      }
      $this->layout = 'admin';
    }
  }
  function _setErrorLayout() {  
    if ($this->name == 'CakeError') {  
      $this->layout = 'general';  
    }    
  }              

  function beforeRender () {  
    $this->_setErrorLayout();
  }
  /* Utility Functions */
  // protected function _setFlash($message, $style = 'default') {
  //   $this->Session->setFlash(__($message), "flash/{$style}");
  // }

  // protected function _setAutoModal($modalElement, $params = array()) {
  //   $this->Session->setFlash('', "modal/{$modalElement}", $params, 'auto_modal');
  // }

  // protected function _saveSessionFormData() {
  //   $validationErrors = array();
  //   if (!empty($this->uses)) {
  //     foreach ($this->uses as $modelClass) {
  //       $validationErrors[$modelClass] = $this->{$modelClass}->validationErrors;
  //     }
  //   }

  //   CakeSession::write('sessionForm.data', $this->request->data);
  //   CakeSession::write('sessionForm.validationErrors', $validationErrors);
  // }

  // protected function _loadSessionFormData() {
  //   if ($this->request->is('get')) {
  //     if (CakeSession::check('sessionForm')) {
  //       $this->request->data = CakeSession::read('sessionForm.data');
  //       $validationErrors = CakeSession::read('sessionForm.validationErrors');
  //       foreach ($validationErrors as $modalClass => $errors) {
  //         $this->loadModel($modalClass);
  //         $this->{$modalClass}->validationErrors = $errors;
  //       }
  //       CakeSession::delete('sessionForm');
  //     }
  //   }
  // }
}
