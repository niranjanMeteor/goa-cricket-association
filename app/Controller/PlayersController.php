<?php
App::uses('AppController', 'Controller');
App::uses('UserLevel', 'Lib/Enum');
App::uses('PlayerArm', 'Lib/Enum');
App::uses('BowlingStyle', 'Lib/Enum');
App::uses('BattingHand', 'Lib/Enum');
App::uses('MatchLevel', 'Lib/Enum');
App::uses('PlayerRole', 'Lib/Enum');
/**
 * Players Controller
 *
 * @property Player $Player
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PlayersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	/**
	 * beforeFilter callback
	 *
	 * @return void
	 */
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('view', 'search', 'gpl_auction');
		}
	

	public function view($id) {
		$this->layout = 'general';
		if ($this->Player->exists($id)) {
			$player = $this->Player->getPlayerById($id);
			$this->set(compact('player'));
		} else {
			$this->Session->setFlash(__('The team could not be found. Please, try searching from here.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect(array('controller' => 'players', 'action' => 'search'));
		}
	}

	public function search() {
		$this->layout = 'general';
		$options = array();
		$this->Paginator->settings = [
		  'limit' => 10,
		  'order' => [
		    'id' => 'asc'
		  ]
		];
		if ( ! empty($this->request->query['q'])) {
			$options = array(
				'OR' => array(
					'Player.first_name LIKE' => '%' . $this->request->query['q'] . '%',
					'Player.last_name LIKE' => '%' . $this->request->query['q'] . '%',
					'Player.name LIKE' => '%' . $this->request->query['q'] . '%'
				)
			);
		}
		$players = $this->Paginator->paginate('Player', $options);
		$this->set(compact('players'));
	}

	public function gpl_auction() {
		$this->layout = 'blank';
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Player->recursive = 0;
		$this->set('players', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Player->exists($id)) {
			throw new NotFoundException(__('Invalid player'));
		}
		$options = array('conditions' => array('Player.' . $this->Player->primaryKey => $id));
		$this->set('player', $this->Player->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Player->create();
			if ($this->Player->save($this->request->data)) {
				$this->Session->setFlash(__('The player has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The player could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$cities = $this->Player->City->find('list');
		$bowlingStyles = BowlingStyle::options();
		$playerArms = PlayerArm::options();
		$roles = PlayerRole::options();
		$battingHands = BattingHand::options();
		$this->set(compact('cities', 'bowlingStyles', 'playerArms', 'roles', 'battingHands'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Player->exists($id)) {
			throw new NotFoundException(__('Invalid player'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Player->save($this->request->data)) {
				$this->Session->setFlash(__('The player has been saved.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The player could not be saved. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Player.' . $this->Player->primaryKey => $id));
			$this->request->data = $this->Player->find('first', $options);
		}
		$cities = $this->Player->City->find('list');
		$bowlingStyles = BowlingStyle::options();
		$playerArms = PlayerArm::options();
		$roles = PlayerRole::options();
		$battingHands = BattingHand::options();
		$this->set(compact('cities', 'bowlingStyles', 'playerArms', 'roles', 'battingHands'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Player->id = $id;
		if (!$this->Player->exists()) {
			throw new NotFoundException(__('Invalid player'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Player->delete()) {
			$this->Session->setFlash(__('The player has been deleted.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('The player could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
