<?php
App::uses('AppController', 'Controller');
/**
 * MatchBatsmanScores Controller
 *
 * @property MatchBatsmanScore $MatchBatsmanScore
 * @property PaginatorComponent $Paginator
 */
class MatchBatsmanScoresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->MatchBatsmanScore->recursive = 0;
		$this->set('matchBatsmanScores', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->MatchBatsmanScore->exists($id)) {
			throw new NotFoundException(__('Invalid match batsman score'));
		}
		$options = array('conditions' => array('MatchBatsmanScore.' . $this->MatchBatsmanScore->primaryKey => $id));
		$this->set('matchBatsmanScore', $this->MatchBatsmanScore->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->MatchBatsmanScore->create();
			if ($this->MatchBatsmanScore->save($this->request->data)) {
				return $this->flash(__('The match batsman score has been saved.'), array('action' => 'index'));
			}
		}
		$matchInningScores = $this->MatchBatsmanScore->MatchInningScore->find('list');
		$players = $this->MatchBatsmanScore->Player->find('list');
		$this->set(compact('matchInningScores', 'players'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->MatchBatsmanScore->exists($id)) {
			throw new NotFoundException(__('Invalid match batsman score'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MatchBatsmanScore->save($this->request->data)) {
				return $this->flash(__('The match batsman score has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('MatchBatsmanScore.' . $this->MatchBatsmanScore->primaryKey => $id));
			$this->request->data = $this->MatchBatsmanScore->find('first', $options);
		}
		$matchInningScores = $this->MatchBatsmanScore->MatchInningScore->find('list');
		$players = $this->MatchBatsmanScore->Player->find('list');
		$this->set(compact('matchInningScores', 'players'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->MatchBatsmanScore->id = $id;
		if (!$this->MatchBatsmanScore->exists()) {
			throw new NotFoundException(__('Invalid match batsman score'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MatchBatsmanScore->delete()) {
			return $this->flash(__('The match batsman score has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The match batsman score could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}
}
