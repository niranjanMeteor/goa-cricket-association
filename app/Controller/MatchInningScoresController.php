<?php
App::uses('AppController', 'Controller');
App::uses('UserLevel', 'Lib/Enum');
/**
 * MatchInningScores Controller
 *
 * @property MatchInningScore $MatchInningScore
 * @property PaginatorComponent $Paginator
 */
class MatchInningScoresController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	/**
	 * beforeFilter callback
	 *
	 * @return void
	 */
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('get_scorecard','get_mini_scorecard');
		}

		public function get_scorecard($matchId) {
			
			$response = $this->MatchInningScore->fetchMatchScorecard($matchId);
			echo json_encode($response); die;
		}

		public function get_mini_scorecard() {
			$response = $this->MatchInningScore->fetchMiniScorecard();
			echo json_encode($response); die;
		}
	

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->MatchInningScore->recursive = 0;
		$this->set('matchInningScores', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->MatchInningScore->exists($id)) {
			throw new NotFoundException(__('Invalid match inning score'));
		}
		$options = array('conditions' => array('MatchInningScore.' . $this->MatchInningScore->primaryKey => $id));
		$this->set('matchInningScore', $this->MatchInningScore->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->MatchInningScore->create();
			if ($this->MatchInningScore->save($this->request->data)) {
				$this->Session->setFlash(__('Match Inning Score saved.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Match Inning Score not saved.'), 'default', array('class' => 'alert alert-error'));
			}
		}
		$matches = $this->MatchInningScore->Match->find('list');
		$teams = $this->MatchInningScore->Team->find('list');
		$this->set(compact('matches', 'teams'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->MatchInningScore->exists($id)) {
			throw new NotFoundException(__('Invalid match inning score'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->MatchInningScore->save($this->request->data)) {
				$this->Session->setFlash(__('Match Inning Score saved.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Match Inning Score not saved.'), 'default', array('class' => 'alert alert-error'));
			}
		} else {
			$options = array('conditions' => array('MatchInningScore.' . $this->MatchInningScore->primaryKey => $id));
			$this->request->data = $this->MatchInningScore->find('first', $options);
		}
		$matches = $this->MatchInningScore->Match->find('list');
		$teams = $this->MatchInningScore->Team->find('list');
		$this->set(compact('matches', 'teams'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->MatchInningScore->id = $id;
		if (!$this->MatchInningScore->exists()) {
			throw new NotFoundException(__('Invalid match inning score'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->MatchInningScore->delete()) {
			return $this->flash(__('The match inning score has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The match inning score could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}

	public function admin_match_players($matchId,$inning) {
		echo $this->MatchInningScore->getMatchBatsmenAndBowlers($matchId,$inning); die;
	}
}
