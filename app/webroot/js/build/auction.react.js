/**
 * @jsx React.DOM
 */

var imagearray = ["img/1.jpg", "img/2.jpg", "img/3.jpg"];
var imageCount = 0;
var PlayerSold = React.createClass({displayName: 'PlayerSold',
  render: function() {
    return (
      React.DOM.div({className: "container sold"}, 
        React.DOM.div({className: "row"}, 
          React.DOM.div({className: "col-md-12 text-center"}, 
            React.DOM.h2({className: "heading"}, "Player Sold")
          )
        ), 
        React.DOM.hr(null), 
        React.DOM.div({className: "row"}, 
          React.DOM.div({className: "col-md-12 text-center sold-player-name"}, 
            React.DOM.h3(null, this.props.player.name)
          )
        ), 
        React.DOM.div({className: "row"}, 
          React.DOM.div({className: "col-md-12 text-center"}, 
            PlayerPic({playerUrl: this.props.player.url, playerPic: this.props.player.image_url})
          )
        ), 
        React.DOM.div({className: "row"}, 
          React.DOM.div({className: "col-md-4"}), 
          React.DOM.div({className: "col-md-4 text-center"}, 
            React.DOM.div({className: "sold-details"}, 
              React.DOM.h3({className: "sold-to-team"}, this.props.team.name), 
              React.DOM.p(null, React.DOM.span({className: "fa fa-inr"}), " ", this.props.amount)
            )
          ), 
          React.DOM.div({className: "col-md-4"})
        ), 
        React.DOM.div({className: "row"}, 
          React.DOM.div({className: "col-md-12 text-center congrats"}, 
            React.DOM.h2(null, "Congratulations, ", this.props.player.name, ", GCA wishes you a pleasant journey in GPL-2015. All The Best!")
          )
        )
      )
    );
  }
});

var AuctionTeam = React.createClass({displayName: 'AuctionTeam',
  render: function() {
    if (this.props.team.current == true) {
      var activeTeamClass = (this.props.team.current == true) ? 'team active' : 'team';
    } else {
      var activeTeamClass = (this.props.team.is_eligible_for_bid == true)? 'team': 'team not-eligible';      
    }
    var team_data_display = this.props.team.players_bought_count + " player bought";
    if (this.props.team.players_bought_count > 1 ) {
      team_data_display =  this.props.team.players_bought_count + " players bought";
    }
    return (
      React.DOM.div({className: "col-md-3"}, 
        React.DOM.div({className: activeTeamClass}, 
          React.DOM.span({className: "fa fa-flag"}), 
          React.DOM.div({className: "team-details"}, 
            React.DOM.div({className: "pull-left left-section"}, 
              React.DOM.a({href: this.props.team.url}, React.DOM.img({src: this.props.team.logo_url, className: "team-logo"}))
            ), 
            React.DOM.div({className: "pull-left right-section"}, 
              React.DOM.div({className: "team-name"}, this.props.team.name), 
              React.DOM.div({className: "team-location"}, 
               team_data_display
              )
            ), 
            React.DOM.div({className: "clearfix"})
          ), 
          React.DOM.div({className: "money-details"}, 
            React.DOM.div({className: "money-element"}, 
              React.DOM.div({className: "text pull-left"}, "Total"), 
              React.DOM.div({className: "value pull-right"}, React.DOM.span({className: "fa fa-inr"}), " ", this.props.team.total), 
              React.DOM.div({className: "clearfix"})
            ), 
            React.DOM.div({className: "money-element"}, 
              React.DOM.div({className: "text pull-left"}, "Min Base Amount"), 
              React.DOM.div({className: "value pull-right"}, React.DOM.span({className: "fa fa-inr"}), " ", this.props.team.required), 
              React.DOM.div({className: "clearfix"})
            ), 
            React.DOM.div({className: "money-element"}, 
              React.DOM.div({className: "text pull-left"}, "Available"), 
              React.DOM.div({className: "value pull-right"}, React.DOM.span({className: "fa fa-inr"}), " ", this.props.team.available), 
              React.DOM.div({className: "clearfix"})
            )
          )
        )
      )
    );
  }
});
var AuctionTeams = React.createClass({displayName: 'AuctionTeams',
  render: function() {
    var auctionTeams = this.props.teams.map(function(team) {
      return (
        AuctionTeam({team: team, key: team.id})
      )
    })
    return (
      React.DOM.div({className: "row auction-teams"}, 
        auctionTeams
      )
    );
  }
});
var PlayerStatElement = React.createClass({displayName: 'PlayerStatElement',
  render: function() {
    return (
      React.DOM.div({className: "stat-element"}, 
        React.DOM.div({className: "stat-text pull-left"}, this.props.text), 
        React.DOM.div({className: "stat-data pull-right"}, this.props.data), 
        React.DOM.div({className: "clearfix"})
      )
    );
  }
});
var WicketKeeper = React.createClass({displayName: 'WicketKeeper',
  render: function() {
    return (
      React.DOM.div({className: "wicketkeeper stat-element"}, 
        React.DOM.div({className: "pull-left left-section"}, 
          React.DOM.img({src: "img/keeper.png"})
        ), 
        React.DOM.div({className: "pull-left right-section"}, 
          "WicketKeeper"
        ), 
        React.DOM.div({className: "clearfix"})
      )
    );
  }
});
var LeftArmBowler = React.createClass({displayName: 'LeftArmBowler',
  render: function() {
    return (
      React.DOM.div({className: "left-arm-bowl stat-element"}, 
        React.DOM.div({className: "pull-left left-section"}, 
          React.DOM.img({src: "img/left-bowler.png"})
        ), 
        React.DOM.div({className: "pull-left right-section"}, 
          "Left Arm Bowler"
        ), 
        React.DOM.div({className: "clearfix"})
      )
    );
  }
});
var RightArmBowler = React.createClass({displayName: 'RightArmBowler',
  render: function() {
    return (
      React.DOM.div({className: "right-arm-bowl stat-element"}, 
        React.DOM.div({className: "pull-left left-section"}, 
          React.DOM.img({src: "img/right-bowler.png"})
        ), 
        React.DOM.div({className: "pull-left right-section"}, 
          "Right Arm Bowler"
        ), 
        React.DOM.div({className: "clearfix"})
      )
    );
  }
});
var RightHandBatsman = React.createClass({displayName: 'RightHandBatsman',
  render: function() {
    return (
      React.DOM.div({className: "right-hand-bat stat-element"}, 
        React.DOM.div({className: "pull-left left-section"}, 
          React.DOM.img({src: "img/righty-bat.png"})
        ), 
        React.DOM.div({className: "pull-left right-section"}, 
          "Right Hand Batsman"
        ), 
        React.DOM.div({className: "clearfix"})
      )
    );
  }
});
var LeftHandBatsman = React.createClass({displayName: 'LeftHandBatsman',
  render: function() {
    return (
      React.DOM.div({className: "left-hand-bat stat-element"}, 
        React.DOM.div({className: "pull-left left-section"}, 
          React.DOM.img({src: "img/left-bat.png"})
        ), 
        React.DOM.div({className: "pull-left right-section"}, 
          "Left Hand Batsman"
        ), 
        React.DOM.div({className: "clearfix"})
      )
    );
  }
});
var PlayerStats = React.createClass({displayName: 'PlayerStats',
  render: function() {
    var firstElement;
    var secondElement;
    console.log(this.props.player);
    if ((this.props.player.role == 'Batsman') || (this.props.player.role == 'All Rounder')) {
      if (this.props.player.batting_hand == 'Right Hand') {
        firstElement = RightHandBatsman(null)
      } else if ((this.props.player.batting_hand == 'Left Hand')) {
        firstElement = LeftHandBatsman(null)
      }
      if (this.props.player.bowling_arm == 'Right Arm') {
        secondElement = RightArmBowler(null)
      } else if (this.props.player.bowling_arm == 'Left Arm') {
        secondElement = LeftArmBowler(null)
      }
    } else if (this.props.player.role == 'Bowler') {
      if (this.props.player.batting_hand == 'Right Hand') {
        secondElement = RightHandBatsman(null)
      } else if ((this.props.player.batting_hand == 'Left Hand')) {
        secondElement = LeftHandBatsman(null)
      }
      if (this.props.player.bowling_arm == 'Right Arm') {
        firstElement = RightArmBowler(null)
      } else if (this.props.player.bowling_arm == 'Left Arm') {
        firstElement = LeftArmBowler(null)
      }
    } else if (this.props.player.role == 'Wicket-Keeper') {
      if (this.props.player.batting_hand == 'Right Hand') {
        secondElement = RightHandBatsman(null)
      } else if ((this.props.player.batting_hand == 'Left Hand')) {
        secondElement = LeftHandBatsman(null)
      }
      firstElement = WicketKeeper(null)
    }
    return (
      React.DOM.div({className: "player-stats"}, 
        firstElement, 
        secondElement
      )
    );
  }
});
var PlayerPic = React.createClass({displayName: 'PlayerPic',
  componentWillUpdate: function(nextProp) {
    if (nextProp.playerUrl != this.props.playerUrl) {
      $('body').backstretch(imagearray[imageCount%3]);
      imageCount++;
    }
  },
  componentDidMount: function() {
    $('body').backstretch(imagearray[imageCount%3]);
    imageCount++;
  },
  render: function() {
    return (
      React.DOM.div({className: "player-image"}, 
        React.DOM.a({href: this.props.playerUrl}, React.DOM.img({src: this.props.playerPic, className: "img-circle"}))
      )
    );
  }
});
var Bidder = React.createClass({displayName: 'Bidder',
  render: function() {
    var bidderClassName =  (this.props.lastClassName == 'current') ? 'bidder current' : 'bidder';
    return (
    React.DOM.div({className: bidderClassName}, 
      React.DOM.div({className: "left-section pull-left"}, 
        React.DOM.img({src: this.props.bidderDetail.Team.logo_url, className: "bidder-logo"})
      ), 
      React.DOM.div({className: "right-section pull-left"}, 
        React.DOM.div({className: "bidder-name"}, 
        React.DOM.a({href: this.props.bidderDetail.Team.url}, this.props.bidderDetail.Team.name)
        ), 
        React.DOM.div({className: "bidder-location"}, 
          React.DOM.span({className: "fa fa-inr"}), " ", this.props.bidderDetail.BidHistory.amount
        )
      ), 
      React.DOM.div({className: "clearfix"}), 
      React.DOM.div({className: "bidder-time"}, 
        React.DOM.span({className: "fa fa-flag"}), " ", moment(this.props.bidderDetail.BidHistory.created).format('h:mm:ss A')
      )
    )
    );
  }
});
var BidderWrapper = React.createClass({displayName: 'BidderWrapper',
  componentWillUpdate: function() {
    var node = this.getDOMNode();
    this.shouldScrollBottom = node.scrollTop + node.offsetHeight === node.scrollHeight;
  },
  componentDidUpdate: function() {
    if (this.shouldScrollBottom) {
      var node = this.getDOMNode();
      node.scrollTop = node.scrollHeight
    }
  },
  componentDidMount: function() {
    $('.bidder-wrapper').slimScroll({
        height: '210px',
        start: 'bottom'
    });
    var node = this.getDOMNode();
    node.scrollTop = node.scrollHeight;
  },
  render: function() {
    var length = this.props.biddingDetails.length;
    var bidders = this.props.biddingDetails.map(function(bidderDetail, index) {
      if (index + 1 == length) {
        var lastClass = 'current';
      }
      return (
        Bidder({bidderDetail: bidderDetail, key: bidderDetail.BidHistory.id, lastClassName: lastClass})
      )
    });
    return (
      React.DOM.div({className: "bidder-wrapper"}, 
        bidders
      )
    );
  }
})
var BiddingDetails = React.createClass({displayName: 'BiddingDetails',
  render: function() {
    return (
      React.DOM.div({className: "bidding-details"}, 
        React.DOM.h4({className: "heading"}, "Bidding Status - ", React.DOM.span({className: "bid-count"}, this.props.biddingDetails.length, " Bids")), 
        BidderWrapper({biddingDetails: this.props.biddingDetails})
      )
    );
  }
});
var AmountArea = React.createClass({displayName: 'AmountArea',
  render: function() {
    return (
      React.DOM.div({className: "row amount-row"}, 
        React.DOM.div({className: "col-md-3"}, 
          React.DOM.div({className: "text-center amount min"}, 
            React.DOM.div({className: "arrow-right"}), 
            React.DOM.div({className: "min-amount"}, 
              "Base Price ", React.DOM.span({className: "fa fa-inr"}), "  ", this.props.bidAmount.minimum_bid
            )
          )
        ), 
        React.DOM.div({className: "col-md-6"}, 
          React.DOM.div({className: "text-center player-name"}, this.props.bidAmount.player_details.name), 
          PlayerPic({playerPic: this.props.player.image_url, playerUrl: this.props.player.url})
        ), 
        React.DOM.div({className: "col-md-3"}, 
          React.DOM.div({className: "text-center amount current"}, 
            React.DOM.div({className: "arrow-left"}), 
            React.DOM.div({className: "current-amount current-bid"}, 
              React.DOM.span({className: "fa fa-inr"}), "  ", this.props.bidAmount.current_bid
            )
          )
        )
      )
    );
  }
});
var Heading = React.createClass({displayName: 'Heading',
  render: function() {
    return (
      React.DOM.div({className: "row"}, 
        React.DOM.div({className: "col-md-12 text-center heading"}, 
          React.DOM.img({className: "gca-logo", src: "img/gca-site-logo.png"}), 
          React.DOM.h3(null, "GPL Auction ", this.props.roundDetails.group_name, " ", React.DOM.span({className: "round-number"}, "Round ", this.props.roundDetails.round_number, " "), " "), 
          React.DOM.hr(null)
        )
      )
    )
  }
});


var Auction = React.createClass({displayName: 'Auction',
  getInitialState: function() {
    return ({hide:false});
  },
  componentWillReceiveProps: function(nextProps) {
    if (nextProps.player.url != this.props.player.url) {
      this.setState({hide: false});
    }
  },
  componentDidUpdate: function(prevProps) {
    if (prevProps.player.url != this.props.player.url) {
      var globalThis = this;
      $('.gif-wrapper img').attr('src',"img/gpl.gif");
      setTimeout(function() {
        globalThis.setState({hide:true});
      }, 5000);
    }
  },
  componentDidMount: function() {
    var globalThis = this;
    $('.gif-wrapper img').attr('src',"img/gpl.gif");
    setTimeout(function() {
      globalThis.setState({hide:true});
    }, 5000);
  },
  render: function() {
    var randomizeGif = RandomGif({hide: this.state.hide});
    return(
      React.DOM.div({className: "container auction"}, 
        Heading({roundDetails: this.props.roundDetails}), 
        AmountArea({bidAmount: this.props.bidAmount, player: this.props.player}), 
        React.DOM.div({className: "row details"}, 
          randomizeGif, 
          React.DOM.div({className: "col-md-4"}, 
            BiddingDetails({biddingDetails: this.props.biddingDetails})
          ), 
          React.DOM.div({className: "col-md-4 text-center"}, 
            PlayerPic({playerPic: this.props.player.image_url, playerUrl: this.props.player.url})
          ), 
          React.DOM.div({className: "col-md-3 col-md-offset-1"}, 
            PlayerStats({player: this.props.player})
          )
        ), 
        AuctionTeams({teams: this.props.teams})
        
      )
    )
  }
});
var RandomGif = React.createClass({displayName: 'RandomGif',
  render: function() {
    var hideOrShow = (this.props.hide) ? 'gif-wrapper col-md-12 text-center hide' :'gif-wrapper col-md-12 text-center';
    return (
      React.DOM.div({className: hideOrShow}, 
        React.DOM.img({className: "random-stuff", src: "img/gpl.gif"})
      )
    );
  }
});

var TeamTabElement = React.createClass({displayName: 'TeamTabElement',
  render:function() {
    return (
      React.DOM.div({className: "col-md-4"}, 
        React.DOM.div({className: "sold-player"}, 
          React.DOM.div({className: "pull-left left-section"}, 
            React.DOM.div({className: "player-pic"}
            )
          ), 
          React.DOM.div({className: "pull-left right-section"}, 
            React.DOM.div({className: "player-name"}, this.props.player.Player.name), 
            React.DOM.div({className: "selling-details"}, 
              React.DOM.div({className: "sold-label pull-left"}, "Price Sold"), 
              React.DOM.div({className: "selling-amount pull-left"}, React.DOM.span({className: "fa fa-inr"}), " ", this.props.player.buying_amount), 
              React.DOM.div({className: "clearfix"})
            )
          ), 
          React.DOM.div({className: "clearfix"})
        )
      )
    );
  }
});

var TeamTabContent = React.createClass({displayName: 'TeamTabContent',
  printResult: function() {
    var printIdNew = 'print-' + this.props.team.Team.id;
    $("#"+printIdNew).print({
        globalStyles: true,
        mediaPrint: false,
        stylesheet: null,
        noPrintSelector: ".no-print",
        iframe: true,
        append: null,
        prepend: null,
        manuallyCopyFormValues: true,
        deferred: $.Deferred()
    });
  },
  render: function() {
    var defaultTab = (this.props.classActive == 0) ? 'tab-pane active' : 'tab-pane';
    var printId = 'print-' + this.props.team.Team.id;
    var teamTabElements = this.props.team.Bid.map(function(player){
      return (
        TeamTabElement({player: player, key: player.id})
      );
    })
    return (
      React.DOM.div({role: "tabpanel", className: defaultTab, id: this.props.teamId}, 
        React.DOM.span({onClick: this.printResult, className: "fa fa-print pull-right fa-2x"}), 
        React.DOM.div({className: "print-result", id: printId}, 
          React.DOM.h2({className: "text-center"}, this.props.team.Team.name), 
          React.DOM.hr(null), 
          React.DOM.div({className: "row"}, 
            teamTabElements
          )
        )
      )
    );
  }
});
var TeamTab = React.createClass({displayName: 'TeamTab',
  render: function() {
    var defaultTab = (this.props.classActive == 0) ? 'active' : '';
    var hrefHash = '#' + this.props.teamId;
    return (
      React.DOM.li({role: "presentation", className: defaultTab}, React.DOM.a({href: hrefHash, 'aria-controls': "home", role: "tab", 'data-toggle': "tab"}, this.props.name))
    );
  }
});
var AuctionComplete = React.createClass({displayName: 'AuctionComplete',
    componentDidMount: function() {
      $('body').backstretch([
        "img/1.jpg"
      , "img/2.jpg"
      , "img/3.jpg"
      ], {duration: 3000, fade: 750});

    },
    render: function() {
      var teamsTabs = this.props.teams.map(function(team, index) {
        return (
          TeamTab({name: team.Team.name, key: team.Team.id, teamId: team.Team.id, classActive: index})    
        )
      });
      var teamsTabContents = this.props.teams.map(function(team, index) {
        return (
          TeamTabContent({team: team, key: team.Team.id, teamId: team.Team.id, classActive: index})    
        )
      });
      return (
        React.DOM.div({className: "final-results"}, 
          React.DOM.div({className: "container"}, 
            React.DOM.div({className: "row"}, 
              React.DOM.div({className: "col-md-12"}, 
                React.DOM.h1({className: "text-center"}, "Bidding is Over!")
              )
            ), 
            React.DOM.div({className: "row"}, 
              React.DOM.div({className: "col-md-12"}, 
                React.DOM.div({role: "tabpanel"}, 

                  React.DOM.ul({className: "nav nav-tabs", role: "tablist"}, 
                    teamsTabs
                  ), 

                  React.DOM.div({className: "tab-content"}, 
                    teamsTabContents
                  )

                )
              )
            )
          )
        )
      );
    }
});
var BiddingAboutToStart = React.createClass({displayName: 'BiddingAboutToStart',
  componentDidMount: function() {
    $('body').backstretch([
      "img/1.jpg"
    , "img/2.jpg"
    , "img/3.jpg"
    ], {duration: 3000, fade: 750});

    
    $('.timer').countdown('2015/02/09 17:00:00', function(event) {
      $(this).find('.days').text(event.offset.totalDays);
      $(this).find('.hours').text(event.offset.hours);
      $(this).find('.minutes').text(event.offset.minutes);
      $(this).find('.seconds').text(event.offset.seconds);
    });
  },
  render: function() {
    return (
      React.DOM.div({className: "about-to-start"}, 
        React.DOM.div({className: "container"}, 
          React.DOM.div({className: "row"}, 
            React.DOM.div({className: "col-md-12"}, 
              React.DOM.h1({className: "text-center"}, "Bidding will start in")
            )
          ), 
          React.DOM.div({className: "row"}, 
            React.DOM.div({className: "col-md-12 text-center"}, 
              React.DOM.div({className: "timer"}, 
                React.DOM.div({className: "days-wrapper"}, 
                  React.DOM.span({className: "days"}, "24"), " ", React.DOM.br(null), "days"
                ), 
                React.DOM.div({className: "hours-wrapper"}, 
                  React.DOM.span({className: "hours"}, "23"), " ", React.DOM.br(null), "hours"
                ), 
                React.DOM.div({className: "minutes-wrapper"}, 
                  React.DOM.span({className: "minutes"}, "41"), " ", React.DOM.br(null), "minutes"
                ), 
                React.DOM.div({className: "seconds-wrapper"}, 
                  React.DOM.span({className: "seconds"}, "25"), " ", React.DOM.br(null), "seconds"
                )
              )
            )
          )
        )
      )
    );
  }
});
var BiddingPaused = React.createClass({displayName: 'BiddingPaused',
  componentDidMount: function() {
    $('body').backstretch([
      "img/1.jpg"
    , "img/2.jpg"
    , "img/3.jpg"
    ], {duration: 3000, fade: 750});
  },
  render: function() {
    return (
      React.DOM.div({className: "about-to-start"}, 
        React.DOM.div({className: "container"}, 
          React.DOM.div({className: "row"}, 
            React.DOM.div({className: "col-md-12 break-text"}, 
              React.DOM.h1({className: "text-center"}, "We are taking a break.")
            )
          ), 
          React.DOM.div({className: "row"}, 
            React.DOM.div({className: "col-md-12 text-center coffee-cloud"}, 
              React.DOM.span({className: "fa fa-skyatlas fa-5x"})
            )
          ), 
          React.DOM.div({className: "row"}, 
            React.DOM.div({className: "col-md-12 text-center coffee"}, 
              React.DOM.span({className: "fa fa-coffee fa-5x"})
            )
          )
        )
      )
    );
  }
});
var AudioSection = React.createClass({displayName: 'AudioSection',
  componentDidMount: function() {
    audio = this.getDOMNode();
    audio.load();
    audio.play();
  },
  componentDidUpdate: function(prevProps) {
    if (this.props.audio != prevProps.audio){
      audio = this.getDOMNode();
      audio.load();
      audio.play();
    }
  },
  render: function() {
    console.log(this.props.audio);
    var audioFilePath = 'audio/' + this.props.audio;
    return (
      React.DOM.audio({controls: true, autoPlay: true}, 
        React.DOM.source({src: audioFilePath, type: "audio/mpeg"})
      )
    );
  }
});

var PageContentView = React.createClass({displayName: 'PageContentView',
  render: function() {
    var pageToRender;
    if (this.props.renderedPage == 'auction_page') {
      pageToRender = Auction({roundDetails: this.props.roundDetails, bidAmount: this.props.bidAmount, biddingDetails: this.props.biddingDetails, player: this.props.player, teams: this.props.teams})
    } else if (this.props.renderedPage == 'auction_complete') {
      pageToRender = AuctionComplete({teams: this.props.teams})
    } else if (this.props.renderedPage == 'player_sold') {
      pageToRender = PlayerSold({amount: this.props.amount, player: this.props.player, team: this.props.team})
    } else if (this.props.renderedPage == 'bidding_about_to_start') {
      pageToRender = BiddingAboutToStart(null)
    } else if (this.props.renderedPage == 'bidding_paused') {
      pageToRender = BiddingPaused(null)
    }
    return (
      React.DOM.div(null, 
        pageToRender
      )
    )
  }
});

var PageContent = React.createClass({displayName: 'PageContent',
  getInitialState: function() {
    return {roundDetails:[], bidAmount:[], player:[], biddingDetails:[], teams:[], display:'', team:[], amount: '', audio: ''};
  },
  loadDataFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      success: function(data) {
        console.log(data);
        if (data.status == 200) {
          var jsonData = data.data;

          var display = 'auction_page';
          if ("round_details" in jsonData) {
            var roundDetails = jsonData.round_details;
          }
          if ("bid_amount" in jsonData) {
            var bidAmount = jsonData.bid_amount;
          }
          if ("bidding_details" in jsonData) {
            var biddingDetails = jsonData.bidding_details;
          }
          if ("player" in jsonData) {
            var player = jsonData.player;
          }
          if ("teams" in jsonData) {
            var teams = jsonData.teams;
          }
          this.setState({
            display:'auction_page',
            roundDetails: roundDetails,
            bidAmount: bidAmount,
            biddingDetails: biddingDetails,
            player: player,
            teams: teams
          });
        } else if (data.status == 400) {
          var jsonData = data.data;
          if ("teams" in jsonData) {
            var teams = jsonData.teams;
          }
          var display = 'auction_complete';
          this.setState({display:display, teams:teams});
        } else if (data.status == 300){
          var jsonData = data.data;
          var display = 'player_sold';
          if ("team" in jsonData) {
            var team = jsonData.team;
          }
          if ("amount" in jsonData) {
            var amount = jsonData.amount;
          }
          if ("player" in jsonData) {
            var player = jsonData.player;
          }
          if ("audio" in jsonData) {
            var audio = jsonData.audio;
          }
          this.setState({
            display: display,
            team: team,
            player: player,
            amount: amount,
            audio: audio
          });
        } else if(data.status == 100) {
          var display = 'bidding_about_to_start';
          this.setState({display: display});
        } else if(data.status == 600) {
          var display = 'bidding_paused';
          this.setState({display: display});
        }
      }.bind(this),
      error: function(xhr) {
        console.error(xhr.responseText);
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadDataFromServer();
  },
  render: function() {
    return (
      PageContentView({
        roundDetails: this.state.roundDetails, 
        bidAmount: this.state.bidAmount, 
        biddingDetails: this.state.biddingDetails, 
        player: this.state.player, 
        teams: this.state.teams, 
        team: this.state.team, 
        amount: this.state.amount, 
        audio: this.state.audio, 
        renderedPage: this.state.display})
    )
  }
});

React.renderComponent (
  PageContent({url: "/goa-cricket/rounds/prepare_auction_data.json", pollInterval: 2000}),
  document.getElementById('reactId')
);