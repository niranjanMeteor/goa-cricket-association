$(function() {
  $('.carousel').carousel();
  $('.top-nav a').smoothScroll({
    offset: -50
  });
  var header = $(".header-wrapper");
  $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= $('.pre-header').height()) {
      header.addClass("fixed");
    } else {
      header.removeClass("fixed");
    }
  });
  $('.header').on('click', '.mobile-toggle', function(e) {
    e.preventDefault();
    $('ul.top-nav').toggleClass('open');
  })
});