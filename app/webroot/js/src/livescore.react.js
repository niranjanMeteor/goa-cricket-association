var React = require('react');

var socket = io.connect('http://livescore.goacricket.org');

socket.emit('join_live_banner');

var LiveBatsmanElement = React.createClass({
  render: function () {
    var class_name = "live-batsman";
    if (this.props.active == true) {
      class_name = "live-batsman current";
    }
    return (
      <div className={class_name}>
        <span className="live-batsman-name pull-left">{this.props.batsman.name} </span> 
        <span className="live-batsman-stats pull-right">{this.props.batsman.runs} </span>
      </div>
    ); 
  }
});
  
var LiveBatsmanWrapper = React.createClass({
  render: function () {
    if($.isEmptyObject(this.props.striker) == false) {
      var batsman_element = <LiveBatsmanElement batsman={this.props.striker} key={this.props.striker.id} active={true} /> 
    }
    return (
      <div className="col-md-6">
        <div className="live-batsman-wrapper">
         {batsman_element}
         <LiveBatsmanElement batsman={this.props.non_striker} key={this.props.non_striker.id} active={false} />
        </div>
      </div>
    ); 
  }
});
  
var LiveBowlerElement = React.createClass({
  render: function () {
    var class_name = "live-bowler";
    if (this.props.active == true) {
      class_name = "live-bowler current";
    }
    return (
      <div className={class_name}>
        <span className="live-bowler-name pull-left">{this.props.bowler.name} </span> 
         <span className="live-bowler-stats pull-right">{this.props.bowler.overs} - {this.props.bowler.dot_balls} - {this.props.bowler.runs} - {this.props.bowler.wickets} </span>
      </div>
    ); 
  }
});
  
var LiveBowlerWrapper = React.createClass({
  render: function () {
    bowlers = this.props.bowlers.map(function (bowler, index) {
      if (index == 0) {
        return (
          <LiveBowlerElement bowler={bowler} key={bowler.id} active={true} />
        );
      } else {
        return (
          <LiveBowlerElement bowler={bowler} key={bowler.id} active={false} />
        );
      }
    });
    return (
      <div className="col-md-6">
        <div className="live-bowler-wrapper">
          {bowlers}
        </div>
      </div>
    ); 
  }
});
  

var LiveTeamStats = React.createClass({
  render: function () {
    var class_name = "live-team-stats target";
    if (this.props.active == true) {
      class_name = "live-team-stats";
    }
    if (this.props.visible == false) {
      class_name= "live-team-stats hide"
    }
    if (parseInt(this.props.teamData.overs) > 1) {
      var overs_string = this.props.teamData.overs + " Overs";
    } else {
      overs_string = this.props.teamData.overs + " Over";
    }
    return (
      <div className="col-md-6">
        <div className={class_name}>
          <div className="live-team-runs">{this.props.teamData.runs}/{this.props.teamData.wickets}</div>
          <div className="live-team-overs">{overs_string}</div>
        </div>
      </div>
    ); 
  }
});
   

var LiveBanner = React.createClass({
  render: function () {
    return (
      <div className="live-banner">
        <span className="live-text">Live</span>
        <div className="arrow-down"></div>
      </div>
    ); 
  }
});

var TeamTwoBatting = React.createClass({
  render: function () {
    return (
      <div className="row">
        <div className="col-md-6 col-sm-6 col-xs-6">
          <div className="row">
            <LiveBowlerWrapper bowlers={this.props.battingTeamData.bowlers} />
            <LiveTeamStats active={false} teamData={this.props.bowlingTeamData} />
          </div>
        </div>
        <div className="col-md-6 col-sm-6 col-xs-6">
          <div className="row">
            <LiveTeamStats active={true} teamData={this.props.battingTeamData} />
            <LiveBatsmanWrapper striker={this.props.battingTeamData.strike_batsman} non_striker={this.props.battingTeamData.non_strike_batsman} />
          </div>
        </div>
      </div>
    ); 
  }
});

var TeamOneBatting = React.createClass({
  render: function () {
    return (
      <div className="row">
        <div className="col-md-6 col-sm-6 col-xs-6">
          <div className="row">
            <LiveBatsmanWrapper striker={this.props.battingTeamData.strike_batsman} non_striker={this.props.battingTeamData.non_strike_batsman} />
            <LiveTeamStats active={true}  teamData={this.props.battingTeamData} />
          </div>
        </div>
        <div className="col-md-6 col-sm-6 col-xs-6">
          <div className="row">
            <LiveTeamStats active={false} visible={false} teamData={this.props.bowlingTeamData}/>
            <LiveBowlerWrapper bowlers={this.props.battingTeamData.bowlers} />
          </div>
        </div>
      </div>
    ); 
  }
});

var TimeElement = React.createClass({
  componentDidMount: function() {
    $('.timer').countdown(moment(this.props.time).format("YYYY/MM/DD HH:mm:ss"), function(event) {
      $(this).find('.days').text(event.offset.totalDays);
      $(this).find('.hours').text(event.offset.hours);
      $(this).find('.minutes').text(event.offset.minutes);
      $(this).find('.seconds').text(event.offset.seconds);
    }).on('finish.countdown', function(event) {
      $(this).html('<div class="finished">Match will start in a while...</div>');
    });
  },
  render: function() {
    return (
      <div className="about-to-start">
        <div className="container">
          <div className="row">
            <div className="col-md-12 text-center">
              <div className="timer">
                <div className="days-wrapper">
                  <span className="days">24</span> <br />days
                </div>
                <div className="hours-wrapper">
                  <span className="hours">23</span> <br />hours
                </div>
                <div className="minutes-wrapper">
                  <span className="minutes">41</span> <br />minutes
                </div>
                <div className="seconds-wrapper">
                  <span className="seconds">25</span> <br />seconds
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

var PageContent = React.createClass({
  getInitialState: function() {
    socket.on('mini_score', this.updateScore);
    return {matchData:{}};
  },
  updateScore: function(data) {
    if (typeof data == 'string') {
      var parsedData = JSON.parse(data);
      if (parsedData.status == 200) {
        var jsonData = parsedData.data;
        if ("match" in jsonData) {
          var matchData = jsonData.match;
        }
        this.setState({matchData:matchData});
      }
    }
  },
  componentDidMount: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      success: function(data) {
        if (data.status == 200) {
          var jsonData = data.data;
          if ("match" in jsonData) {
            var matchData = jsonData.match;
          }
          this.setState({matchData:matchData});
        }
      }.bind(this),
      error: function(xhr) {
        console.error(xhr.responseText);
      }.bind(this)
    });
  },
  render: function() {
    if ($.isEmptyObject(this.state.matchData) == false ) {
      if(this.state.matchData.in_progress == true) {
        var banner = <LiveBanner />;
        if (this.state.matchData.team_one.is_batting == false) {
          var team_one_class = "live-bowling-team";
          var component_to_display = <TeamTwoBatting battingTeamData={this.state.matchData.team_two} bowlingTeamData={this.state.matchData.team_one} />
        } else if  (this.state.matchData.team_two.is_batting == false) {
          var team_two_class = "live-bowling-team";
          var component_to_display = <TeamOneBatting battingTeamData={this.state.matchData.team_one} bowlingTeamData={this.state.matchData.team_two} />; 
        }
        var liveLink = "http://livescore.goacricket.org/matches/live_scores/" + this.state.matchData.id;
        return (
          <div className="live-score-wrapper">
            <img src="img/zooter_small_logo.png" className="zooter-logo" />
            <div className="container text-center ">
              {banner}
              <div className="match-title">{this.state.matchData.series_name} - {this.state.matchData.type} Match</div>
              <div className="row">
                <div className={"col-md-5 col-sm-5 col-xs-5 text-right live-team-name " + team_one_class}>{this.state.matchData.team_one.name}</div>
                <div className="col-md-2 col-sm-2 col-xs-2 live-vs"><span className="vs">vs</span></div>
                <div className={"col-md-5 col-sm-5 col-xs-5 text-left live-team-name " + team_two_class}>{this.state.matchData.team_two.name}</div>
              </div>
              <hr className="live-divider" />
              {component_to_display}
              <hr className="live-divider" />
              <div className="text-center optional-text">
                {this.state.matchData.team_two.target}
              </div>

              <div className="live-actions">
                <div className="row">
                  <div className="col-md-2 col-sm-2 col-md-offset-3 col-sm-offset-3"><a href={liveLink} className="btn btn-danger btn-block" target="_blank">Live</a></div>
                  <div className="col-md-2 col-sm-2"><a href="gpl_stats" className="btn btn-primary btn-block">Stats</a></div>
                  <div className="col-md-2 col-sm-2"><a href="#fixtures" className="btn btn-primary btn-block">Fixtures</a></div>
                </div>
              </div>
            </div>
          </div>
        );
      } else {
        return (
          <div className="live-score-wrapper">
            <img src="img/zooter_small_logo.png" className="zooter-logo" />
            <div className="container text-center ">
                <div className="match-title">{this.state.matchData.series_name} - {this.state.matchData.type} Match</div>
                <div className="row">
                  <div className={"col-md-5 col-sm-5 col-xs-5 text-right live-team-name "}>{this.state.matchData.team_one.name}</div>
                  <div className="col-md-2 col-sm-2 col-xs-2 live-vs"><span className="vs">vs</span></div>
                  <div className={"col-md-5 col-sm-5 col-xs-5 text-left live-team-name "}>{this.state.matchData.team_two.name}</div>
                </div>
                <hr className="live-divider" />

                <TimeElement time={this.state.matchData.start_date_time} />
                
                <hr className="live-divider" />
                <div className="text-center optional-text">
                  
                </div>
              </div>
          </div>
        );
      }
    } else {
      return (
        <div>
        </div>
      );
    }
  }
});

React.render(
  <PageContent url="match_inning_scores/get_mini_scorecard" />,
  document.getElementById('live-score-wrapper')
);