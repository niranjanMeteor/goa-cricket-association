/**
 * @jsx React.DOM
 */

var imagearray = ["img/1.jpg", "img/2.jpg", "img/3.jpg"];
var imageCount = 0;
var PlayerSold = React.createClass({
  render: function() {
    return (
      <div className="container sold">
        <div className="row">
          <div className="col-md-12 text-center">
            <h2 className="heading">Player Sold</h2>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="col-md-12 text-center sold-player-name">
            <h3>{this.props.player.name}</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 text-center">
            <PlayerPic playerUrl={this.props.player.url} playerPic={this.props.player.image_url} />
          </div>
        </div>
        <div className="row">
          <div className="col-md-4"></div>
          <div className="col-md-4 text-center">
            <div className="sold-details">
              <h3 className="sold-to-team">{this.props.team.name}</h3>
              <p><span className="fa fa-inr"></span> {this.props.amount}</p>
            </div>
          </div>
          <div className="col-md-4"></div>
        </div>
        <div className="row">
          <div className="col-md-12 text-center congrats">
            <h2>Congratulations, {this.props.player.name}, GCA wishes you a pleasant journey in GPL-2015. All The Best!</h2>
          </div>
        </div>
      </div>
    );
  }
});

var AuctionTeam = React.createClass({
  render: function() {
    if (this.props.team.current == true) {
      var activeTeamClass = (this.props.team.current == true) ? 'team active' : 'team';
    } else {
      var activeTeamClass = (this.props.team.is_eligible_for_bid == true)? 'team': 'team not-eligible';      
    }
    var team_data_display = this.props.team.players_bought_count + " player bought";
    if (this.props.team.players_bought_count > 1 ) {
      team_data_display =  this.props.team.players_bought_count + " players bought";
    }
    return (
      <div className="col-md-3">
        <div className={activeTeamClass}>
          <span className="fa fa-flag"></span>
          <div className="team-details">
            <div className="pull-left left-section">
              <a href={this.props.team.url}><img src={this.props.team.logo_url} className="team-logo" /></a>
            </div>
            <div className="pull-left right-section">
              <div className="team-name">{this.props.team.name}</div>
              <div className="team-location">
               {team_data_display}
              </div>
            </div>
            <div className="clearfix"></div>
          </div>
          <div className="money-details">
            <div className="money-element">
              <div className="text pull-left">Total</div>
              <div className="value pull-right"><span className="fa fa-inr"></span> {this.props.team.total}</div>
              <div className="clearfix"></div>
            </div>
            <div className="money-element">
              <div className="text pull-left">Min Base Amount</div>
              <div className="value pull-right"><span className="fa fa-inr"></span> {this.props.team.required}</div>
              <div className="clearfix"></div>
            </div>
            <div className="money-element">
              <div className="text pull-left">Available</div>
              <div className="value pull-right"><span className="fa fa-inr"></span> {this.props.team.available}</div>
              <div className="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
var AuctionTeams = React.createClass({
  render: function() {
    var auctionTeams = this.props.teams.map(function(team) {
      return (
        <AuctionTeam team={team} key={team.id} />
      )
    })
    return (
      <div className="row auction-teams">
        {auctionTeams}
      </div>
    );
  }
});
var PlayerStatElement = React.createClass({
  render: function() {
    return (
      <div className="stat-element">
        <div className="stat-text pull-left">{this.props.text}</div>
        <div className="stat-data pull-right">{this.props.data}</div>
        <div className="clearfix"></div>
      </div>
    );
  }
});
var WicketKeeper = React.createClass({
  render: function() {
    return (
      <div className="wicketkeeper stat-element">
        <div className="pull-left left-section">
          <img src="img/keeper.png" />
        </div>
        <div className="pull-left right-section">
          WicketKeeper
        </div>
        <div className="clearfix"></div>
      </div>
    );
  }
});
var LeftArmBowler = React.createClass({
  render: function() {
    return (
      <div className="left-arm-bowl stat-element">
        <div className="pull-left left-section">
          <img src="img/left-bowler.png" />
        </div>
        <div className="pull-left right-section">
          Left Arm Bowler
        </div>
        <div className="clearfix"></div>
      </div>
    );
  }
});
var RightArmBowler = React.createClass({
  render: function() {
    return (
      <div className="right-arm-bowl stat-element">
        <div className="pull-left left-section">
          <img src="img/right-bowler.png" />
        </div>
        <div className="pull-left right-section">
          Right Arm Bowler
        </div>
        <div className="clearfix"></div>
      </div>
    );
  }
});
var RightHandBatsman = React.createClass({
  render: function() {
    return (
      <div className="right-hand-bat stat-element">
        <div className="pull-left left-section">
          <img src="img/righty-bat.png" />
        </div>
        <div className="pull-left right-section">
          Right Hand Batsman
        </div>
        <div className="clearfix"></div>
      </div>
    );
  }
});
var LeftHandBatsman = React.createClass({
  render: function() {
    return (
      <div className="left-hand-bat stat-element">
        <div className="pull-left left-section">
          <img src="img/left-bat.png" />
        </div>
        <div className="pull-left right-section">
          Left Hand Batsman
        </div>
        <div className="clearfix"></div>
      </div>
    );
  }
});
var PlayerStats = React.createClass({
  render: function() {
    var firstElement;
    var secondElement;
    console.log(this.props.player);
    if ((this.props.player.role == 'Batsman') || (this.props.player.role == 'All Rounder')) {
      if (this.props.player.batting_hand == 'Right Hand') {
        firstElement = <RightHandBatsman />
      } else if ((this.props.player.batting_hand == 'Left Hand')) {
        firstElement = <LeftHandBatsman />
      }
      if (this.props.player.bowling_arm == 'Right Arm') {
        secondElement = <RightArmBowler />
      } else if (this.props.player.bowling_arm == 'Left Arm') {
        secondElement = <LeftArmBowler />
      }
    } else if (this.props.player.role == 'Bowler') {
      if (this.props.player.batting_hand == 'Right Hand') {
        secondElement = <RightHandBatsman />
      } else if ((this.props.player.batting_hand == 'Left Hand')) {
        secondElement = <LeftHandBatsman />
      }
      if (this.props.player.bowling_arm == 'Right Arm') {
        firstElement = <RightArmBowler />
      } else if (this.props.player.bowling_arm == 'Left Arm') {
        firstElement = <LeftArmBowler />
      }
    } else if (this.props.player.role == 'Wicket-Keeper') {
      if (this.props.player.batting_hand == 'Right Hand') {
        secondElement = <RightHandBatsman />
      } else if ((this.props.player.batting_hand == 'Left Hand')) {
        secondElement = <LeftHandBatsman />
      }
      firstElement = <WicketKeeper />
    }
    return (
      <div className="player-stats">
        {firstElement}
        {secondElement}
      </div>
    );
  }
});
var PlayerPic = React.createClass({
  componentWillUpdate: function(nextProp) {
    if (nextProp.playerUrl != this.props.playerUrl) {
      $('body').backstretch(imagearray[imageCount%3]);
      imageCount++;
    }
  },
  componentDidMount: function() {
    $('body').backstretch(imagearray[imageCount%3]);
    imageCount++;
  },
  render: function() {
    return (
      <div className="player-image">
        <a href={this.props.playerUrl}><img src={this.props.playerPic} className="img-circle" /></a>
      </div>
    );
  }
});
var Bidder = React.createClass({
  render: function() {
    var bidderClassName =  (this.props.lastClassName == 'current') ? 'bidder current' : 'bidder';
    return (
    <div className={bidderClassName}>
      <div className="left-section pull-left">
        <img src={this.props.bidderDetail.Team.logo_url} className="bidder-logo" />
      </div>
      <div className="right-section pull-left">
        <div className="bidder-name">
        <a href={this.props.bidderDetail.Team.url}>{this.props.bidderDetail.Team.name}</a>
        </div>
        <div className="bidder-location">
          <span className="fa fa-inr"></span> {this.props.bidderDetail.BidHistory.amount}
        </div>
      </div>
      <div className="clearfix"></div>
      <div className="bidder-time">
        <span className="fa fa-flag"></span> {moment(this.props.bidderDetail.BidHistory.created).format('h:mm:ss A')}
      </div>
    </div>
    );
  }
});
var BidderWrapper = React.createClass({
  componentWillUpdate: function() {
    var node = this.getDOMNode();
    this.shouldScrollBottom = node.scrollTop + node.offsetHeight === node.scrollHeight;
  },
  componentDidUpdate: function() {
    if (this.shouldScrollBottom) {
      var node = this.getDOMNode();
      node.scrollTop = node.scrollHeight
    }
  },
  componentDidMount: function() {
    $('.bidder-wrapper').slimScroll({
        height: '210px',
        start: 'bottom'
    });
    var node = this.getDOMNode();
    node.scrollTop = node.scrollHeight;
  },
  render: function() {
    var length = this.props.biddingDetails.length;
    var bidders = this.props.biddingDetails.map(function(bidderDetail, index) {
      if (index + 1 == length) {
        var lastClass = 'current';
      }
      return (
        <Bidder bidderDetail={bidderDetail} key={bidderDetail.BidHistory.id} lastClassName={lastClass} />
      )
    });
    return (
      <div className="bidder-wrapper">
        {bidders}
      </div>
    );
  }
})
var BiddingDetails = React.createClass({
  render: function() {
    return (
      <div className="bidding-details">
        <h4 className="heading">Bidding Status - <span className="bid-count">{this.props.biddingDetails.length} Bids</span></h4>
        <BidderWrapper biddingDetails={this.props.biddingDetails} />
      </div>
    );
  }
});
var AmountArea = React.createClass({
  render: function() {
    return (
      <div className="row amount-row">
        <div className="col-md-3">
          <div className="text-center amount min">
            <div className="arrow-right"></div>
            <div className="min-amount">
              Base Price&nbsp;<span className="fa fa-inr"></span>&nbsp; {this.props.bidAmount.minimum_bid}
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="text-center player-name">{this.props.bidAmount.player_details.name}</div>
          <PlayerPic playerPic={this.props.player.image_url} playerUrl={this.props.player.url} />
        </div>
        <div className="col-md-3">
          <div className="text-center amount current">
            <div className="arrow-left"></div>
            <div className="current-amount current-bid">
              <span className="fa fa-inr"></span>&nbsp; {this.props.bidAmount.current_bid}
            </div>
          </div>
        </div>
      </div>
    );
  }
});
var Heading = React.createClass({
  render: function() {
    return (
      <div className="row">
        <div className="col-md-12 text-center heading">
          <img className="gca-logo" src="img/gca-site-logo.png" />
          <h3>GPL Auction {this.props.roundDetails.group_name} <span className="round-number">Round {this.props.roundDetails.round_number} </span> </h3>
          <hr />
        </div>
      </div>
    )
  }
});


var Auction = React.createClass({
  getInitialState: function() {
    return ({hide:false});
  },
  componentWillReceiveProps: function(nextProps) {
    if (nextProps.player.url != this.props.player.url) {
      this.setState({hide: false});
    }
  },
  componentDidUpdate: function(prevProps) {
    if (prevProps.player.url != this.props.player.url) {
      var globalThis = this;
      $('.gif-wrapper img').attr('src',"img/gpl.gif");
      setTimeout(function() {
        globalThis.setState({hide:true});
      }, 5000);
    }
  },
  componentDidMount: function() {
    var globalThis = this;
    $('.gif-wrapper img').attr('src',"img/gpl.gif");
    setTimeout(function() {
      globalThis.setState({hide:true});
    }, 5000);
  },
  render: function() {
    var randomizeGif = <RandomGif hide={this.state.hide}  />;
    return(
      <div className="container auction">
        <Heading roundDetails={this.props.roundDetails} />
        <AmountArea bidAmount={this.props.bidAmount} player={this.props.player} />
        <div className="row details">
          {randomizeGif}
          <div className="col-md-4">
            <BiddingDetails biddingDetails={this.props.biddingDetails} />
          </div>
          <div className="col-md-4 text-center">
            <PlayerPic playerPic={this.props.player.image_url} playerUrl={this.props.player.url} />
          </div>
          <div className="col-md-3 col-md-offset-1">
            <PlayerStats player={this.props.player} />
          </div>
        </div>
        <AuctionTeams teams={this.props.teams} />
        
      </div>
    )
  }
});
var RandomGif = React.createClass({
  render: function() {
    var hideOrShow = (this.props.hide) ? 'gif-wrapper col-md-12 text-center hide' :'gif-wrapper col-md-12 text-center';
    return (
      <div className={hideOrShow}>
        <img className="random-stuff" src="img/gpl.gif" />
      </div>
    );
  }
});

var TeamTabElement = React.createClass({
  render:function() {
    return (
      <div className="col-md-4">
        <div className="sold-player">
          <div className="pull-left left-section">
            <div className="player-pic">
            </div>
          </div>
          <div className="pull-left right-section">
            <div className="player-name">{this.props.player.Player.name}</div>
            <div className="selling-details">
              <div className="sold-label pull-left">Price Sold</div>
              <div className="selling-amount pull-left"><span className="fa fa-inr"></span> {this.props.player.buying_amount}</div>
              <div className="clearfix"></div>
            </div>
          </div>
          <div className="clearfix"></div>
        </div>
      </div>
    );
  }
});

var TeamTabContent = React.createClass({
  printResult: function() {
    var printIdNew = 'print-' + this.props.team.Team.id;
    $("#"+printIdNew).print({
        globalStyles: true,
        mediaPrint: false,
        stylesheet: null,
        noPrintSelector: ".no-print",
        iframe: true,
        append: null,
        prepend: null,
        manuallyCopyFormValues: true,
        deferred: $.Deferred()
    });
  },
  render: function() {
    var defaultTab = (this.props.classActive == 0) ? 'tab-pane active' : 'tab-pane';
    var printId = 'print-' + this.props.team.Team.id;
    var teamTabElements = this.props.team.Bid.map(function(player){
      return (
        <TeamTabElement player={player} key={player.id}/>
      );
    })
    return (
      <div role="tabpanel" className={defaultTab} id={this.props.teamId}>
        <span onClick={this.printResult} className="fa fa-print pull-right fa-2x"></span>
        <div className="print-result" id={printId}>
          <h2 className="text-center">{this.props.team.Team.name}</h2>
          <hr />
          <div className="row">
            {teamTabElements}
          </div>
        </div>
      </div>
    );
  }
});
var TeamTab = React.createClass({
  render: function() {
    var defaultTab = (this.props.classActive == 0) ? 'active' : '';
    var hrefHash = '#' + this.props.teamId;
    return (
      <li role="presentation" className={defaultTab}><a href={hrefHash} aria-controls="home" role="tab" data-toggle="tab">{this.props.name}</a></li>
    );
  }
});
var AuctionComplete = React.createClass({
    componentDidMount: function() {
      $('body').backstretch([
        "img/1.jpg"
      , "img/2.jpg"
      , "img/3.jpg"
      ], {duration: 3000, fade: 750});

    },
    render: function() {
      var teamsTabs = this.props.teams.map(function(team, index) {
        return (
          <TeamTab name={team.Team.name} key={team.Team.id} teamId={team.Team.id} classActive={index} />    
        )
      });
      var teamsTabContents = this.props.teams.map(function(team, index) {
        return (
          <TeamTabContent team={team} key={team.Team.id} teamId={team.Team.id} classActive={index} />    
        )
      });
      return (
        <div className="final-results">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <h1 className="text-center">Bidding is Over!</h1>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div role="tabpanel">

                  <ul className="nav nav-tabs" role="tablist">
                    {teamsTabs}
                  </ul>

                  <div className="tab-content">
                    {teamsTabContents}
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
});
var BiddingAboutToStart = React.createClass({
  componentDidMount: function() {
    $('body').backstretch([
      "img/1.jpg"
    , "img/2.jpg"
    , "img/3.jpg"
    ], {duration: 3000, fade: 750});

    
    $('.timer').countdown('2015/02/09 17:00:00', function(event) {
      $(this).find('.days').text(event.offset.totalDays);
      $(this).find('.hours').text(event.offset.hours);
      $(this).find('.minutes').text(event.offset.minutes);
      $(this).find('.seconds').text(event.offset.seconds);
    });
  },
  render: function() {
    return (
      <div className="about-to-start">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h1 className="text-center">Bidding will start in</h1>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 text-center">
              <div className="timer">
                <div className="days-wrapper">
                  <span className="days">24</span> <br />days
                </div>
                <div className="hours-wrapper">
                  <span className="hours">23</span> <br />hours
                </div>
                <div className="minutes-wrapper">
                  <span className="minutes">41</span> <br />minutes
                </div>
                <div className="seconds-wrapper">
                  <span className="seconds">25</span> <br />seconds
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
var BiddingPaused = React.createClass({
  componentDidMount: function() {
    $('body').backstretch([
      "img/1.jpg"
    , "img/2.jpg"
    , "img/3.jpg"
    ], {duration: 3000, fade: 750});
  },
  render: function() {
    return (
      <div className="about-to-start">
        <div className="container">
          <div className="row">
            <div className="col-md-12 break-text">
              <h1 className="text-center">We are taking a break.</h1>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 text-center coffee-cloud">
              <span className="fa fa-skyatlas fa-5x"></span>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 text-center coffee">
              <span className="fa fa-coffee fa-5x"></span>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
var AudioSection = React.createClass({
  componentDidMount: function() {
    audio = this.getDOMNode();
    audio.load();
    audio.play();
  },
  componentDidUpdate: function(prevProps) {
    if (this.props.audio != prevProps.audio){
      audio = this.getDOMNode();
      audio.load();
      audio.play();
    }
  },
  render: function() {
    console.log(this.props.audio);
    var audioFilePath = 'audio/' + this.props.audio;
    return (
      <audio controls autoPlay>
        <source src={audioFilePath} type="audio/mpeg" />
      </audio>
    );
  }
});

var PageContentView = React.createClass({
  render: function() {
    var pageToRender;
    if (this.props.renderedPage == 'auction_page') {
      pageToRender = <Auction roundDetails={this.props.roundDetails} bidAmount={this.props.bidAmount} biddingDetails={this.props.biddingDetails} player={this.props.player} teams={this.props.teams} />
    } else if (this.props.renderedPage == 'auction_complete') {
      pageToRender = <AuctionComplete teams={this.props.teams} />
    } else if (this.props.renderedPage == 'player_sold') {
      pageToRender = <PlayerSold amount={this.props.amount} player={this.props.player} team={this.props.team} />
    } else if (this.props.renderedPage == 'bidding_about_to_start') {
      pageToRender = <BiddingAboutToStart />
    } else if (this.props.renderedPage == 'bidding_paused') {
      pageToRender = <BiddingPaused />
    }
    return (
      <div>
        {pageToRender}
      </div>
    )
  }
});

var PageContent = React.createClass({
  getInitialState: function() {
    return {roundDetails:[], bidAmount:[], player:[], biddingDetails:[], teams:[], display:'', team:[], amount: '', audio: ''};
  },
  loadDataFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      success: function(data) {
        console.log(data);
        if (data.status == 200) {
          var jsonData = data.data;

          var display = 'auction_page';
          if ("round_details" in jsonData) {
            var roundDetails = jsonData.round_details;
          }
          if ("bid_amount" in jsonData) {
            var bidAmount = jsonData.bid_amount;
          }
          if ("bidding_details" in jsonData) {
            var biddingDetails = jsonData.bidding_details;
          }
          if ("player" in jsonData) {
            var player = jsonData.player;
          }
          if ("teams" in jsonData) {
            var teams = jsonData.teams;
          }
          this.setState({
            display:'auction_page',
            roundDetails: roundDetails,
            bidAmount: bidAmount,
            biddingDetails: biddingDetails,
            player: player,
            teams: teams
          });
        } else if (data.status == 400) {
          var jsonData = data.data;
          if ("teams" in jsonData) {
            var teams = jsonData.teams;
          }
          var display = 'auction_complete';
          this.setState({display:display, teams:teams});
        } else if (data.status == 300){
          var jsonData = data.data;
          var display = 'player_sold';
          if ("team" in jsonData) {
            var team = jsonData.team;
          }
          if ("amount" in jsonData) {
            var amount = jsonData.amount;
          }
          if ("player" in jsonData) {
            var player = jsonData.player;
          }
          if ("audio" in jsonData) {
            var audio = jsonData.audio;
          }
          this.setState({
            display: display,
            team: team,
            player: player,
            amount: amount,
            audio: audio
          });
        } else if(data.status == 100) {
          var display = 'bidding_about_to_start';
          this.setState({display: display});
        } else if(data.status == 600) {
          var display = 'bidding_paused';
          this.setState({display: display});
        }
      }.bind(this),
      error: function(xhr) {
        console.error(xhr.responseText);
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadDataFromServer();
  },
  render: function() {
    return (
      <PageContentView 
        roundDetails={this.state.roundDetails}
        bidAmount={this.state.bidAmount}
        biddingDetails={this.state.biddingDetails}
        player={this.state.player}
        teams={this.state.teams}
        team={this.state.team}
        amount={this.state.amount}
        audio={this.state.audio}
        renderedPage={this.state.display} />
    )
  }
});

React.renderComponent (
  <PageContent url='/goa-cricket/rounds/prepare_auction_data.json' pollInterval={2000} />,
  document.getElementById('reactId')
);