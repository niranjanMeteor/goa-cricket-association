$(function() {
  $('.selectpicker').selectpicker();

  function populateBatsmenSelect(batsmen) {
    $.each(batsmen, function(i, value) {
      $('#MatchBallScoreStrikerId').append($('<option>').text(value).attr('value', i));
        $('#MatchBallScoreNonStrikerId').append($('<option>').text(value).attr('value', i));
    });
  }

  function populateBowlersSelect(bowlers) {
    $.each(bowlers, function(i, value) {
        $('#MatchBallScoreBowlerId').append($('<option>').text(value).attr('value', i));
    });
  }

  $('#MatchBallScoreInningsId').change(function() {
    var inningsNumber = $('#MatchBallScoreInningsId option:selected').val();
    var matchId = $('#MatchBallScoreMatchId option:selected').val();
    var postUrl = WEBROOT + 'matches/getBatsmenAndBowlers/' + matchId + '/' + inningsNumber;
    $.post(postUrl, function(data) {
      if (data.s) {
        populateBatsmenSelect(data.batsmen);
        populateBowlersSelect(data.bowlers);
      }
    }, 'json')
  });
  $('#MatchBallScoreMatchId').change(function() {
    $('#MatchBallScoreInningsId').val(0);
  });
  $('.chosen-select').chosen();
  $('.match-level-select').change(function() {
    var level = $('.match-level-select option:selected').val();
    var levelText = $('.match-level-select option:selected').text();
    if (levelText == 'ODI') {
      $('#overs-per-inning').val(50);
    } else if (levelText == 'Twenty20') {
      $('#overs-per-inning').val(20);
    }
  })
});