$(function() {
  $('.page-header').on('click', '.menu-toggler', function(e) {
    e.preventDefault();
    console.log('clicked');
    $('#wrapper').toggleClass('toggled');
    $('.menu-toggler .caret').toggleClass('sidebar-menu-open');
  });

  $('#wrapper').on('click', '.page-header-menu .mobile-nav-tabs', function(e) {
    e.preventDefault();
    var index = $(this).index();
    $('.mobile-dash-tab').hide();
    $(".mobile-dash-tab:eq(" + index + ")").show();
  });

});
