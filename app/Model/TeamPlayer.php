<?php
App::uses('AppModel', 'Model');
App::uses('TeamPlayerStatus', 'Lib/Enum');
/**
 * TeamPlayer Model
 *
 * @property Team $Team
 * @property Player $Player
 */
class TeamPlayer extends AppModel {

	public $validate = array(
	  'team_id' => array(
	    'unique' => array(
	      'rule' => array('checkUnique', array('team_id', 'player_id'), false), 
	      'message' => 'Player already exists in the team'
	    )
	  )
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => 'team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Player' => array(
			'className' => 'Player',
			'foreignKey' => 'player_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public function getPlayerList($teamId) {
		return $this->find('all', array(
			'conditions' => array(
				'TeamPlayer.team_id' => $teamId,
				'TeamPlayer.status' => TeamPlayerStatus::ACTIVE
			),
			'contain' => array(
				'Player'
			)
		));
	}
}
