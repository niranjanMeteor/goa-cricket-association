<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
  public $recursive = -1;
  public $actsAs = array('Containable');

  public function _uploadFile($imageName, $imageTempName, $folder) {
    $imageUrl = null;
    $uploadDir = WWW_ROOT . 'files' . DS . $folder;
    $imageName = preg_replace("/[^A-Z0-9._-]/i", "_", $imageName); //Ensure safe file name 
    $uuId = uniqid();
    $finalImageName = $uuId . '_' . $imageName; // Ensure unique file name
    $imageNameWithPath = $uploadDir . DS . $finalImageName;
    if( ! file_exists($uploadDir)) {
      mkdir($uploadDir, 0777, true);
    }
    $success = move_uploaded_file($imageTempName, $imageNameWithPath);
    if ($success) {
      $imageUrl = '/files/' . $folder . '/' . $finalImageName;
    }
    return $imageUrl;
  }
  public function checkUnique($ignoredData, $fields, $or = true) {
    return $this->isUnique($fields, $or);
  }
}
