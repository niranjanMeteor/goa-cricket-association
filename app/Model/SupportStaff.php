<?php
App::uses('AppModel', 'Model');
/**
 * SupportStaff Model
 *
 * @property TeamSupportStaff $TeamSupportStaff
 */
class SupportStaff extends AppModel {

  public $virtualFields = array(
    'name' => 'CONCAT(SupportStaff.first_name, " ", SupportStaff.last_name)'
  );
  var $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'role' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'first_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'last_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'TeamSupportStaff' => array(
			'className' => 'TeamSupportStaff',
			'foreignKey' => 'support_staff_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	public $actsAs = array(
    'Upload.Upload' => array(
      'image' => array(
        'fields' => array(
          'dir' => 'dir'
        ),
        'thumbnailSizes' => array(
          'xvga' => '1024x768',
          'vga' => '149x178',
          'profile' => '150x150',
          'thumb' => '80x80'
        ),
        'thumbnailMethod' => 'php'
      )
    )
  );

}
