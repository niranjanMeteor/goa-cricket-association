<?php
App::uses('AppModel', 'Model');
/**
 * BidHistory Model
 *
 * @property Bid $Bid
 * @property Team $Team
 */
class BidHistory extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'bid_history';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Bid' => array(
			'className' => 'Bid',
			'foreignKey' => 'bid_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => 'team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public function addNewBid($bidId, $teamId, $minimiumAmount, $raise) {
		$currentBidCount = $this->find('count', array(
			'conditions' => array(
				'BidHistory.bid_id' => $bidId
			)
		));
		$bidAmount = $minimiumAmount + ($currentBidCount * $raise) + $raise;
		$data = array(
			'bid_id' => $bidId,
			'team_id' => $teamId,
			'amount' => $bidAmount
		);
		$lastBid = $this->find('first', array(
			'conditions' => array(
				'BidHistory.bid_id' => $bidId
			),
			'order' => array('id DESC')
		));
		$team = $this->Team->AuctionTeam->findByTeamId($teamId);
		if ($team['AuctionTeam']['is_eligible_for_bid']) {
			if (empty($lastBid) || (!empty($lastBid) && ($lastBid['BidHistory']['team_id'] != $teamId))) {
				$db = $this->getDataSource();
				$db->begin();
				if ($this->Team->AuctionTeam->adjustBidAmounts($bidId, $teamId, $bidAmount)) {
					$this->create();
					if ($this->save($data)) {
						$db->commit();
						$newBid = $this->findById($this->getLastInsertID());
						return $newBid;
					} else {
						$db->rollback();
					}
				}
			}
		}
		return false;
	}
	public function getLastBidByBidId($bidId) {
		return $this->find('first', array(
			'conditions' => array(
				'BidHistory.bid_id' => $bidId
			),
			'order' => array('id desc')
		));
	}
	public function isBidDoneForPlayer($bidId) {
		return $this->find('count', array(
			'conditions' => array(
				'BidHistory.bid_id' => $bidId
			)
		));
	}
	public function getBiddingDetails($bidId) {
		return $this->find('all', array(
			'conditions' => array(
				'BidHistory.bid_id' => $bidId
			),
			'contain' => array(
				'Team' => array(
					'fields' => array('Team.id', 'Team.name', 'Team.image', 'Team.dir')
				)
			),
			'fields' => array('BidHistory.id', 'BidHistory.amount', 'BidHistory.created')
		));
	}
	public function getLastTeamId() {
		$lastTeam = $this->find('first', array(
			'order' => array(
				'BidHistory.created DESC'
			)
		));
		return !empty($lastTeam['BidHistory']['team_id']) ? $lastTeam['BidHistory']['team_id'] : 0;
	}
}
