<?php
App::uses('AppModel', 'Model');
/**
 * News Model
 *
 */
class News extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

  public $actsAs = array(
    'Upload.Upload' => array(
      'image' => array(
        'fields' => array(
          'dir' => 'dir'
        ),
        'thumbnailSizes' => array(
          'xvga' => '1024x768',
          'vga' => '149x178',
          'profile' => '150x150',
          'thumb' => '80x80'
        ),
        'thumbnailMethod' => 'php'
      )
    )
  );

  public function getRecentNews($id) {
    $recentNews = $this->find('list', array(
      'conditions' => array(
        'News.id !=' => $id
      ),
      'order' => array('id DESC'),
      'limit' => 5
    ));
    return $recentNews;
  }

}
