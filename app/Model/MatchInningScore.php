<?php
App::uses('AppModel', 'Model');
App::uses('BatsmanStatus', 'Lib/Enum');
App::uses('PlayerRole', 'Lib/Enum');
App::uses('TossDecision', 'Lib/Enum');
App::uses('ResultType', 'Lib/Enum');
App::uses('MatchLevel', 'Lib/Enum');
App::uses('InningNumber', 'Lib/Enum');
App::uses('OutType', 'Lib/Enum');
App::uses('MatchType', 'Lib/Enum');
/**
 * MatchInningScore Model
 *
 * @property Match $Match
 * @property Team $Team
 * @property MatchBallScore $MatchBallScore
 * @property MatchBatsmanScore $MatchBatsmanScore
 * @property MatchBowlerScore $MatchBowlerScore
 */
class MatchInningScore extends AppModel {

		/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'match_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Key or value does not exist: match_id',
				'allowEmpty' => false,
				'required' => true,
				'on' => 'create',
			),
		),
		'inning' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Key or value does not exist: inning',
				'allowEmpty' => false,
				'required' => true,
				'on' => 'create',
			),
		),
		'team_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Key or value does not exist: teams_id',
				'allowEmpty' => false,
				'required' => true,
				'on' => 'create',
			),
		)
	);



	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Match' => array(
			'className' => 'Match',
			'foreignKey' => 'match_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => 'team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'MatchBallScore' => array(
			'className' => 'MatchBallScore',
			'foreignKey' => 'match_inning_score_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'MatchBatsmanScore' => array(
			'className' => 'MatchBatsmanScore',
			'foreignKey' => 'match_inning_score_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'MatchBowlerScore' => array(
			'className' => 'MatchBowlerScore',
			'foreignKey' => 'match_inning_score_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function saveBallScore($striker,$nonStriker,$bowler,$matchInning,$currentBall,$nextBall) {
		$data = array(
			'MatchInningScore' => $matchInning,
			'MatchBowlerScore' => array($bowler),
			'MatchBallScore' => array($currentBall,$nextBall),
			'MatchBatsmanScore' => array($striker,$nonStriker)
		);
		if ($this->saveAssociated($data, array('deep' => true))) {
			return true;
		} 
		return false;
	}

	public function getMatchInningScoreId($matchId,$inning) {
		$data = $this->find('all',array(
			'conditions' => array(
				'match_id' => $matchId,
				'inning' => $inning
			),
			'fields' => array('id')
		));
		return $data['MatchInningScore']['id'];
	}

	public function isMatchInningsComplete($matchId,$inning) {
		return $this->find('count',array(
			'conditions' => array(
				'match_id' => $matchId,
				'inning' => $inning,
				'is_complete' => true
			)
		));
	}

	public function checkIfInningHasCompleted($id) {
		$matchInning = $this->findById($id);
		$matchInning = $matchInning['MatchInningScore'];
		$matchId = $matchInning['match_id'];
		$match = $this->Match->findById($matchId);
		$match = $match['Match'];
		if ($match['level'] == MatchLevel::T20 || $match['level'] == MatchLevel::ODI) {
			$ifAnyBatsmenPairLeft = $this->MatchBatsmanScore->checkIfAnyBatsmenPairLeftToPlay($id,$match['players_per_side']);
			if ( $matchInning['inning'] == InningNumber::FIRST && $matchInning['overs'] == $match['overs_per_innings'] || (!$ifAnyBatsmenPairLeft) ) {
				return true;						
			} else if ($matchInning['inning'] == InningNumber::SECOND) {
				$overs = !empty($match['required_overs']) ? $match['required_overs'] : $match['overs_per_innings'];
				if ( $matchInning['overs'] == $overs || $matchInning['runs'] >= $match['target'] || (!$ifAnyBatsmenPairLeft) ) {
					return true;
				}
			}
		}
		return false;
	}

	public function checkIfMatchIsComplete($matchId) {
		$inningsCompleted = $this->find('count',array(
			'conditions' => array(
				'match_id' => $matchId,
				'is_complete' => true
			)
		));
		$match = $this->Match->findById($matchId);
		$match = $match['Match'];
		if ($match['level'] == MatchLevel::T20 || $match['level'] == MatchLevel::ODI) {
			if ($inningsCompleted == 2) {
				return true;
			}
		}
		return false;
	}

	public function updateInningsCompletion($id) {
		$data = array(
			'MatchInningScore' => array(
				'id' => $id,
				'is_complete' => true,
				'in_progress' => false
			)
		);
		if ($this->save($data)) {
			return true;
		}
		return false;
	}

	public function setMatchInningsInProgress($id) {
		$data = array(
			'MatchInningScore' => array(
				'id' => $id,
				'in_progress' => true
			)
		);
		if ($this->save($data)) {
			return true;
		}
		return false;
	}

	public function checksIfInningsToResume($matchId,$inningsId) {
		return $this->find('count',array(
			'conditions' => array(
				'match_id' => $matchId,
				'inning' => $inningsId,
				'in_progress' => true,
				'OR' => array(
					'is_complete' => false,
					'is_complete' => null
				)				
			)
		));
	}

	public function fetchMiniScorecard() {
		$matchData = array();
		$match = $this->Match->getCurrentMatchData();
		if (empty($match)) {
			$nextMatchInSchedule = $this->Match->getNextMatchInSchedule();
			if (!empty($nextMatchInSchedule)) {
				return array('status' => 200, 'data' => array('match' => $this->_prepareNextMatchData($nextMatchInSchedule)));
			} else {
				return array('status' => 101, 'fetchMiniScorecard : Match Data does not Exist');				
			}
		}
		$match = $match['Match'];
		$matchData['id'] = $match['id'];
		$matchData['name'] = $match['name'];
		$matchData['series_name'] = $match['series_name'];
		$matchData['start_date_time'] = $match['start_date_time'];
		$matchData['in_progress'] = $match['in_progress'];
		$matchData['location'] = $match['location'];
		$matchData['type'] = MatchType::stringValue($match['type']);
		$matchData['overs_per_innings'] = $match['overs_per_innings'];
		$matchData['required_overs'] = $match['required_overs'];
		$matchInnings = $this->find('all',array(
			'conditions' => array(
				'MatchInningScore.match_id' => $match['id']
			),
			'contain' => array(
				'Team' => array(
					'fields' => array('id','name','image','dir')
				),
			),				
			'order' => 'MatchInningScore.inning ASC'
		));
		$teamOne = array();
		$teamTwo = array();
		$numOfInnings = Count($matchInnings);
		$matchData['no_of_innings'] = $numOfInnings;
		if ($numOfInnings > 0) {
			$teamOne = $this->__prepareTeamMiniScore($matchInnings[0],$match['id']);
			$matchData['team_one'] = $teamOne;
		}
		if ($numOfInnings > 1) {
			$teamTwo = $this->__prepareTeamMiniScore($matchInnings[1],$match['id']);
			$matchData['team_two'] = $teamTwo;
		}	
		$response = array('status' => 200, 'data' => array('match' => $matchData));
		return $response;
	}

	private function _prepareNextMatchData($match) {
		$firstTeam = $match['FirstTeam'];
		$secondTeam = $match['SecondTeam'];
		$match = $match['Match'];
		$nextMatch['id'] = $match['id'];
		$nextMatch['name'] = $match['name'];
		$nextMatch['series_name'] = $match['series_name'];
		$nextMatch['start_date_time'] = $match['start_date_time'];
		$nextMatch['in_progress'] = $match['in_progress'];
		$nextMatch['location'] = $match['location'];
		$nextMatch['type'] = MatchType::stringValue($match['type']);
		$nextMatch['overs_per_innings'] = $match['overs_per_innings'];
		$nextMatch['required_overs'] = $match['required_overs'];
		$nextMatch['team_one']['id'] = $firstTeam['id'];
		$nextMatch['team_one']['name'] = $firstTeam['name'];
		$nextMatch['team_one']['image'] = 'http://goacricket.org/files/team/image/'.$firstTeam['dir'].'/thumb_'.$firstTeam['image'];
		$nextMatch['team_two']['id'] = $secondTeam['id'];
		$nextMatch['team_two']['name'] = $secondTeam['name'];
		$nextMatch['team_two']['image'] = 'http://goacricket.org/files/team/image/'.$secondTeam['dir'].'/thumb_'.$secondTeam['image'];
		return $nextMatch;
	}

	private function __prepareTeamMiniScore($inning,$matchId) {
		$team = array();
		$team['id'] = $inning['Team']['id'];
		$team['name'] = $inning['Team']['name'];
		$team['image'] = 'http://goacricket.org/files/team/image/'.$inning['Team']['dir'].'/thumb_'.$inning['Team']['image'];
		if ($inning['MatchInningScore']['in_progress'] == true && $inning['MatchInningScore']['is_complete'] != true) {
			$team['is_batting'] = true;
		} else {
			$team['is_batting'] = false;
		}
		$team['runs'] = !empty($inning['MatchInningScore']['runs']) ? $inning['MatchInningScore']['runs'] : 0;
		$team['wickets'] = !empty($inning['MatchInningScore']['wickets']) ? $inning['MatchInningScore']['wickets'] : 0;
		$team['overs'] = !empty($inning['MatchInningScore']['overs']) ? $inning['MatchInningScore']['overs'] : 0;
		$team['run_rate'] = $this->prepareInningsRunRate($team['runs'],$team['overs']);
		$team['strike_batsman'] = array();
		$team['non_strike_batsman'] = array();
		$batsmen = $this->MatchBatsmanScore->getCurrentBattingPair($inning['MatchInningScore']['id']);
		$strikerAdded = false;
		foreach ($batsmen as $key => $batsman) {
			if ((!$strikerAdded) && ($batsman['MatchBatsmanScore']['status'] == BatsmanStatus::NOT_OUT || $batsman['MatchBatsmanScore']['status'] == BatsmanStatus::STRIKER)) {
				$team['strike_batsman']['id'] = $batsman['Player']['id'];
				$team['strike_batsman']['name'] = $batsman['Player']['name'];
				$team['strike_batsman']['runs'] = !empty($batsman['MatchBatsmanScore']['runs']) ? $batsman['MatchBatsmanScore']['runs'] : 0;
				$team['strike_batsman']['balls'] = !empty($batsman['MatchBatsmanScore']['balls']) ? $batsman['MatchBatsmanScore']['balls'] : 0;
				$strikerAdded = true;
			} else if ($batsman['MatchBatsmanScore']['status'] == BatsmanStatus::NON_STRIKER) {
				$team['non_strike_batsman']['id'] = $batsman['Player']['id'];
				$team['non_strike_batsman']['name'] = $batsman['Player']['name'];
				$team['non_strike_batsman']['runs'] = !empty($batsman['MatchBatsmanScore']['runs']) ? $batsman['MatchBatsmanScore']['runs'] : 0;
				$team['non_strike_batsman']['balls'] = !empty($batsman['MatchBatsmanScore']['balls']) ? $batsman['MatchBatsmanScore']['balls'] : 0;
			}
		}
		$team['bowlers'] = array();
		$bowlers = $this->MatchBowlerScore->getCurrentBowlingPair($inning['MatchInningScore']['id']);
		foreach ($bowlers as $key => $bowler) {
			$team['bowlers'][$key]['id'] = $bowler['Player']['id'];
			$team['bowlers'][$key]['name'] = $bowler['Player']['name'];
			$team['bowlers'][$key]['overs'] = !empty($bowler['MatchBowlerScore']['overs']) ? $bowler['MatchBowlerScore']['overs'] : 0;
			$team['bowlers'][$key]['dot_balls'] = !empty($bowler['MatchBowlerScore']['dot_balls']) ? $bowler['MatchBowlerScore']['dot_balls'] : 0;
			$team['bowlers'][$key]['runs'] = !empty($bowler['MatchBowlerScore']['runs_conceded']) ? $bowler['MatchBowlerScore']['runs_conceded'] : 0;
			$team['bowlers'][$key]['wickets'] = !empty($bowler['MatchBowlerScore']['wickets']) ? $bowler['MatchBowlerScore']['wickets'] : 0;
		}
		if ($inning['MatchInningScore']['inning'] == 2) {
			$team['target'] = $this->__prepareTargetForSecondInnings($inning);
		}
		$team['recent_overs'] = $this->MatchBallScore->getOversData($inning['MatchInningScore']['id']);
		return $team;
	}

	private function __prepareTargetForSecondInnings($secondInning) {
		$team = $secondInning['Team'];
		$inning = $secondInning['MatchInningScore'];
		$match = $this->Match->findById($inning['match_id']);
		$match = $match['Match'];
		$otherTeam = $this->Match->getBowlingTeam($match['id'],$team['id']);
		$ifAnyRemainingPlayers = $this->MatchBatsmanScore->checkIfAnyBatsmenPairLeftToPlay($inning['id'],$match['players_per_side']);
		$targetString = '';
		$remainingOvers = !empty($match['required_overs']) ? $match['required_overs'] : $match['overs_per_innings'];
		$requiredOversInBalls = ( ((((int)$remainingOvers) * 10) / 10) * 6) + (($remainingOvers * 10) % 10);
		$currentOversInBalls = (((((int)$inning['overs']) * 10) / 10) * 6)  + (($inning['overs'] * 10) % 10);
		$remainingRuns = $match['target'] - $inning['runs'];
		$remainingOvers = (int)(($requiredOversInBalls - $currentOversInBalls) / 6).'.'.(($requiredOversInBalls - $currentOversInBalls) % 6);
		$remainingBalls = $requiredOversInBalls - $currentOversInBalls;
		if ($inning['in_progress'] == true || $inning['is_complete'] == true) {
			if ($inning['runs'] == ($match['target']-1) && ($remainingOvers == 0 || $ifAnyRemainingPlayers == true)) {
				if ($inning['is_complete'] == true) {
					$targetString = 'Match Drawn';
				} else {
					$targetString = 'Match is Tied Currently';					
				}
			} else if ($remainingRuns <= 0) {
				$targetString = $team['name'].' won the Match';
			} else if ($remainingRuns > 0  && ($remainingOvers <= 0 || $ifAnyRemainingPlayers == false)) {
				$targetString = $otherTeam['name'].' won the Match';
			} else if ( $remainingRuns > 0 && $remainingOvers > 0 && $ifAnyRemainingPlayers == true ) {
				if ($remainingOvers < 10) {
					$targetString = $team['name'].' need '.$remainingRuns.' runs in '.$remainingBalls.' balls to win';
				} else {
					$targetString = $team['name'].' need '.$remainingRuns.' runs in '.$remainingOvers.' overs to win';					
				}
			} 
		}
		return $targetString;
	}

	public function fetchMatchScorecard($matchId) {
		$matchData = array();
		$match = $this->Match->getMatchDataForScore($matchId);
		if (empty($match)) {
			return array('status' => 101, 'fetchMatchScorecard : Match Data does not Exist');
		}
		$tossTeam = $match['TossWinningTeam'];
		$resultTeam = $match['MatchWinningTeam'];
		$match = $match['Match'];
		$matchData['id'] = $matchId;
		$matchData['name'] = $match['name'];
		$matchData['series_name'] = $match['series_name'];
		$matchData['start_date_time'] = $match['start_date_time'];
		$matchData['end_date_time'] = $match['end_date_time'];
		$matchData['in_progress'] = $match['in_progress'];
		$matchData['location'] = $match['location'];
		$matchData['type'] = MatchType::stringValue($match['type']);
		$matchData['actual_overs'] = !empty($match['overs']) ? $match['overs'] : 0;
		$matchData['overs_per_innings'] = !empty($match['overs_per_innings']) ? $match['overs_per_innings'] : 0;
		$matchData['target_runs'] = !empty($match['target']) ? $match['target'] : 0;
		$matchData['required_overs'] = !empty($match['required_overs']) ? $match['required_overs'] : 0;
		$matchData['toss_winning_team']['id'] = $match['toss_winning_team_id'];
		$matchData['toss_winning_team']['name'] = $tossTeam['name'];
		$matchData['toss_decision'] = TossDecision::stringValue($match['toss_decision']);
		$matchData['toss'] = $this->prepareTossString($match['toss_decision'],$tossTeam);
		$matchData['result_type'] = ResultType::stringValue($match['result_type']);
		$matchData['winning_team']['id'] = $match['winning_team_id'];
		$matchData['winning_team']['name'] = $resultTeam['name'];
		$matchData['target'] = '';
		$matchInnings = $this->find('all',array(
			'conditions' => array(
				'MatchInningScore.match_id' => $matchId
			),
			'contain' => array(
				'Team' => array(
					'fields' => array('id','name','image','dir','location')
				),
				'MatchBatsmanScore' => array(
					'order' => 'MatchBatsmanScore.created ASC',
					'Player' => array(
						'fields' => array('id','name')
					),
					'MatchBatsmanBowler' => array(
						'fields' => array('id','name')
					),
					'MatchBatsmanFielder' => array(
						'fields' => array('id','name')
					),
				),
				'MatchBowlerScore' => array(
					'order' => 'MatchBowlerScore.created ASC',
					'Player' => array(
						'fields' => array('id','name')
					)
				)
			),
			'order' => 'MatchInningScore.inning ASC'
		));
		$inningsOneData = array();
		$inningsTwoData = array();
		$numOfInnings = Count($matchInnings);
		$matchData['no_of_innings'] = $numOfInnings;
		if ($numOfInnings > 0) {
			$inningsOneData = $this->__preapareInningsData($matchInnings[0],$matchId);
			$matchData['innings_one'] = $inningsOneData;
		}
		if ($numOfInnings > 1) {
			if ($matchInnings[1]['MatchInningScore']['in_progress'] == true || $matchInnings[1]['MatchInningScore']['is_complete'] == true) {
				$inningsTwoData = $this->__preapareInningsData($matchInnings[1],$matchId);
			}
			$matchData['innings_two'] = $inningsTwoData;
			$matchData['target'] = $this->__prepareTargetForSecondInnings($matchInnings[1]);
		}
		$response = array('status' => 200, 'data' => array('match' => $matchData));
		return $response;
	}

	private function __preapareInningsData($inning,$matchId) {
		$battingTeam = array();
		$bowlingTeam = array();
		$battingTeam = $this->__prepareBattingTeamInning(
			$matchId,
			$inning['Team'],
			$inning['MatchInningScore'],
			$inning['MatchBatsmanScore']
		);
		$bowlingTeam = $this->__prepareBowlingTeamInning(
			$matchId,
			$inning['Team']['id'],
			$inning['MatchBowlerScore']
		);
		return array('batting_team' => $battingTeam, 'bowling_team' => $bowlingTeam);
	}

	private function __prepareBattingTeamInning($matchId,$team,$matchInning,$matchBatsmen) {
		$battingTeamInning = array();
		$battingTeamInning['id'] = $team['id'];
		$battingTeamInning['name'] = $team['name'];
		$battingTeamInning['logo_url'] = 'http://goacricket.org/files/team/image/'.$team['dir'].'/thumb_'.$team['image'];
		$battingTeamInning['location'] = $team['location'];
		$battingTeamInning['runs'] = $matchInning['runs'];
		$battingTeamInning['wickets'] = $matchInning['wickets'];
		$battingTeamInning['overs'] = $matchInning['overs'];
		$battingTeamInning['run_rate'] = $this->prepareInningsRunRate($battingTeamInning['runs'],$battingTeamInning['overs']);
		$battingTeamInning['wides'] = $matchInning['wides'];
		$battingTeamInning['no_balls'] = $matchInning['no_balls'];
		$battingTeamInning['byes'] = $matchInning['byes'];
		$battingTeamInning['leg_byes'] = $matchInning['leg_byes'];
		$battingTeamInning['total_extras'] = $matchInning['wides']+$matchInning['no_balls']+$matchInning['leg_byes']+$matchInning['byes'];
		$battingTeamInning['batsmen'] = $this->__prepareBatsmenData($matchBatsmen,$matchId);
		$battingTeamInning['did_not_bat'] = $this->__prepareDidNotBatData($matchId,$team['id'],$matchBatsmen);
		$battingTeamInning['fall_of_wickets'] = $this->MatchBallScore->prepareFallOfWickets($matchInning['id']);
		$battingTeamInning['overs_data'] = $this->MatchBallScore->getOversData($matchInning['id']);
		return $battingTeamInning;
	}

	private function __prepareBowlingTeamInning($matchId,$battingTeamId,$matchBowlers) {
		$bowlingTeamInning = array();
		$bowlingTeam = $this->Match->getBowlingTeam($matchId,$battingTeamId);

		$bowlingTeamInning['id'] = $bowlingTeam['id'];
		$bowlingTeamInning['name'] = $bowlingTeam['name'];
		$bowlingTeamInning['logo_url'] = 'http://goacricket.org/files/team/image/'.$bowlingTeam['dir'].'/thumb_'.$bowlingTeam['image'];
		$bowlingTeamInning['location'] = $bowlingTeam['location'];
		$bowlingTeamInning['bowlers'] = $this->__prepareBowlersData($matchBowlers,$matchId);
		return $bowlingTeamInning;
	}

	private function __prepareBatsmenData($batsmen,$matchId) {
		$batsmenData = array();
		foreach ($batsmen as $key => $batsman) {
			$batsmenData[$key]['id'] = $batsman['player_id'];
			$batsmenData[$key]['name'] = $batsman['Player']['name'];
			$batsmenData[$key]['status'] = BatsmanStatus::stringValue(BatsmanStatus::NOT_OUT);
			$batsmenData[$key]['is_striker'] = false;
			$batsmenData[$key]['is_out'] = false;
			if ($batsman['status'] == BatsmanStatus::OUT) {
				$batsmenData[$key]['is_out'] = true;
				$batsmenData[$key]['status'] = $this->prepareBatsmanDismissalString($batsman);
			} else if ($batsman['status'] == BatsmanStatus::STRIKER) {
				$batsmenData[$key]['is_striker'] = true;
			} else if ($batsman['status'] == BatsmanStatus::RETIRED_HURT) {
				$batsmenData[$key]['status'] = BatsmanStatus::stringValue(BatsmanStatus::RETIRED_HURT);
			}
			$batsmenData[$key]['is_keeper'] = $this->Match->MatchPlayer->checkPlayerRole(
				$matchId,
				$batsman['player_id'],
				PlayerRole::WICKETKEEPER
			);
			$batsmenData[$key]['role'] = PlayerRole::stringValue($this->Match->MatchPlayer->getPlayerRoleInMatch($matchId,$batsman['player_id']));
			$batsmenData[$key]['is_captain'] = $this->Match->MatchPlayer->isPlayerCaptain($matchId,$batsman['player_id']);
			$batsmenData[$key]['runs'] = $batsman['runs'];
			$batsmenData[$key]['balls'] = $batsman['balls'];
			$batsmenData[$key]['fours'] = $batsman['fours'];
			$batsmenData[$key]['sixes'] = $batsman['sixes'];
			if (!empty($batsman['balls']) && ($batsman['balls'] != 0)) {
				if (empty($batsman['runs'])) {
					$batsmenData[$key]['strike_rate'] = 0;
				} else {
					$batsmenData[$key]['strike_rate'] = round((100 * ($batsman['runs'] / $batsman['balls'])),1,PHP_ROUND_HALF_UP);					
				}
			} else {
				$batsmenData[$key]['strike_rate'] = 'NA';
			}			
		}
		return $batsmenData;
	}

	private function prepareBatsmanDismissalString($batsman) {
		$outType = $batsman['out_type'];
		$unconventionalOutTypes = [OutType::HANDLED_THE_BALL,OutType::HIT_THE_BALL_TWICE,OutType::TIMED_OUT,OutType::OBSTRUCTING_THE_FIELD,OutType::RETIRED];		
		if (in_array($outType, $unconventionalOutTypes)) {
			return 'out ('.OutType::stringValue($outType).')';
		}
		$bowlerName = !empty($batsman['MatchBatsmanBowler']['name']) ? $batsman['MatchBatsmanBowler']['name'] : '';
		$fielderName = !empty($batsman['MatchBatsmanFielder']['name']) ? $batsman['MatchBatsmanFielder']['name'] : '';
		switch ($outType) {
			case OutType::BOWLED:
				return 'b '.$bowlerName;
				break;
			case OutType::CAUGHT:
				$outString = ($bowlerName == $fielderName) ?  'c & b ' . $bowlerName : 'c ' . $fielderName . ' b ' . $bowlerName;
				return $outString;
				break;
			case OutType::LBW:
				return 'lbw b '.$bowlerName;
				break;
			case OutType::RUNOUT:
				return 'run out ('.$fielderName.')';
				break;
			case OutType::STUMPED:
				return 'st '.$fielderName.' b '.$bowlerName;
				break;
			case OutType::HIT_WICKET:
				return 'hit-wicket b '.$bowlerName;
				break;
		}
	}

	private function __prepareDidNotBatData($matchId,$teamId,$matchBatsmen) {
		$excludeList = array();
		foreach ($matchBatsmen as $key => $batsman) {
			$excludeList[$key] = $batsman['player_id'];
		}
		$didNotBatPlayers = $this->Match->MatchPlayer->getDidNotBatPlayers($matchId,$teamId,$excludeList);
		return $didNotBatPlayers;
	}

	private function __prepareBowlersData($matchBowlers,$matchId) {
		$bowlersData = array();
		foreach ($matchBowlers as $key => $bowler) {
			$bowlersData[$key]['id'] = $bowler['player_id'];
			$bowlersData[$key]['name'] = $bowler['Player']['name'];
			$bowlersData[$key]['overs'] = $bowler['overs'];
			$bowlersData[$key]['dot_balls'] = $bowler['dot_balls'];
			$bowlersData[$key]['runs_conceded'] = $bowler['runs_conceded'];
			$bowlersData[$key]['wickets'] = $bowler['wickets'];
			$bowlersData[$key]['wides'] = $bowler['wides'];
			$bowlersData[$key]['no_balls'] = $bowler['no_balls'];
			$bowlersData[$key]['economy'] = $this->getBowlingEconomy($bowler['runs_conceded'],$bowler['overs']);
		}
		return $bowlersData;
	}

	private function getBowlingEconomy($runsConceded,$overs) {
		if (!empty($overs) && ($overs != 0)) {
			$quotient = ( ((int)$overs) * 10 ) / 10;
			$remainder = ($overs * 10) % 10;
			$balls = (6 * $quotient) + $remainder;
			return round(( ($runsConceded / $balls) * 6),1,PHP_ROUND_HALF_UP);
		}	else return 'NA';
	}
	public function getCurrentInning($matchId) {
		return $this->find('first', array(
			'conditions' => array(
				'MatchInningScore.match_id' => $matchId,
				'MatchInningScore.in_progress' => true
			)
		));
	}

	public function prepareTossString($tossDecision,$tossTeam) {
		$tossString = '';
		if (!empty($tossDecision) && !empty($tossTeam)) {
			if ($tossDecision == TossDecision::BATTING) {
				$tossString = $tossTeam['name'].' won the toss and elected to Bat First';
			} else if ($tossDecision == TossDecision::BOWLING) {
				$tossString = $tossTeam['name'].' won the toss and elected to Bowl First';
			}
		}
		return $tossString;
	}

	public function prepareInningsRunRate($runs,$overs) {
		if (empty($overs) || $overs == 0) {
			return 'NA';
		} else if (empty($runs) || $runs == 0) {
			return 0.0;
		} else {
			$oversInBalls = ( ((((int)$overs) * 10) / 10) * 6) + (($overs * 10) % 10);
			return round( ( ($runs / $oversInBalls) * 6),1,PHP_ROUND_HALF_UP );
		}
	}

	public function calculateTeamStats($teams) {
		foreach ($teams as $id => $team) {
			$matchIds = $this->find('list', array(
				'conditions' => array(
					'team_id' => $team['FirstTeam']['id']
				),
				'fields' => array('id', 'match_id')
			));
			if (!empty($matchIds)) {
				$runsScored = 0;
				$ballsBatted = 0;
				$runsGiven = 0;
				$ballsBowled = 0;
				foreach ($matchIds as $matchId) {
					$match = $this->Match->findById($matchId);
					if ($this->isMatchEligibleForNRR($match)) {
						$match = $match['Match'];
						$innings = $this->findAllByMatchIdAndIsComplete($matchId, true);
						$oversForMatch = !empty($match['required_overs']) ? $match['required_overs'] : $match['overs_per_innings'];
						foreach ($innings as $inning) {
							if ($inning['MatchInningScore']['team_id'] == $team['FirstTeam']['id']) {
								$runsScored += $inning['MatchInningScore']['runs'];								
								if ( (($match['players_per_side']-1) - ($inning['MatchInningScore']['wickets'])) > 0 ) {
									$overs = $inning['MatchInningScore']['overs'];
								} else {
									$overs = $oversForMatch;
								}
								$oversInBalls = ( ((((int)$overs) * 10) / 10) * 6) + (($overs * 10) % 10);														
								$ballsBatted += $oversInBalls;
							} else {
								$runsGiven += $inning['MatchInningScore']['runs'];							
								if ( ($match['players_per_side']-1) - $inning['MatchInningScore']['wickets'] > 0 ) {
									$oversBowled = $inning['MatchInningScore']['overs'];
								} else {
									$oversBowled = $oversForMatch;
								}
								$oversInBallsBolwed = ( ((((int)$oversBowled) * 10) / 10) * 6) + (($oversBowled * 10) % 10);								
								$ballsBowled += $oversInBallsBolwed; 
							}
						}
					}
				}
				$battingRunRate = !empty($ballsBatted) ? round( ( ($runsScored / $ballsBatted) * 6),3,PHP_ROUND_HALF_UP ) : 0;
				$bowlingRunRate = !empty($ballsBowled) ? round( ( ($runsGiven / $ballsBowled) * 6),3,PHP_ROUND_HALF_UP ) : 0;
				$NRR = $battingRunRate - $bowlingRunRate;
				$teams[$id]['FirstTeam']['nrr'] = $NRR;
				$teams[$id]['FirstTeam']['match_count'] = count($matchIds);
				$winCount = $this->Match->getWonMatchCount($team['FirstTeam']['id']);
				$teams[$id]['FirstTeam']['win_count'] = $winCount;
				$tieCount = $this->Match->getTiedMatchCount($team['FirstTeam']['id']);
				$noResultCount = $this->Match->getNoResultMatchCount($team['FirstTeam']['id']);
				$teams[$id]['FirstTeam']['no_result_count'] = $noResultCount;
				$points = ($winCount * 4) + ( ($tieCount + $noResultCount) * 2);
				$teams[$id]['FirstTeam']['points'] = $points;
				$teams[$id]['FirstTeam']['tie_count'] = $tieCount;
			} else {
				unset($teams[$id]);
			}
		}
    $pointsSort = array(); //array used to store value to compare for multisort
    $nrrSort = array();
    foreach ($teams as $id => $team) {
    	$pointsSort[$id] = $team['FirstTeam']['points'];
    	$nrrSort[$id] = $team['FirstTeam']['nrr'];
    }
    array_multisort($pointsSort, SORT_DESC, $nrrSort, SORT_DESC, $teams);
		return $teams;
	}
	public function isMatchEligibleForNRR($match) {
		if (($match['Match']['result_type'] == ResultType::ABANDONED) || ($match['Match']['result_type'] == ResultType::CANCELLED)) {
			return false;
		}
		return true;
	}
}
