<?php
App::uses('AppModel', 'Model');
App::uses('BatsmanStatus', 'Lib/Enum');
/**
 * MatchBatsmanScore Model
 *
 * @property MatchInningScore $MatchInningScore
 * @property Player $Player
 */
class MatchBatsmanScore extends AppModel {

	public $validate = array(
	  'player_id' => array(
	    'unique' => array(
	      'rule' => array('checkUnique', array('match_inning_score_id', 'player_id'), false), 
	      'message' => 'Batsman already exists in the team'
	    )
	  )
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'MatchInningScore' => array(
			'className' => 'MatchInningScore',
			'foreignKey' => 'match_inning_score_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Player' => array(
			'className' => 'Player',
			'foreignKey' => 'player_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'MatchBatsmanBowler' => array(
			'className' => 'Player',
			'foreignKey' => 'out_by_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'MatchBatsmanFielder' => array(
			'className' => 'Player',
			'foreignKey' => 'out_other_by_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function createNewBatsman($matchInningScoreId,$strikerId,$nonStrikerId) {
		$data = array();
		if (!empty($strikerId)) {	
			$data1 = array(
				'MatchBatsmanScore' => array(
					'match_inning_score_id' => $matchInningScoreId,
					'player_id' => $strikerId,
					'status' => BatsmanStatus::STRIKER
				)
			);
			array_push($data, $data1);			
		}
		if (!empty($nonStrikerId)) {	
				$data2 = array(
					'MatchBatsmanScore' => array(
						'match_inning_score_id' => $matchInningScoreId,
						'player_id' => $nonStrikerId,
						'status' => BatsmanStatus::NON_STRIKER
					)
				);
				array_push($data, $data2);
		}
		if (!empty($data)) {
			if ($this->saveMany($data)) {
				return true;
			}
		}
		return false;
	}

	public function getBatsmanRecordForMatchInning($playerId,$matchInningScoreId) {
		$data = $this->find('first',array(
			'conditions' => array(
				'player_id' => $playerId,
				'match_inning_score_id' => $matchInningScoreId
			)
		));
		if (empty($data)) {
			$new = array(
				'MatchBatsmanScore' => array(
					'match_inning_score_id' => $matchInningScoreId,
					'player_id' => $playerId,
					'status' => BatsmanStatus::NOT_OUT
				)
			);
			if ($this->save($new)) {
				$data = $this->findById($this->getLastInsertID());
			}
		}
		return $data['MatchBatsmanScore'];
	}

	public function getAlreadyDismissedBatsmen($matchInningScoreId) {
		return $this->find('list',array(
			'conditions' => array(
				'match_inning_score_id' => $matchInningScoreId,
				'status' => BatsmanStatus::OUT
			),
			'fields' => array(
				'id', 'player_id'
			)
		));
	}

	public function checkIfAnyBatsmenPairLeftToPlay($matchInningScoreId,$playersPerSide) {
		$count = $this->find('count',array(
			'conditions' => array(
				'match_inning_score_id' => $matchInningScoreId,
				'status' => BatsmanStatus::OUT
			)
		));
		if ( $count < ($playersPerSide-1) ) {
			return true;
		}
		return false;
	}

	public function getCurrentBattingPair($matchInningScoreId) {
		return $this->find('all',array(
			'conditions' => array(
				'match_inning_score_id' => $matchInningScoreId,
				'status' => [BatsmanStatus::STRIKER,BatsmanStatus::NON_STRIKER,BatsmanStatus::NOT_OUT]
			),
			'contain' => array(
				'Player' => array(
					'fields' => array('id','name')
				)
			),
			'order' => 'MatchBatsmanScore.modified DESC',
			'limit' => 2
		));
	}
	public function getTopBatsmen() {
		$this->virtualFields += $this->Player->virtualFields;
		$batsmen = $this->find('all', array(
			'fields' => array('player_id', 'sum(runs) as total', 'round((sum(runs)/sum(balls))*100,1) as strike_rate'),
			'group' => array('MatchBatsmanScore.player_id'),
			'order' => array('total DESC'),
			'limit' => 10,
			'contain' => array(
				'Player' => array(
					'fields' => array('Player.id', 'MatchBatsmanScore.name'),
					'TeamPlayer' => array(
						'Team' => array(
							'conditions' => array('Team.is_gpl_team' => true) 
						)
					)
				)
			)
		));
		return $batsmen;
	}
}
