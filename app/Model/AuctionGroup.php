<?php
App::uses('AppModel', 'Model');
/**
 * AuctionGroup Model
 *
 */
class AuctionGroup extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

  public $hasMany = array(
    'PlayerAuctionGroup' => array(
      'className' => 'PlayerAuctionGroup',
      'foreignKey' => 'auction_group_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
    ),
    'Round' => array(
      'className' => 'Round',
      'foreignKey' => 'auction_group_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
    )
  );
}
