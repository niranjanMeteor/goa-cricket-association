<?php
App::uses('AppModel', 'Model');
/**
 * Round Model
 *
 * @property AuctionGroup $AuctionGroup
 */
class Round extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'AuctionGroup' => array(
			'className' => 'AuctionGroup',
			'foreignKey' => 'auction_group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getNextRoundPlayer() {
		$nextRound = $this->getNextRound();
		if (!empty($nextRound)) {
			$nextPlayerToBidFor = $this->AuctionGroup->PlayerAuctionGroup->getNextPlayer($nextRound['Round']['auction_group_id'], $nextRound['Round']['id']);
			if (!empty($nextPlayerToBidFor)) {
				$this->AuctionGroup->PlayerAuctionGroup->Player->Bid->addPlayerInBidding($nextPlayerToBidFor['Player']['id']);
				return $nextPlayerToBidFor;
			}
		}
		return false;
	}
	public function getNextRoundPlayerForClient() {
		$nextRound = $this->getNextRound();
		$nextPlayerToBidFor = $this->AuctionGroup->PlayerAuctionGroup->getNextPlayerForClient($nextRound['Round']['auction_group_id'], $nextRound['Round']['id']);
		if (!empty($nextPlayerToBidFor)) {
			return $nextPlayerToBidFor;
		}
		return false;
	}
	public function getCurrentBidAmount($playerId) {
		return $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->getCurrentBidAmount($playerId);
	}
	public function setRoundToInProgress($roundId) {
		$this->id = $roundId;
		$this->saveField('in_progress', true);
	}
	public function findRoundCount($auctionGroupId) {
		return $this->find('count', array(
			'conditions' => array(
				'Round.auction_group_id' => $auctionGroupId,
				'OR' => array(
					'Round.in_progress' => true,
					'Round.is_completed' => true
				)
			)
		));
	}
	public function setRoundCompleted($auctionGroupId) {
		return $this->updateAll(
			array(
				'Round.in_progress' => false,
				'Round.is_completed' => true
			),
			array(
				'Round.auction_group_id' => $auctionGroupId,
				'Round.in_progress' => true
			)
		);
	}
	public function roundsLeftForAuctionGroup($auctionGroupId) {
		return $this->find('count', array(
			'conditions' => array(
				'Round.is_completed' => false,
				'Round.auction_group_id' => $auctionGroupId
			)
		));
	}
	public function getNextRoundForAuctionGroup($auctionGroupId) {
		return $this->find('first', array(
			'conditions' => array(
				'Round.is_completed' => false,
				'Round.auction_group_id' => $auctionGroupId
			)
		));
	}
	public function setRoundCompletedByRoundId($roundId) {
		return $this->updateAll(
			array(
				'Round.in_progress' => false,
				'Round.is_completed' => true
			),
			array(
				'Round.id' => $roundId,
			)
		);
	}
	public function getNextRound() {
		$nextRound = $this->find('first', array(
			'conditions' => array(
				'Round.is_completed' => false
			),
			'order' => array('id ASC')
		));
		return $nextRound;
	}
	public function findPlayersInThisRound() {
		$currentRound = $this->getCurrentRound();
		$unsoldThisRound = $this->AuctionGroup->PlayerAuctionGroup->getUnsoldCount($currentRound['Round']['auction_group_id']);
		return $unsoldThisRound;
	}
	public function getCurrentRound() {
		return $this->find('first', array(
			'conditions' => array(
				'Round.in_progress' => true
			)
		));
	}
	public function formatTeams($teams, $playerId) {
		$lastBidTeamId = $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->BidHistory->getLastTeamId();
		$currentBid = $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->findByPlayerId($playerId);
		if (!empty($currentBid)) {
			$isThereAnyBidsForPlayer = $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->BidHistory->find('count', array('conditions' => array('BidHistory.bid_id' => $currentBid['Bid']['id'])));
		} else {
			$isThereAnyBidsForPlayer = 0;
		}
		$data = array();
		foreach ($teams as $id => $team) {
			$playersBought = $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->getTotalNumberOfPlayersForTeam($team['Team']['id']);
			$data[$id]['id'] = $team['Team']['id'];
			$data[$id]['name'] = $team['Team']['name'];
			$data[$id]['is_eligible_for_bid'] = $team['AuctionTeam']['is_eligible_for_bid'];
			$data[$id]['logo_url'] = 'http://' . Configure::read('webUrl') . '/files/team/image/' . $team['Team']['dir'] . '/' . 'thumb_' . $team['Team']['image'];
			$data[$id]['url'] = 'http://' . Configure::read('webUrl') . '/teams/view/' . $team['Team']['id'];
			$data[$id]['total'] = $team['AuctionTeam']['budget'] + $team['AuctionTeam']['spent'];
			$data[$id]['available'] = $team['AuctionTeam']['budget'];
			$data[$id]['required'] = $team['AuctionTeam']['required'];
			$data[$id]['players_bought_count'] = $playersBought;
			$data[$id]['current'] = (($lastBidTeamId == $team['Team']['id']) && ($isThereAnyBidsForPlayer)) ? true : false;
		}
		return $data;
	}
	public function formatTeamsForServer($teams, $playerId) {
		$lastBidTeamId = $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->BidHistory->getLastTeamId();
		$currentBid = $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->findByPlayerId($playerId);
		if (!empty($currentBid)) {
			$isThereAnyBidsForPlayer = $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->BidHistory->find('count', array('conditions' => array('BidHistory.bid_id' => $currentBid['Bid']['id'])));
		} else {
			$isThereAnyBidsForPlayer = 0;
		}
		foreach ($teams as $id => $team) {
			$playersBought = $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->getTotalNumberOfPlayersForTeam($team['Team']['id']);
			$teams[$id]['Team']['id'] = $team['Team']['id'];
			$teams[$id]['Team']['name'] = $team['Team']['name'];
			$teams[$id]['Team']['is_eligible_for_bid'] = $team['AuctionTeam']['is_eligible_for_bid'];
			$teams[$id]['Team']['logo_url'] = 'http://' . Configure::read('webUrl') . '/files/team/image/' . $team['Team']['dir'] . '/' . 'thumb_' . $team['Team']['image'];
			$teams[$id]['Team']['url'] = 'http://' . Configure::read('webUrl') . '/teams/view/' . $team['Team']['id'];
			$teams[$id]['Team']['total'] = $team['AuctionTeam']['budget'] + $team['AuctionTeam']['spent'];
			$teams[$id]['Team']['available'] = $team['AuctionTeam']['budget'];
			$teams[$id]['Team']['required'] = $team['AuctionTeam']['required'];
			$teams[$id]['Team']['players_bought_count'] = $playersBought;
			$teams[$id]['Team']['current'] = (($lastBidTeamId == $team['Team']['id']) && ($isThereAnyBidsForPlayer)) ? true : false;
		}
		return $teams;
	}
	public function formatTeamsFinalList($teams) {
		foreach ($teams as $id => $team) {
			$teams[$id]['Team']['url'] = 'http://' . Configure::read('webUrl') . '/teams/view/' . $team['Team']['id'];
			$teams[$id]['Team']['total'] = $team['AuctionTeam']['budget'] + $team['AuctionTeam']['spent'];
			$teams[$id]['Team']['available'] = $team['AuctionTeam']['budget'];
			$teams[$id]['Team']['required'] = $team['AuctionTeam']['required'];
		}
		return $teams;	
	}
	public function getBiddingDetails($playerId) {
		$bid = $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->findByPlayerId($playerId);
		if (!empty($bid)) {
			$bidHistory = $this->AuctionGroup->PlayerAuctionGroup->Player->Bid->BidHistory->getBiddingDetails($bid['Bid']['id']);
			if (!empty($bidHistory)) {
				foreach ($bidHistory as $id => $bidHistoryElement) {
					$bidHistory[$id]['Team']['logo_url'] = 'http://' . Configure::read('webUrl') . '/files/team/image/' . $bidHistoryElement['Team']['dir'] . '/' . 'thumb_' . $bidHistoryElement['Team']['image'];
					$bidHistory[$id]['Team']['url'] = 'http://' . Configure::read('webUrl') . '/teams/view/' . $bidHistoryElement['Team']['id'];
				}
			}
			return $bidHistory;
		}
		return [];
	}
}
