<?php
App::uses('AppModel', 'Model');
App::uses('OutType', 'Lib/Enum');
App::uses('BatsmanStatus' ,'Lib/Enum');
App::uses('DeliveryType' ,'Lib/Enum');
/**
 * MatchBallScore Model
 *
 * @property MatchInningScore $MatchInningScore
 */
class MatchBallScore extends AppModel {

	/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'match_inning_score_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Key or value does not exist: match_inning_score_id',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'bowler_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Key or value does not exist: bowler_id',
				'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'striker_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Key or value does not exist: striker_id',
				'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'non_striker_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Key or value does not exist: non_striker_id',
				'allowEmpty' => false,
				'required' => false,
				//'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'MatchInningScore' => array(
			'className' => 'MatchInningScore',
			'foreignKey' => 'match_inning_score_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Bowler' => array(
			'className' => 'Player',
			'foreignKey' => 'bowler_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Fielder' => array(
			'className' => 'Player',
			'foreignKey' => 'out_other_by_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Striker' => array(
			'className' => 'Player',
			'foreignKey' => 'striker_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'NonStriker' => array(
			'className' => 'Player',
			'foreignKey' => 'non_striker_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'OutBatsman' => array(
			'className' => 'Player',
			'foreignKey' => 'out_batsman_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
	 * beforeSave callback
	 *
	 * @param $options array
	 * @return boolean
	 */
		public function beforeSave($options=array()) {
			//pr($this->data);
			return true;
		}
	

	public function saveData($data) {
		$currentData = $data['MatchBallScore'];
		$sanitizedData['MatchBallScore']['striker_id'] = $currentData['striker_id'];
		$sanitizedData['MatchBallScore']['non_striker_id'] = $currentData['non_striker_id'];
		$sanitizedData['MatchBallScore']['bowler_id'] = $currentData['bowler_id'];
		$matchInningScore = $this->MatchInningScore->findByMatchIdAndInning($currentData['match_id'], $currentData['innings_id']);
		$sanitizedData['MatchBallScore']['match_inning_score_id'] = $matchInningScore['MatchInningScore']['id'];
		$sanitizedData['MatchBallScore']['overs'] = 0;
		$sanitizedData['MatchBallScore']['over_balls'] = 1;
		$response = false;
		$dataSource = $this->getDataSource();
    $dataSource->begin();
		if ($this->save($sanitizedData)) {
			$response = $this->MatchInningScore->MatchBatsmanScore->createNewBatsman(
				$matchInningScore['MatchInningScore']['id'],
				$sanitizedData['MatchBallScore']['striker_id'],
				$sanitizedData['MatchBallScore']['non_striker_id']
			);
			if ($response) {
				$response = $this->MatchInningScore->MatchBowlerScore->createNewBowler(
					$matchInningScore['MatchInningScore']['id'],
					$sanitizedData['MatchBallScore']['bowler_id']
				);
			}
			if ($response) {
				$response = $this->MatchInningScore->Match->setMatchInProgress($currentData['match_id']);
			}
			if ($response) {
				$response = $this->MatchInningScore->setMatchInningsInProgress($matchInningScore['MatchInningScore']['id']);
			}
		}

		if ($response) {
			$dataSource->commit();
		} else  {
			$dataSource->rollback();
		}
		return $response;
	}

	public function getMatchBallData($id) {
		$matchBall = $this->find('first',array(
			'conditions' => array(
				'MatchBallScore.id' => $id
			),
			'contain' => array(
				'MatchInningScore' => array(
					'fields' => array('match_id','inning')
				)
			)
		));
		$response['id'] = $matchBall['MatchBallScore']['id'];
		$response['match_inning_score_id'] = $matchBall['MatchBallScore']['match_inning_score_id'];
		$response['striker_id'] = $matchBall['MatchBallScore']['striker_id'];
		$response['non_striker_id'] = $matchBall['MatchBallScore']['non_striker_id'];
		$response['bowler_id'] = $matchBall['MatchBallScore']['bowler_id'];
		$response['match_id'] = $matchBall['MatchInningScore']['match_id'];
		$response['inning'] = $matchBall['MatchInningScore']['inning'];
		$response['is_out'] = $matchBall['MatchBallScore']['is_out'];
		$response['is_retired_hurt'] = $matchBall['MatchBallScore']['is_retired_hurt'];
		return $response;
	}
	
	public function updateScorecard($data) {
		$currentBall['id'] = $data['id'];
		$currentBall['match_inning_score_id'] = $data['match_inning_score_id'];
		$currentBall['striker_id'] = $data['striker_id'];
		$currentBall['non_striker_id'] = $data['non_striker_id'];
		$currentBall['bowler_id'] = $data['bowler_id'];

		$dataSource = $this->getDataSource();
    $dataSource->begin();
		$strikerData = $this->MatchInningScore->MatchBatsmanScore->getBatsmanRecordForMatchInning($data['striker_id'],$data['match_inning_score_id']);
		if (empty($strikerData)) {
			return false;
		}
		$striker['id'] = $strikerData['id'];
		$striker['player_id'] = $strikerData['player_id'];
		$striker['match_inning_score_id'] = $data['match_inning_score_id'];

		$nonStrikerData = $this->MatchInningScore->MatchBatsmanScore->getBatsmanRecordForMatchInning($data['non_striker_id'],$data['match_inning_score_id']);
		if (empty($nonStrikerData)) {
			$dataSource->rollback();
			return false;
		}
		$nonStriker['id'] = $nonStrikerData['id'];
		$nonStriker['player_id'] = $nonStrikerData['player_id'];
		$nonStriker['match_inning_score_id'] = $data['match_inning_score_id'];
		
		$bowlerData = $this->MatchInningScore->MatchBowlerScore->getBowlerRecordForMatchInning($data['bowler_id'],$data['match_inning_score_id']);
		if (empty($bowlerData)) {
			$dataSource->rollback();
			return false;
		}
		$bowler['id'] = $bowlerData['id'];
		$bowler['player_id'] = $bowlerData['player_id'];
		$bowler['match_inning_score_id'] = $data['match_inning_score_id'];

		$inningData = $this->MatchInningScore->findById($data['match_inning_score_id']);
		$inningData = $inningData['MatchInningScore'];		
		$matchInning['id'] = $data['match_inning_score_id'];
		
		$updateData = array();
		$deliveryType = $this->__findDeliveryType($data);
		switch ($deliveryType) {
			case DeliveryType::DOT_BALL:
				$updateData = $this->__prepareDotBallUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,DeliveryType::DOT_BALL,$currentBall);
				break;
			case DeliveryType::BALL_WITH_RUN_WITHOUT_EXTRA:
				$updateData = $this->__prepareBallWithRunWithoutExtraUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,DeliveryType::BALL_WITH_RUN_WITHOUT_EXTRA,$currentBall);
				break;
			case DeliveryType::NO_BALL:
				$updateData = $this->__prepareNoBallUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,DeliveryType::NO_BALL,$currentBall);
				break;
			case DeliveryType::WIDE_BALL:
				$updateData = $this->__prepareWideBallUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,DeliveryType::WIDE_BALL,$currentBall);
				break;
			case DeliveryType::LEG_BYES:
				$updateData = $this->__prepareLegByesUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,DeliveryType::LEG_BYES,$currentBall);
				break;
			case DeliveryType::BYES:
				$updateData = $this->__prepareByesUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,DeliveryType::BYES,$currentBall);
				break;
		}
		$striker = $updateData['striker'];
		$nonStriker = $updateData['non_striker'];
		$bowler = $updateData['bowler'];
		$matchInning = $updateData['match_inning'];
		$currentBall = $updateData['current_ball'];
		$nextBall = $this->__prepareNextBallData($data['id'],$updateData['is_over_completed'],$striker,$nonStriker,$data['bowler_id']);	
		$response = $this->MatchInningScore->saveBallScore($striker,$nonStriker,$bowler,$matchInning,$currentBall,$nextBall);	
		$ifInningsCompleted = $this->MatchInningScore->checkIfInningHasCompleted($data['match_inning_score_id']);
		if ($ifInningsCompleted) {
			$response = $this->MatchInningScore->updateInningsCompletion($data['match_inning_score_id']);
			if ($response) {
				$response = $this->MatchInningScore->Match->updateMatchTargetForChase($inningData['match_id']);
			}
		}
		if ($response) {
			$dataSource->commit();
		} else  {
			$dataSource->rollback();
		}
		
		return $response;
	}

	public function clearCache($matchId) {
		Cache::delete('match_scorecard_'.$matchId);
		Cache::delete('mini_scorecard');
	}

	private function __prepareDotBallUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,$deliveryType,$currentBall) {
		$striker['balls'] = $strikerData['balls']+1;
		$bowlerOvers = $this->incrementOver($bowlerData['overs']);
		$bowler['overs'] = $bowlerOvers['overs'];
		$inningOvers = $this->incrementOver($inningData['overs']);
		$matchInning['overs'] = $inningOvers['overs'];
		$bowler['dot_balls'] = $bowlerData['dot_balls'] + 1;
		$currentBall['ball_type'] = $deliveryType;
		$finalData = $this->handleBatsmenStatus($data,$striker,$nonStriker,$bowler,$bowlerData,$matchInning,$inningData,$inningOvers['is_over_completed'],$deliveryType,$currentBall);
		return $finalData;
	}

	private function __prepareBallWithRunWithoutExtraUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,$deliveryType,$currentBall) {
		$striker['runs'] = $strikerData['runs']+$data['runs_taken_by_batsman'];
		$striker['balls'] = $strikerData['balls']+1;
		$bowlerOvers = $this->incrementOver($bowlerData['overs']);
		$bowler['overs'] = $bowlerOvers['overs'];
		$bowler['runs_conceded'] = $bowlerData['runs_conceded']+$data['runs_taken_by_batsman'];
		$inningOvers = $this->incrementOver($inningData['overs']);
		$matchInning['overs'] = $inningOvers['overs'];
		$matchInning['runs'] = $inningData['runs']+$data['runs_taken_by_batsman'];
		if (!empty($data['is_four'])) {
			$striker['fours'] = $strikerData['fours']+1;
			$matchInning['fours'] = $inningData['fours']+1;
			$currentBall['is_four'] = true;
		} else if (!empty($data['is_six'])) {
			$striker['sixes'] = $strikerData['sixes']+1;
			$matchInning['sixes'] = $inningData['sixes']+1;
			$currentBall['is_six'] = true;
		}
		$currentBall['ball_type'] = $deliveryType;
		$currentBall['runs'] = $data['runs_taken_by_batsman'];
		$currentBall['runs_taken_by_batsman'] = $data['runs_taken_by_batsman'];
		$finalData = $this->handleBatsmenStatus($data,$striker,$nonStriker,$bowler,$bowlerData,$matchInning,$inningData,$inningOvers['is_over_completed'],$deliveryType,$currentBall);
		return $finalData;
	}

	private function __prepareNoBallUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,$deliveryType,$currentBall) {
		$bowler['no_balls'] = $bowlerData['no_balls']+1;
		$striker['balls'] = $strikerData['balls']+1;
		if (!empty($data['runs_taken_by_batsman'])) {
			$bowler['runs_conceded'] = $bowlerData['runs_conceded']+$data['runs_taken_by_batsman']+1;
			$striker['runs'] = $strikerData['runs']+$data['runs_taken_by_batsman'];
			$matchInning['runs'] = $inningData['runs']+$data['runs_taken_by_batsman']+1;
			$currentBall['runs_taken_by_batsman'] = $data['runs_taken_by_batsman'];
			$currentBall['runs'] = $data['runs_taken_by_batsman']+1;
			$matchInning['no_balls'] = $inningData['no_balls']+1;
			if (!empty($data['is_four'])) {
				$striker['fours'] = $strikerData['fours']+1;
				$matchInning['fours'] = $inningData['fours']+1;
				$currentBall['is_four'] = true;
			} else if (!empty($data['is_six'])) {
				$striker['sixes'] = $strikerData['sixes']+1;
				$matchInning['sixes'] = $inningData['sixes']+1;
				$currentBall['is_six'] = true;
			}
		} else if (!empty($data['byes'])) {
			$matchInning['runs'] = $inningData['runs']+1+$data['byes'];
			$matchInning['no_balls'] = $inningData['no_balls']+1+$data['byes'];
			$bowler['runs_conceded'] = $bowlerData['runs_conceded']+1;
			$currentBall['byes'] = $data['byes'];
			$currentBall['runs'] = 1+$data['byes'];
		} else if (!empty($data['leg_byes'])) {
			$matchInning['runs'] = $inningData['runs']+1+$data['leg_byes'];
			$matchInning['no_balls'] = $inningData['no_balls']+1+$data['leg_byes'];
			$bowler['runs_conceded'] = $bowlerData['runs_conceded']+1;
			$currentBall['leg_byes'] = $data['leg_byes'];
			$currentBall['runs'] = 1+$data['leg_byes'];
		} else {
			$matchInning['runs'] = $inningData['runs']+1;
			$matchInning['no_balls'] = $inningData['no_balls']+1;
			$bowler['runs_conceded'] = $bowlerData['runs_conceded']+1;
			$currentBall['runs'] = 1;
		}
		$currentBall['ball_type'] = $deliveryType;
		$currentBall['no_balls'] = $data['no_balls'];
		$finalData = $this->handleBatsmenStatus($data,$striker,$nonStriker,$bowler,$bowlerData,$matchInning,$inningData,false,$deliveryType,$currentBall);
		return $finalData;
	}

	private function __prepareWideBallUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,$deliveryType,$currentBall) {
		$bowler['wides'] = $bowlerData['wides']+$data['wides'];
		$bowler['runs_conceded'] = $bowlerData['runs_conceded']+$data['wides'];
		$matchInning['wides'] = $inningData['wides']+$data['wides'];
		$matchInning['runs'] = $inningData['runs']+$data['wides'];
		$currentBall['ball_type'] = $deliveryType;
		$currentBall['runs'] = $data['wides'];
		$currentBall['wides'] = $data['wides'];
		$finalData = $this->handleBatsmenStatus($data,$striker,$nonStriker,$bowler,$bowlerData,$matchInning,$inningData,false,$deliveryType,$currentBall);
		return $finalData;
	}

	private function __prepareLegByesUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,$deliveryType,$currentBall) {
		$striker['balls'] = $strikerData['balls']+1;
		$bowlerOvers = $this->incrementOver($bowlerData['overs']);
		$bowler['overs'] = $bowlerOvers['overs'];
		$inningOvers = $this->incrementOver($inningData['overs']);
		$matchInning['overs'] = $inningOvers['overs'];
		$matchInning['runs'] = $inningData['runs']+$data['leg_byes'];
		$matchInning['leg_byes'] = $inningData['leg_byes']+$data['leg_byes'];
		$currentBall['ball_type'] = $deliveryType;
		$currentBall['leg_byes'] = $data['leg_byes'];
		$currentBall['runs'] = $data['leg_byes'];
		$finalData = $this->handleBatsmenStatus($data,$striker,$nonStriker,$bowler,$bowlerData,$matchInning,$inningData,$inningOvers['is_over_completed'],$deliveryType,$currentBall);
		return $finalData;
	}

	private function __prepareByesUpdate($data,$striker,$strikerData,$nonStriker,$nonStrikerData,$bowler,$bowlerData,$matchInning,$inningData,$deliveryType,$currentBall) {
		$striker['balls'] = $strikerData['balls']+1;
		$bowlerOvers = $this->incrementOver($bowlerData['overs']);
		$bowler['overs'] = $bowlerOvers['overs'];
		$inningOvers = $this->incrementOver($inningData['overs']);
		$matchInning['overs'] = $inningOvers['overs'];
		$matchInning['runs'] = $inningData['runs']+$data['byes'];
		$matchInning['byes'] = $inningData['byes']+$data['byes'];
		$currentBall['ball_type'] = $deliveryType;
		$currentBall['byes'] = $data['byes'];
		$currentBall['runs'] = $data['byes'];
		$finalData = $this->handleBatsmenStatus($data,$striker,$nonStriker,$bowler,$bowlerData,$matchInning,$inningData,$inningOvers['is_over_completed'],$deliveryType,$currentBall);
		return $finalData;
	}

	private function handleBatsmenStatus($data,$striker,$nonStriker,$bowler,$bowlerData,$matchInning,$inningData,$isOverCompleted,$deliveryType,$currentBall) {
		$statusData = array();
		$unconventionalOutTypes = [OutType::HANDLED_THE_BALL,OutType::HIT_THE_BALL_TWICE,OutType::TIMED_OUT,OutType::OBSTRUCTING_THE_FIELD,OutType::RETIRED];
		if (!empty($data['is_out'])) {		
			if (! (!empty($data['no_balls']) && ($data['out_type'] == OutType::BOWLED || $data['out_type'] == OutType::CAUGHT || $data['out_type'] == OutType::HIT_WICKET || $data['out_type'] == OutType::LBW))) {
				$statusData = $this->__prepareBatsmanOut($data,$striker,$nonStriker,$bowler,$bowlerData,$matchInning,$inningData,$currentBall);
				$bowler = $statusData['bowler'];
				$matchInning = $statusData['match_inning'];
				$currentBall = $statusData['current_ball'];
			}	else {
				$statusData = $this->__prepareBatsmenStrike($data,$striker,$nonStriker,$deliveryType,$isOverCompleted);
			}
		} else if (!empty($data['is_retired_hurt'])) {
			$statusData = $this->__prepareBatsmanRetiredHurt($data,$striker,$nonStriker,$currentBall);
			$currentBall = $statusData['current_ball'];
		} else {
			$statusData = $this->__prepareBatsmenStrike($data,$striker,$nonStriker,$deliveryType,$isOverCompleted);
		}
		if (!empty($statusData)) {
			$striker = $statusData['striker'];
			$nonStriker = $statusData['non_striker'];
		}
		return array('striker' => $striker, 'non_striker' => $nonStriker, 'bowler' => $bowler, 'match_inning' => $matchInning, 'current_ball' => $currentBall,'is_over_completed' => $isOverCompleted);
	}

	private function __prepareBatsmanOut($data,$striker,$nonStriker,$bowler,$bowlerData,$matchInning,$inningData,$currentBall) {
		$matchInning['wickets'] = $inningData['wickets']+1;
		$unconventionalOutTypes = [OutType::HANDLED_THE_BALL,OutType::HIT_THE_BALL_TWICE,OutType::TIMED_OUT,OutType::OBSTRUCTING_THE_FIELD,OutType::RETIRED];
		if ($data['out_batsman_id'] == $data['striker_id']) {
			$striker['status'] = BatsmanStatus::OUT;
			$nonStriker['status'] = BatsmanStatus::NOT_OUT;
			$striker['out_type'] = $data['out_type'];
			if (! in_array($data['out_type'], $unconventionalOutTypes)) {
				if ($data['out_type'] != OutType::RUNOUT) {
					$striker['out_by_id'] = $data['bowler_id'];
					$bowler['wickets'] = $bowlerData['wickets']+1;					
				}
				if ($data['out_type'] != OutType::BOWLED || $data['out_type'] != OutType::HIT_WICKET || $data['out_type'] != OutType::LBW) {
					$striker['out_other_by_id'] = $data['out_other_by_id'];	
					$currentBall['out_other_by_id'] = $data['out_other_by_id'];			
				}				
			}
		} else if ($data['out_batsman_id'] == $data['non_striker_id']) {
			$nonStriker['status'] = BatsmanStatus::OUT;
			$striker['status'] = BatsmanStatus::NOT_OUT;
			$nonStriker['out_type'] = $data['out_type'];
			if (! in_array($data['out_type'], $unconventionalOutTypes)) {
				if ($data['out_type'] != OutType::RUNOUT) {
					$nonStriker['out_by_id'] = $data['bowler_id'];	
					$bowler['wickets'] = $bowlerData['wickets']+1;				
				}
				if ($data['out_type'] != OutType::BOWLED || $data['out_type'] != OutType::HIT_WICKET || $data['out_type'] != OutType::LBW) {
					$nonStriker['out_other_by_id'] = $data['out_other_by_id'];	
					$currentBall['out_other_by_id'] = $data['out_other_by_id'];			
				}				
			}
		}
		$currentBall['is_out'] = true;
		$currentBall['out_type'] = $data['out_type'];
		$currentBall['out_batsman_id'] = $data['out_batsman_id'];

		return array('striker' => $striker, 'non_striker' => $nonStriker, 'bowler' => $bowler, 'match_inning' => $matchInning, 'current_ball' => $currentBall);
	}

	private function __prepareBatsmanRetiredHurt($data,$striker,$nonStriker,$currentBall) {
		if ($data['retired_hurt_batsman_id'] == $data['striker_id']) {
			$striker['status'] = BatsmanStatus::RETIRED_HURT;
			$nonStriker['status'] = BatsmanStatus::NOT_OUT;
		} else if ($data['retired_hurt_batsman_id'] == $data['non_striker_id']) {
			$nonStriker['status'] = BatsmanStatus::RETIRED_HURT;
			$striker['status'] = BatsmanStatus::NOT_OUT;
		}
		$currentBall['is_retired_hurt'] = true;
		$currentBall['retired_hurt_batsman_id'] = $data['retired_hurt_batsman_id'];

		return array('striker' => $striker, 'non_striker' => $nonStriker, 'current_ball' => $currentBall);
	}

	private function __prepareBatsmenStrike($data,$striker,$nonStriker,$deliveryType,$isOverCompleted) {
		switch ($deliveryType) {
			case DeliveryType::DOT_BALL:
				$striker['status'] = BatsmanStatus::STRIKER;
				$nonStriker['status'] = BatsmanStatus::NON_STRIKER;
				break;
			case DeliveryType::BALL_WITH_RUN_WITHOUT_EXTRA:
				if ($data['runs_taken_by_batsman'] % 2 == 0) {
					$striker['status'] = BatsmanStatus::STRIKER;
					$nonStriker['status'] = BatsmanStatus::NON_STRIKER;
				} else {
					$striker['status'] = BatsmanStatus::NON_STRIKER;
					$nonStriker['status'] = BatsmanStatus::STRIKER;
				}
				break;
			case DeliveryType::NO_BALL:
				if ((!empty($data['runs_taken_by_batsman']) && ($data['runs_taken_by_batsman'] % 2 != 0)) ||
						(!empty($data['byes']) && ($data['byes'] % 2 != 0)) ||
						(!empty($data['leg_byes']) && ($data['leg_byes'] % 2 != 0))) {
					$striker['status'] = BatsmanStatus::NON_STRIKER;
					$nonStriker['status'] = BatsmanStatus::STRIKER;
				} else {		
					$striker['status'] = BatsmanStatus::STRIKER;
					$nonStriker['status'] = BatsmanStatus::NON_STRIKER;
				}
				break;
			case DeliveryType::WIDE_BALL:
				if (( ($data['wides'] - 1) % 2) != 0) {
					$striker['status'] = BatsmanStatus::NON_STRIKER;
					$nonStriker['status'] = BatsmanStatus::STRIKER;
				} else {		
					$striker['status'] = BatsmanStatus::STRIKER;
					$nonStriker['status'] = BatsmanStatus::NON_STRIKER;
				}
				break;
			case DeliveryType::LEG_BYES:
				if ($data['leg_byes'] % 2 == 0) {
					$striker['status'] = BatsmanStatus::STRIKER;
					$nonStriker['status'] = BatsmanStatus::NON_STRIKER;
				} else {
					$striker['status'] = BatsmanStatus::NON_STRIKER;
					$nonStriker['status'] = BatsmanStatus::STRIKER;
				}
				break;
			case DeliveryType::BYES:
				if ($data['byes'] % 2 == 0) {
					$striker['status'] = BatsmanStatus::STRIKER;
					$nonStriker['status'] = BatsmanStatus::NON_STRIKER;
				} else {
					$striker['status'] = BatsmanStatus::NON_STRIKER;
					$nonStriker['status'] = BatsmanStatus::STRIKER;
				}
				break;
		}
		if ($isOverCompleted) {
			$temp = $striker['status'];
			$striker['status'] = $nonStriker['status'];
			$nonStriker['status'] = $temp;
		}
		return array('striker' => $striker, 'non_striker' => $nonStriker);
	}

	private function __prepareNextBallData($id,$isOverCompleted,$striker,$nonStriker,$bowlerId) {
		$nextBall = array();
		$currentData = $this->findById($id);
		$currentData = $currentData['MatchBallScore'];
		$nextBall['match_inning_score_id'] = $currentData['match_inning_score_id'];
		if ($isOverCompleted) {
			$nextBall['overs'] = $currentData['overs']+1;
			$nextBall['over_balls'] = 1;
		} else {
			$nextBall['overs'] = $currentData['overs'];
			$nextBall['over_balls'] = $currentData['over_balls']+1;
			$nextBall['bowler_id'] = $bowlerId;
		}
		if (!empty($striker['status']) && !empty($nonStriker['status'])) {
			if ($striker['status'] == BatsmanStatus::STRIKER && $nonStriker['status'] == BatsmanStatus::NON_STRIKER) {
				$nextBall['striker_id'] = $striker['player_id'];
				$nextBall['non_striker_id'] = $nonStriker['player_id'];
			} else if ($striker['status'] == BatsmanStatus::NON_STRIKER && $nonStriker['status'] == BatsmanStatus::STRIKER) {
				$nextBall['striker_id'] = $nonStriker['player_id'];
				$nextBall['non_striker_id'] = $striker['player_id'];
			}
		}
		return $nextBall;
	}

	private function checkBatsmanStatus() {
		if (!empty($data['is_out'])) {
			return BatsmanStatus::OUT;
		}
		if (!empty($data['is_retired_hurt'])) {
			return BatsmanStatus::RETIRED_HURT;
		}
		return BatsmanStatus::NOT_OUT;
	}

	private function incrementOver($overs) {
		$isOverCompleted = false;
		if ($overs == 0 || ((($overs*10) % 10) == 0) ) {
			$overs = $overs + 0.1;
		} else if ((($overs*10) % 5) == 0) {
			$isOverCompleted = true;
			$overs = $overs + 0.5;
		} else {
			$overs = $overs + 0.1;
		}
		return array('overs' => $overs, 'is_over_completed' => $isOverCompleted);
	}

	private function __findDeliveryType($data) {
		$type = null;
		if (!empty($data['is_extra'])) {
			if (!empty($data['no_balls'])) {
				$type = DeliveryType::NO_BALL;
			} else if (!empty($data['wides'])) {
				$type = DeliveryType::WIDE_BALL;
			} else if (!empty($data['byes'])) {
				$type = DeliveryType::BYES;
			} else if (!empty($data['leg_byes'])) {
				$type = DeliveryType::LEG_BYES;
			}
		} else if (!empty($data['runs_taken_by_batsman'])) {
			$type = DeliveryType::BALL_WITH_RUN_WITHOUT_EXTRA;
		} else {
			$type = DeliveryType::DOT_BALL;
		}
		return $type;
	}

	public function isOverComplete($matchInningScoreId,$overs) {
		$intOvers = (int)$overs;
		if ($intOvers == 0) {
			return false;
		}
		if ( ( ($overs*10) % 10) == 0) {
			$ifBallEntryForNewOver = $this->find('count',array(
				'conditions' => array(
					'match_inning_score_id' => $matchInningScoreId,
					'overs' => $intOvers
				)
			));
			if ($ifBallEntryForNewOver == 1) {
				return true;
			}
		}
		return false;
	}

	public function getlastBallEntryId($matchId,$inningsId) {
		$matchInning = $this->MatchInningScore->findByMatchIdAndInning($matchId,$inningsId);
		$matchInning = $matchInning['MatchInningScore'];
		$data = $this->find('first',array(
			'conditions' => array(
				'match_inning_score_id' => $matchInning['id']
			),
			'order' => 'MatchBallScore.id DESC'
		));
		return $data['MatchBallScore']['id'];
	}

	public function getCurrentBall($currentInningId) {
		return $this->find('first', array(
			'conditions' => array(
				'MatchBallScore.match_inning_score_id' => $currentInningId
			),
			'order' => 'MatchBallScore.id DESC'
		));
	}

	public function prepareFallOfWickets($matchInningScoreId) {
		$fallOfwickets = array();
		$wicketBalls = $this->find('all',array(
			'conditions' => array(
				'match_inning_score_id' => $matchInningScoreId,
				'is_out' => true
			),
			'contain' => array(
				'OutBatsman' => array(
					'fields' => array('id','name')
				)
			),
			'order' => 'MatchBallScore.id ASC'
		));
		foreach ($wicketBalls as $key => $ball) {
			$fallOfwickets[$key]['wicket_number'] = $key+1;
			$fallOfwickets[$key]['overs'] = $this->__findTeamOversTillThisBall(
				$ball['MatchBallScore']['id'],
				$ball['MatchBallScore']['match_inning_score_id'],
				$ball['MatchBallScore']['overs']
			);
			$fallOfwickets[$key]['team_total_runs'] = $this->__findTeamTotalRunsTillThisBall(
				$ball['MatchBallScore']['id'],
				$ball['MatchBallScore']['match_inning_score_id']
			);
			$fallOfwickets[$key]['batsman']['id'] = $ball['OutBatsman']['id'];
			$fallOfwickets[$key]['batsman']['name'] = $ball['OutBatsman']['name'];
		}
		return $fallOfwickets;
	}

	private function __findTeamOversTillThisBall($ballId,$matchInningScoreId,$overs) {
		$deliveryTypes = [DeliveryType::DOT_BALL,DeliveryType::BALL_WITH_RUN_WITHOUT_EXTRA,DeliveryType::BYES,DeliveryType::LEG_BYES];
		$ballCount = $this->find('count',array(
			'conditions' => array(
				'match_inning_score_id' => $matchInningScoreId,
				'id <=' => $ballId,
				'overs' => $overs,
				'ball_type' => $deliveryTypes
			)
		));
		if ($ballCount == 6) {
			return ($overs+1).'.0';
		} else {
			return ($overs).'.'.$ballCount;
		}		
	}

	private function __findTeamTotalRunsTillThisBall($ballId,$matchInningScoreId) {
		$data = $this->find('all',array(
			'conditions' => array(
				'match_inning_score_id' => $matchInningScoreId,
				'id <=' => $ballId,
			),
			'fields' => array('SUM(runs) as total')
		));
		return $data[0][0]['total'];
	}

	public function getOversData($matchInningScoreId) {
		$oversData = array();
		$overBalls = $this->find('all',array(
			'conditions' => array(
				'match_inning_score_id' => $matchInningScoreId,
				'ball_type !=' => null,
				'bowler_id !=' => null
			),
			'fields' => array('id','overs','over_balls','runs_taken_by_batsman','ball_type','no_balls','wides','leg_byes','byes','is_out','is_retired_hurt'),
			'order' => 'MatchBallScore.created DESC'
		));
		$index = -1;
		foreach ($overBalls as $ball) {
			$ball = $ball['MatchBallScore'];
			$data = '';
			switch ($ball['ball_type']) {
				case DeliveryType::DOT_BALL:
					if ($ball['is_out'] ==  true) {
						$data = 'W';
					}	else {
						$data = '.';
					}	
					break;
				case DeliveryType::BALL_WITH_RUN_WITHOUT_EXTRA:
					$data = $ball['runs_taken_by_batsman'].'';
					break;
				case DeliveryType::NO_BALL:
					$data = '(NB';
					if ( !empty($ball['runs_taken_by_batsman']) ) {
						$data = $data.','.$ball['runs_taken_by_batsman'];
					} else if (!empty($ball['leg_byes'])) {
						$data = $data.','.$ball['leg_byes'].'LB';
					} else if (!empty($ball['byes'])) {
						$data = $data.','.$ball['byes'].'B';
					}
					if ($ball['is_out'] == true) {
						$data = $data.',W';
					}
					break;
				case DeliveryType::WIDE_BALL:
				$data = $ball['wides'].'WD';
					if ($ball['is_out'] == true) {
						$data = $data.',W';
					}
					break;
				case DeliveryType::LEG_BYES:
				$data = $ball['leg_byes'].'LB';
					if ($ball['is_out'] == true) {
						$data = $data.',W';
					}
					break;
				case DeliveryType::BYES:
				$data = $ball['byes'].'B';
					if ($ball['is_out'] == true) {
						$data = $data.',W';
					}
					break;
			}
			if ($ball['overs'] != $index) {
				$overs = $ball['overs']+1;
				array_push($oversData, array('over' => 'OVER '.$overs));
				$index = $ball['overs'];
			}
			array_push($oversData, array('ball' => $data));
		}
		return $oversData;
	}

}
