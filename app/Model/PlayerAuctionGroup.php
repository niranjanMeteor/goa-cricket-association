<?php
App::uses('AppModel', 'Model');
App::uses('MatchLevel', 'Lib/Enum');
/**
 * PlayerAuctionGroup Model
 *
 * @property Player $Player
 * @property AuctionGroup $AuctionGroup
 */
class PlayerAuctionGroup extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Player' => array(
			'className' => 'Player',
			'foreignKey' => 'player_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'AuctionGroup' => array(
			'className' => 'AuctionGroup',
			'foreignKey' => 'auction_group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getNextPlayer($auctionGroupId, $roundId) {
		$player = $this->getNextPlayerToBid($auctionGroupId, $roundId);
		if (empty($player)) {
			$this->AuctionGroup->Round->setRoundCompleted($auctionGroupId);
			if ($this->AuctionGroup->Round->roundsLeftForAuctionGroup($auctionGroupId)) {
				$nextRound = $this->AuctionGroup->Round->getNextRoundForAuctionGroup($auctionGroupId);
				$this->setPlayersAvailableForBidding($auctionGroupId);
				$player = $this->getNextPlayerToBid($auctionGroupId, $nextRound['Round']['id']);
				if (empty($player)) {
					$this->AuctionGroup->Round->setRoundCompletedByRoundId($nextRound['Round']['id']);
					$nextRound = $this->AuctionGroup->Round->getNextRound();
					if (empty($nextRound)) {
						return false;
					} else {
						$this->moveUnboughtPlayersToNextGroup($auctionGroupId, $nextRound['Round']['auction_group_id']);
						$player = $this->getNextPlayerToBid($nextRound['Round']['auction_group_id'], $nextRound['Round']['id']);
					}
				}
			} else {
				$nextRound = $this->AuctionGroup->Round->getNextRound();
				if (empty($nextRound)) {
					return false;
				} else {
					$this->moveUnboughtPlayersToNextGroup($auctionGroupId, $nextRound['Round']['auction_group_id']);
					$player = $this->getNextPlayerToBid($nextRound['Round']['auction_group_id'], $nextRound['Round']['id']);
					if ($nextRound['Round']['id'] == 3) { //Start of Group A bidding 
						$this->AuctionGroup->PlayerAuctionGroup->Player->TeamPlayer->Team->updateRequiredForOutstationTeams();
					}
				}
			}
		}
		return $player;
	}
	public function getNextPlayerForClient($auctionGroupId, $roundId) {
		$player = $this->getNextPlayerToBid($auctionGroupId, $roundId);
		if (empty($player)) {
			return false;
		}
		return $player;
	}
	public function getNextPlayerToBidForClient($auctionGroupId, $roundId) {
		$player = $this->find('first', array(
			'conditions' => array(
				'PlayerAuctionGroup.is_active' => true,
				'PlayerAuctionGroup.auction_group_id' => $auctionGroupId
			),
			'contain' =>array(
				'Player' => array(
					'PlayerBattingRecord' => array(
						'conditions' => array(
							'PlayerBattingRecord.level' => MatchLevel::T20
						)
					), 
					'PlayerBowlingRecord' => array(
						'conditions' => array(
							'PlayerBowlingRecord.level' => MatchLevel::T20
						)		
					)
				), 
				'AuctionGroup'
			)
		));
		return $player;
	}
	public function getNextPlayerToBid($auctionGroupId, $roundId) {
		$player = array();
		$currentActiveBid = $this->getCurrentActiveBidPlayer($auctionGroupId);
		if (!empty($currentActiveBid)) {
			$player = $this->getPlayerDataForAuction($currentActiveBid['PlayerAuctionGroup']['player_id'], $auctionGroupId);
		} else {
			$playersAvailableCurrently = $this->find('list', array(
				'conditions' => array(
					'PlayerAuctionGroup.is_deleted' => false,
					'PlayerAuctionGroup.auction_group_id' => $auctionGroupId
				),
				'fields' => array('PlayerAuctionGroup.id', 'PlayerAuctionGroup.player_id')
			));
			if (!empty($playersAvailableCurrently)) {
				$playerIds = array_values($playersAvailableCurrently);
				shuffle($playerIds);
				$playerIdForBId = $playerIds[0];
				$player = $this->getPlayerDataForAuction($playerIdForBId, $auctionGroupId);
				if (!empty($player)) {
					$this->markPlayerAsActive($player['PlayerAuctionGroup']['id']);
				}
			}		
		}
		$this->AuctionGroup->Round->setRoundToInProgress($roundId);
		return $player;
	}
	public function getCurrentActiveBidPlayer($auctionGroupId) {
		return $this->find('first', array(
			'conditions'=> array(
				'PlayerAuctionGroup.is_active' => true,
				'PlayerAuctionGroup.auction_group_id' => $auctionGroupId
			)
		));
	}
	public function getPlayerDataForAuction($playerId, $auctionGroupId) {
		return $this->find('first', array(
			'conditions' => array(
				'PlayerAuctionGroup.player_id' => $playerId,
				'PlayerAuctionGroup.auction_group_id' => $auctionGroupId
			),
			'contain' =>array(
				'Player' => array(
					'PlayerBattingRecord' => array(
						'conditions' => array(
							'PlayerBattingRecord.level' => MatchLevel::T20
						)
					), 
					'PlayerBowlingRecord' => array(
						'conditions' => array(
							'PlayerBowlingRecord.level' => MatchLevel::T20
						)		
					)
				), 
				'AuctionGroup'
			)
		));
	}
	public function markPlayerAsActive($playerAuctinGroupId) {
		$this->id = $playerAuctinGroupId;
		$this->saveField('is_active', true);
	}
	public function setPlayersAvailableForBidding($auctionGroupId) {
		$this->updateAll(
			array(
				'PlayerAuctionGroup.is_deleted' => false,
				'PlayerAuctionGroup.is_active' => false
			),
			array(
				'PlayerAuctionGroup.auction_group_id' => $auctionGroupId,
				'PlayerAuctionGroup.is_bought' => false
			)
		);
	}
	public function moveUnboughtPlayersToNextGroup($previousAuctionGroupId, $nextAuctionGroupId) {
		$playerIds = $this->find('list', array(
			'conditions' => array(
				'PlayerAuctionGroup.auction_group_id' => $previousAuctionGroupId,
				'PlayerAuctionGroup.is_bought' => false
			),
			'fields' => array('PlayerAuctionGroup.id', 'PlayerAuctionGroup.player_id')
		));
		$data = array();
		$count = 0;
		if (!empty($playerIds)) {
			foreach ($playerIds as $id => $playerId) {
				$data[$count]['player_id'] = $playerId;
				$data[$count]['auction_group_id'] = $nextAuctionGroupId;
				$count++;
			}
			return $this->saveMany($data);
		}
		return false;
	}
	public function updatePlayerStatus($playerId) {
		return $this->updateAll(
			array(
				'PlayerAuctionGroup.is_bought' => true,
				'PlayerAuctionGroup.is_deleted' => true,
				'PlayerAuctionGroup.is_active' => false
			),
			array(
				'PlayerAuctionGroup.player_id' => $playerId,
				'PlayerAuctionGroup.is_deleted' => false
			)
		);
	}
	public function deactivatePlayerForThisRound($playerId) {
		return $this->updateAll(
			array(
				'PlayerAuctionGroup.is_deleted' => true,
				'PlayerAuctionGroup.is_bought' => false,
				'PlayerAuctionGroup.is_active' => false
			),
			array(
				'PlayerAuctionGroup.player_id' => $playerId,
				'PlayerAuctionGroup.is_deleted' => false
			)
		);
	}
	public function getUnsoldCount($auctionGroupId) {
		return $this->find('count', array(
			'conditions' => array(
				'PlayerAuctionGroup.auction_group_id' => $auctionGroupId,
				'PlayerAuctionGroup.is_deleted' => false
			)
		));
	}
}
