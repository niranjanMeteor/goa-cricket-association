<?php
App::uses('AppModel', 'Model');
/**
 * AuctionTeam Model
 *
 * @property Team $Team
 */
class AuctionTeam extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => 'team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public function adjustBidAmounts($bidId, $teamId, $bidAmount) {
		$lastBid = $this->Team->BidHistory->getLastBidByBidId($bidId);
		if (!empty($lastBid)) {
			$this->refundBidAmount($lastBid['BidHistory']['team_id'], $lastBid['BidHistory']['amount']);
		}
		return $this->deductBidAmount($teamId, $bidAmount);
	}
	public function refundBidAmount($teamId, $amount) {
		$auctionTeam = $this->findByTeamId($teamId);
		$newBudget = $auctionTeam['AuctionTeam']['budget'] + $amount;
		$newSpent = $auctionTeam['AuctionTeam']['spent'] - $amount;
		$data = array(
			'id' => $auctionTeam['AuctionTeam']['id'],
			'budget' => $newBudget,
			'spent' => $newSpent
		);
		$this->save($data);
	}
	public function deductBidAmount($teamId, $amount) {
		$auctionTeam = $this->findByTeamId($teamId);
		$newBudget = $auctionTeam['AuctionTeam']['budget'] - $amount;
		if ($newBudget >= 0) {
			$newSpent = $auctionTeam['AuctionTeam']['spent'] + $amount;
			$data = array(
				'AuctionTeam' => array(
					'id' => $auctionTeam['AuctionTeam']['id'],
					'budget' => $newBudget,
					'spent' => $newSpent
				)
			);
			return $this->save($data);	
		} else {
			return false;
		}
	}
	public function setInEligible($id) {
		$this->id = $id;
		$this->saveField('is_eligible_for_bid', false);
	}
	public function setEligible($id) {
		$this->id = $id;
		$this->saveField('is_eligible_for_bid', true);
	}
	public function updateRequiredAmount($teamId, $playerId) {
		$basePriceOfPlayer = $this->Team->TeamPlayer->Player->findBasePriceOfPlayer($playerId);
		$auctionTeam = $this->findByTeamId($teamId);
		$newRequiredValue = $auctionTeam['AuctionTeam']['required'] - $basePriceOfPlayer;
		return $this->updateAll(
			array(
				'AuctionTeam.required' => $newRequiredValue
			),
			array(
				'AuctionTeam.team_id' => $teamId
			)
		);
	}
	public function updateRequiredValue($teamId, $newRequiredAmount) {
		$this->updateAll(
			array('AuctionTeam.required' => $newRequiredAmount),
			array('AuctionTeam.team_id' => $teamId)
		);
	}
}
