<?php
App::uses('AppModel', 'Model');
App::uses('RemoteAction','Model');
App::uses('Audio','Model');
/**
 * Bid Model
 *
 * @property Player $Player
 * @property Team $Team
 * @property BidHistory $BidHistory
 */
class Bid extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Player' => array(
			'className' => 'Player',
			'foreignKey' => 'player_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => 'team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'BidHistory' => array(
			'className' => 'BidHistory',
			'foreignKey' => 'bid_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => 'BidHistory.id DESC',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	public function getCurrentBidAmount($playerId) {
		$currentBid = $this->find('first', array(
			'conditions' => array(
				'Bid.player_id' => $playerId,
			),
			'contain' => array('BidHistory'),
			'order' => array('Bid.id DESC'),
		));
		if (!empty($currentBid['BidHistory'])) {
			return $currentBid['BidHistory'][0]['amount'];
		}
		return 0;
	}
	public function findBidDetails($playerId) {
		$bid = $this->find('first', array(
			'conditions' => array(
				'Bid.player_id' => $playerId
			),
			'contain' => array(
				'Player' => array(
					'PlayerAuctionGroup' => array(
						'conditions' => array('PlayerAuctionGroup.is_deleted' => false),
						'AuctionGroup'
					)
				)
			)
		));
		return $bid;
	}
	public function sellPlayer($playerId) {
		$bid = $this->findByPlayerId($playerId);
		$lastBid = $this->BidHistory->getLastBidByBidId($bid['Bid']['id']);
		$db = $this->getDataSource();
		$db ->begin();
		$data = array(
			'Bid' => array(
				'id' => $bid['Bid']['id'],
				'team_id' => $lastBid['BidHistory']['team_id'],
				'buying_amount' => $lastBid['BidHistory']['amount']
			)
		);
		if ($this->save($data)) {
			if ($this->Player->PlayerAuctionGroup->updatePlayerStatus($playerId)) {
				if ($this->Team->AuctionTeam->updateRequiredAmount($lastBid['BidHistory']['team_id'], $playerId)) {
					$RemoteAction = new RemoteAction();
					$remoteAction = $RemoteAction->activateSoldPlayer();
					$player = $this->Player->findById($playerId);
					$team = $this->Player->TeamPlayer->Team->findById($lastBid['BidHistory']['team_id']);
					$audioText = $player['Player']['name'] . ' sold to ' . $team['Team']['name'] . ' for rupees ' . $lastBid['BidHistory']['amount'];
					$this->getAudio($audioText, $player['Player']['id'], 'sold');
					$db->commit();
					return true;
				} else {
					$db->rollback();
				}
			} 
		}
		return false;
	}
	public function getAudio($text, $id, $slug) {
		$base_url = 'http://translate.google.com/translate_tts?';
		$qs = http_build_query(array(
		    'tl' => 'en',
		    'ie' => 'UTF-8',
		    'q' => utf8_encode($text)
		));
		$fileName = $slug . '-' . $id .'.mp3';
		$fileNameWithPath = WWW_ROOT . 'audio' . DS . $slug . '-' . $id .'.mp3';
		if (file_put_contents($fileNameWithPath , file_get_contents($base_url . $qs))) {
			$Audio = new Audio();
			$Audio->saveFileName($fileName);
		}
	}
	public function addPlayerInBidding($playerId) {
		if ( ! $this->playerAlreadyAdded($playerId)) {
			$data = array(
				'Bid' => array(
					'player_id' => $playerId
				)
			);
			$this->create();
			$this->save($data);
		}
	}
	public function playerAlreadyAdded($playerId) {
		return $this->find('count', array(
			'conditions' => array(
				'Bid.player_id' => $playerId
			)
		));
	}
	public function canProceedToNextBid($playerId) {
		$bid = $this->findByPlayerId($playerId);
		$isBidDoneForPlayer = $this->BidHistory->isBidDoneForPlayer($bid['Bid']['id']);
		if ($isBidDoneForPlayer) {
			if ($this->sellPlayer($playerId)) {
				return true;
			}
		} else {
			if ($this->Player->PlayerAuctionGroup->deactivatePlayerForThisRound($playerId)) {
				return true;
			}
		}
		return false;
	}
	public function getTotalNumberOfPlayersForTeam($teamId) {
		return $this->find('count', array(
			'conditions' => array(
				'Bid.team_id' => $teamId
			)
		));
	}
	public function getLastSoldPlayer() {
		return $this->find('first', array(
			'conditions' => array(
				'NOT' => array(
					'Bid.team_id' => null
				)
			),
			'order' => array('Bid.modified DESC'),
			'contain' => array(
				'Team',
				'Player'
			)
		));
	}
	public function hasBoughtThreePlayersFromAGroup($teamId) {
		$numberOfPlayersBought = $this->find('count', array(
			'conditions' => array(
				'Bid.team_id' => $teamId
			)
		));
		return ($numberOfPlayersBought == 4);
	}
	public function hasBoughtSixPlayersFromBGroup($teamId) {
		$numberOfPlayersBought = $this->find('count', array(
			'conditions' => array(
				'Bid.team_id' => $teamId
			)
		));
		return ($numberOfPlayersBought == 10);
	}
	public function hasBoughtFourPlayers($teamId) {
		$numberOfPlayersBought = $this->find('count', array(
			'conditions' => array(
				'Bid.team_id' => $teamId
			)
		));
		return ($numberOfPlayersBought == 4);
	}
}
