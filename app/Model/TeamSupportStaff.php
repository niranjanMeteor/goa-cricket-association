<?php
App::uses('AppModel', 'Model');
/**
 * TeamSupportStaff Model
 *
 * @property Team $Team
 * @property SupportStaff $SupportStaff
 */
class TeamSupportStaff extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => 'team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SupportStaff' => array(
			'className' => 'SupportStaff',
			'foreignKey' => 'support_staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
