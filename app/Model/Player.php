<?php
App::uses('AppModel', 'Model');
/**
 * Player Model
 *
 * @property City $City
 * @property PlayerBattingRecord $PlayerBattingRecord
 * @property PlayerBowlingRecord $PlayerBowlingRecord
 * @property TeamPlayer $TeamPlayer
 */
class Player extends AppModel {

	// public $virtualFields = array(
 //    'name' => 'CONCAT(Player.first_name, " ", Player.last_name)'
	// );
  public function __construct($id = false, $table = null, $ds = null) {
    parent::__construct($id, $table, $ds);
    $this->virtualFields['name'] = sprintf(
        'CONCAT(%s.first_name, " ", %s.last_name)', $this->alias, $this->alias
    );
	}
	var $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'first_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'last_name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'City' => array(
			'className' => 'City',
			'foreignKey' => 'city_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'PlayerBattingRecord' => array(
			'className' => 'PlayerBattingRecord',
			'foreignKey' => 'player_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'PlayerBowlingRecord' => array(
			'className' => 'PlayerBowlingRecord',
			'foreignKey' => 'player_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'TeamPlayer' => array(
			'className' => 'TeamPlayer',
			'foreignKey' => 'player_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'PlayerAuctionGroup' => array(
			'className' => 'PlayerAuctionGroup',
			'foreignKey' => 'player_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
    'Bid' => array(
      'className' => 'Bid',
      'foreignKey' => 'player_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
    ),
    'BowlerBowling' => array(
			'className' => 'MatchBallScore',
			'foreignKey' => 'bowler_id'
		),
		'FielderForBall' => array(
			'className' => 'MatchBallScore',
			'foreignKey' => 'out_other_by_id'
		),
		'StrikerBatsman' => array(
			'className' => 'MatchBallScore',
			'foreignKey' => 'striker_id'
		),
		'NonStrikerBatsman' => array(
			'className' => 'MatchBallScore',
			'foreignKey' => 'non_striker_id'
		),
		'BatsmanDismissed' => array(
			'className' => 'MatchBallScore',
			'foreignKey' => 'out_batsman_id'
		),
		'Batsman' => array(
			'className' => 'MatchBatsmanScore',
			'foreignKey' => 'player_id'
		),
		'BowlerBowling' => array(
			'className' => 'MatchBatsmanScore',
			'foreignKey' => 'out_by_id'
		),
		'FielderFielding' => array(
			'className' => 'MatchBatsmanScore',
			'foreignKey' => 'out_other_by_id'
		),
		'StrikeBowler' => array(
			'className' => 'MatchBowlerScore',
			'foreignKey' => 'player_id'
		)
	);
  
	public $actsAs = array(
    'Upload.Upload' => array(
      'image' => array(
        'fields' => array(
          'dir' => 'dir'
        ),
        'thumbnailSizes' => array(
          'xvga' => '1024x768',
          'vga' => '149x178',
          'profile' => '150x150',
          'thumb' => '80x80'
        ),
        'thumbnailMethod' => 'php'
      )
    )
  );

  public function getPlayerById($id) {
  	$player = $this->find('first', array(
  		'conditions' => array(
  			'Player.id' => $id
			),
			'contain' => array(
				'City',
				'TeamPlayer' => array('Team'),
				'PlayerBattingRecord',
				'PlayerBowlingRecord'
			)
		));
		return $player;
  }
  public function findBasePriceOfPlayer($playerId) {
    $player = $this->find('first', array(
      'conditions' => array(
        'Player.id' => $playerId
      ),
      'contain' => array(
        'PlayerAuctionGroup' => array('AuctionGroup')
      )
    ));
    $associationPrice = $player['PlayerAuctionGroup'][0]['AuctionGroup']['minimum_amount'];
    $raise = $player['PlayerAuctionGroup'][0]['AuctionGroup']['raise'];
    $actualBasePrice = $associationPrice + $raise;
    return $actualBasePrice;
  }
  public function getBatsmenList($batsmenIds) {
    return $this->find('list', array(
      'conditions' => array(
        'id' => $batsmenIds
      ),
      'fields' => array('id', 'name')
    ));
  }
  public function getBowlersList($bowlersIds) {
    return $this->find('list', array(
      'conditions' => array(
        'id' => $bowlersIds
      ),
      'fields' => array('id', 'name')
    ));
  }
}
