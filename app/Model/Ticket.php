<?php
App::uses('AppModel', 'Model');
/**
 * Ticket Model
 *
 * @property Project $Project
 * @property User $Creator
 * @property User $Assignee
 * @property TicketAttachment $TicketAttachment
 * @property TicketRelation $TicketRelation
 * @property TicketUpdate $TicketUpdate
 * @property TicketWatcher $TicketWatcher
 */
class Ticket extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'project_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'creator_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'assignee_id' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'category' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'project_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'counterCache' => true
		),
		'Creator' => array(
			'className' => 'User',
			'foreignKey' => 'creator_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Assignee' => array(
			'className' => 'User',
			'foreignKey' => 'assignee_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'TicketAttachment' => array(
			'className' => 'TicketAttachment',
			'foreignKey' => 'ticket_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'PrimaryTicket' => array(
			'className' => 'TicketRelation',
			'foreignKey' => 'ticket_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'RelatedTicket' => array(
			'className' => 'TicketRelation',
			'foreignKey' => 'related_ticket_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'TicketUpdate' => array(
			'className' => 'TicketUpdate',
			'foreignKey' => 'ticket_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'TicketWatcher' => array(
			'className' => 'TicketWatcher',
			'foreignKey' => 'ticket_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	public function saveTicket($data, $ticketID = null) {
		if ($ticketID) {
			$data['Ticket']['id'] = $ticketID;
		}
		if (!empty($data['TicketWatcher']['user_id'])) {
			foreach ($data['TicketWatcher']['user_id'] as $id => $userId) {
				$data['TicketWatcher'][$id]['user_id'] = $userId;
			}
		}
		unset($data['TicketWatcher']['user_id']);
		if (empty($data['TicketWatcher'])) {
			unset($data['TicketWatcher']);
		}
		$data['Ticket']['creator_id'] = AuthComponent::user('id');
		if ( ! empty($data['Ticket']['attachments'][0]['name'])) {
			foreach ($data['Ticket']['attachments'] as $id => $attachment) {
				$data['TicketAttachment'][$id]['name'] = $attachment['name'];
				$data['TicketAttachment'][$id]['url'] = $this->_uploadFile($attachment['name'], $attachment['tmp_name'], 'ticket_attachments');
				$data['TicketAttachment'][$id]['size'] = $attachment['size'];
				$data['TicketAttachment'][$id]['mime_type'] = $attachment['type'];
			}
		}
		if ( ! empty($data['Ticket']['description'])) {
			$data['Ticket']['description'] = nl2br($data['Ticket']['description']);
		}
		if ( ! empty($data['TicketUpdate'])) {
			if ( ! empty($data['TicketUpdate'][0]['comment'])) {
				$data['TicketUpdate'][0]['user_id'] = AuthComponent::user('id');
			} else {
				unset($data['TicketUpdate']);
			}
		}
		$this->create();
		if ($this->saveAssociated($data)) {
			$ticketId = $this->getInsertID();
			if (empty($ticketId)) {
				$ticketId = $ticketID;
			}
			return $ticketId;
		}
		return false;
	}
}
