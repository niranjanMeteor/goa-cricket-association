<?php
App::uses('AppModel', 'Model');
/**
 * MatchBowlerScore Model
 *
 * @property MatchInningScore $MatchInningScore
 * @property Player $Player
 */
class MatchBowlerScore extends AppModel {

	public $validate = array(
	  'player_id' => array(
	    'unique' => array(
	      'rule' => array('checkUnique', array('match_inning_score_id', 'player_id'), false), 
	      'message' => 'Batsman already exists in the team'
	    )
	  )
	);


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'MatchInningScore' => array(
			'className' => 'MatchInningScore',
			'foreignKey' => 'match_inning_score_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Player' => array(
			'className' => 'Player',
			'foreignKey' => 'player_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function createNewBowler($matchInningScoreId,$bowlerId) {
		$data = array();
		if (!empty($bowlerId)) {	
			$data = array(
				'MatchBowlerScore' => array(
					'match_inning_score_id' => $matchInningScoreId,
					'player_id' => $bowlerId
				)
			);			
		}
		if (!empty($data)) {
			if ($this->save($data)) {
				return true;
			}
		}
		return false;
	}

	private function __playerAlreadyEntered($playerId,$matchInningScoreId) {
		return $this->find('count',array(
			'conditions' => array(
				'match_inning_score_id' => $matchInningScoreId,
				'player_id' => $playerId
			)
		));
	}

	public function getBowlerRecordForMatchInning($playerId,$matchInningScoreId) {
		$data = $this->find('first',array(
			'conditions' => array(
				'player_id' => $playerId,
				'match_inning_score_id' => $matchInningScoreId
			)
		));
		if (empty($data)) {
			$new = array(
				'MatchBowlerScore' => array(
					'match_inning_score_id' => $matchInningScoreId,
					'player_id' => $playerId
				)
			);
			if ($this->save($new)) {
				$data = $this->findById($this->getLastInsertID());
			}
		}
		return $data['MatchBowlerScore'];
	}

	public function getCurrentBowlingPair($matchInningScoreId) {
		return $this->find('all',array(
			'conditions' => array(
				'match_inning_score_id' => $matchInningScoreId,
			),
			'contain' => array(
				'Player' => array(
					'fields' => array('id','name')
				)
			),
			'order' => 'MatchBowlerScore.modified DESC',
			'limit' => 2
		));
	}
	public function getTopBowlers() {
		$this->virtualFields += $this->Player->virtualFields;
		$bowlers = $this->find('all', array(
			'fields' => array('player_id', 'sum(wickets) as total'),
			'group' => array('MatchBowlerScore.player_id'),
			'order' => array('total DESC'),
			'limit' => 10,
			'contain' => array(
				'Player' => array(
					'fields' => array('Player.id', 'MatchBowlerScore.name'),
					'TeamPlayer' => array(
						'Team' => array(
							'conditions' => array('Team.is_gpl_team' => true) 
						)
					)
				)
			)
		));
		return $bowlers;
	}

}
