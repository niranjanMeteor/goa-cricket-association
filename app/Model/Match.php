<?php
App::uses('AppModel', 'Model');
App::uses('ResultType', 'Lib/Enum');
App::uses('MatchLevel', 'Lib/Enum');
App::uses('InningNumber', 'Lib/Enum');
/**
 * Match Model
 *
 * @property MatchInningScore $MatchInningScore
 * @property MatchPlayer $MatchPlayer
 */
class Match extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'MatchInningScore' => array(
			'className' => 'MatchInningScore',
			'foreignKey' => 'match_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => ''
		),
		'MatchPlayer' => array(
			'className' => 'MatchPlayer',
			'foreignKey' => 'match_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => ''
		)
	);

	public $belongsTo = array(
		'FirstTeam' => array(
			'className' => 'Team',
			'foreignKey' => 'first_team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'SecondTeam' => array(
			'className' => 'Team',
			'foreignKey' => 'second_team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TossWinningTeam' => array(
			'className' => 'Team',
			'foreignKey' => 'toss_winning_team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'MatchWinningTeam' => array(
			'className' => 'Team',
			'foreignKey' => 'winning_team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getBowlingTeam($matchId,$battingTeamId) {
		$bowlingTeam = array();
		$match = $this->find('first',array(
			'conditions' => array(
				'Match.id' => $matchId
			),
			'fields' => array('id','first_team_id','second_team_id'),
			'contain' => array(
				'FirstTeam' => array(
					'fields' => array('id','name','image','dir','location')
				),
				'SecondTeam' => array(
					'fields' => array('id','name','image','dir','location')
				)
			)
		));
		if ($match['Match']['first_team_id'] == $battingTeamId) {
			$bowlingTeam = $match['SecondTeam'];
		} else if ($match['Match']['second_team_id'] == $battingTeamId	) {
			$bowlingTeam = $match['FirstTeam'];
		}
		return $bowlingTeam;
	}

	public function getMatchDataForScore($matchId) {
		$match = $this->find('first', array(
			'conditions' => array(
				'Match.id' => $matchId
			),
			'contain' => array(
				'TossWinningTeam' => array(
					'fields' => array('id','name')
				),
				'MatchWinningTeam' => array(
					'fields' => array('id','name')
				)
			)
		));
		return $match;
	}

	public function getBatsmenList($matchId, $inningsNumber, $matchInningScoreId) {
		$match = $this->findById($matchId);
		$matchInning = $this->MatchInningScore->findByMatchIdAndInning($matchId, $inningsNumber);
		$battingTeamId = $matchInning['MatchInningScore']['team_id'];
		$batsmen = $this->MatchPlayer->getBatsmen($matchId, $battingTeamId, $matchInningScoreId);
		return $batsmen;
	}
	public function getBowlerList($matchId, $inningsNumber) {
		$match = $this->findById($matchId);
		$matchInning = $this->MatchInningScore->findByMatchIdAndInning($matchId, $inningsNumber);
		$battingTeamId = $matchInning['MatchInningScore']['team_id'];
		$matchTeams = array($match['Match']['first_team_id'], $match['Match']['second_team_id']);
		$bowlingTeamId = array_values(array_diff($matchTeams, array($battingTeamId)))[0];
		$bowlers = $this->MatchPlayer->getBowlers($matchId, $bowlingTeamId);
		return $bowlers;
	}
	public function getBowlingTeamPlayersList($matchId, $inningsNumber) {
		$match = $this->findById($matchId);
		$matchInning = $this->MatchInningScore->findByMatchIdAndInning($matchId, $inningsNumber);
		$battingTeamId = $matchInning['MatchInningScore']['team_id'];
		$matchTeams = array($match['Match']['first_team_id'], $match['Match']['second_team_id']);
		$bowlingTeamId = array_values(array_diff($matchTeams, array($battingTeamId)))[0];
		$players = $this->MatchPlayer->getPlayers($matchId, $bowlingTeamId);
		return $players;
	}

	public function setMatchInProgress($matchId) {
		$data = array(
			'Match' => array(
				'id' => $matchId,
				'in_progress' => true
			)
		);
		if ($this->save($data)) {
			return true;
		}
		return false;
	}

	public function getListofMatchesEligibleForScoring() {
		return $this->find('list',array(
			'conditions' => array(
				'start_date_time <=' => date('Y-m-d H:i:s'),
				'result_type' => [null,0],
				'is_complete' => [false,0,null]	
			)
		));
	}

	public function updateMatchTargetForChase($id) {
		$match = $this->findById($id);
		$match = $match['Match'];
		if (!empty($match) && ($match['level'] == Matchlevel::T20 || $match['level'] == Matchlevel::ODI)) {
			$matchInning = $this->MatchInningScore->findByMatchIdAndInning($id,InningNumber::FIRST);
			$matchInning = $matchInning['MatchInningScore'];
			$targetRuns = $matchInning['runs']+1;
			$data = array(
				'Match' => array(
					'id' => $id,
					'target' => $targetRuns
				)
			);
			if ($this->save($data)) {
				return true;
			}
		}		
		return false;
	}

	public function getCurrentMatchData() {
		return $this->find('first',array(
			'conditions' => array(
				'start_date_time <=' => date('Y-m-d H:i:s'),
				'in_progress' => true
			),
			'order' => 'Match.start_date_time DESC'
		));
	}

	public function getNextMatchInSchedule() {
		$nextScheduledMatch = array();
		$lastCompletedMatch = $this->find('first',array(
			'conditions' => array(
				'is_complete' => true
			),
			'order' => 'Match.start_date_time DESC'
		));
		if (!empty($lastCompletedMatch)) {
			$nextScheduledMatch = $this->find('first',array(
				'conditions' => array(
					'start_date_time >' => date('Y-m-d H:i:s', strtotime($lastCompletedMatch['Match']['start_date_time']))
				),
				'order' => 'Match.start_date_time ASC',
				'contain' => array(
					'FirstTeam' => array(
						'fields' => array('id','name','image','dir','location')
					),
					'SecondTeam' => array(
						'fields' => array('id','name','image','dir','location')
					)
				)
			));
		}
		return $nextScheduledMatch;
	}

	public function saveTossData($data) {
		$match = $this->findById($data['Match']['id']);
		$firstTeamId = $match['Match']['first_team_id'];
		$secondTeamId = $match['Match']['second_team_id'];
		if (($data['Match']['toss_decision'] == TossDecision::BATTING) && ($firstTeamId == $data['Match']['toss_winning_team_id'])) {
			$data['MatchInningScore'] = array(
				array('inning' => 1, 'match_id' => $match['Match']['id'], 'team_id' => $firstTeamId),
				array('inning' => 2, 'match_id' => $match['Match']['id'], 'team_id' => $secondTeamId)
			);
		} else if (($data['Match']['toss_decision'] == TossDecision::BATTING) && ($secondTeamId == $data['Match']['toss_winning_team_id'])) {
			$data['MatchInningScore'] = array(
				array('inning' => 1, 'match_id' => $match['Match']['id'], 'team_id' => $secondTeamId),
				array('inning' => 2, 'match_id' => $match['Match']['id'], 'team_id' => $firstTeamId)
			);
		} else if (($data['Match']['toss_decision'] == TossDecision::BOWLING) && ($firstTeamId == $data['Match']['toss_winning_team_id'])) {
			$data['MatchInningScore'] = array(
				array('inning' => 1, 'match_id' => $match['Match']['id'], 'team_id' => $secondTeamId),
				array('inning' => 2, 'match_id' => $match['Match']['id'], 'team_id' => $firstTeamId)
			);
		} else if (($data['Match']['toss_decision'] == TossDecision::BOWLING) && ($secondTeamId == $data['Match']['toss_winning_team_id'])) {
			$data['MatchInningScore'] = array(
				array('inning' => 1, 'match_id' => $match['Match']['id'], 'team_id' => $firstTeamId),
				array('inning' => 2, 'match_id' => $match['Match']['id'], 'team_id' => $secondTeamId)
			);
		}
		if ($this->saveAssociated($data, array('deep' => true))) {
			return true;
		}
		return false;
	}
	public function getWonMatchCount($teamId) {
		return $this->find('count', array(
			'conditions' => array(
				'winning_team_id' => $teamId
			)
		));
	}
	public function getTiedMatchCount($teamId) {
		return $this->find('count', array(
			'conditions' => array(
				'result_type' => ResultType::TIE,
				'OR' => array(
					'first_team_id' => $teamId,
					'second_team_id' => $teamId
				)
			)
		));
	}

	public function getNoResultMatchCount($teamId) {
		return $this->find('count', array(
			'conditions' => array(
				'OR' => array(
					'result_type' => ResultType::ABANDONED,
					'result_type' => ResultType::CANCELLED
				),
				'AND' => array(
					'OR' => array(
						'first_team_id' => $teamId,
						'second_team_id' => $teamId
					)
				)
			)
		));
	}
	
}
