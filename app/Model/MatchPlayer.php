<?php
App::uses('AppModel', 'Model');
App::uses('PlayerRole', 'Lib/Enum');
/**
 * MatchPlayer Model
 *
 * @property Match $Match
 * @property Team $Team
 * @property Player $Player
 */
class MatchPlayer extends AppModel {

	public $validate = array(
	  'player_id' => array(
	    'unique' => array(
	      'rule' => array('checkUnique', array('match_id', 'team_id', 'player_id'), false), 
	      'message' => 'Player already exists in the same team for this match'
	    )
	  )
	);


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Match' => array(
			'className' => 'Match',
			'foreignKey' => 'match_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Team' => array(
			'className' => 'Team',
			'foreignKey' => 'team_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Player' => array(
			'className' => 'Player',
			'foreignKey' => 'player_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function checkPlayerRole($matchId,$playerId,$role) {
		return $this->find('count',array(
			'conditions' => array(
				'match_id' => $matchId,
				'player_id' => $playerId,
				'role' => $role
			)
		));
	}

	public function isPlayerCaptain($matchId,$playerId) {
		return $this->find('count',array(
			'conditions' => array(
				'match_id' => $matchId,
				'player_id' => $playerId,
				'is_captain' => true
			)
		));
	}

	public function getDidNotBatPlayers($matchId,$teamId,$excludeList) {
		$didNotBatPlayers = array();
		$matchPlayers = $this->find('all',array(
			'conditions' => array(
				'MatchPlayer.match_id' => $matchId,
				'MatchPlayer.team_id' => $teamId,
				'MatchPlayer.player_id !=' => $excludeList,
				'MatchPlayer.role !=' => PlayerRole::TWELFTH_MAN
			),
			'contain' => array(
				'Player' => array()
			)
		));
		foreach ($matchPlayers as $key => $player) {
			$didNotBatPlayers[$key]['id'] = $player['Player']['id'];
			$didNotBatPlayers[$key]['name'] = $player['Player']['name'];
		}
		return $didNotBatPlayers;
	}

	public function getBatsmen($matchId, $battingTeamId, $matchInningScoreId) {
		$batsmenIds = $this->find('list', array(
			'conditions' => array(
				'match_id' => $matchId,
				'team_id' => $battingTeamId,
				'NOT' => array(
					'is_substitute' => false,
				),
				'NOT' => array(
					'role' => PlayerRole::TWELFTH_MAN
				)
			),
			'fields' => array(
				'id', 'player_id'
			)
		));
		$excludeBatsmen = $this->Match->MatchInningScore->MatchBatsmanScore->getAlreadyDismissedBatsmen($matchInningScoreId);
		$eligiblePlayers = array_diff($batsmenIds, $excludeBatsmen);
		$batsmen = $this->Player->getBatsmenList($eligiblePlayers);
		return $batsmen;
	}
	public function getBowlers($matchId, $bowlingTeamId) {
		$bowlersIds = $this->find('list', array(
			'conditions' => array(
				'match_id' => $matchId,
				'team_id' => $bowlingTeamId,
				'NOT' => array(
					'is_substitute' => false,
				),
				'NOT' => array(
					'role' => PlayerRole::TWELFTH_MAN
				)
			),
			'fields' => array(
				'id', 'player_id'
			)
		));
		$bowlers = $this->Player->getBowlersList($bowlersIds);
		return $bowlers;
	}
	public function getPlayers($matchId, $bowlingTeamId) {
		$bowlersIds = $this->find('list', array(
			'conditions' => array(
				'match_id' => $matchId,
				'team_id' => $bowlingTeamId,
			),
			'fields' => array(
				'id', 'player_id'
			)
		));
		$bowlers = $this->Player->getBowlersList($bowlersIds);
		return $bowlers;
	}

	public function getPlayerRoleInMatch($matchId,$playerId) {
		$matchPlayer = $this->findByMatchIdAndPlayerId($matchId,$playerId);
		$matchPlayer = $matchPlayer['MatchPlayer'];
		return $matchPlayer['role'];
	}

}
