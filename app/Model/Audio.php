<?php
App::uses('AppModel', 'Model');
/**
 * Audio Model
 *
 */
class Audio extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

  public function saveFileName($fileName) {
    $data = array(
      'Audio' => array(
        'name' => $fileName
      )
    );
    $this->create();
    $this->save($data);
  }

}
