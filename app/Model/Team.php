<?php
App::uses('AppModel', 'Model');
/**
 * Team Model
 *
 * @property TeamFixture $TeamFixture
 * @property TeamPlayer $TeamPlayer
 */
class Team extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'TeamFixture' => array(
			'className' => 'TeamFixture',
			'foreignKey' => 'team_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'TeamPlayer' => array(
			'className' => 'TeamPlayer',
			'foreignKey' => 'team_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
    'TeamSupportStaff' => array(
      'className' => 'TeamSupportStaff',
      'foreignKey' => 'team_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
    ),
    'Bid' => array(
      'className' => 'Bid',
      'foreignKey' => 'team_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
    ),
    'BidHistory' => array(
      'className' => 'BidHistory',
      'foreignKey' => 'team_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
    ),
    'TeamOne' => array(
      'className' => 'Match',
      'foreignKey' => 'first_team_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    ),
    'TeamTwo' => array(
      'className' => 'Match',
      'foreignKey' => 'second_team_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    ),
    'TeamWinningToss' => array(
      'className' => 'Match',
      'foreignKey' => 'toss_winning_team_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    ),
    'TeamWinningMatch' => array(
      'className' => 'Match',
      'foreignKey' => 'winning_team_id',
      'conditions' => '',
      'fields' => '',
      'order' => ''
    )
	);
  
  public $hasOne = array(
    'AuctionTeam' => array(
      'className' => 'AuctionTeam',
      'foreignKey' => 'team_id',
      'dependent' => false,
      'conditions' => '',
      'fields' => '',
      'order' => '',
      'limit' => '',
      'offset' => '',
      'exclusive' => '',
      'finderQuery' => '',
      'counterQuery' => ''
    ),
  );

	public $actsAs = array(
    'Upload.Upload' => array(
      'image' => array(
        'fields' => array(
          'dir' => 'dir'
        ),
        'thumbnailSizes' => array(
          'xvga' => '1024x768',
          'vga' => '149x178',
          'thumb' => '80x80'
        ),
        'thumbnailMethod' => 'php'
      )
    )
  );

  public function getTeamById($id) {
  	$team = $this->find('first', array(
  		'conditions' => array(
  			'Team.id' => $id
      ),
      'contain' => array(
        'TeamPlayer' => array(
          'conditions' => array(
            'TeamPlayer.status' => TeamPlayerStatus::ACTIVE
          ),
          'Player'
        ),
				'TeamFixture',
        'TeamSupportStaff' => array('SupportStaff')
			)
		));


    if (!empty($team['TeamPlayer'])) {
      foreach ($team['TeamPlayer'] as $id => $player) {
        $battingConditions = array(
          'PlayerBattingRecord.player_id' => $player['Player']['id']
        );
        $bowlingConditions = array(
          'PlayerBowlingRecord.player_id' => $player['Player']['id']
        );
        if ($this->TeamPlayer->Player->PlayerBattingRecord->hasAny($battingConditions) || $this->TeamPlayer->Player->PlayerBowlingRecord->hasAny($bowlingConditions)) {
          $team['TeamPlayer'][$id]['has_profile'] = true;
        }
      }
    }
		return $team;
  }
  public function findAuctionTeams() {
    $teams = $this->find('all', array(
      'conditions' => array(
        'Team.is_gpl_team' => true
      ),
      'contain' => array(
        'AuctionTeam'
      )
    ));
    return $teams;
  }
  public function getFinalTeamList() {
   $teams = $this->find('all', array(
      'conditions' => array(
        'Team.is_gpl_team' => true
      ),
      'contain' => array(
        'AuctionTeam',
        'Bid' => array('Player')
      )
    ));
    // $teams = $this->formatFinalTeamList($teams);
    return $teams; 
  }

  public function formatFinalTeamList($teams) {
    foreach ($teams as $id => $team) {
      foreach ($team['Bid'] as $bidId => $bid) {
        $player = $bid;
        $playerImageUrl = 'http://' . Configure::read('webUrl') . '/files/player/image/' . $player['Player']['dir'] . '/' . 'xvga_' . $player['Player']['image'];
        if (@getimagesize($playerImageUrl)) {
          $playerImageUrl = $playerImageUrl;
        } else {
          $playerImageUrl = 'img/default-user.jpeg';
        }
        $teams[$id]['Bid'][$bidId]['Player']['image_url'] = $playerImageUrl;
      }
    }
    return $teams;
  }

  public function updateTeamsEligibilityForNextBid($nextBidAmount, $actualBasePrice) {
    $teams = $this->findAuctionTeams();
    foreach ($teams as $id => $team) {
      $required = $team['AuctionTeam']['required'];
      $budget = $team['AuctionTeam']['budget'];
      if (($nextBidAmount - $actualBasePrice + $required) > $budget) {
        $this->AuctionTeam->setInEligible($team['AuctionTeam']['id']);
      } else {
        $this->AuctionTeam->setEligible($team['AuctionTeam']['id']);
      }
    }
    $teams = $this->findAuctionTeams();
    $teams = $this->updateTeamsEligibilityForThisRound($teams);
    return $teams;
  }
  public function updateTeamsEligibilityForThisRound($teams) {
    $currentRound = $this->TeamPlayer->Player->PlayerAuctionGroup->AuctionGroup->Round->findByInProgress(true);
    foreach ($teams as $team) {
      if ($currentRound['Round']['auction_group_id'] == 1) {
        if ($this->hasBoughtPlayerFromEliteGroup($team['Team']['id'])) {
          $this->AuctionTeam->setInEligible($team['AuctionTeam']['id']);
        }
      } elseif (($currentRound['Round']['auction_group_id'] == 2) &&(!$team['Team']['is_local_team'])) {
        $this->AuctionTeam->setInEligible($team['AuctionTeam']['id']);
      } else if (($currentRound['Round']['auction_group_id'] == 2) && ($team['Team']['is_local_team'])) {
        if ($this->Bid->hasBoughtThreePlayersFromAGroup($team['Team']['id'])) {
          $this->AuctionTeam->setInEligible($team['AuctionTeam']['id']);
        }
      } else if (($currentRound['Round']['auction_group_id'] == 2) && ($team['Team']['is_local_team'])) {
        if ($this->Bid->hasBoughtSixPlayersFromBGroup($team['Team']['id'])) {
          $this->AuctionTeam->setInEligible($team['AuctionTeam']['id']);
        }
      } else if (($currentRound['Round']['auction_group_id'] == 2) && (!$team['Team']['is_local_team'])) {
        if ($this->Bid->hasBoughtFourPlayers($teamId)) {
          $this->AuctionTeam->setInEligible($team['AuctionTeam']['id']);
        } else {
          $this->AuctionTeam->setEligible($team['AuctionTeam']['id']);
        }
      }
      if (($currentRound['Round']['id'] == 3) &&(!$team['Team']['is_local_team'])) {
        $elitePlayerBought = $this->Bid->findByTeamId($team['Team']['id']);
        if (empty($elitePlayerBought)) {
          $newRequiredAmount = 140000;
          $this->AuctionTeam->updateRequiredValue($team['Team']['id'], $newRequiredAmount);
        }
      }
    }
    $teams = $this->findAuctionTeams();
    return $teams;
  }
  public function hasBoughtPlayerFromEliteGroup($teamId) {
    $elitePlayerBought = $this->Bid->findByTeamId($teamId);
    if (!empty($elitePlayerBought)) {
      return true;
    }
    return false;
  }
  public function updateRequiredForOutstationTeams() {
    $outstationTeams = $this->getOutstationTeams();
    foreach ($outstationTeams as $outstationTeam) {
      $elitePlayerBought = $this->Bid->findByTeamId($outstationTeam['Team']['id']);
      if (empty($elitePlayerBought)) {
        $newRequiredAmount = 140000;
        $this->AuctionTeam->updateRequiredValue($outstationTeam['Team']['id'], $newRequiredAmount);
      }
    }
  }
  public function getOutstationTeams() {
    return $this->find('all', array(
      'conditions' => array(
        'Team.is_gpl_team' => true,
        'Team.is_local_team' => false
      )
    ));
  }
  public function getTeamsForMatch($matchId) {
    $teams = array();
    $match = $this->TeamOne->findById($matchId);
    if (!empty($match['TeamOne']['first_team_id']) && !empty($match['TeamOne']['second_team_id'])) {
      $teams = $this->find('list', array(
        'conditions' => array(
          'id' => array($match['TeamOne']['first_team_id'], $match['TeamOne']['second_team_id']),
        )
      ));
    }
    return $teams;
  }
  public function getAllGplTeams() {
    return $this->find('all', array(
      'conditions' => array(
        'is_gpl_team' => true
      )
    ));
  }
}
