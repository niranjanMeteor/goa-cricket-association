<?php
App::uses('AppModel', 'Model');
/**
 * RemoteAction Model
 *
 */
class RemoteAction extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'code' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	public function activateSoldPlayer() {
		$this->clearAllActiveStatus();
		$this->updateAll(
			array('RemoteAction.is_active' => true),
			array('RemoteAction.code' => 300)
		);
	}
	public function activateBidding() {
		$this->clearAllActiveStatus();
		return $this->updateAll(
			array('RemoteAction.is_active' => true),
			array('RemoteAction.code' => 200)
		);
	}
	public function clearAllActiveStatus() {
		$this->updateAll(
			array(
				'RemoteAction.is_active' => false
			)
		);
	}
	public function displayResults() {
		$this->clearAllActiveStatus();
		return $this->updateAll(
			array('RemoteAction.is_active' => true),
			array('RemoteAction.code' => 400)
		);
	}
	public function pauseBidding() {
		$this->clearAllActiveStatus();
		return $this->updateAll(
			array('RemoteAction.is_active' => true),
			array('RemoteAction.code' => 600)
		);
	}
}
