<?php
require_once APP . '../vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
/**
 * AppShell file
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         CakePHP(tm) v 2.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Shell', 'Console');
App::uses('HttpSocket', 'Network/Http');

/**
 * Application Shell
 *
 * Add your application-wide methods in the class below, your shells
 * will inherit them.
 *
 * @package       app.Console.Command
 */
class RabbitShell extends AppShell {

  public function send() {
    $id = 1;
    $connection = new AMQPConnection('localhost', 5672, 'guest', 'guest');
    $channel = $connection->channel();
    $bucketName = 'update_score';
    $channel->queue_declare($bucketName, false, false, false, false);
    $scoreUrl = Configure::read('scoreUrl') . $id;
    $HttpSocket = new HttpSocket();
    $results = $HttpSocket->get($scoreUrl);
    $json = $results['body'];
    $msg = new AMQPMessage($json);
    $channel->basic_publish($msg, '', $bucketName);

    echo " [x] Update score '\n";
    $channel->close();
    $connection->close();
  }
}
