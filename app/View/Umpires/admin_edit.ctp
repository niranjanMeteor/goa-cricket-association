	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Umpire'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('Umpire', array('role' => 'form', 'type' => 'file')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('name', array('class' => 'form-control', 'placeholder' => 'Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('email', array('class' => 'form-control', 'placeholder' => 'Email'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('grade', array('class' => 'form-control', 'placeholder' => 'Grade'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('dob', array('class' => 'form-control', 'placeholder' => 'Dob', 'minYear' => date('Y') - 90, 'maxYear' => date('Y')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('address', array('class' => 'form-control', 'placeholder' => 'Address'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('residence_number', array('class' => 'form-control', 'placeholder' => 'Residence Number'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('mobile', array('class' => 'form-control', 'placeholder' => 'Mobile'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('skype_id', array('class' => 'form-control', 'placeholder' => 'Skype Id', 'type' => 'text'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('accreditation_year', array('class' => 'form-control', 'placeholder' => 'Accreditation Year'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('image', array('class' => 'form-control', 'placeholder' => 'Image', 'type' => 'file'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('dir', array('class' => 'form-control', 'placeholder' => 'Dir', 'type' => 'hidden'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

