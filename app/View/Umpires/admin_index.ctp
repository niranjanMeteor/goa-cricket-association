	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Umpires'); ?></h1>
		</div><!-- end col md 12 -->
	</div><!-- end row -->


	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<thead>
			<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('email'); ?></th>
		<th><?php echo $this->Paginator->sort('grade'); ?></th>
		<th><?php echo $this->Paginator->sort('dob'); ?></th>
		<th><?php echo $this->Paginator->sort('address'); ?></th>
		<th><?php echo $this->Paginator->sort('residence_number'); ?></th>
		<th><?php echo $this->Paginator->sort('mobile'); ?></th>
		<th><?php echo $this->Paginator->sort('skype_id'); ?></th>
		<th><?php echo $this->Paginator->sort('accreditation_year'); ?></th>
		<th><?php echo $this->Paginator->sort('image'); ?></th>
		<th><?php echo $this->Paginator->sort('dir'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('modified'); ?></th>
		<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($umpires as $umpire): ?>
					<tr>
						<td><?php echo h($umpire['Umpire']['id']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['name']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['email']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['grade']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['dob']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['address']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['residence_number']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['mobile']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['skype_id']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['accreditation_year']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['image']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['dir']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['created']); ?>&nbsp;</td>
						<td><?php echo h($umpire['Umpire']['modified']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $umpire['Umpire']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $umpire['Umpire']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $umpire['Umpire']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $umpire['Umpire']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
		</tbody>
	</table>

	<p>
		<small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
	</p>

	<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
	<ul class="pagination pagination-sm">
		<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
	</ul>
	<?php } ?>
