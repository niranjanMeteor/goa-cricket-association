	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Umpire'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Name'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Email'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['email']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Grade'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['grade']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Dob'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['dob']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Address'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['address']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Residence Number'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['residence_number']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Mobile'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['mobile']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Skype Id'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['skype_id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Accreditation Year'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['accreditation_year']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Image'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['image']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Dir'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['dir']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($umpire['Umpire']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>