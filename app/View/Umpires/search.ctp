
<?php echo $this->Html->css('search', array('inline' => false)); ?>
  <div class="row">
    <div class="col-md-9 col-md-offset-1">
      <div class="search-form">
        <?php echo $this->Form->create('Umpire', array('type' => 'get', 'role' => 'form')) ?>
        <div class="form-group">
          <?php echo $this->Form->input('q', array('class' => 'form-control', 'label' => false, 'afterIn')); ?>
        </div>
        <?php echo $this->Form->button(__('Search <span class="fa fa-search"></span>'), array('class' => 'btn btn-success', 'escape' => false)) ?>
        <?php $this->Form->end(); ?>
        <form>
        </form>
      </div>
    </div>
  </div>
<?php if ( ! empty($umpires)): ?>
  <div clas="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="result-list table-responsive">
        <table class="table table-bordered table-hover search-table">

          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('image', 'Photo'); ?></th>
              <th><?php echo $this->Paginator->sort('name'); ?></th>
              <th><?php echo $this->Paginator->sort('dob', 'Birthday'); ?></th>
              <th><?php echo $this->Paginator->sort('grade'); ?></th>
              <th><?php echo $this->Paginator->sort('accreditation_year'); ?></th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($umpires as $umpire): ?>
              <tr>
                <td>
                  <?php echo $this->Html->image("{$umpire['Umpire']['dir']}/thumb_{$umpire['Umpire']['image']}", array('pathPrefix' => 'files/umpire/image/')) ?>
                </td>
                <td>
                  <?php echo __($umpire['Umpire']['name']); ?>
                </td>
                <td>
                  <?php echo date('F d, Y', strtotime($umpire['Umpire']['dob'])); ?>
                </td>
                <td>
                  <?php echo __(UmpireGrade::stringValue($umpire['Umpire']['grade'])); ?>
                </td>
                <td>
                  <?php echo __($umpire['Umpire']['accreditation_year']) ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>

        </table>
      </div>
      <p>
        <small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
      </p>

      <?php
          $params = $this->Paginator->params();
          if ($params['pageCount'] > 1) {
          ?>
      <ul class="pagination pagination-sm">
        <?php
              echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
              echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
              echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
            ?>
      </ul>
      <?php } ?>
    </div>
  </div>
<?php else: ?>

  <h3 class="text-center">No Results to display</h3>

<?php endif; ?>