	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Team'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($team['Team']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Name'); ?></th>
		<td>
			<?php echo h($team['Team']['name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Location'); ?></th>
		<td>
			<?php echo h($team['Team']['location']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Tagline'); ?></th>
		<td>
			<?php echo h($team['Team']['tagline']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Description'); ?></th>
		<td>
			<?php echo h($team['Team']['description']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Image'); ?></th>
		<td>
			<?php echo h($team['Team']['image']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Dir'); ?></th>
		<td>
			<?php echo h($team['Team']['dir']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($team['Team']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($team['Team']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>