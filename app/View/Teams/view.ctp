<?php echo $this->Html->css('view_team', array('inline' => false)); ?>
<div class="container">
  <div class="row">
    <div class="col-md-4">
      <div class="team-info card">
        <?php if (!empty($team['Team']['image'])): ?>
          <?php echo $this->Html->link($this->Html->image("{$team['Team']['dir']}/{$team['Team']['image']}", array('pathPrefix' => 'files/team/image/')), array('controller' => 'teams', 'action' => 'view',$team['Team']['id']), array('escape' => false, 'class' => 'team-pic'));  ?>
        <?php endif; ?>
        <div class="team-name"><?php echo __($team['Team']['name']) ?></div>
        <div class="team-location"><span class="fa fa-map-marker"></span> <?php echo __($team['Team']['location']) ?></div>
        <div class="team-tagline"><?php echo __($team['Team']['tagline']) ?></div>
        <div class="team-description"><?php echo __($team['Team']['description']); ?></div>
      </div>
      <div class="support-staffs card">
        <div class="title">
          <span>Support Staffs</span>
        </div>
        <?php if ( ! empty($team['TeamSupportStaff'])): ?>
          <div class="table-responsive">
            <table class="table table-hover staff-table custom-table">
              <thead>
                <tr>
                  <th>Photo</th>
                  <th>Name</th>
                  <th>Role</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($team['TeamSupportStaff'] as $supportStaff): ?>
                  <tr>
                    <td>
                      <?php if ( ! empty($supportStaff['SupportStaff']['image'])): ?>
                        <?php echo $this->Html->image("{$supportStaff['SupportStaff']['dir']}/thumb_{$supportStaff['SupportStaff']['image']}", array('pathPrefix' => 'files/support_staff/image/', 'class' => 'img-circle player-pic')); ?>
                      <?php endif; ?>
                    </td>
                    <td>
                      <?php echo __($supportStaff['SupportStaff']['name']); ?>
                    </td>
                    <td>
                      <?php echo __(SupportStaffType::stringValue($supportStaff['SupportStaff']['role'])); ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="col-md-5">
      <div class="players card">
        <div class="title">
          <span>Meet Our Team</span>
        </div>
        <?php if(!empty($team['TeamPlayer'])): ?>
          <div class="team-list">
            <div class="table-responsive">
              <table class="table table-hover players-table custom-table">
                    <thead>
                      <tr>
                        <th>Photo</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($team['TeamPlayer'] as $player): ?>
                        <tr>
                          <td>
                            <?php if ( ! empty($player['Player']['image'])): ?>
                              <?php echo $this->Html->image("{$player['Player']['dir']}/thumb_{$player['Player']['image']}", array('pathPrefix' => 'files/player/image/', 'class' => 'img-circle player-pic')); ?>
                            <?php endif; ?>
                          </td>
                          <td>
                            <?php echo __($player['Player']['name']) ?>
                          </td>
                          <td>
                            <?php $roleArray = array(); ?>
                            <?php if ( ! empty(BattingHand::stringValue($player['Player']['batting_arm']))) $roleArray[] = BattingHand::stringValue($player['Player']['batting_arm']) . ' Bat'; ?>
                            <?php if ( ! empty(PlayerArm::stringValue($player['Player']['bowling_arm']))) $roleArray[] = PlayerArm::stringValue($player['Player']['bowling_arm']) . ' ' . BowlingStyle::stringValue($player['Player']['bowling_style'])?>
                            <?php echo __(
                              join(', ', $roleArray)
                            ); ?>
                          </td>
                          <td>
                            <?php if ( ! empty($player['has_profile'])): ?>
                              <?php echo $this->Html->link(__('View Profile'), array('controller' => 'players', 'action' => 'view', $player['Player']['id'])); ?>
                            <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
              </table>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="col-md-3">
      <div class="team-fixtures card">
        <div class="title">
          <span>Team Fixtures</span>
        </div>
        <?php if (!empty($team['TeamFixture'])): ?>
          <div class="table-responsive">
            <table class="table table-hover fixture-table custom-table">
              <thead>
                <tr>
                  <th>Start Date</th>
                  <th>End Date</th>
                  <th>Against</th>
                  <th>Venue</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($team['TeamFixture'] as $fixture): ?>
                  <tr>
                    <td>
                      <?php echo date('dS M Y', strtotime($fixture['start_date_time'])) ?>
                    </td>
                    <td>
                      <?php echo date('dS M Y', strtotime($fixture['end_date_time'])) ?>
                    </td>
                    <td>
                      <?php echo __(ucfirst($fixture['against'])); ?>
                    </td>
                    <td>
                      <?php echo __($fixture['venue']); ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
                
              </tbody>
            </table>
          </div>
        
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>