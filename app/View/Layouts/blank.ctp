<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Goa Cricket Association || Official Website</title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Goa Cricket Association" name="description">
  <meta content="Goa, Cricket, Association, Goa Cricket, Goa Cricket Association" name="keywords">

  <link rel="shortcut icon" href="favicon.ico">
  <!-- Fonts START -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->
  <?php
    echo $this->Html->meta('icon');

    echo $this->fetch('meta');
    echo $this->Html->css('bootstrap.min');
    ?>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <?php
    echo $this->Html->css('main');
    echo $this->fetch('css');
    echo $this->Html->script('jquery-1.11.1.min');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('jquery.smooth-scroll');
    echo $this->Html->script('general');
    echo $this->fetch('script');
  ?>
  <script type="text/javascript">
    var WEBROOT = '<?php echo $this->request->webroot; ?>';
  </script>          
</head>
<body>

  <?php echo $this->Session->flash(); ?>
  <?php echo $this->fetch('content'); ?>

</body>
</html>