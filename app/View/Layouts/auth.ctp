<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Goa Cricket Association || Official Website</title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <meta content="Goa Cricket Association" name="description">
  <meta content="Goa, Cricket, Association, Goa Cricket, Goa Cricket Association" name="keywords">

  <link rel="shortcut icon" href="favicon.ico">
  <!-- Fonts START -->
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->
  <?php
    echo $this->Html->meta('icon');

    echo $this->fetch('meta');
    echo $this->Html->css('bootstrap.min');
    echo $this->Html->css('font-awesome.min');
    echo $this->Html->css('auth');
    echo $this->fetch('css');
    echo $this->Html->script('jquery-1.11.1.min');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('jquery.smooth-scroll');
    echo $this->Html->script('auth');
    echo $this->fetch('script');
  ?>
  <script type="text/javascript">
    var WEBROOT = '<?php echo $this->request->webroot; ?>';
  </script>          
  <!-- Global styles END --> 
   
  <!-- Page level plugin styles START -->
  <!-- <link href="../../assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet"> -->
  <!-- <link href="../../assets/global/plugins/carousel-owl-carousel/owl-carousel/owl.carousel.css" rel="stylesheet"> -->
  <!-- <link href="../../assets/global/plugins/slider-revolution-slider/rs-plugin/css/settings.css" rel="stylesheet"> -->
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <!-- <link href="../../assets/global/css/components.css" rel="stylesheet"> -->
  <!-- <link href="../../assets/frontend/layout/css/style.css" rel="stylesheet"> -->
  <!-- <link href="../../assets/frontend/pages/css/style-revolution-slider.css" rel="stylesheet">-->
  <!-- <link href="../../assets/frontend/layout/css/style-responsive.css" rel="stylesheet"> -->
  <!-- <link href="../../assets/frontend/layout/css/themes/red.css" rel="stylesheet" id="style-color"> -->
  <!-- <link href="../../assets/frontend/layout/css/custom.css" rel="stylesheet"> -->
  <!-- Theme styles END -->
</head>
<body>
  <div class="header-wrapper" id="home">
    <!-- BEGIN TOP BAR -->
    <div class="pre-header">
      <div class="container">
        <div class="row">

          <div class="col-md-6 col-sm-6 additional-contact-info">
            <ul class="list-unstyled list-inline">
              <li>
                <i class="fa fa-phone"></i>
                <span>+91 (0832) 2416844</span>
              </li>
              <li>
                <i class="fa fa-envelope-o"></i>
                <span>info@goacricket.org</span>
              </li>
            </ul>
          </div>

          <div class="col-md-6 col-sm-6 additional-nav">
            <ul class="list-unstyled list-inline pull-right">
              <li><?php echo $this->Html->link(__('Member Login'), array('controller' => 'users', 'action' => 'login')); ?></li>
              <li><?php echo $this->Html->link(__('Registration'), array('controller' => 'users', 'action' => 'register')); ?></li>
            </ul>
          </div>

        </div>
      </div>
    </div>
    <!-- END TOP BAR -->
    <!-- BEGIN HEADER -->
      <div class="header">
        <div class="container">
          <?php echo $this->Html->link($this->Html->image('gca-site-logo.png'), '/', array('escape' => false, 'class' => 'site-logo')); ?>
          <a href="#" class="mobile-toggle">
            <i class="fa fa-bars"></i>
          </a>
        </div>
      </div>
    <!-- END HEADER -->
  </div>


  <?php echo $this->Session->flash(); ?>
  <?php echo $this->fetch('content'); ?>



</body>
</html>