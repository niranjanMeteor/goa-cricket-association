<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo __('Goa Cricket Association'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->fetch('meta');

		echo $this->Html->css('bootstrap.min');
    echo $this->Html->css('font-awesome.min');
    echo $this->Html->css('simple-line-icons');
    echo $this->Html->css('default');
		echo $this->fetch('css');
		
		echo $this->Html->script('jquery-1.11.1.min');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('default');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="wrapper">

      <div id="menu-toggle-item-wrapper">
        <div class="toggle-menu-content">
          <div class="sidebar-user-name">
            Divyanshu Das
          </div>
          <ul class="list-unstyled sidebar-list">
            <li>
              <span class="badge pull-right">5</span>
              <a href="#"><span class="fa fa-envelope"></span> Messages</a>
            </li>
            <li>
              <span class="badge pull-right">5</span>
              <a href="#"><span class="fa fa-bell"></span> Notifications</a>
            </li>
            <li>
              <span class="badge pull-right">5</span>
              <a href="#"><span class="fa fa-user"></span> Requests</a>
            </li>
          </ul>
          <hr class="sidebar-separator" />
          <ul class="list-unstyled sidebar-list">
            <li>
              <a href="#"><span class="fa fa-cog"></span> Account Settings</a>
            </li>
            <li>
              <a href="#"><span class="fa fa-clock-o"></span> Set Reminder</a>
            </li>
            <li>
              <a href="#"><span class="fa fa-key"></span> Logout</a>
            </li>
          </ul>
        </div>
      </div>

      <div id="page-content">
        <!-- BEGIN HEADER -->
        <header>
          <div class="page-header">

            <div class="page-header-top">

              <div class="container">

                <div class="page-logo">
                  <?php echo $this->Html->link($this->Html->image('gca-site-logo.png', array('class' => 'logo-default')),'/', array('escape' => false)); ?>
                </div>

                <a href="#" class="menu-toggler">
                	<?php echo $this->Html->image('member1.jpg', array('class' => 'img-circle')); ?>
                  <span class="caret menu-toggle-caret"></span>
                </a>

                <div class="top-menu">
					        <ul class="nav navbar-nav pull-right">
					          <!-- BEGIN NOTIFICATION DROPDOWN -->
					          <li class="dropdown dropdown-extended dropdown-dark dropdown-notification" id="header_notification_bar">
					            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					            <i class="fa fa-bell"></i>
					            </a>
					            
					          </li>
					          <!-- END NOTIFICATION DROPDOWN -->
					          <!-- BEGIN TODO DROPDOWN -->
					          <li class="dropdown dropdown-extended dropdown-dark dropdown-tasks" id="header_task_bar">
					            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					            	<i class="fa fa-calendar"></i>
					            </a>
					            
					          </li>
					          <!-- END TODO DROPDOWN -->
					          <li class="droddown dropdown-separator">
					            <span class="separator"></span>
					          </li>
					          <!-- BEGIN USER LOGIN DROPDOWN -->
					          <li class="dropdown dropdown-user dropdown-dark">
					            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					            <?php echo $this->Html->image('member1.jpg', array('class' => 'img-circle')); ?>
					            <span class="username username-hide-mobile">Nick</span>
					            </a>
					          </li>
					          <!-- END USER LOGIN DROPDOWN -->
					        </ul>
					      </div>


              </div>
              <div class="page-header-menu">
              	<div class="container">
	                <div class="hor-menu">
	                	<?php echo $this->element('navigation'); ?>
	                </div>
              	</div>
              </div>

            </div>

          </div>
        </header>
        <!-- END HEADER -->
        <div id="content">
          
				<?php echo $this->Session->flash(); ?>

				<?php echo $this->fetch('content'); ?>
        </div>

      </div>

    </div>
</body>
</html>
