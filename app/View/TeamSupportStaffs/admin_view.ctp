	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Team Support Staff'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($teamSupportStaff['TeamSupportStaff']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Team'); ?></th>
		<td>
			<?php echo $this->Html->link($teamSupportStaff['Team']['name'], array('controller' => 'teams', 'action' => 'view', $teamSupportStaff['Team']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Support Staff'); ?></th>
		<td>
			<?php echo $this->Html->link($teamSupportStaff['SupportStaff']['id'], array('controller' => 'support_staffs', 'action' => 'view', $teamSupportStaff['SupportStaff']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('From Date'); ?></th>
		<td>
			<?php echo h($teamSupportStaff['TeamSupportStaff']['from_date']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('To Date'); ?></th>
		<td>
			<?php echo h($teamSupportStaff['TeamSupportStaff']['to_date']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($teamSupportStaff['TeamSupportStaff']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($teamSupportStaff['TeamSupportStaff']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>