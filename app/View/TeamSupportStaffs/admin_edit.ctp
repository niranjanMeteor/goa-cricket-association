	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Team Support Staff'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('TeamSupportStaff', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('team_id', array('class' => 'form-control', 'placeholder' => 'Team Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('support_staff_id', array('class' => 'form-control', 'placeholder' => 'Support Staff Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('from_date', array('class' => 'form-control', 'placeholder' => 'From Date', 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('to_date', array('class' => 'form-control', 'placeholder' => 'To Date', 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

