	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Round'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('Round', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('auction_group_id', array('class' => 'form-control', 'placeholder' => 'Auction Group Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('in_progress', array('class' => 'form-control', 'placeholder' => 'In Progress'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('is_completed', array('class' => 'form-control', 'placeholder' => 'Is Completed'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

