<?php echo $this->Html->css('gpl_auction', array('inline' => false)) ?>
<?php echo $this->Html->script('gpl_auction', array('inline' => false)); ?>


<div class="container auction">
  <div class="row">
    <div class="col-md-12 text-center heading">
      <h3>GPL Auction <?php echo __($player['AuctionGroup']['name']) ?> (Round <?php echo $roundCount; ?>) </h3>
      <hr />
    </div>
  </div>
  <div class="row amount-row">
    <div class="col-md-3">
      <div class="text-center amount min">
        <div class="arrow-right"></div>
        <div class="min-amount">
          Min Bid &nbsp;<span class="fa fa-inr"></span>&nbsp; <?php echo $player['AuctionGroup']['minimum_amount']; ?>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="text-center player-name"><?php echo $player['Player']['name']; ?></div>
    </div>
    <div class="col-md-3">
      <div class="text-center amount current">
        <div class="arrow-left"></div>
        <div class="current-amount">
          Current bid &nbsp;<span class="fa fa-inr"></span>&nbsp; <?php echo $currentBid; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row details">
    <div class="col-md-4">
      <div class="bidding-details">
        <h4 class="heading">Bidding Status</h4>
        <div class="bidder">
          <div class="left-section pull-left">
            <?php echo $this->Html->image(__('bidder_logo.png'), array('class' => 'bidder-logo')); ?>
          </div>
          <div class="right-section pull-left">
            <div class="bidder-name">
              <?php echo $this->Html->link(__('Sun Estates Warriors'), array('controller' => 'auction_teams', 'action' => 'view',1)); ?>
            </div>
            <div class="bidder-location">
              <span class="fa fa-map-marker"></span> Bangalore
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="bidder-time">
            <span class="fa fa-flag"></span> 11:30 AM
          </div>
        </div>
        <div class="bidder">
          <div class="left-section pull-left">
            <?php echo $this->Html->image(__('bidder_logo.png'), array('class' => 'bidder-logo')); ?>
          </div>
          <div class="right-section pull-left">
            <div class="bidder-name">
              <?php echo $this->Html->link(__('Sun Estates Warriors'), array('controller' => 'auction_teams', 'action' => 'view',1)); ?>
            </div>
            <div class="bidder-location">
              <span class="fa fa-map-marker"></span> Bangalore
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="bidder-time">
            <span class="fa fa-flag"></span> 11:30 AM
          </div>
        </div>
        <div class="bidder current">
          <div class="left-section pull-left">
            <?php echo $this->Html->image(__('bidder_logo.png'), array('class' => 'bidder-logo')); ?>
          </div>
          <div class="right-section pull-left">
            <div class="bidder-name">
              <?php echo $this->Html->link(__('Sun Estates Warriors'), array('controller' => 'auction_teams', 'action' => 'view',1)); ?>
            </div>
            <div class="bidder-location">
              <span class="fa fa-map-marker"></span> Bangalore
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="bidder-time">
            <span class="fa fa-flag"></span> 11:30 AM
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 text-center">
      <div class="player-image">
        <?php echo $this->Html->link($this->Html->image("{$player['Player']['dir']}/profile_{$player['Player']['image']}", array('pathPrefix' => 'files/player/image/', 'class' => 'img-circle player-pic')), array('controller' => 'players', 'action' => 'view',1), array('escape' => false)); ?>
      </div>
    </div>
    <div class="col-md-4">
        <div class="row">
    <div class="col-md-6">
      <?php echo $this->Html->link(__('Sold'), array('controller' => 'bids', 'action' => 'sold', $player['Player']['id']), array('class' => 'btn btn-primary btn-block', 'escape' => false)); ?>
    </div>
    <div class="col-md-6">
      <?php echo $this->Html->link(__('Next Bid <span class="glyphicon glyphicon-arrow-right"></span>'), array('controller' => 'bids', 'action' => 'next_bid', $player['Player']['id']), array('class' => 'btn btn-success btn-block', 'escape' => false)); ?>
    </div>
  </div>
  <div class="row">
    <br />
    <div class="col-md-6">
      <?php echo $this->Html->link(__('Pause'), array('controller' => 'bids', 'action' => 'pause'), array('class' => 'btn btn-primary btn-block', 'escape' => false)); ?>
    </div>
    <div class="col-md-6">
      <?php echo $this->Html->link(__('Bid and Pause'), array('controller' => 'bids', 'action' => 'bid_and_pause', $player['Player']['id']), array('class' => 'btn btn-primary btn-block', 'escape' => false)); ?>
    </div>
  </div>
  <div class="row">
    <br />
    <div class="col-md-6">
      <?php echo $this->Html->link(__('Restart Bidding on client'), array('controller' => 'remote_actions', 'action' => 'start_bidding'), array('class' => 'btn btn-primary btn-block', 'escape' => false)); ?>
    </div>
  </div>
    </div>
  </div>
  <div class="row auction-teams">
    <?php foreach ($teams as $id => $team): ?>
      <?php 
        if (!empty($team['Team']['current']))  {
          $className = ($team['Team']['current']) ? 'team active' : 'team';
        } else {
          $className = ($team['Team']['is_eligible_for_bid']) ? 'team' : 'team not-eligible';
        }
      ?>
      <div class="col-md-3">
        <div class="<?php echo $className; ?>">
          <span class="fa fa-flag"></span>
          <div class="team-details">
            <div class="pull-left left-section">
              <?php echo $this->Html->link($this->Html->image('bidder_logo.png', array('class' => 'team-logo')), array('controller' => 'bids', 'action' => 'add_bid',$player['Player']['id'], $team['Team']['id']), array('escape' => false)); ?>
            </div>
            <div class="pull-left right-section">
              <div class="team-name"><?php echo $team['Team']['name'] ?></div>
              <div class="team-location">
                <span class="fa fa-map-marker"></span> <?php echo $team['Team']['location'] ?>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="money-details">
            <div class="money-element">
              <div class="text pull-left">Total</div>
              <div class="value pull-right"><span class="fa fa-inr"></span> <?php echo ($team['AuctionTeam']['budget'] + $team['AuctionTeam']['spent']); ?></div>
              <div class="clearfix"></div>
            </div>
            <div class="money-element">
              <div class="text pull-left">Required</div>
              <div class="value pull-right"><span class="fa fa-inr"></span> <?php echo $team['AuctionTeam']['required']; ?></div>
              <div class="clearfix"></div>
            </div>
            <div class="money-element">
              <div class="text pull-left">Available</div>
              <div class="value pull-right"><span class="fa fa-inr"></span> <?php echo $team['AuctionTeam']['budget']; ?></div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>
</div>