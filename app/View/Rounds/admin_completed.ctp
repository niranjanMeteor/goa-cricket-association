<blockquote>
  <h1>GPL Auction is over.</h1>
  <div><?php echo $this->Html->link(__('Click to View Results'), array('controller' => 'rounds', 'action' => 'results', 'admin' => true), array('class' => 'btn btn-success')); ?></div>
  <div><?php echo $this->Html->link(__('Display Results'), array('controller' => 'remote_actions', 'action' => 'display_results', 'admin' => true), array('class' => 'btn btn-success')); ?></div>
</blockquote>