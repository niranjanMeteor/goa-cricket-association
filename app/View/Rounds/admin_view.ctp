	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Round'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($round['Round']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Auction Group'); ?></th>
		<td>
			<?php echo $this->Html->link($round['AuctionGroup']['name'], array('controller' => 'auction_groups', 'action' => 'view', $round['AuctionGroup']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('In Progress'); ?></th>
		<td>
			<?php echo h($round['Round']['in_progress']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Completed'); ?></th>
		<td>
			<?php echo h($round['Round']['is_completed']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($round['Round']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($round['Round']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>