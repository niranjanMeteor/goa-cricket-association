	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Player Auction Group'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($playerAuctionGroup['PlayerAuctionGroup']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Player'); ?></th>
		<td>
			<?php echo $this->Html->link($playerAuctionGroup['Player']['name'], array('controller' => 'players', 'action' => 'view', $playerAuctionGroup['Player']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Auction Group'); ?></th>
		<td>
			<?php echo $this->Html->link($playerAuctionGroup['AuctionGroup']['name'], array('controller' => 'auction_groups', 'action' => 'view', $playerAuctionGroup['AuctionGroup']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Bought'); ?></th>
		<td>
			<?php echo h($playerAuctionGroup['PlayerAuctionGroup']['is_bought']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Deleted'); ?></th>
		<td>
			<?php echo h($playerAuctionGroup['PlayerAuctionGroup']['is_deleted']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($playerAuctionGroup['PlayerAuctionGroup']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($playerAuctionGroup['PlayerAuctionGroup']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>