	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Player Auction Group'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('PlayerAuctionGroup', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('player_id', array('class' => 'form-control', 'placeholder' => 'Player Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('auction_group_id', array('class' => 'form-control', 'placeholder' => 'Auction Group Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('is_bought', array('class' => 'form-control', 'placeholder' => 'Is Bought'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('is_deleted', array('class' => 'form-control', 'placeholder' => 'Is Deleted'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

