	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Bid'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($bid['Bid']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Player'); ?></th>
		<td>
			<?php echo $this->Html->link($bid['Player']['name'], array('controller' => 'players', 'action' => 'view', $bid['Player']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Team'); ?></th>
		<td>
			<?php echo $this->Html->link($bid['Team']['name'], array('controller' => 'teams', 'action' => 'view', $bid['Team']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Buying Amount'); ?></th>
		<td>
			<?php echo h($bid['Bid']['buying_amount']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($bid['Bid']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($bid['Bid']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>