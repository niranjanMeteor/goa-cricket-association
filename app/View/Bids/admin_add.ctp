	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Add Bid'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('Bid', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('player_id', array('class' => 'form-control', 'placeholder' => 'Player Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('team_id', array('class' => 'form-control', 'placeholder' => 'Team Id', 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('buying_amount', array('class' => 'form-control', 'placeholder' => 'Buying Amount'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

