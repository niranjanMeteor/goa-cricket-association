	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Ticket Relation'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($ticketRelation['TicketRelation']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Primary I D'); ?></th>
		<td>
			<?php echo $this->Html->link($ticketRelation['PrimaryID']['title'], array('controller' => 'tickets', 'action' => 'view', $ticketRelation['PrimaryID']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Related I D'); ?></th>
		<td>
			<?php echo $this->Html->link($ticketRelation['RelatedID']['title'], array('controller' => 'tickets', 'action' => 'view', $ticketRelation['RelatedID']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Type'); ?></th>
		<td>
			<?php echo h($ticketRelation['TicketRelation']['type']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($ticketRelation['TicketRelation']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($ticketRelation['TicketRelation']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>