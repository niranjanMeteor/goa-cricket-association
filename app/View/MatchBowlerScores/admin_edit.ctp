	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Match Bowler Score'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('MatchBowlerScore', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('match_inning_score_id', array('class' => 'form-control', 'placeholder' => 'Match Inning Score Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('player_id', array('class' => 'form-control', 'placeholder' => 'Player Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('overs', array('class' => 'form-control', 'placeholder' => 'Overs'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('maidens', array('class' => 'form-control', 'placeholder' => 'Maidens'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('dot_balls', array('class' => 'form-control', 'placeholder' => 'Dot Balls'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('wickets', array('class' => 'form-control', 'placeholder' => 'Wickets'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('runs_conceded', array('class' => 'form-control', 'placeholder' => 'Runs Conceded'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('no_balls', array('class' => 'form-control', 'placeholder' => 'No Balls'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('wides', array('class' => 'form-control', 'placeholder' => 'Wides'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

