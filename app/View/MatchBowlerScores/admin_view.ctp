	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Match Bowler Score'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($matchBowlerScore['MatchBowlerScore']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Match Inning Score'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBowlerScore['MatchInningScore']['id'], array('controller' => 'match_inning_scores', 'action' => 'view', $matchBowlerScore['MatchInningScore']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Player'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBowlerScore['Player']['name'], array('controller' => 'players', 'action' => 'view', $matchBowlerScore['Player']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Overs'); ?></th>
		<td>
			<?php echo h($matchBowlerScore['MatchBowlerScore']['overs']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Maidens'); ?></th>
		<td>
			<?php echo h($matchBowlerScore['MatchBowlerScore']['maidens']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Dot Balls'); ?></th>
		<td>
			<?php echo h($matchBowlerScore['MatchBowlerScore']['dot_balls']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Wickets'); ?></th>
		<td>
			<?php echo h($matchBowlerScore['MatchBowlerScore']['wickets']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Runs Conceded'); ?></th>
		<td>
			<?php echo h($matchBowlerScore['MatchBowlerScore']['runs_conceded']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('No Balls'); ?></th>
		<td>
			<?php echo h($matchBowlerScore['MatchBowlerScore']['no_balls']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Wides'); ?></th>
		<td>
			<?php echo h($matchBowlerScore['MatchBowlerScore']['wides']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($matchBowlerScore['MatchBowlerScore']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($matchBowlerScore['MatchBowlerScore']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>