	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Remote Action'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($remoteAction['RemoteAction']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Name'); ?></th>
		<td>
			<?php echo h($remoteAction['RemoteAction']['name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Code'); ?></th>
		<td>
			<?php echo h($remoteAction['RemoteAction']['code']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Active'); ?></th>
		<td>
			<?php echo h($remoteAction['RemoteAction']['is_active']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($remoteAction['RemoteAction']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($remoteAction['RemoteAction']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>