
<li class="dropdown">
  <?php echo $this->Html->link(__('Users') . ' <b class="caret"></b>', 'javascript:void(0);', array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false)); ?>
  <ul class="dropdown-menu">
    <li><?php echo $this->Html->link(__('Manage Users'), array('controller' => 'users', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New User'), array('controller' => 'users', 'action' => 'add', 'admin' => true)) ?></li>
  </ul>
</li>

<li class="dropdown">
  <?php echo $this->Html->link(__('Teams & Players') . ' <b class="caret"></b>', 'javascript:void(0);', array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false)); ?>
  <ul class="dropdown-menu">
    <li><?php echo $this->Html->link(__('Manage Teams'), array('controller' => 'teams', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New Team'), array('controller' => 'teams', 'action' => 'add', 'admin' => true)) ?></li>
    <li class="divider"></li>
    <li><?php echo $this->Html->link(__('Manage Players'), array('controller' => 'players', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New Player'), array('controller' => 'players', 'action' => 'add', 'admin' => true)) ?></li>
    <li class="divider"></li>
    <li><?php echo $this->Html->link(__('Manage Team Players'), array('controller' => 'team_players', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add Players to Team'), array('controller' => 'team_players', 'action' => 'add', 'admin' => true)) ?></li>
  </ul>
</li>
<li class="dropdown">
  <?php echo $this->Html->link(__('GPL') . ' <b class="caret"></b>', 'javascript:void(0);', array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false)); ?>
  <ul class="dropdown-menu">
    <li><?php echo $this->Html->link(__('Manage GPL'), array('controller' => 'rounds', 'action' => 'start')); ?></li>
    <li class="divider"></li>
    <li><?php echo $this->Html->link(__('Manage Matches'), array('controller' => 'matches', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New Match'), array('controller' => 'matches', 'action' => 'add', 'admin' => true)) ?></li>
    <li class="divider"></li>
    <li><?php echo $this->Html->link(__('Manage Match Innings Scores'), array('controller' => 'match_inning_scores', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New Match Innings Score'), array('controller' => 'match_inning_scores', 'action' => 'add', 'admin' => true)) ?></li>
    <li class="divider"></li>
    <li><?php echo $this->Html->link(__('Manage Scoring'), array('controller' => 'match_ball_scores', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Start Scoring'), array('controller' => 'match_ball_scores', 'action' => 'add', 'admin' => true)) ?></li>
    <li class="divider"></li>
    <li><?php echo $this->Html->link(__('Manage Match Players'), array('controller' => 'match_players', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add Players to Match'), array('controller' => 'match_players', 'action' => 'add', 'admin' => true)) ?></li>
    <li class="divider"></li>
  </ul>
</li>
<li class="dropdown">
  <?php echo $this->Html->link(__('Umpires') . ' <b class="caret"></b>', 'javascript:void(0);', array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false)); ?>
  <ul class="dropdown-menu">
    <li><?php echo $this->Html->link(__('Manage Umpires'), array('controller' => 'umpires', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New Umpire'), array('controller' => 'umpires', 'action' => 'add', 'admin' => true)) ?></li>
  </ul>
</li>
<li class="dropdown">
  <?php echo $this->Html->link(__('Support Staffs') . ' <b class="caret"></b>', 'javascript:void(0);', array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false)); ?>
  <ul class="dropdown-menu">
    <li><?php echo $this->Html->link(__('Manage Support Staffs'), array('controller' => 'support_staffs', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New Support Staff'), array('controller' => 'support_staffs', 'action' => 'add', 'admin' => true)) ?></li>
  </ul>
</li>
<li class="dropdown">
  <?php echo $this->Html->link(__('Data') . ' <b class="caret"></b>', 'javascript:void(0);', array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false)); ?>
  <ul class="dropdown-menu">
    <li><?php echo $this->Html->link(__('Manage Countries'), array('controller' => 'countries', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New Country'), array('controller' => 'countries', 'action' => 'add', 'admin' => true)) ?></li>
    <li class="divider"></li>
    <li><?php echo $this->Html->link(__('Manage States'), array('controller' => 'states', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New State'), array('controller' => 'states', 'action' => 'add', 'admin' => true)) ?></li>
    <li class="divider"></li>
    <li><?php echo $this->Html->link(__('Manage Cities'), array('controller' => 'cities', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New City'), array('controller' => 'cities', 'action' => 'add', 'admin' => true)) ?></li>
  </ul>
</li>
<li class="dropdown">
  <?php echo $this->Html->link(__('News Related') . ' <b class="caret"></b>', 'javascript:void(0);', array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'escape' => false)); ?>
  <ul class="dropdown-menu">
    <li><?php echo $this->Html->link(__('Manage News'), array('controller' => 'news', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New news'), array('controller' => 'news', 'action' => 'add', 'admin' => true)) ?></li>
    <li class="divider"></li>
    <li><?php echo $this->Html->link(__('Manage Press'), array('controller' => 'press_releases', 'action' => 'index', 'admin' => true)) ?></li>
    <li><?php echo $this->Html->link(__('Add New Press Release'), array('controller' => 'press_releases', 'action' => 'add', 'admin' => true)) ?></li>
  </ul>
</li>