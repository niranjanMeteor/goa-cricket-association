  <div id="footer">
    <div class="pre-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12 footer-block">
            <h2>Quick Links</h2>
            <div class="row">
              <div class="col-md-3">
                <ul class="list-unstyled">
                  <li><?php echo $this->Html->link(__('Teams'), array('controller' => 'teams', 'action' => 'search')); ?></li>
                  <li><?php echo $this->Html->link(__('Players'), array('controller' => 'players', 'action' => 'search')); ?></li>
                  <li><?php echo $this->Html->link(__('Stadium'), '/stadium'); ?></li>
                </ul>
              </div>
              <div class="col-md-3">
                <ul class="list-unstyled">
                  <li><?php echo $this->Html->link(__('Jobs'), '/jobs'); ?></li>
                  <li><?php echo $this->Html->link(__('Press'), '/press'); ?></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12 footer-block">
            <h2>Follow Us On</h2>
            <ul class="social-icons list-inline list-unstyled">
              <!-- <li>
                <?php echo $this->Html->link(__('Facebook'), 'https://www.facebook.com/pages/Goa-cricket-association-GCA/200985499969736', array('target' => '_blank')); ?>
              </li> -->
              <li>
                <?php echo $this->Html->link(__('<span class="fa fa-twitter"></span> Twitter'), 'https://twitter.com/goa_cricket', array('target' => '_blank', 'escape' => false)); ?>
              </li>
            </ul>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12 footer-block">
            <h2>Contacts</h2>
            <address class="margin-bottom-40">
              Phone: +91 (0832) 2416844<br />
              Email: <a href="mailto:info@goacricket.org">info@goacricket.org</a>
            </address>
          </div>
        </div>
      </div>
    </div>
  </div>