<ul class="nav navbar-nav">
  <li class="active">
    <?php echo $this->Html->link(__('Dashboard'), array('controller' => 'users', 'action' => 'dashboard')); ?>
  </li>
  <li>
    <?php echo $this->Html->link(__('Work Management'), array('controller' => 'projects', 'action' => 'index')); ?>
  </li>
</ul>