<div class="header-navigation pull-right font-transform-inherit">
  <ul class="top-nav">
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#home">
        Home 
      </a>
    </li>
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#about">
        About 
        
      </a>
        
    </li>
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#services">
        Vision 
        
      </a>
    </li>
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#teams">
        Teams                
      </a>
    </li>
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="#fixtures">
        Fixtures                
      </a>
    </li>
    <li class="dropdown">
      <?php echo $this->Html->link(__('Press'), '/press'); ?>
    </li>
    <li class="dropdown">
      <?php echo $this->Html->link(__('GPL'), '/gpl'); ?>
    </li>
    <li class="dropdown">
      <?php echo $this->Html->link(__('Sports Village'), '/stadium', array('class' => 'btn btn-success alert-link')); ?>
    </li>
  </ul>
</div>