<div class="header-navigation pull-right font-transform-inherit">
  <ul class="top-nav">
    <li class="dropdown">
      <?php echo $this->Html->link(__('Teams'), array('controller' => 'teams', 'action' => 'search')); ?>
    </li>
    <li class="dropdown">
      <?php echo $this->Html->link(__('Players'), array('controller' => 'players', 'action' => 'search')); ?>
    </li>
    <li class="dropdown">
      <?php echo $this->Html->link(__('Managing Committee'), '/managing_committee'); ?>
    </li>
    <li class="dropdown">
      <?php echo $this->Html->link(__('Sports Village'), '/stadium', array('class' => 'btn btn-success alert-link')); ?>
    </li>
  </ul>
</div>