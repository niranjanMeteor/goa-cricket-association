	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('User Project'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($userProject['UserProject']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Project'); ?></th>
		<td>
			<?php echo $this->Html->link($userProject['Project']['name'], array('controller' => 'projects', 'action' => 'view', $userProject['Project']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('User'); ?></th>
		<td>
			<?php echo $this->Html->link($userProject['User']['email'], array('controller' => 'users', 'action' => 'view', $userProject['User']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Role'); ?></th>
		<td>
			<?php echo h($userProject['UserProject']['role']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($userProject['UserProject']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($userProject['UserProject']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>