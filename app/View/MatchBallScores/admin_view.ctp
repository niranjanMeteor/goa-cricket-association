	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Match Ball Score'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Match Inning Score'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBallScore['MatchInningScore']['id'], array('controller' => 'match_inning_scores', 'action' => 'view', $matchBallScore['MatchInningScore']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Bowler'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBallScore['Bowler']['name'], array('controller' => 'players', 'action' => 'view', $matchBallScore['Bowler']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Overs'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['overs']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Over Balls'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['over_balls']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Striker'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBallScore['Striker']['name'], array('controller' => 'players', 'action' => 'view', $matchBallScore['Striker']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Non Striker'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBallScore['NonStriker']['name'], array('controller' => 'players', 'action' => 'view', $matchBallScore['NonStriker']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Runs'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['runs']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Runs Taken By Batsman'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['runs_taken_by_batsman']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Wides'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['wides']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('No Balls'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['no_balls']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Byes'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['byes']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Leg Byes'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['leg_byes']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Four'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['is_four']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Six'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['is_six']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Out'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['is_out']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Retired Hurt'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['is_retired_hurt']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Retired Hurt Batsman Id'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['retired_hurt_batsman_id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Out Type'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['out_type']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Out Batsman'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBallScore['OutBatsman']['name'], array('controller' => 'players', 'action' => 'view', $matchBallScore['OutBatsman']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Fielder'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBallScore['Fielder']['name'], array('controller' => 'players', 'action' => 'view', $matchBallScore['Fielder']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($matchBallScore['MatchBallScore']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>