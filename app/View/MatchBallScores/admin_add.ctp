	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Add Basic Details'); ?></h1>
		</div>
	</div>

	<?php echo $this->Form->create('MatchBallScore', array('role' => 'form')); ?>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<?php echo $this->Form->input('match_id', array('class' => 'form-control', 'type' => 'select', 'options' => $matches));?>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<?php echo $this->Form->input('innings_id', array('class' => 'form-control', 'type' => 'select', 'options' => $innings, 'empty' => true));?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<?php echo $this->Form->input('striker_id', array('class' => 'form-control', 'placeholder' => 'Striker Id'));?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<?php echo $this->Form->input('non_striker_id', array('class' => 'form-control', 'placeholder' => 'Non Striker Id'));?>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<?php echo $this->Form->input('bowler_id', array('class' => 'form-control', 'placeholder' => 'Bowler Id'));?>
			</div>
		</div>
	</div>
	<div class="form-group">
		<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-success')); ?>
	</div>

	<?php echo $this->Form->end() ?>

