	<div class="row">
		<div class="col-md-2">
			<h3><?php echo __('Live Score -'); ?></h3>
		</div>
		<div class="col-md-3 text-left">
			<div class="current-score">
				<h2><span><?php echo $runs; ?></span> / <span><?php echo $wickets; ?></span> - <span><?php echo $overs; ?></span></h2>
			</div>
		</div>
		<div class="col-md-7">
			<?php echo $this->Html->link(__('Abandon Match'), array('controller' => 'matches', 'action' => 'abandon', $match['Match']['id']), array('class' => 'btn btn-danger')); ?>
			<?php echo $this->Html->link(__('Cancel Match'), array('controller' => 'matches', 'action' => 'cancel', $match['Match']['id']), array('class' => 'btn btn-danger')); ?>
			<?php echo $this->Html->link(__('Declare Result'), array('controller' => 'matches', 'action' => 'declare_result', $match['Match']['id']), array('class' => 'btn btn-success')); ?>
		</div>
	</div>

	<?php echo $this->Form->create('MatchBallScore', array('role' => 'form')); ?>

	<div class="form-group">
		<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
	</div>
	<div class="form-group">
		<?php echo $this->Form->input('match_inning_score_id', array('class' => 'form-control', 'placeholder' => 'Match Inning score id', 'type' => 'hidden', 'value' => $this->request->data['MatchBallScore']['match_inning_score_id']));?>
	</div>
	<fieldset>
		<div class="row">
			<div class="col-md-12">
				<h4><?php echo __('Current Ball Details') ?></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<?php echo __($match['Match']['name']); ?>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					Inning :- <?php echo __($inning) ?>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('striker_id', array('class' => 'form-control selectpicker', 'placeholder' => 'Striker Id', 'options' => $batsmen, 'empty' => true, 'required' => true));?>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('non_striker_id', array('class' => 'form-control selectpicker', 'placeholder' => 'Non Striker Id', 'options' => $batsmen, 'empty' => true, 'required' => true));?>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('bowler_id', array('class' => 'form-control selectpicker', 'placeholder' => 'Bowler Id', 'empty' => true, 'required' => true));?>
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<div class="row">
			<div class="col-md-12">
				<h4><?php echo __('Player Runs Details') ?></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('runs_taken_by_batsman', array('class' => 'form-control', 'placeholder' => 'Runs Taken By Batsman'));?>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('is_four', array('class' => 'form-control', 'type' => 'checkbox')); ?>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('is_six', array('class' => 'form-control', 'type' => 'checkbox')); ?>
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<div class="row">
			<div class="col-md-12">
				<h4><?php echo __('Extras') ?></h4>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<?php echo $this->Form->input('is_extra', array('class' => 'form-control', 'placeholder' => 'Is Extra', 'type' => 'checkbox'));?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<?php echo $this->Form->input('wides', array('class' => 'form-control', 'placeholder' => 'Wides'));?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<?php echo $this->Form->input('no_balls', array('class' => 'form-control', 'placeholder' => 'No Balls'));?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<?php echo $this->Form->input('byes', array('class' => 'form-control', 'placeholder' => 'Byes'));?>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
				<?php echo $this->Form->input('leg_byes', array('class' => 'form-control', 'placeholder' => 'Leg Byes'));?>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<div class="row">
			<div class="col-md-12">
				<h4><?php echo __('Out Details') ?></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('is_out', array('class' => 'form-control', 'placeholder' => 'Is Out'));?>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('out_batsman_id', array('class' => 'form-control selectpicker', 'placeholder' => 'Out Batsman Id', 'options' => $batsmen, 'empty' => true));?>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('out_type', array('class' => 'form-control selectpicker', 'placeholder' => 'Out Type', 'type' => 'select', 'empty' => true));?>
				</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('out_other_by_id', array('class' => 'form-control selectpicker', 'placeholder' => 'Out Other By Id', 'options' => $bowlingTeamPlayers, 'empty' => true));?>
				</div>
			</div>
		</div>
	</fieldset>
	<fieldset>
		<div class="row">
			<div class="col-md-12">
				<h4><?php echo __('Retired Hurt') ?></h4>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="form-group">
					<?php echo $this->Form->input('is_retired_hurt', array('class' => 'form-control', 'placeholder' => 'Is Retired Hurt'));?>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<?php echo $this->Form->input('retired_hurt_batsman_id', array('class' => 'form-control selectpicker', 'placeholder' => 'Retired Hurt Batsman Id', 'options' => $batsmen, 'empty' => true));?>
				</div>
			</div>
		</div>
	</fieldset>

	<div class="form-group">
		<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
	</div>

	<?php echo $this->Form->end() ?>

