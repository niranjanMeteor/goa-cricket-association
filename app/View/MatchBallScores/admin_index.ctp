	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Match Ball Scores'); ?></h1>
		</div><!-- end col md 12 -->
	</div><!-- end row -->


	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<thead>
			<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('match_inning_score_id'); ?></th>
		<th><?php echo $this->Paginator->sort('bowler_id'); ?></th>
		<th><?php echo $this->Paginator->sort('overs'); ?></th>
		<th><?php echo $this->Paginator->sort('over_balls'); ?></th>
		<th><?php echo $this->Paginator->sort('striker_id'); ?></th>
		<th><?php echo $this->Paginator->sort('non_striker_id'); ?></th>
		<th><?php echo $this->Paginator->sort('runs'); ?></th>
		<th><?php echo $this->Paginator->sort('runs_taken_by_batsman'); ?></th>
		<th><?php echo $this->Paginator->sort('wides'); ?></th>
		<th><?php echo $this->Paginator->sort('no_balls'); ?></th>
		<th><?php echo $this->Paginator->sort('byes'); ?></th>
		<th><?php echo $this->Paginator->sort('leg_byes'); ?></th>
		<th><?php echo $this->Paginator->sort('is_four'); ?></th>
		<th><?php echo $this->Paginator->sort('is_six'); ?></th>
		<th><?php echo $this->Paginator->sort('is_out'); ?></th>
		<th><?php echo $this->Paginator->sort('is_retired_hurt'); ?></th>
		<th><?php echo $this->Paginator->sort('retired_hurt_batsman_id'); ?></th>
		<th><?php echo $this->Paginator->sort('out_type'); ?></th>
		<th><?php echo $this->Paginator->sort('out_batsman_id'); ?></th>
		<th><?php echo $this->Paginator->sort('out_other_by_id'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('modified'); ?></th>
		<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($matchBallScores as $matchBallScore): ?>
					<tr>
						<td><?php echo h($matchBallScore['MatchBallScore']['id']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($matchBallScore['MatchInningScore']['id'], array('controller' => 'match_inning_scores', 'action' => 'view', $matchBallScore['MatchInningScore']['id'])); ?>
		</td>
								<td>
			<?php echo $this->Html->link($matchBallScore['Bowler']['name'], array('controller' => 'players', 'action' => 'view', $matchBallScore['Bowler']['id'])); ?>
		</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['overs']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['over_balls']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($matchBallScore['Striker']['name'], array('controller' => 'players', 'action' => 'view', $matchBallScore['Striker']['id'])); ?>
		</td>
								<td>
			<?php echo $this->Html->link($matchBallScore['NonStriker']['name'], array('controller' => 'players', 'action' => 'view', $matchBallScore['NonStriker']['id'])); ?>
		</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['runs']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['runs_taken_by_batsman']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['wides']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['no_balls']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['byes']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['leg_byes']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['is_four']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['is_six']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['is_out']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['is_retired_hurt']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['retired_hurt_batsman_id']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['out_type']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($matchBallScore['OutBatsman']['name'], array('controller' => 'players', 'action' => 'view', $matchBallScore['OutBatsman']['id'])); ?>
		</td>
								<td>
			<?php echo $this->Html->link($matchBallScore['Fielder']['name'], array('controller' => 'players', 'action' => 'view', $matchBallScore['Fielder']['id'])); ?>
		</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['created']); ?>&nbsp;</td>
						<td><?php echo h($matchBallScore['MatchBallScore']['modified']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $matchBallScore['MatchBallScore']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $matchBallScore['MatchBallScore']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $matchBallScore['MatchBallScore']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $matchBallScore['MatchBallScore']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
		</tbody>
	</table>

	<p>
		<small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
	</p>

	<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
	<ul class="pagination pagination-sm">
		<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
	</ul>
	<?php } ?>
