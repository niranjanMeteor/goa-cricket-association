  <div class="row">
    <div class="col-md-12">
      <h1><?php echo __('Admin Add Toss'); ?></h1>
    </div>
  </div>
<div class="row">
  <div class="col-md-6">
    <?php echo $this->Form->create('Match', array('role' => 'form')); ?>
      <div class="form-group">
        <?php echo $this->Form->input('toss_winning_team_id', array('class' => 'form-control chosen-select', 'type' => 'select', 'options' => $teams)); ?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->input('toss_decision', array('class' => 'form-control chosen-select', 'type' => 'select')); ?>
      </div>
      <div class="form-group">
        <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
      </div>

    <?php echo $this->Form->end() ?>
  </div>
</div>

