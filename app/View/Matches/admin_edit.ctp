	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Match'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('Match', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('name', array('class' => 'form-control', 'placeholder' => 'Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('series_name', array('class' => 'form-control', 'placeholder' => 'Series Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('start_date_time', array('class' => 'form-control', 'placeholder' => 'Start Date Time'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('end_date_time', array('class' => 'form-control', 'placeholder' => 'End Date Time', 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('location', array('class' => 'form-control', 'placeholder' => 'Location'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('level', array('class' => 'form-control', 'placeholder' => 'Level'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('type', array('class' => 'form-control', 'placeholder' => 'Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('overs_per_innings', array('class' => 'form-control', 'placeholder' => 'Overs Per Innings'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('first_team_id', array('class' => 'form-control', 'placeholder' => 'First Team Id', 'options' => $teams, 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('second_team_id', array('class' => 'form-control', 'placeholder' => 'Second Team Id', 'options' => $teams, 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('result_type', array('class' => 'form-control', 'placeholder' => 'Result Type', 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('winning_team_id', array('class' => 'form-control', 'placeholder' => 'Winning Team Id', 'options' => $teams, 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

