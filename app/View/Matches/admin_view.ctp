	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Match'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($match['Match']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Name'); ?></th>
		<td>
			<?php echo h($match['Match']['name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Series Name'); ?></th>
		<td>
			<?php echo h($match['Match']['series_name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Start Date Time'); ?></th>
		<td>
			<?php echo h($match['Match']['start_date_time']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('End Date Time'); ?></th>
		<td>
			<?php echo h($match['Match']['end_date_time']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Location'); ?></th>
		<td>
			<?php echo h($match['Match']['location']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Level'); ?></th>
		<td>
			<?php echo h($match['Match']['level']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Type'); ?></th>
		<td>
			<?php echo h($match['Match']['type']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Overs Per Innings'); ?></th>
		<td>
			<?php echo h($match['Match']['overs_per_innings']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('First Team'); ?></th>
		<td>
			<?php echo $this->Html->link($match['FirstTeam']['name'], array('controller' => 'teams', 'action' => 'view', $match['FirstTeam']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Second Team'); ?></th>
		<td>
			<?php echo $this->Html->link($match['SecondTeam']['name'], array('controller' => 'teams', 'action' => 'view', $match['SecondTeam']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Toss Winning Team'); ?></th>
		<td>
			<?php echo $this->Html->link($match['TossWinningTeam']['name'], array('controller' => 'teams', 'action' => 'view', $match['TossWinningTeam']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Toss Decision'); ?></th>
		<td>
			<?php echo h($match['Match']['toss_decision']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Result Type'); ?></th>
		<td>
			<?php echo h($match['Match']['result_type']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Winning Team Id'); ?></th>
		<td>
			<?php echo h($match['Match']['winning_team_id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($match['Match']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($match['Match']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>