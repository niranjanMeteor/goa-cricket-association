	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Add Match'); ?></h1>
		</div>
	</div>
	<div class="row">
		<?php echo $this->Form->create('Match', array('role' => 'form')); ?>
			<div class="col-md-6">
				<div class="form-group">
					<?php echo $this->Form->input('name', array('class' => 'form-control', 'placeholder' => 'Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('series_name', array('class' => 'form-control', 'placeholder' => 'Series Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('start_date_time', array('class' => 'form-control', 'placeholder' => 'Start Date Time'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('end_date_time', array('class' => 'form-control', 'placeholder' => 'End Date Time', 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('location', array('class' => 'form-control', 'placeholder' => 'Location'));?>
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<?php echo $this->Form->input('level', array('class' => 'form-control chosen-select match-level-select', 'placeholder' => 'Level'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('type', array('class' => 'form-control chosen-select', 'placeholder' => 'Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('overs_per_innings', array('class' => 'form-control', 'placeholder' => 'Overs Per Innings', 'id' => 'overs-per-inning'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('first_team_id', array('class' => 'form-control chosen-select', 'placeholder' => 'First Team Id' , 'options' => $teams, 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('second_team_id', array('class' => 'form-control chosen-select', 'placeholder' => 'Second Team Id', 'options' => $teams, 'empty' => true));?>
				</div>
			</div>
	</div>
		<div class="form-group">
			<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>
		</div>

	<?php echo $this->Form->end() ?>



