	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Matches'); ?></h1>
		</div><!-- end col md 12 -->
	</div><!-- end row -->


	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<thead>
			<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('series_name'); ?></th>
		<th><?php echo $this->Paginator->sort('start_date_time', 'start_time'); ?></th>
		<th><?php echo $this->Paginator->sort('end_date_time', 'end_time'); ?></th>
		<th><?php echo $this->Paginator->sort('location'); ?></th>
		<th><?php echo $this->Paginator->sort('level'); ?></th>
		<th><?php echo $this->Paginator->sort('type'); ?></th>
		<th><?php echo $this->Paginator->sort('overs_per_innings', 'overs'); ?></th>
		<th><?php echo $this->Paginator->sort('first_team_id'); ?></th>
		<th><?php echo $this->Paginator->sort('second_team_id'); ?></th>
		<th><?php echo $this->Paginator->sort('toss_winning_team_id'); ?></th>
		<th><?php echo $this->Paginator->sort('toss_decision'); ?></th>
		<th><?php echo $this->Paginator->sort('result_type'); ?></th>
		<th><?php echo $this->Paginator->sort('winning_team_id'); ?></th>
		<th class="user-actions">User actions</th>
		<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($matches as $match): ?>
					<tr>
						<td><?php echo h($match['Match']['id']); ?>&nbsp;</td>
						<td><?php echo h($match['Match']['name']); ?>&nbsp;</td>
						<td><?php echo h($match['Match']['series_name']); ?>&nbsp;</td>
						<td><?php echo h($match['Match']['start_date_time']); ?>&nbsp;</td>
						<td><?php echo h($match['Match']['end_date_time']); ?>&nbsp;</td>
						<td><?php echo h($match['Match']['location']); ?>&nbsp;</td>
						<td><?php echo h(MatchLevel::stringValue($match['Match']['level'])); ?>&nbsp;</td>
						<td><?php echo h(MatchType::stringValue($match['Match']['type'])); ?>&nbsp;</td>
						<td><?php echo h($match['Match']['overs_per_innings']); ?>&nbsp;</td>
						<td>
							<?php echo $this->Html->link($match['FirstTeam']['name'], array('controller' => 'teams', 'action' => 'view', $match['FirstTeam']['id'])); ?>
						</td>
						<td>
							<?php echo $this->Html->link($match['SecondTeam']['name'], array('controller' => 'teams', 'action' => 'view', $match['SecondTeam']['id'])); ?>
						</td>
						<td>
							<?php echo $this->Html->link($match['TossWinningTeam']['name'], array('controller' => 'teams', 'action' => 'view', $match['TossWinningTeam']['id'])); ?>
						</td>
						<td><?php echo h(TossDecision::stringValue($match['Match']['toss_decision'])); ?>&nbsp;</td>
						<td><?php echo h(ResultType::stringValue($match['Match']['result_type'])); ?>&nbsp;</td>
						<td>
							<?php echo h($match['Match']['winning_team_id']); ?>&nbsp;
						</td>
						<td>
							<?php if (empty($match['Match']['toss_decision'])): ?>
								<?php echo $this->Html->link(_('Add Toss'), array('controller' => 'matches', 'action' => 'toss', $match['Match']['id'])); ?>
							<?php endif; ?>
							<?php if ($match['Match']['in_progress']): ?>
								<?php echo $this->Html->link(__('Resume Scoring'), array('controller' => 'match_ball_scores', 'action' => 'resume_scoring', $match['Match']['id']), array('class' => 'btn btn-success')); ?>
							<?php endif; ?>
							<?php echo $this->Html->link(_('Add Players'), array('controller' => 'matches', 'action' => 'add_players', $match['Match']['id'])); ?>
						</td>
						<td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $match['Match']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $match['Match']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $match['Match']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $match['Match']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
		</tbody>
	</table>

	<p>
		<small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
	</p>

	<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
	<ul class="pagination pagination-sm">
		<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
	</ul>
	<?php } ?>
