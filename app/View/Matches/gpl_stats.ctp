<?php echo $this->Html->css('gpl_stats', array('inline' => false)) ?>
<?php echo $this->Html->script('gpl_stats', array('inline' => false)); ?>
  <div class="container title-container">
    <div class="row">
      <div class="col-md-12 text-center">
        <?php echo $this->Html->image(__('gpl_logo_no_bg.png'), array('class' => 'gpl-logo pull-left')); ?>
        <h3>Goa Professional League</h3>
        <div class="tagline"><small class="text-primary">Invitational T-20 tournament by EVENTURES @ Goa. </small></div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="batting-stats card">
          <div class="title">
            Top Batsmen
          </div>
            <table class="table table-hover custom-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Runs</th>
                  <th>Strike Rate</th>
                  <th>Team</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($topBatsmen as $batsman): ?>
                  <tr class="batsmen">
                    <td><?php echo $this->Html->link(__($batsman['MatchBatsmanScore']['name']), array('controller' => 'players', 'action' => 'view', $batsman['Player']['id'])); ?></td>
                    <td class="text-center"><?php echo __($batsman[0]['total']) ?></td>
                    <td><?php echo __($batsman[0]['strike_rate']) ?></td>
                    <td>
                      <?php foreach ($batsman['Player']['TeamPlayer'] as $team): ?>
                        <?php if (!empty($team['Team'])): ?>
                          <?php echo $this->Html->link(__($team['Team']['name']), array('controller' => 'teams', 'action' => 'view', $team['Team']['id'])); ?>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="batting-stats card">
          <div class="title">
            Top Bowlers
          </div>
            <table class="table table-hover custom-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Wickets</th>
                  <th>Team</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($topBowlers as $bowler): ?>
                  <tr class="bowler">
                    <td><?php echo $this->Html->link(__($bowler['MatchBowlerScore']['name']), array('controller' => 'players', 'action' => 'view', $bowler['Player']['id'])); ?></td>
                    <td class="text-center"><?php echo __($bowler[0]['total']) ?></td>
                    <td>
                      <?php foreach ($bowler['Player']['TeamPlayer'] as $team): ?>
                        <?php if (!empty($team['Team'])): ?>
                          <?php echo $this->Html->link(__($team['Team']['name']), array('controller' => 'teams', 'action' => 'view', $team['Team']['id'])); ?>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
        </div>
      </div>
      <div class="col-md-4">
        <div class="batting-stats card">
          <div class="title">
            Teams
          </div>
            <table class="table table-hover custom-table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Matches</th>
                  <th>W</th>
                  <th>T</th>
                  <th>NR</th>
                  <th>Points</th>
                  <th>NRR</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($teams as $team): ?>
                  <tr class="team">
                    <td><?php echo $this->Html->link(__($team['FirstTeam']['name']), array('controller' => 'teams', 'action' => 'view', $team['FirstTeam']['id'])); ?></td>
                    <td class="text-center"><?php echo $team['FirstTeam']['match_count']; ?></td>
                    <td class="text-center"><?php echo $team['FirstTeam']['win_count']; ?></td>
                    <td class="text-center"><?php echo $team['FirstTeam']['tie_count']; ?></td>
                    <td class="text-center"><?php echo $team['FirstTeam']['no_result_count']; ?></td>
                    <td class="text-center"><?php echo $team['FirstTeam']['points']; ?></td>
                    <td><?php echo $team['FirstTeam']['nrr']; ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>


