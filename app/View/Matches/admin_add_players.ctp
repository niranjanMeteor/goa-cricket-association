<div class="row">
  <div class="col-md-12">
    <h1><?php echo __('Admin Add Players to Match'); ?></h1>
  </div>
</div>

<div class="row">
  <?php echo $this->Form->create('MatchPlayer'); ?>
  <div class="col-md-6">
    <div class="team-one-wrapper">
      <h3><?php echo $firstTeam['FirstTeam']['name']; ?></h3>
      <?php $formCounter = 0; ?>
      <?php if (!empty($firstTeamList)): ?>
        <?php foreach($firstTeamList as $id => $player): ?>
          <div class="row">
            <?php echo $this->Form->input('MatchPlayer.' . $formCounter . '.team_id', array('type' => 'hidden', 'value' => $firstTeam['FirstTeam']['id'])); ?>
            <div class="col-md-6">
              <div class="form-group">
                <?php echo $this->Form->input('MatchPlayer.' . $formCounter . '.player_id', array('class' => 'form-control', 'type' => 'checkbox', 'label' => $player['Player']['name'], 'value' => $player['Player']['id'])); ?>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <?php echo $this->Form->input('MatchPlayer.' . $formCounter . '.role', array('class' => 'form-control', 'type' => 'select', 'options' => $roles, 'selected' => $player['Player']['role'])); ?>
              </div>
            </div>
          </div>
          <?php $formCounter++; ?>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
  </div>
  <div class="col-md-6">
    <div class="team-two-wrapper">
      <h3><?php echo $secondTeam['SecondTeam']['name']; ?></h3>
      <?php if (!empty($secondTeamList)): ?>
        <?php foreach($secondTeamList as $id => $player): ?>
          <div class="row">
            <?php echo $this->Form->input('MatchPlayer.' . $formCounter . '.team_id', array('type' => 'hidden', 'value' => $secondTeam['SecondTeam']['id'])); ?>
            <div class="col-md-6">
              <div class="form-group">
                <?php echo $this->Form->input('MatchPlayer.' . $formCounter . '.player_id', array('class' => 'form-control', 'type' => 'checkbox', 'label' => $player['Player']['name'], 'value' => $player['Player']['id'])); ?>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <?php echo $this->Form->input('MatchPlayer.' . $formCounter . '.role', array('class' => 'form-control', 'type' => 'select', 'options' => $roles, 'selected' => $player['Player']['role'])); ?>
              </div>
            </div>
          </div>
          <?php $formCounter++; ?>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12 text-center">
    <div class="form-group">
      <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>
    </div>

    <?php echo $this->Form->end() ?>
  </div>
</div>