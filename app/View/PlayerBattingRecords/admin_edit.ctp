	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Player Batting Record'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('PlayerBattingRecord', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('player_id', array('class' => 'form-control', 'placeholder' => 'Player Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('level', array('class' => 'form-control', 'placeholder' => 'Level'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('match_count', array('class' => 'form-control', 'placeholder' => 'Match Count'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('innings_count', array('class' => 'form-control', 'placeholder' => 'Innings Count'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('not_outs', array('class' => 'form-control', 'placeholder' => 'Not Outs'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('runs', array('class' => 'form-control', 'placeholder' => 'Runs'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('high_score', array('class' => 'form-control', 'placeholder' => 'High Score'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('batting_average', array('class' => 'form-control', 'placeholder' => 'Batting Average'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('ball_faced', array('class' => 'form-control', 'placeholder' => 'Ball Faced'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('strike_rate', array('class' => 'form-control', 'placeholder' => 'Strike Rate'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('hundreds', array('class' => 'form-control', 'placeholder' => 'Hundreds'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('fifties', array('class' => 'form-control', 'placeholder' => 'Fifties'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('fours', array('class' => 'form-control', 'placeholder' => 'Fours'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('sixes', array('class' => 'form-control', 'placeholder' => 'Sixes'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('catches_taken', array('class' => 'form-control', 'placeholder' => 'Catches Taken'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('stumpings_made', array('class' => 'form-control', 'placeholder' => 'Stumpings Made'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

