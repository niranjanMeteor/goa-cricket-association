	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Player Batting Record'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Player'); ?></th>
		<td>
			<?php echo $this->Html->link($playerBattingRecord['Player']['id'], array('controller' => 'players', 'action' => 'view', $playerBattingRecord['Player']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Level'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['level']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Match Count'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['match_count']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Innings Count'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['innings_count']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Not Outs'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['not_outs']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Runs'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['runs']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('High Score'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['high_score']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Batting Average'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['batting_average']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Ball Faced'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['ball_faced']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Strike Rate'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['strike_rate']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Hundreds'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['hundreds']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Fifties'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['fifties']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Fours'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['fours']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Sixes'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['sixes']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Catches Taken'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['catches_taken']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Stumpings Made'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['stumpings_made']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($playerBattingRecord['PlayerBattingRecord']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>