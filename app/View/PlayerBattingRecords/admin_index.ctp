	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Player Batting Records'); ?></h1>
		</div><!-- end col md 12 -->
	</div><!-- end row -->


	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<thead>
			<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('player_id'); ?></th>
		<th><?php echo $this->Paginator->sort('level'); ?></th>
		<th><?php echo $this->Paginator->sort('match_count'); ?></th>
		<th><?php echo $this->Paginator->sort('innings_count'); ?></th>
		<th><?php echo $this->Paginator->sort('not_outs'); ?></th>
		<th><?php echo $this->Paginator->sort('runs'); ?></th>
		<th><?php echo $this->Paginator->sort('high_score'); ?></th>
		<th><?php echo $this->Paginator->sort('batting_average'); ?></th>
		<th><?php echo $this->Paginator->sort('ball_faced'); ?></th>
		<th><?php echo $this->Paginator->sort('strike_rate'); ?></th>
		<th><?php echo $this->Paginator->sort('hundreds'); ?></th>
		<th><?php echo $this->Paginator->sort('fifties'); ?></th>
		<th><?php echo $this->Paginator->sort('fours'); ?></th>
		<th><?php echo $this->Paginator->sort('sixes'); ?></th>
		<th><?php echo $this->Paginator->sort('catches_taken'); ?></th>
		<th><?php echo $this->Paginator->sort('stumpings_made'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('modified'); ?></th>
		<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($playerBattingRecords as $playerBattingRecord): ?>
					<tr>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['id']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($playerBattingRecord['Player']['id'], array('controller' => 'players', 'action' => 'view', $playerBattingRecord['Player']['id'])); ?>
		</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['level']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['match_count']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['innings_count']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['not_outs']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['runs']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['high_score']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['batting_average']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['ball_faced']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['strike_rate']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['hundreds']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['fifties']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['fours']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['sixes']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['catches_taken']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['stumpings_made']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['created']); ?>&nbsp;</td>
						<td><?php echo h($playerBattingRecord['PlayerBattingRecord']['modified']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $playerBattingRecord['PlayerBattingRecord']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $playerBattingRecord['PlayerBattingRecord']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $playerBattingRecord['PlayerBattingRecord']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $playerBattingRecord['PlayerBattingRecord']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
		</tbody>
	</table>

	<p>
		<small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
	</p>

	<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
	<ul class="pagination pagination-sm">
		<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
	</ul>
	<?php } ?>
