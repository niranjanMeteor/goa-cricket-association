<?php echo $this->Html->css('chosen.min', array('inline' => false)); ?>
<?php echo $this->Html->css('add_project', array('inline' => false)); ?>
<?php echo $this->Html->script('chosen.jquery.min', array('inline' => false)); ?>
<?php echo $this->Html->script('add_project', array('inline' => false)); ?>

<div class="page-container">
  <div class="page-head">
    <div class="container">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>New Project <small>Tickets &amp; task management</small></h1>
      </div>
      <!-- END PAGE TITLE -->
    </div>
  </div>
  <div class="page-content">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="form-wrapper">
            <div class="form-title">
              <h3 class="font-green-sharp">Create Project</h3>
            </div>
            <hr />
            <div class="row">
              <div class="col-md-10">
                <?php 
                  echo $this->Form->create('Project', array(
                    'class' => 'form-horizontal form-without-legend new-project-form',
                    'role' => 'form'
                  )); 
                ?>
                <?php echo $this->Form->input('name', array('class' => 'form-control', 'wrap' => 'col-sm-3', 'afterInput' => '<span class="fa fa-asterisk required-sign required-select"></span>')); ?>
                <?php echo $this->Form->input('description', array('class' => 'form-control')); ?>
                <div class="row inner-row">
                  <div class="col-md-6">
                    <?php echo $this->Form->input('start_date', array('class' => 'form-control','wrap' => 'col-sm-10 custom-date')) ?>
                  </div>
                  <div class="col-md-6">
                    <?php echo $this->Form->input('end_date', array('class' => 'form-control','wrap' => 'col-sm-10 custom-date')) ?>
                  </div>
                </div>
                <div class="row inner-row">
                  <div class="col-md-3">
                    <?php echo $this->Form->submit(__('Create'), array('class' => 'btn btn-success btn-block')); ?>
                  </div>
                </div>
                <?php echo $this->Form->end(); ?>
              </div>  
            </div>
          </div>
            
        </div>
      </div>
    </div>
  </div>
</div>