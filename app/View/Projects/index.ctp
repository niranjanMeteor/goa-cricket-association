<?php echo $this->Html->css('tasks', array('inline' => false)); ?>
<?php echo $this->Html->script('clamp.min', array('inline' => false)); ?>
<?php echo $this->Html->script('tasks', array('inline' => false)); ?>

<div class="page-container">
  <div class="page-head">
    <div class="container">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>Ticket &amp; Tasks <small>Tickets &amp; task management</small></h1>
      </div>
      <!-- END PAGE TITLE -->
    </div>
  </div>
  <div class="page-content">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="project-sidebar portlet">
            <div class="project-wrapper">
              <div class="portlet-title">
                <div class="actions pull-right">
                  <div class="btn-group">
                    <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      <span class="fa fa-cog"></span> <span class="fa fa-angle-down"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?></li>
                      <li class="divider"></li>
                      <li><?php echo $this->Html->link(__('Pending'), array('controller' => 'projects', 'action' => 'pending')); ?></li>
                      <li><?php echo $this->Html->link(__('Overdue'), array('controller' => 'projects', 'action' => 'overdue')); ?></li>
                      <li><?php echo $this->Html->link(__('Finished'), array('controller' => 'projects', 'action' => 'finished')); ?></li>
                    </ul>
                  </div>
                </div>
                <div class="caption font-green-sharp">Projects</div>
              </div>
              <hr />

              <div class="project-list">
                <?php if ( ! empty($projects)): ?>
                  <ul class="nav nav-pills nav-stacked">
                    <?php foreach ($projects as $id => $project): ?>

                      <li class="<?php echo ($id == 0) ? 'active' : '';?>">
                        <?php echo $this->Html->link(__('<span class="badge badge-success">' . $project['Project']['ticket_count'] . '</span>' . $project["Project"]["name"] . '</a>'), array('controller' => 'projects', 'action' => 'view',$project['Project']['id']), array('escape' => false)); ?>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                <?php else: ?>
                <div>No Projects to display. <?php echo $this->Html->link(__('<span class="fa fa-plus"></span> Create One'), array('controller' => 'projects', 'action' => 'add'), array('escape' => false, 'class' => 'font-green-sharp')); ?> </div>
                <?php endif; ?>
              </div>

            </div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="portlet right">
            <div class="project-wrapper">

              <div class="portlet-title">
                <div class="actions pull-right">
                  <div class="btn-group">
                    <button type="button" class="btn btn-green dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      Manage <span class="fa fa-angle-down"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><?php echo $this->Html->link(__('New Ticket'), array('controller' => 'tickets', 'action' => 'add')); ?></li>
                      <li class="divider"></li>
                      <li><a href="#">Pending Tickets</a></li>
                      <li><a href="#">Overdue Tickets</a></li>
                      <li><a href="#">Finished Tickets</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Delete Projects</a></li>
                    </ul>
                  </div>
                </div>
                </div>
                <div class="caption font-green-sharp"><span class="caption-helper">Project:</span> GlobalEx Systems</div>
              </div>

              <hr />

              <div class="portlet-content">
                <div class="row">
                  <div class="col-md-12">
                    <?php if (!empty($tickets)): ?>
                      <div class="ticket-filters">
                        <div class="row">
                          <div class="col-md-2">
                            <select>
                              <option value="" selcted>Select Assignee</option>
                              <option value="1">Chetan Desai</option>
                              <option value="2">Shekhar Salkar</option>
                              <option value="3">Amaresh Naik</option>
                              <option value="4">Sudin Kamat</option>
                            </select>
                          </div>
                          <div class="col-md-2">
                            <select>
                              <option value="" selcted>Sort By</option>
                              <option value="1">Start Date Asc</option>
                              <option value="2">Due Date Asc</option>
                              <option value="3">Start Date Desc</option>
                              <option value="4">Due Date Desc</option>
                              <option value="5">Priotiy Asc</option>
                              <option value="6">Priotiy Desc</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="ticket-list">

                        <?php foreach ($tickets as $id => $ticket): ?>
                          <div class="ticket-item ticket-item-border-green">
                            <div class="actions pull-right">
                              <?php echo $this->Html->link(__('<span class="fa fa-edit"></span>'), array('controller' => 'tickets', 'action' => 'edit', $ticket['Ticket']['id']), array('escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Edit')); ?>
                              <?php echo $this->Html->link(__('<span class="fa fa-eye"></span>'), array('controller' => 'tickets', 'action' => 'watch', $ticket['Ticket']['id']), array('escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Watch')); ?>
                              <?php echo $this->Html->link(__('<span class="fa fa-trash-o"></span>'), array('controller' => 'tickets', 'action' => 'delete', $ticket['Ticket']['id']), array('escape' => false, 'data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Delete')); ?>
                            </div>
                            <?php 
                              if ( ! empty($ticket['Assignee']['Profile']['image_url'])) {
                                echo $this->Html->link($this->Html->image($ticket['Assignee']['Profile']['image_url'], array('class' => 'ticket-userpic pull-left', 'fullBase' => true)), array('controller' => 'users', 'action' => 'view', $ticket['Assignee']['id']), array('escape' => false));
                              } else {
                                echo $this->Html->link($this->Html->image('male.png', array('class' => 'ticket-userpic pull-left')), array('controller' => 'users', 'action' => 'view', $ticket['Assignee']['id']), array('escape' => false));
                              }
                            ?>
                            <div class="ticket-item-title">
                               <?php echo __($ticket['Ticket']['title']); ?>
                            </div>
                            <div class="ticket-item-text">
                               <?php echo __($ticket['Ticket']['description']); ?>
                            </div>
                            <div>
                              <?php echo $this->Html->link(__('View Ticket <span class="fa fa-external-link"></span>'), array('controller' => 'tickets', 'action' => 'view', $ticket['Ticket']['id']), array('class' => 'font-green-sharp', 'escape' => false)); ?>
                            </div>
                            <div class="ticket-controls pull-left">
                              <span class="ticket-date"><i class="fa fa-calendar"></i> <?php echo date('d M Y', strtotime($ticket['Ticket']['end_date'])) ?> </span>
                              <span class="ticket-badge badge badge-roundless"><?php echo TicketPriority::stringValue($ticket['Ticket']['priority']) ?></span>
                            </div>
                          </div>
                        <?php endforeach; ?>
   
                      </div>
                      <p>
                        <small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
                      </p>

                      <?php
                          $params = $this->Paginator->params();
                          if ($params['pageCount'] > 1) {
                          ?>
                      <ul class="pagination pagination-sm">
                        <?php
                              echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
                              echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
                              echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
                            ?>
                      </ul>
                      <?php } ?>
                      
                    <?php endif; ?>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
    </div>
    </div>
  </div>
</div>