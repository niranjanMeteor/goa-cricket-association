	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Add Match Batsman Score'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('MatchBatsmanScore', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('match_inning_score_id', array('class' => 'form-control', 'placeholder' => 'Match Inning Score Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('player_id', array('class' => 'form-control', 'placeholder' => 'Player Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('runs', array('class' => 'form-control', 'placeholder' => 'Runs'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('balls', array('class' => 'form-control', 'placeholder' => 'Balls'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('fours', array('class' => 'form-control', 'placeholder' => 'Fours'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('sixes', array('class' => 'form-control', 'placeholder' => 'Sixes'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('status', array('class' => 'form-control', 'placeholder' => 'Status'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('out_type', array('class' => 'form-control', 'placeholder' => 'Out Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('out_by_id', array('class' => 'form-control', 'placeholder' => 'Out By Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('out_other_by_id', array('class' => 'form-control', 'placeholder' => 'Out Other By Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

