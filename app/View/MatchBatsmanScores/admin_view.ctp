	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Match Batsman Score'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($matchBatsmanScore['MatchBatsmanScore']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Match Inning Score'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBatsmanScore['MatchInningScore']['id'], array('controller' => 'match_inning_scores', 'action' => 'view', $matchBatsmanScore['MatchInningScore']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Player'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBatsmanScore['Player']['name'], array('controller' => 'players', 'action' => 'view', $matchBatsmanScore['Player']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Runs'); ?></th>
		<td>
			<?php echo h($matchBatsmanScore['MatchBatsmanScore']['runs']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Balls'); ?></th>
		<td>
			<?php echo h($matchBatsmanScore['MatchBatsmanScore']['balls']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Fours'); ?></th>
		<td>
			<?php echo h($matchBatsmanScore['MatchBatsmanScore']['fours']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Sixes'); ?></th>
		<td>
			<?php echo h($matchBatsmanScore['MatchBatsmanScore']['sixes']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Status'); ?></th>
		<td>
			<?php echo h($matchBatsmanScore['MatchBatsmanScore']['status']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Out Type'); ?></th>
		<td>
			<?php echo h($matchBatsmanScore['MatchBatsmanScore']['out_type']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Bowler'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBatsmanScore['Bowler']['name'], array('controller' => 'players', 'action' => 'view', $matchBatsmanScore['Bowler']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Fielder'); ?></th>
		<td>
			<?php echo $this->Html->link($matchBatsmanScore['Fielder']['name'], array('controller' => 'players', 'action' => 'view', $matchBatsmanScore['Fielder']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($matchBatsmanScore['MatchBatsmanScore']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($matchBatsmanScore['MatchBatsmanScore']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>