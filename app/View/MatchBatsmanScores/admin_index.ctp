	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Match Batsman Scores'); ?></h1>
		</div><!-- end col md 12 -->
	</div><!-- end row -->


	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<thead>
			<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('match_inning_score_id'); ?></th>
		<th><?php echo $this->Paginator->sort('player_id'); ?></th>
		<th><?php echo $this->Paginator->sort('runs'); ?></th>
		<th><?php echo $this->Paginator->sort('balls'); ?></th>
		<th><?php echo $this->Paginator->sort('fours'); ?></th>
		<th><?php echo $this->Paginator->sort('sixes'); ?></th>
		<th><?php echo $this->Paginator->sort('status'); ?></th>
		<th><?php echo $this->Paginator->sort('out_type'); ?></th>
		<th><?php echo $this->Paginator->sort('out_by_id'); ?></th>
		<th><?php echo $this->Paginator->sort('out_other_by_id'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('modified'); ?></th>
		<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($matchBatsmanScores as $matchBatsmanScore): ?>
					<tr>
						<td><?php echo h($matchBatsmanScore['MatchBatsmanScore']['id']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($matchBatsmanScore['MatchInningScore']['id'], array('controller' => 'match_inning_scores', 'action' => 'view', $matchBatsmanScore['MatchInningScore']['id'])); ?>
		</td>
								<td>
			<?php echo $this->Html->link($matchBatsmanScore['Player']['name'], array('controller' => 'players', 'action' => 'view', $matchBatsmanScore['Player']['id'])); ?>
		</td>
						<td><?php echo h($matchBatsmanScore['MatchBatsmanScore']['runs']); ?>&nbsp;</td>
						<td><?php echo h($matchBatsmanScore['MatchBatsmanScore']['balls']); ?>&nbsp;</td>
						<td><?php echo h($matchBatsmanScore['MatchBatsmanScore']['fours']); ?>&nbsp;</td>
						<td><?php echo h($matchBatsmanScore['MatchBatsmanScore']['sixes']); ?>&nbsp;</td>
						<td><?php echo h($matchBatsmanScore['MatchBatsmanScore']['status']); ?>&nbsp;</td>
						<td><?php echo h($matchBatsmanScore['MatchBatsmanScore']['out_type']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($matchBatsmanScore['Bowler']['name'], array('controller' => 'players', 'action' => 'view', $matchBatsmanScore['Bowler']['id'])); ?>
		</td>
								<td>
			<?php echo $this->Html->link($matchBatsmanScore['Fielder']['name'], array('controller' => 'players', 'action' => 'view', $matchBatsmanScore['Fielder']['id'])); ?>
		</td>
						<td><?php echo h($matchBatsmanScore['MatchBatsmanScore']['created']); ?>&nbsp;</td>
						<td><?php echo h($matchBatsmanScore['MatchBatsmanScore']['modified']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $matchBatsmanScore['MatchBatsmanScore']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $matchBatsmanScore['MatchBatsmanScore']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $matchBatsmanScore['MatchBatsmanScore']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $matchBatsmanScore['MatchBatsmanScore']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
		</tbody>
	</table>

	<p>
		<small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
	</p>

	<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
	<ul class="pagination pagination-sm">
		<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
	</ul>
	<?php } ?>
