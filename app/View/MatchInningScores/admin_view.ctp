	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Match Inning Score'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Match'); ?></th>
		<td>
			<?php echo $this->Html->link($matchInningScore['Match']['name'], array('controller' => 'matches', 'action' => 'view', $matchInningScore['Match']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Inning'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['inning']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Team'); ?></th>
		<td>
			<?php echo $this->Html->link($matchInningScore['Team']['name'], array('controller' => 'teams', 'action' => 'view', $matchInningScore['Team']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Runs'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['runs']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Wickets'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['wickets']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Overs'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['overs']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Fours'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['fours']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Sixes'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['sixes']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Wides'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['wides']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('No Balls'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['no_balls']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Byes'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['byes']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Leg Byes'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['leg_byes']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($matchInningScore['MatchInningScore']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>