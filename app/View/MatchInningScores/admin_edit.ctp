	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Match Inning Score'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('MatchInningScore', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('match_id', array('class' => 'form-control', 'placeholder' => 'Match Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('inning', array('class' => 'form-control', 'placeholder' => 'Inning'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('team_id', array('class' => 'form-control', 'placeholder' => 'Team Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('runs', array('class' => 'form-control', 'placeholder' => 'Runs'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('wickets', array('class' => 'form-control', 'placeholder' => 'Wickets'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('overs', array('class' => 'form-control', 'placeholder' => 'Overs'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('fours', array('class' => 'form-control', 'placeholder' => 'Fours'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('sixes', array('class' => 'form-control', 'placeholder' => 'Sixes'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('wides', array('class' => 'form-control', 'placeholder' => 'Wides'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('no_balls', array('class' => 'form-control', 'placeholder' => 'No Balls'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('byes', array('class' => 'form-control', 'placeholder' => 'Byes'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('leg_byes', array('class' => 'form-control', 'placeholder' => 'Leg Byes'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

