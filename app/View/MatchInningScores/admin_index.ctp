	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Match Inning Scores'); ?></h1>
		</div><!-- end col md 12 -->
	</div><!-- end row -->


	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<thead>
			<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('match_id'); ?></th>
		<th><?php echo $this->Paginator->sort('inning'); ?></th>
		<th><?php echo $this->Paginator->sort('team_id'); ?></th>
		<th><?php echo $this->Paginator->sort('runs'); ?></th>
		<th><?php echo $this->Paginator->sort('wickets'); ?></th>
		<th><?php echo $this->Paginator->sort('overs'); ?></th>
		<th><?php echo $this->Paginator->sort('fours'); ?></th>
		<th><?php echo $this->Paginator->sort('sixes'); ?></th>
		<th><?php echo $this->Paginator->sort('wides'); ?></th>
		<th><?php echo $this->Paginator->sort('no_balls'); ?></th>
		<th><?php echo $this->Paginator->sort('byes'); ?></th>
		<th><?php echo $this->Paginator->sort('leg_byes'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('modified'); ?></th>
		<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($matchInningScores as $matchInningScore): ?>
					<tr>
						<td><?php echo h($matchInningScore['MatchInningScore']['id']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($matchInningScore['Match']['name'], array('controller' => 'matches', 'action' => 'view', $matchInningScore['Match']['id'])); ?>
		</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['inning']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($matchInningScore['Team']['name'], array('controller' => 'teams', 'action' => 'view', $matchInningScore['Team']['id'])); ?>
		</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['runs']); ?>&nbsp;</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['wickets']); ?>&nbsp;</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['overs']); ?>&nbsp;</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['fours']); ?>&nbsp;</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['sixes']); ?>&nbsp;</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['wides']); ?>&nbsp;</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['no_balls']); ?>&nbsp;</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['byes']); ?>&nbsp;</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['leg_byes']); ?>&nbsp;</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['created']); ?>&nbsp;</td>
						<td><?php echo h($matchInningScore['MatchInningScore']['modified']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $matchInningScore['MatchInningScore']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $matchInningScore['MatchInningScore']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $matchInningScore['MatchInningScore']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $matchInningScore['MatchInningScore']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
		</tbody>
	</table>

	<p>
		<small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
	</p>

	<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
	<ul class="pagination pagination-sm">
		<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
	</ul>
	<?php } ?>
