	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Match Player'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('MatchPlayer', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('match_id', array('class' => 'form-control', 'placeholder' => 'Match Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('team_id', array('class' => 'form-control', 'placeholder' => 'Team Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('player_id', array('class' => 'form-control', 'placeholder' => 'Player Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('is_captain', array('class' => 'form-control', 'placeholder' => 'Is Captain'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('role', array('class' => 'form-control', 'placeholder' => 'Role'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

