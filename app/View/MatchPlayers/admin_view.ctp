	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Match Player'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($matchPlayer['MatchPlayer']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Match'); ?></th>
		<td>
			<?php echo $this->Html->link($matchPlayer['Match']['name'], array('controller' => 'matches', 'action' => 'view', $matchPlayer['Match']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Team'); ?></th>
		<td>
			<?php echo $this->Html->link($matchPlayer['Team']['name'], array('controller' => 'teams', 'action' => 'view', $matchPlayer['Team']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Player'); ?></th>
		<td>
			<?php echo $this->Html->link($matchPlayer['Player']['name'], array('controller' => 'players', 'action' => 'view', $matchPlayer['Player']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Captain'); ?></th>
		<td>
			<?php echo h($matchPlayer['MatchPlayer']['is_captain']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Role'); ?></th>
		<td>
			<?php echo h($matchPlayer['MatchPlayer']['role']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($matchPlayer['MatchPlayer']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($matchPlayer['MatchPlayer']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>