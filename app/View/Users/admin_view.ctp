	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('User'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('EmployeeID'); ?></th>
		<td>
			<?php echo h($user['User']['employeeID']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Email'); ?></th>
		<td>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Password'); ?></th>
		<td>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Password Reset'); ?></th>
		<td>
			<?php echo h($user['User']['password_reset']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Verified'); ?></th>
		<td>
			<?php echo h($user['User']['is_verified']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Level'); ?></th>
		<td>
			<?php echo h($user['User']['level']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Role'); ?></th>
		<td>
			<?php echo h($user['User']['role']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Last Logged In Time'); ?></th>
		<td>
			<?php echo h($user['User']['last_logged_in_time']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Last Logged In Ip'); ?></th>
		<td>
			<?php echo h($user['User']['last_logged_in_ip']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>