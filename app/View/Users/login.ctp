<div class="container auth-page">
  <div class="row">
    <div class="col-md-3">
    </div>
    <div class="col-md-9">
      <h1>Login</h1>
      <div class="content-form">
        <div class="row">
          <div class="col-md-7">
            <?php $mainLabelOptions = array('class' => 'col-sm-2 control-label'); ?>
            <?php 
              echo $this->Form->create('User', array(
                'class' => 'form-horizontal form-without-legend',
                'role' => 'form',
                'inputDefaults' => array(
                  'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                  'div' => array('class' => 'form-group'),
                  'label' => $mainLabelOptions,
                  'between' => '<div class="col-sm-10">',
                  'after' => '</div>',
                  'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                )
              )); 
            ?>
              <div class="form-group">
                <?php $emailLabelOptions = array('text' => 'Email'); ?>
                <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => array_merge($mainLabelOptions, $emailLabelOptions))); ?>
              </div>
              <div class="form-group">
                <?php $passwordLabelOptions = array('text' => 'Password'); ?>
                <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => array_merge($mainLabelOptions, $passwordLabelOptions))); ?>
              </div>
            <div class="form-group text-center">
              <?php echo $this->Form->submit(__('Login'), array('class' => 'btn btn-danger')); ?>
              <p>Do not Have an account ? <?php echo $this->Html->link(__('Create an account'), array('controller' => 'users', 'action' => 'register')); ?></p>
              <p><?php echo $this->Html->link(__('Forgot Password ?'), array('controller' => 'users', 'action' => 'forgot_password')); ?></p>
            </div>

            <?php echo $this->Form->end(); ?>
            
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-2">
    </div>
  </div>
</div>

