<div class="container auth-page">
  <div class="row">

    <div class="col-md-9 col-md-offset-3">
      <h1>Create an account</h1>
      <div class="content-form">
        <div class="row">
          <div class="col-md-7 col-sm-7">
            <?php $mainLabelOptions = array('class' => 'col-lg-4 control-label'); ?>
            <?php 
              echo $this->Form->create('User', array(
                'class' => 'form-horizontal',
                'role' => 'form',
                'type' => 'file',
                'inputDefaults' => array(
                  'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                  'div' => array('class' => 'form-group'),
                  'label' => $mainLabelOptions,
                  'between' => '<div class="col-lg-8">',
                  'after' => '</div>',
                  'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                )
              )); 
            ?>
            <fieldset>
              <legend>Your personal details</legend>
              <div class="form-group">
                <?php $firstNameLabelOptions = array('text' => 'First Name'); ?>
                <?php echo $this->Form->input('first_name', array('class' => 'form-control', 'label' => array_merge($mainLabelOptions, $firstNameLabelOptions))); ?>  
              </div>
              <div class="form-group">
                <?php $lastNameLabelOptions = array('text' => 'Last Name'); ?>
                <?php echo $this->Form->input('last_name', array('class' => 'form-control', 'label' => array_merge($mainLabelOptions, $lastNameLabelOptions))); ?>  
              </div>
              <div class="form-group">
                <?php $phoneLabelOptions = array('text' => 'Phone'); ?>
                <?php echo $this->Form->input('phone', array('class' => 'form-control', 'label' => array_merge($mainLabelOptions, $phoneLabelOptions))); ?>  
              </div>
              <div class="form-group">
                <?php $emailLabelOptions = array('text' => 'Email'); ?>
                <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => array_merge($mainLabelOptions, $emailLabelOptions))); ?>
              </div>
              <div class="form-group">
                <?php $imageLabelOptions = array('text' => 'Photo'); ?>
                <?php echo $this->Form->input('image', array('class' => 'form-control', 'type' => 'file', 'label' => array_merge($mainLabelOptions, $imageLabelOptions))); ?>
              </div>
            </fieldset>
            <fieldset>
              <legend>Your Password</legend>
              <div class="form-group">
                <?php $passwordLabelOptions = array('text' => 'Password'); ?>
                <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => array_merge($mainLabelOptions, $passwordLabelOptions))); ?>
              </div>
              <div class="form-group">
                <?php $confirmPasswordLabelOptions = array('text' => 'Confirm Password'); ?>
                <?php echo $this->Form->input('password_confirm', array('class' => 'form-control', 'type' => 'password', 'label' => array_merge($mainLabelOptions, $confirmPasswordLabelOptions))); ?>
              </div>
            </fieldset>

            <div class="form-group submit-area text-center">
              <?php echo $this->Form->submit(__('Create an account'), array('class' => 'btn btn-danger')); ?>
              <p>Already Have an account ? <?php echo $this->Html->link(__('Sign In'), array('controller' => 'users', 'action' => 'login')); ?></p>
            </div>

            <?php echo $this->Form->end(); ?>
            
          </div>
          <div class="col-md-4 col-sm-4 pull-right">
            <div class="form-info">
              <h2><em>Important</em> Information</h2>
              <p>Please use your GCA email id only for registraion.</p>

              <p>Your GCA email address ends in @goa-cricket.org</p>
            </div>
          </div>
          
        </div>
      </div>
    </div>

  </div>
</div>