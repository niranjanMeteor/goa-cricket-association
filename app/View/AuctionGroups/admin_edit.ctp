	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Auction Group'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('AuctionGroup', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('name', array('class' => 'form-control', 'placeholder' => 'Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('minimum_amount', array('class' => 'form-control', 'placeholder' => 'Minimum Amount'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('raise', array('class' => 'form-control', 'placeholder' => 'Raise'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

