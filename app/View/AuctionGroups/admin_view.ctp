	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Auction Group'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($auctionGroup['AuctionGroup']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Name'); ?></th>
		<td>
			<?php echo h($auctionGroup['AuctionGroup']['name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Minimum Amount'); ?></th>
		<td>
			<?php echo h($auctionGroup['AuctionGroup']['minimum_amount']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Raise'); ?></th>
		<td>
			<?php echo h($auctionGroup['AuctionGroup']['raise']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($auctionGroup['AuctionGroup']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($auctionGroup['AuctionGroup']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>