	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Team Fixture'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('TeamFixture', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('team_id', array('class' => 'form-control', 'placeholder' => 'Team Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('against', array('class' => 'form-control', 'placeholder' => 'Against'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('start_date_time', array('class' => 'form-control', 'placeholder' => 'Start Date Time'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('end_date_time', array('class' => 'form-control', 'placeholder' => 'End Date Time'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('venue', array('class' => 'form-control', 'placeholder' => 'Venue'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

