	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Team Fixture'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($teamFixture['TeamFixture']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Team'); ?></th>
		<td>
			<?php echo $this->Html->link($teamFixture['Team']['name'], array('controller' => 'teams', 'action' => 'view', $teamFixture['Team']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Against'); ?></th>
		<td>
			<?php echo h($teamFixture['TeamFixture']['against']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Start Date Time'); ?></th>
		<td>
			<?php echo h($teamFixture['TeamFixture']['start_date_time']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('End Date Time'); ?></th>
		<td>
			<?php echo h($teamFixture['TeamFixture']['end_date_time']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Venue'); ?></th>
		<td>
			<?php echo h($teamFixture['TeamFixture']['venue']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($teamFixture['TeamFixture']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($teamFixture['TeamFixture']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>