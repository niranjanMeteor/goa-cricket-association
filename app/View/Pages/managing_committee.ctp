<?php echo $this->Html->css('search', array('inline' => false)); ?>
  <div clas="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="result-list table-responsive">
        <table class="table table-bordered table-hover search-table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Designation</th>
              <th>Email</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>Dr. SHEKHAR SALKAR</td>
              <td>President</td>
              <td>president@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. AMARESH NAIK</td>
              <td>1st Vice President</td>
              <td>vp1@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. SANJAY KANEKAR</td>
              <td>2nd Vice President</td>
              <td>vp2@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. MANOHAR NAIK</td>
              <td>3rd Vice President</td>
              <td>vp3@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. CHETAN DESAI</td>
              <td>Hon. Secretary</td>
              <td>secretary@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. VILAS DESSAI</td>
              <td>Hon. Treasurer</td>
              <td>treasurer@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. DESH KINLEKAR</td>
              <td>Jt. Secretary</td>
              <td>jointsec1@goacricket.org</td>
            </tr>
            <tr>
              <td> Mr. SUDIN KAMAT</td>
              <td>Jt. Secretary</td>
              <td>jointsec2@goacricket.org</td>
            </tr>
            <tr>
              <td>Mrs. ANAGHA ARLEKAR</td>
              <td>Jt. Secretary (Women)</td>
              <td>jointsec3@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. AKBAR MULLA</td>
              <td>Member</td>
              <td>akbar@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. SURYAKANT NAIK</td>
              <td>Member</td>
              <td>suryakant@goacricket.org</td>
            </tr>
            <tr>
              <td>MR. SURAJ LOTLIKAR</td>
              <td>Member</td>
              <td>suraj@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. DATTARAM SHETGAONKAR</td>
              <td>Member</td>
              <td>dattaram@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. ATUL NAIK</td>
              <td>Member</td>
              <td>atul@goacricket.org</td>
            </tr>
            <tr>
              <td>Mr. P.S SANDHU</td>
              <td>Member</td>
              <td>sandhu@goacricket.org</td>
            </tr>
          </tbody>

        </table>
      </div>
    </div>
  </div>