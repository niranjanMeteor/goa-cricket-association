<?php echo $this->Html->css('jobs', array('inline' => false)); ?>
<div class="row">
  <div class="col-md-12">
    <div class="job-header text-center">
      <p class="first-heading">Love Cricket ?</p>
      <p class="second-heading"><strong>So do we.</strong></p>
      <p class="third-heading">We are moving forward to a new age of digitalisation of cricket. <br /> We'd love if you join our growing team. <br />Come join us!</p>
      <?php echo $this->Html->link(__('Current Openings'), '#current-openings', array('class' => 'btn btn-primary')); ?>
    </div>

  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="about-gca">
        <h1><strong>Careers</strong></h1>
        <p>We at Goa Cricket Association shape up the young and budding cricketers from the State of Goa. We prepare them for the tough competitive level of cricket which separates men from boys.</p>
        <p>And working at this scale involves solving complicated problems. Come take a look at the problems we solve on an everyday basis.</p>
      </div>
    </div>
  </div>
</div>
<div class="container" id="current-openings">
  <div class="row">
    <div class="col-md-12">
      <div class="job-openings">
        <h2><strong>Come be one of Us in...</strong></h2>
        <h4>The land of Sun, Sand and Sea - Goa</h4>
        <ul class="list-unstyled arrow-list underline-list">
          <li><?php echo $this->Html->link(__('Chief Administration Officer'), array('controller' => 'pages', 'action' => 'cricket_administrator')); ?></li>
          <li><?php echo $this->Html->link(__('Finance Officer'), array('controller' => 'pages', 'action' => 'finance_officer')); ?></li>
        </ul>
      </div>
    </div>
  </div>
</div>