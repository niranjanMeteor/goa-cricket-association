<?php echo $this->Html->css('jobs', array('inline' => false)); ?>
<div class="row">
  <div class="col-md-12">
    <div class="job-header text-center">
      <p class="first-heading">Finance Officer</p>
      <p class="third-heading">We are looking for someone to revolutionize the way cricket is played in Goa. <br /> We'd love if you join our growing team. <br />Come join us!</p>
    </div>

  </div>
</div>
<div class="container" id="current-openings">
  <div class="row">
    <div class="col-md-9">
      <div>
        <h3>Roles & responsibilities:</h3>
        <ul class="list-unstyled arrow-list underline-list">
          <li>Ensure the best in class Financial and accounting control systems to meet the accounting and reporting requirement.</li>
          <li>Work on annual budget process along with treasurer/executive committee members.</li>
          <li>Able to handle the financial and taxation of association in liasion with office bearers of Goa Cricket association.</li>
          <li>Generate and analyse various management reports required by association.</li>
          <li>Provide professional guidance to all accounting staff and Treasurer of Goa Cricket Association.</li>
        </ul>
      </div>
      <div>
        <h3>Skills Required:</h3>
        <ul class="list-unstyled arrow-list underline-list">
          <li><strong>Essential:- </strong>M.Com / CA</li>
          <li><strong>Desirable:-</strong> 5-10 years post-qualification experience in accounting functions.<br /> Strong in accounting principles and processes. <br /> Strong interpersonal and good communication skills.<br /> Should be well versed with excel, word and should be able to work in ERP environment.</li>
        </ul>
      </div>
      <div>
        <h3>Salary & Compensation:</h3>
        <ul class="list-unstyled arrow-list underline-list">
          <li>Salary is negotiable and will not be a constraint for deserving candidates as per the qualifications and experience.</li>
          <li>Other benefits will be admissible as per association norms.</li>
        </ul>
      </div>
      <div>
        <h3>How to apply:</h3>
        <ul class="list-unstyled arrow-list underline-list">
          <li>Application alongwith CV and testimonials (if any) may be emailed to <a href="mailto:office@goacricket.org?subject=Job: Chief Administration Officer" target="_blank" class="mailto">office@goacricket.org</a> on or before 17th January 2015.</li>
          <li>Shortlisted candidates will be contacted accordingly.</li>
        </ul>
      </div>
    </div>

    <div class="col-md-3">
      <div class="apply-now">
        <h3><strong>Interested?</strong></h3>
        <p>Send a little something about yourself with links to your work.</p>
        <?php echo $this->Html->link(__('Apply Now'), 'mailto:office@goacricket.org?subject=Applying for the post of Cricket Administrator.', array('target' => '_blank', 'class' => 'btn btn-success btn-block')); ?>
      </div>
    </div>

  </div>
</div>