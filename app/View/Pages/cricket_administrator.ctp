<?php echo $this->Html->css('jobs', array('inline' => false)); ?>
<div class="row">
  <div class="col-md-12">
    <div class="job-header text-center">
      <p class="first-heading">Chief Administration Officer</p>
      <p class="third-heading">We are looking for someone to revolutionize the way cricket is played in Goa. <br /> We'd love if you join our growing team. <br />Come join us!</p>
    </div>

  </div>
</div>
<div class="container" id="current-openings">
  <div class="row">
    <div class="col-md-9">
      <div>
        <h3>Roles & responsibilities:</h3>
        <ul class="list-unstyled arrow-list underline-list">
          <li>Ensure the best in class general administration to meet the running day to day activity of the association.</li>
          <li>Expected to conduct various meetings binding with laws of the association along with committee members.</li>
          <li>Able to handle independently organising various tournaments both local and BCCI in the state of Goa under Goa Cricket Association.</li>
          <li>Generate and analyse various management reports required by association.</li>
          <li>Provide professional guidance to all administration staff and committee members of Goa Cricket Association.</li>
        </ul>
      </div>
      <div>
        <h3>Skills Required:</h3>
        <ul class="list-unstyled arrow-list underline-list">
          <li><strong>Essential:-</strong> Post Graduation with Sports Management experience. <br /> Preferably past experience of working with any cricket association or Sports Body.</li>
          <li><strong>Desirable:-</strong> MBA and Cricket representation of state in the Game. <br /> Strong interpersonal and good communication skills.<br /> Should be well versed with excel, word and should be able to work in ERP environment.</li>
        </ul>
      </div>
      <div>
        <h3>Salary & Compensation:</h3>
        <ul class="list-unstyled arrow-list underline-list">
          <li>Salary is negotiable and will not be a constraint for deserving candidates as per the qualifications and experience.</li>
          <li>Other benefits will be admissible as per association norms.</li>
        </ul>
      </div>
      <div>
        <h3>How to apply:</h3>
        <ul class="list-unstyled arrow-list underline-list">
          <li>Application alongwith CV and testimonials (if any) may be emailed to <a href="mailto:office@goacricket.org?subject=Job: Chief Administration Officer" target="_blank" class="mailto">office@goacricket.org</a> on or before 17th January 2015.</li>
          <li>Shortlisted candidates will be contacted accordingly.</li>
        </ul>
      </div>
    </div>

    <div class="col-md-3">
      <div class="apply-now">
        <h3><strong>Interested?</strong></h3>
        <p>Send a little something about yourself with links to your work.</p>
        <?php echo $this->Html->link(__('Apply Now'), 'mailto:office@goacricket.org?subject=Job: Chief Administration Officer.', array('target' => '_blank', 'class' => 'btn btn-success btn-block')); ?>
      </div>
    </div>

  </div>
</div>