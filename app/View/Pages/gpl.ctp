<?php echo $this->Html->css('gpl', array('inline' => false)) ?>
<?php echo $this->Html->script('gpl', array('inline' => false)); ?>
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <?php echo $this->Html->image(__('gpl_logo_no_bg.png'), array('class' => 'gpl-logo pull-left')); ?>
        <h3 class="title">Goa Professional League</h3>
        <div class="tagline"><small class="text-primary">Invitational T-20 tournament by EVENTURES @ Goa. </small></div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row card">
      <div class="col-md-3">
        <ul class="list-unstyled gpl-navigation-list">
          <li class="active">
            <a href="#"><i class="fa fa-info-circle"></i> About</a>
          </li>
          <li>
            <a href="#"><i class="fa fa-archive"></i>Player Basket</a>
          </li>
          <li>
            <?php echo $this->Html->link(__('<i class="fa fa-calendar"></i>Fixtures'), '#', array('escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link(__('<i class="fa fa-bar-chart"></i>Statistics'), '/gpl_stats', array('escape' => false)); ?>
          </li>
          <li>
            <?php echo $this->Html->link(__('<i class="fa fa-book"></i> Playing Conditions'), 'http://www.bcci.tv/about/2015/twenty20-playing-conditions', array('escape' => false, 'target' => '_blank')); ?>
          </li>
        </ul>
      </div>
      <div class="col-md-9 nav-content current">
        <p><strong>Goa Professional League (GPL)</strong> was a phenomenal success, with the Goan cricket fans celebrating as CRICKET FESTIVAL OF THE SEASON since its inception from 2010.  <strong>GPL</strong> witnessed participation of 8  franchises,  blending Goan and out station professional cricketers. Over the last two editions, ever since its matches are played under flood lights Goa Professional league taken a big leap forward.. GPL  popular among Goan cricket fans as it featured many out station star cricketers across India like Sudeep Tyagi, Parvez Rasool, Ramdayal Punia, Shahabuddin, Ian Dev Signh, Jod Shah, Kamran Khan, Indersekar Redy, Sourab Dubey, Vishal Singh, Amit Mishra, Paras Dogra..etc.  Overall GPL made its mark as a unique CRICKETING FESTIVAL where Goan fans witness nail biting finish in almost every match.</p>
        <p>This season 8 franchise will compete for CHAMPIONSHIP. “<strong>Dempo Cricket Club</strong>” eyeing for HAT-TRICK this year to create history in GPL-2015... “<strong>SARVODAYA ROYALS</strong>“ Promises a better performance this  season and aims at its maiden GPL T-20 Championship title.... "<strong>SALGAOCAR Sports Club</strong>" bidding with  energy to “CATCH THE CUP”.... “<strong>GENO Sports Club</strong>” Last season runners-up making all its effort to re- enter to FINALS. “<strong>CHOWGULE Warriors</strong>”, fighters on field assuring great fight to break the GPL jinx...  “<strong>CACULO Goans</strong>” Backed by Caculos making debut with this season of GPL. In addition to these, two  outstation teams confirmed participation in GPL-2015 will raise the cricket standards.</p>
      </div>
      <div class="col-md-9 nav-content player-basket">

        <div class="text-center bg-primary auction-list">GPL PLayer Auction List</div>

        <div class="text-center bg-success"><strong>Cateogy - ELITE </strong></div>
        <ol>
          <li>Amogh Desai</li>
          <li>Darshan Misal</li>
          <li>Sher Bahadur Yadav</li>
          <li>Robin D'Souza</li>
          <li>Sagun Kamat</li>
          <li>Saurabh Bandekar</li>
          <li>Shadab Jakati</li>
          <li>Swapnil Asnodkar</li>
        </ol>
        <div class="text-center bg-success"><strong>Category - A</strong></div>
        <ol>
          <li>Amit Yadav</li>
          <li>Amulya Pandrekar</li>
          <li>Deepraj Gaonkar</li>
          <li>Ganeshraj Narvekar</li>
          <li>Gauresh Gawas</li>
          <li>Keenan Vaz</li>
          <li>Lakshay Garg</li>
          <li>Prathamesh Gawas</li>
          <li>Prathmesh Salunke</li>
          <li>Rahul Keni</li>
          <li>Raj Shekhar Harikant</li>
          <li>Regan Pinto</li>
          <li>Rohan Belekar</li>
          <li>Rohit Asnodkar</li>
          <li>Ryan D'Souza</li>
          <li>Sagar Naik</li>
          <li>Samar Dubhashi</li>
          <li>Snehal Kauthankar</li>
          <li>Sumiran Amonkar</li>
          <li>Suraj Dongre</li>
          <li>Vedant Naik</li>
        </ol>
        <div class="text-center bg-success"><strong>Category - B</strong></div>
          <ol>
            <div class="row">
              <div class="col-md-4">
                <li>Achit Singwan</li>
                <li>Aditya Angle</li>
                <li>Ajay Kevat</li>
                <li>Akshay Naik</li>
                <li>Aniket Desai</li>
                <li>Arun Waghji</li>
                <li>Atul Yadav</li>
                <li>Basappa Madar</li>
                <li>Felix Alemao</li>
                <li>Gajanan Azgaonkar</li>
                <li>Gaurav Desai</li>
                <li>Harish Kutty</li>
                <li>Hemant Shetgaonkar</li>
                <li>Heramb Parab</li>
                <li>Hrishikesh Naik</li>
                <li>Iliyas Naroo</li>
                <li>Infancio Corrreia</li>
                <li>Ishan Gadekar</li>
                <li>Joaquiem Morrera</li>
                <li>Kallol Maitra</li>
              </div>
              <div class="col-md-4">
                <li>Kashyap Bakhle</li>
                <li>Kevin D'Costa</li>
                <li>Mahesh Rao</li>
                <li>Maliksab Shiroor</li>
                <li>Manjunath Vantamuri</li>
                <li>Maruthi Huggi</li>
                <li>Mayuresh Chari</li>
                <li>Ninad Pawaskar</li>
                <li>Neeraj Yadav</li>
                <li>Rajat Shet</li>
                <li>Rohit Naik (Jr.)</li>
                <li>Rohit Naik (Sr.)</li>
                <li>Sahil Dhuri</li>
                <li>Saurabh Shetgaonkar</li>
                <li>Shailesh Misquin</li>
                <li>Sharman Vaz</li>
                <li>Shivam Mapsekar</li>
                <li>Shiv Sagar Kashyap</li>
                <li>Shubham Dessai</li>
                <li>Siddesh Ramani</li>
              </div>
              <div class="col-md-4">
                <li>Shivprasad Purohit</li>
                <li>Srinivas Phadte</li>
                <li>Sufian Alam</li>
                <li>Shubham Banaulikar</li>
                <li>Sunny Kanekar</li>
                <li>Suyesh Prabhudessai</li>
                <li>Tunish Sawkar</li>
                <li>Vaishanv Pednekar</li>
                <li>Vijesh Prabhudessai</li>
                <li>Yogesh Barde</li>
                <li>Yogesh Chodankar</li>
              </div>
            </div>
          </ol>
      </div>
      <div class="col-md-9 nav-content player-basket">
        <table class="table table-hover table-responsive fixture-table">
          <thead>
            <tr>
              <th>Match Time</th>
              <th>Name</th>
              <th>Type</th>
              <th>Result</th>
            </tr>
          </thead>
          <tbody>
            <?php if (!empty($matches)): ?>
              <?php foreach ($matches as $match): ?>
                <tr>
                  <td><?php echo date('dS M Y, h:i A', strtotime($match['Match']['start_date_time'])) ?></td>
                  <td><?php echo $this->Html->link($match['FirstTeam']['name'], array('controller' => 'teams', 'action' => 'view', $match['FirstTeam']['id'])); ?> vs <?php echo $this->Html->link($match['SecondTeam']['name'], array('controller' => 'teams', 'action' => 'view', $match['SecondTeam']['id'])); ?></td>
                  <td><?php echo MatchType::stringValue($match['Match']['type']); ?></td>
                  <?php if ($match['Match']['is_complete']): ?>
                    <td><?php echo $this->Html->link(__('View Full Scorecard'), 'http://livescore.goacricket.org/matches/live_scores/' . $match['Match']['id'], array('class' => 'btn btn-success', 'target' => '_blank')); ?></td>
                  <?php endif; ?>
                </tr>
              <?php endforeach; ?>
            <?php endif; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>