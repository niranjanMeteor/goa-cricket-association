<?php echo $this->Html->script('build/react', array('inline' => false)); ?>
<?php echo $this->Html->script('socket.io', array('inline' => false)); ?>
<?php echo $this->Html->css('jquery.simplyscroll', array('inline' => false)); ?>
<?php echo $this->Html->script('jquery.simplyscroll.min', array('inline' => false)); ?>
<?php echo $this->Html->script('jquery.countdown.min', array('inline' => false)); ?>
<?php echo $this->Html->script('moment.min', array('inline' => false)); ?>
<?php echo $this->Html->script('index', array('inline' => false)); ?>
<div id="live-score-wrapper">
</div>
 <script src="js/build/livescore.react.js"></script>
<?php if ( ! empty($news)): ?>
  <div class="row">
    <div class="col-md-12 text-center">
      <ul id="scroller" class="list-inline">
        <?php foreach ($news as $id => $newsItem): ?>
          <li><?php echo $this->Html->link(__($newsItem), array('controller' => 'news', 'action' => 'view', $id), array('escape' => false)); ?></li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
<?php endif; ?>

<!--- BEGIN SLIDER -->
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      <li data-target="#carousel-example-generic" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="img/slide3.jpg" alt="first-slide">
        <div class="carousel-caption">
          Goa Ranji Team
        </div>
      </div>
      <div class="item">
        <img src="img/slide2.jpg" alt="second-slide">
        <div class="carousel-caption">
          Goa Women's Team
        </div>
      </div>
      <div class="item">
        <img src="img/slide1.jpg" alt="third-slide">
        <div class="carousel-caption">
          Meeting with CM
        </div>
      </div>
      <div class="item">
        <img src="img/slide4.jpg" alt="fourth-slide">
        <div class="carousel-caption">
          
        </div>
      </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
<!-- END SLIDER -->
<!-- BEGIN ABOUT SECTION -->
  <div id="about">
    <div class="container">
      <div class="row">
        <div class="col-md-3 text-center">
          <img src="img/combined_logo.png" />
        </div>
        <div class="col-md-6 text-center about-gca">
          <h2>Goa Cricket Association</h2>
          <h4>Affiliated to BCCI</h4>
          <p>Goa Cricket Association is the governing body of Cricket activities in the Indian state of Goa and the Goa cricket team. It is affiliated to the Board of Control for Cricket in India.</p>
        </div>
        <div class="col-md-3 text-center bcci-logo">
          <img src="img/bcci.png" />
        </div>
      </div>
      <div class="row team-block content">
        <h2 class="meet-the-team">Meet <span>the team</span></h2>
        <h4>This is the top brass of Goa Cricket Association.</h4>
        <div class="col-md-4 text-center member">
          <img src="img/member1.jpg" alt="Shekhar Salkar" class="img-responsive img-circle">
          <h3>Dr. Shekhar Salkar</h3>
          <em>President</em>
        </div>
        <div class="col-md-4 text-center member">
          <img src="img/member2.jpg" alt="Chetan Desai" class="img-responsive img-circle">
          <h3>Chetan Desai</h3>
          <em>Secretary</em>
        </div>
        <div class="col-md-4 text-center member">
          <img src="img/member3.jpg" alt="Vilas Desai" class="img-responsive img-circle">
          <h3>Vilas Desai</h3>
          <em>Treasurer</em>
        </div>
        <div class="text-center">
          <?php echo $this->Html->link(__('Managing Committee <span class="fa fa-external-link"></span>'), '/managing_committee', array('escape' => false, 'class' => 'btn btn-primary')); ?>
        </div>
        <br />
        <div class="text-center">
          <?php echo $this->Html->link(__('GCA Staffs <span class="fa fa-external-link"></span>'), '/gca_staffs', array('escape' => false, 'class' => 'btn btn-primary')); ?>
        </div>

      </div>
    </div>
  </div>
<!-- END ABOUT SECTION -->

<!-- BEGIN SERVICES SECTION -->
<div class="services-block content" id="services">
  <div class="container">
    <h2>Vision <span>projects</span></h2>
    <h4>Projects planned for phase-wise target oriented cricket development in Goa.</h4>
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-12 item">
        <i class="fa fa-heart"></i>
        <h3>Game Development Unit</h3>
        <p>Designed to take Goa Cricket <br /> A STEP FOWARD...</p>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 item">
        <i class="fa fa-mobile"></i>
        <h3>Club Activity Center</h3>
        <p>Single window e-support for GCA affliated clubs.</p>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 item">
        <i class="fa fa-group"></i>
        <h3>State Special Units</h3>
        <p>Aimed at modular development of Goan Cricket.</p>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-12 item">
        <i class="fa fa-cogs"></i>
        <h3>League Restructure</h3>
        <p>To enchance the standard of cricket at club level.</p>
      </div>
    </div>
  </div>
</div>
<!--- END SERVICES SECTION -->

<!-- BEGIN SHOWCASE SECTION -->
  <div id="showcase" class="content">
    <div class="container">
      <h2 class="margin-bottom-50">Latest <span>Showcase</span></h2>
    </div>

    <div class="row">
      <div class="item col-md-4 col-sm-6 col-xs-12">
        <img src="img/portfolio/1.jpg" class="img-responsive">
        <?php echo $this->Html->link(__(
          '<div class="valign-center-elem">
            <strong>GCA Stadium Project</strong>
            <em>Property</em>
            <b>Details</b>
          </div>'
          ), 
          '/stadium', 
          array('class' => 'zoom valign-center', 'escape' => false)); 
        ?>
      </div>

      <div class="item col-md-4 col-sm-6 col-xs-12">
        <img src="img/portfolio/2.jpg" class="img-responsive">
        <?php echo $this->Html->link(__(
              '<div class="valign-center-elem">
                <strong>Ranji Season 2014</strong>
                <em>Team</em>
                <b>Details</b>
              </div>'
          ), 
          array(
            'controller' => 'teams', 'action' => 'view', 1
          ),
          array('class' => 'zoom valign-center', 'escape' => false)
          ); 
        ?>
      </div>

      <div class="item col-md-4 col-sm-6 col-xs-12">
        <img src="img/portfolio/3.jpg" class="img-responsive">
        <?php echo $this->Html->link(__(
              '<div class="valign-center-elem">
                <strong>GPL</strong>
                <em>Tournament</em>
                <b>Details</b>
              </div>'
          ), 
          '/gpl',
          array('class' => 'zoom valign-center', 'escape' => false)
          ); 
        ?>
      </div>

      <!-- <div class="item col-md-2 col-sm-6 col-xs-12">
        <img src="img/portfolio/4.jpg" class="img-responsive">
        <a href="img/portfolio/4.jpg" class="zoom valign-center">
          <div class="valign-center-elem">
            <strong>e-Governance Project</strong>
            <em>Property</em>
            <b>Details</b>
          </div>
        </a>
      </div>

      <div class="item col-md-2 col-sm-6 col-xs-12">
        <img src="img/portfolio/5.jpg" class="img-responsive">
        <a href="img/portfolio/5.jpg" class="zoom valign-center">
          <div class="valign-center-elem">
            <strong>Women's Cricket Development Project</strong>
            <em>Property</em>
            <b>Details</b>
          </div>
        </a>
      </div>

      <div class="item col-md-2 col-sm-6 col-xs-12">
        <img src="img/portfolio/6.jpg" class="img-responsive">
        <a href="img/portfolio/6.jpg" class="zoom valign-center">
          <div class="valign-center-elem">
            <strong>School Cricket Development Project</strong>
            <em>Property</em>
            <b>Details</b>
          </div>
        </a>
      </div> -->

    </div>

  </div>
<!-- END SHOWCASE SECTION -->

<!--- BEGIN TEAMS SECTION -->
<div class="teams-block content" id="teams">
  <div class="container">
    <h2>Our <span>teams</span></h2>
    <h4>Various Teams representing the state of goa.</h4>
    <div class="row">
      <?php if (!empty($teams)): ?>
        <?php foreach ($teams as $team): ?>
          <div class="col-md-3 col-sm-3 col-xs-12 item">
            <h3><?php echo __($team['Team']['name']) ?></h3>
            <p><?php echo __($team['Team']['tagline']) ?></p>
            <?php echo $this->Html->link(__('View More <span class="glyphicon glyphicon-new-window"></span>'), array('controller' => 'teams', 'action' => 'view', $team['Team']['id']), array('escape' => false)); ?>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
  </div>
</div>
<!-- END TEAMS SECTION -->

<!-- BEGIN FIXURES SECTION -->
<div class="fixtures-block content" id="fixtures">
  <div class="container">
    <h2>Team <span>Fixtures</span></h2>
    <h4>Goa State team fixtures</h4>
    <div class="row">
      <div class="col-md-12">
        <div role="tabpanel">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#gpl" aria-controls="gpl" role="tab" data-toggle="tab">GPL-2015</a></li>
            <li role="presentation"><a href="#ranji" aria-controls="ranji" role="tab" data-toggle="tab">Ranji</a></li>
            <!-- <li role="presentation"><a href="#u-23" aria-controls="u-23" role="tab" data-toggle="tab">U-23</a></li>
            <li role="presentation"><a href="#u-19" aria-controls="u-19" role="tab" data-toggle="tab">U-19</a></li>
            <li role="presentation"><a href="#u-16" aria-controls="u-16" role="tab" data-toggle="tab">U-16</a></li>
            <li role="presentation"><a href="#u-14" aria-controls="u-14" role="tab" data-toggle="tab">U-14</a></li> -->
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="gpl">
              <table class="table table-hover table-responsive fixture-table">
                <thead>
                  <tr>
                    <th>Match Time</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Result</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if (!empty($matches)): ?>
                    <?php foreach ($matches as $match): ?>
                      <tr>
                        <td><?php echo date('dS M Y, h:i A', strtotime($match['Match']['start_date_time'])) ?></td>
                        <td><?php echo $this->Html->link($match['FirstTeam']['name'], array('controller' => 'teams', 'action' => 'view', $match['FirstTeam']['id'])); ?> vs <?php echo $this->Html->link($match['SecondTeam']['name'], array('controller' => 'teams', 'action' => 'view', $match['SecondTeam']['id'])); ?></td>
                        <td><?php echo MatchType::stringValue($match['Match']['type']); ?></td>
                        <?php if ($match['Match']['is_complete']): ?>
                          <td><?php echo $this->Html->link(__('View Full Scorecard'), 'http://livescore.goacricket.org/matches/live_scores/' . $match['Match']['id'], array('class' => 'btn btn-success', 'target' => '_blank')); ?></td>
                        <?php endif; ?>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="ranji">
              <table class="table table-hover table-responsive fixture-table">
                <thead>
                  <tr>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Against</th>
                    <th>Venue</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($teamFixtures as $fixture): ?>
                    <tr>
                      <td>
                        <?php echo date('dS M Y', strtotime($fixture['TeamFixture']['start_date_time'])) ?>
                      </td>
                      <td>
                        <?php echo date('dS M Y', strtotime($fixture['TeamFixture']['end_date_time'])) ?>
                      </td>
                      <td>
                        <?php echo __(ucfirst($fixture['TeamFixture']['against'])); ?>
                      </td>
                      <td>
                        <?php echo __($fixture['TeamFixture']['venue']); ?>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            </div>
            <!-- <div role="tabpanel" class="tab-pane" id="u-23">
              <table class="table table-hover table-responsive fixture-table">
                <thead>
                  <tr>
                    <th>Start Date Time</th>
                    <th>End Date Time</th>
                    <th>Match</th>
                    <th>Venue</th>
                    <th>Result</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      8th Dec 2014, 9:30 am
                    </td>
                    <td>
                      12th Dec 2014, 6:00pm
                    </td>
                    <td>
                      Goa vs Jharkhand
                    </td>
                    <td>
                      Fatorda Stadium
                    </td>
                    <td>
                      In Progress
                    </td>
                  </tr>
                  <tr>
                    <td>
                      15th Dec 2014, 9:30 am
                    </td>
                    <td>
                      19th Dec 2014, 6:00pm
                    </td>
                    <td>
                      Goa vs Karnataka
                    </td>
                    <td>
                      Fatorda Stadium
                    </td>
                    <td>
                      Yet to start
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="u-19">
              <table class="table table-hover table-responsive fixture-table">
                <thead>
                  <tr>
                    <th>Start Date Time</th>
                    <th>End Date Time</th>
                    <th>Match</th>
                    <th>Venue</th>
                    <th>Result</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      8th Dec 2014, 9:30 am
                    </td>
                    <td>
                      12th Dec 2014, 6:00pm
                    </td>
                    <td>
                      Goa vs Jharkhand
                    </td>
                    <td>
                      Fatorda Stadium
                    </td>
                    <td>
                      In Progress
                    </td>
                  </tr>
                  <tr>
                    <td>
                      15th Dec 2014, 9:30 am
                    </td>
                    <td>
                      19th Dec 2014, 6:00pm
                    </td>
                    <td>
                      Goa vs Karnataka
                    </td>
                    <td>
                      Fatorda Stadium
                    </td>
                    <td>
                      Yet to start
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="u-16">
              <table class="table table-hover table-responsive fixture-table">
                <thead>
                  <tr>
                    <th>Start Date Time</th>
                    <th>End Date Time</th>
                    <th>Match</th>
                    <th>Venue</th>
                    <th>Result</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      8th Dec 2014, 9:30 am
                    </td>
                    <td>
                      12th Dec 2014, 6:00pm
                    </td>
                    <td>
                      Goa vs Jharkhand
                    </td>
                    <td>
                      Fatorda Stadium
                    </td>
                    <td>
                      In Progress
                    </td>
                  </tr>
                  <tr>
                    <td>
                      15th Dec 2014, 9:30 am
                    </td>
                    <td>
                      19th Dec 2014, 6:00pm
                    </td>
                    <td>
                      Goa vs Karnataka
                    </td>
                    <td>
                      Fatorda Stadium
                    </td>
                    <td>
                      Yet to start
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div role="tabpanel" class="tab-pane" id="u-14">
              <table class="table table-hover table-responsive fixture-table">
                <thead>
                  <tr>
                    <th>Start Date Time</th>
                    <th>End Date Time</th>
                    <th>Match</th>
                    <th>Venue</th>
                    <th>Result</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      8th Dec 2014, 9:30 am
                    </td>
                    <td>
                      12th Dec 2014, 6:00pm
                    </td>
                    <td>
                      Goa vs Jharkhand
                    </td>
                    <td>
                      Fatorda Stadium
                    </td>
                    <td>
                      In Progress
                    </td>
                  </tr>
                  <tr>
                    <td>
                      15th Dec 2014, 9:30 am
                    </td>
                    <td>
                      19th Dec 2014, 6:00pm
                    </td>
                    <td>
                      Goa vs Karnataka
                    </td>
                    <td>
                      Fatorda Stadium
                    </td>
                    <td>
                      Yet to start
                    </td>
                  </tr>
                </tbody>
              </table>
            </div> -->
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- END FIXTURES SECTION -->