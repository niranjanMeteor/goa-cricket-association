<?php echo $this->Html->css('search', array('inline' => false)); ?>
  <div clas="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="result-list table-responsive">
        <table class="table table-bordered table-hover search-table">
          <thead>
            <tr>
              <th>Name</th>
              <th>Designation</th>
              <th>Email</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>Kajal Sawant</td>
              <td>Accountant</td>
              <td>kajal.sawant@goacricket.org</td>
            </tr>
            <tr>
              <td>Greta Pinto D'Souza</td>
              <td>Assistant</td>
              <td>greta.pinto@goacricket.org</td>
            </tr>
            <tr>
              <td>Victor Fernandes</td>
              <td>PA to Secretary</td>
              <td>victor.fernandes@goacricket.org</td>
            </tr>
            <tr>
              <td>Rohit Naik</td>
              <td>Office Clerk</td>
              <td>rohit.naik@goacricket.org</td>
            </tr>
            <tr>
              <td>Rajesh Redkar</td>
              <td>Receptionist</td>
              <td>rajesh.redkar@goacricket.org</td>
            </tr>
            <tr>
              <td>Patsy Fernandes</td>
              <td>Receptionist</td>
              <td>patsy.fernandes@goacricket.org</td>
            </tr>
            <tr>
              <td>Pratik Shirgaonkar</td>
              <td>Receptionist</td>
              <td>pratik.shirgaonkar@goacricket.org</td>
            </tr>
            <tr>
              <td>Pushkar Sawant</td>
              <td>Video Analyst</td>
              <td>pushkar.sawant@goacricket.org</td>
            </tr>
            <tr>
              <td>Santosh Karigar</td>
              <td>Video Analyst</td>
              <td>santosh.karigar@goacricket.org</td>
            </tr>
            <tr>
              <td>Tracy Fernandes</td>
              <td>Physiotherapist</td>
              <td>tracy.fernandes@goacricket.org</td>
            </tr>
            <tr>
              <td>Danny Pereira</td>
              <td>Physiotherapist</td>
              <td>danny.pereira@goacricket.org</td>
            </tr>
            <tr>
              <td>Prachi Lotlikar</td>
              <td>Physiotherapist</td>
              <td>prachi.lotlikar@goacricket.org</td>
            </tr>
            <tr>
              <td>Pradha Naik Shirodkar</td>
              <td>House Keeping</td>
              <td>pradhanaik.shirodkar@goacricket.org</td>
            </tr>
            <tr>
              <td>Prabhakar Bairgond</td>
              <td>Director of Training</td>
              <td>prabhakar.bairgond@goacricket.org</td>
            </tr>
            <tr>
              <td>Collin Rodrigues</td>
              <td>Coach</td>
              <td>collin.rodrigues@goacricket.org</td>
            </tr>
            <tr>
              <td>Sarvesh Naik</td>
              <td>Coach</td>
              <td>sarvesh.naik@goacricket.org</td>
            </tr>
            <tr>
              <td>Kashinath Chari</td>
              <td>Coach</td>
              <td>kashinath.chari@goacricket.org</td>
            </tr>
            <tr>
              <td>Anuradha Redkar</td>
              <td>Coach</td>
              <td>anuradha.redkar@goacricket.org</td>
            </tr>
            <tr>
              <td>Santoshi Rane</td>
              <td>Coach</td>
              <td>santoshi.rane@goacricket.org</td>
            </tr>
          </tbody>

        </table>
      </div>
    </div>
  </div>