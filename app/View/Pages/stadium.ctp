<?php echo $this->Html->css('stadium', array('inline' => false)); ?>

<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="stadium-overview card">
        <div class="title">
          <span>Goa Sports Village</span>
        </div>
        <div class="stadium-cover">
          <?php echo $this->Html->image(__('stadium.jpg'), array('class' => 'img-responsive')); ?>
          <div class="caption">
            <h3>Goa Cricket Stadium and Sports Village Plan</h3>
            <p> A step towards cricketing future...</p>
          </div>
        </div>
        <div class="stadium-description">
          <p>Proposed development of the Sports Village at Bicholim, Goa having cricket stadium of international standard with associated facilities and other amenities like water sports, amusement park, state-of-the-art accomodation facilities with required infrastructure to make the project viable, is a part of such development at Bicholim taluka in Goa State.</p>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="important-information card">
        <div class="title">
          <span>Important Announcements</span>
        </div>
        <div class="text-center">
          <?php echo $this->Html->link(__('Schedule of Bidding Process <span class="fa fa-external-link"></span>'), '/bidding_schedule', array('escape' => false, 'class' => 'btn red important-link')); ?>
        </div>
      </div>
      <div class="downloads card">
        <div class="title">
          Downloads
        </div>
        <?php echo $this->Html->link(__('<span>RFQ Document </span>
          <em>
          <i class="fa fa-tags"></i>
          RFQ for cricket stadium <br /> & sports village at bicholim </em>
          <i class="fa fa-book top-download-icon"></i>'),'/downloads/rfq_document.docx', array(
          'class' => 'btn green', 
          'escape' => false
          )); 
        ?>
        <?php echo $this->Html->link(__('<span>Site Plan </span>
          <em>
          <i class="fa fa-tags"></i>
          Annexure IX - Site Plan </em>
          <i class="fa fa-image top-download-icon"></i>'),array('controller' => 'pages', 'action' => 'download_file', 'site_plan.jpg'), array(
          'class' => 'btn yellow', 
          'escape' => false
          )); 
        ?>
        <?php echo $this->Html->link(__('<span>Area Statement </span>
          <em>
          <i class="fa fa-tags"></i>
          Annexure IX - Area Statement </em>
          <i class="fa fa-image top-download-icon"></i>'),array('controller' => 'pages', 'action' => 'download_file', 'area_statement.jpg'), array(
          'class' => 'btn red', 
          'escape' => false
          )); 
        ?>
        <?php echo $this->Html->link(__('<span>Survey Plan </span>
          <em>
          <i class="fa fa-tags"></i>
          Annexure VII - Survey Plan </em>
          <i class="fa fa-file top-download-icon"></i>'),array('controller' => 'pages', 'action' => 'download_file', 'survey_plan.pdf'), array(
          'class' => 'btn blue', 
          'escape' => false
          )); 
        ?>
        <?php echo $this->Html->link(__('<span>Contour Plan </span>
          <em>
          <i class="fa fa-tags"></i>
          Annexure VIII - Contour Plan </em>
          <i class="fa fa-file top-download-icon"></i>'),array('controller' => 'pages', 'action' => 'download_file', 'contour_plan.pdf'), array(
          'class' => 'btn blue-madison', 
          'escape' => false
          )); 
        ?>
      </div>
    </div>
  </div>
</div>