<?php echo $this->Html->css('bidding_schedule', array('inline' => false)); ?>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="bidding_schedule card">
        <div class="title">
          Addendum No 1 - Schedule of billing process
        </div>
        <div class="bidding-content">
        <h5>Addedum - 1 to Tender Clause 1.3: Schedule of Bidding Process</h5>
        <h3><strong>1.3 Schdule of Bidding Process</strong></h3>
        <p>The GCA shall endeavor to adhere to the following schedule:</p>
        <h2>Event <span>Description</span></h2>

        <div class="schedule-table">
          <div class="table-responsive">
            <table class="table table-hover custom-table">
              <thead>
                <tr>
                  <th><strong>Qualification Stage</strong></th>
                  <th><strong>Date</strong></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1. Last date for receiving queries</td>
                  <td>15 days from the issue of RFQ<br /> <strong>20th January 2015 (Tuesday)</strong></td>
                </tr>
                <tr>
                  <td>2. Pre-Application Conference</td>
                  <td>20 days from date of issue of RFQ <br /><strong>25th January 2015 (Sunday)</strong></td>
                </tr>
                <tr>
                  <td>3. GCA Response to queries</td>
                  <td>20 days from date of Pre-Application Conference <br /><strong>On or before 15th February 2015</strong></td>
                </tr>
                <tr>
                  <td>4. Application due date</td>
                  <td>50 days from date of Pre-Application Conference <br /><strong>15th March 2015</strong> </td>
                </tr>
                <tr>
                  <td>5. Announcement of qualified bidders</td>
                  <td>Within 30 days of Application due date <br /><strong>On or Before 15th April 2015</strong></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
          
        </div>
      </div>


    </div>
  </div>
</div>