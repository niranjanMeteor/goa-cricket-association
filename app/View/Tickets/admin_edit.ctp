	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Ticket'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('Ticket', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('project_id', array('class' => 'form-control', 'placeholder' => 'Project Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('creator_id', array('class' => 'form-control', 'placeholder' => 'Creator Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('assignee_id', array('class' => 'form-control', 'placeholder' => 'Assignee Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('category', array('class' => 'form-control', 'placeholder' => 'Category'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('title', array('class' => 'form-control', 'placeholder' => 'Title'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('description', array('class' => 'form-control', 'placeholder' => 'Description'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('status', array('class' => 'form-control', 'placeholder' => 'Status'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('start_date', array('class' => 'form-control', 'placeholder' => 'Start Date'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('end_date', array('class' => 'form-control', 'placeholder' => 'End Date'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('done', array('class' => 'form-control', 'placeholder' => 'Done'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('story_points', array('class' => 'form-control', 'placeholder' => 'Story Points'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('priority', array('class' => 'form-control', 'placeholder' => 'Priority'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('is_postponed', array('class' => 'form-control', 'placeholder' => 'Is Postponed'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

