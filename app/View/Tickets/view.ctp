<?php echo $this->Html->css('view_ticket', array('inline' => false)); ?>
<?php echo $this->Html->script('jquery.smooth-scroll', array('inline' => false)); ?>
<?php echo $this->Html->script('view_ticket', array('inline' => false)); ?>

<div class="page-container">
  <div class="page-head">
    <div class="container">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>View Ticket <small>Tickets &amp; task management</small></h1>
      </div>
      <!-- END PAGE TITLE -->
    </div>
  </div>
  <div class="page-content">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <div class="ticket-wrapper">
            <div class="ticket-header">
              <div class="ticket-actions pull-right">
                <?php echo $this->Html->link(__('<span class="fa fa-edit"></span> Update'), '#ticket-edit-form', array('escape' => false, 'class' => 'update-ticket')); ?>
                <?php echo $this->Html->link(__('<span class="fa fa-eye"></span> Watch'), array('controller' => 'tickets', 'action' => 'watch', $ticket['Ticket']['id']), array('escape' => false, 'class' => 'watch-ticket')); ?>
                <?php echo $this->Html->link(__('<span class="fa fa-trash"></span> Delete'), array('controller' => 'tickets', 'action' => 'delete', $ticket['Ticket']['id']), array('escape' => false, 'class' => 'delete-ticket')); ?>
              </div>
              <div>
                <span class="fa fa-ticket font-green-sharp"></span> <span class="font-green-sharp ticket-number">Ticket #<?php echo __($ticket['Ticket']['id']); ?></span> <span class="ticket-created"><?php echo date('M d, Y h:i:s', strtotime($ticket['Ticket']['created'])); ?></span>
              </div>
            </div>
            <hr />
            <div class="ticket-about">
              <h4 class="ticket-title"><?php echo __($ticket['Ticket']['title']); ?></h4>
              <div>Added by <?php echo $this->Html->link(__($ticket['Creator']['Profile']['name']), array('controller' => 'profiles', 'action' => 'view', $ticket['Ticket']['creator_id']), array('class' => 'font-green-sharp')); ?> <small><?php echo $this->Time->timeAgoInWords(strtotime($ticket['Ticket']['created'])); ?></small>. Updated <small><?php echo $this->Time->timeAgoInWords(strtotime($ticket['Ticket']['modified'])); ?></small>.</div>
              <div class="row">
                <div class="col-md-6">
                  <div class="static-data">
                    <div class="col-md-5 col-xs-5">
                      <strong>Status:</strong>
                    </div>
                    <div class="col-md-7 col-xs-7">
                      <?php echo __(TicketStatus::stringValue($ticket['Ticket']['status'])) ?>
                    </div>
                  </div>
                  <div class="static-data">
                    <div class="col-md-5 col-xs-5">
                      <strong>Priority:</strong>
                    </div>
                    <div class="col-md-7 col-xs-7">
                      <?php echo __(TicketPriority::stringValue($ticket['Ticket']['priority'])) ?>
                    </div>
                  </div>
                  <div class="static-data">
                    <div class="col-md-5 col-xs-5">
                      <strong>Assignee:</strong>
                    </div>
                    <div class="col-md-7 col-xs-7">
                      <?php echo $this->Html->link(__($ticket['Assignee']['Profile']['name']), array('controller' => 'profiles', 'action' => 'view',$ticket['Ticket']['assignee_id']), array('class' => 'font-green-sharp')); ?>
                    </div>
                  </div>
                  <div class="static-data">
                    <div class="col-md-5 col-xs-5">
                      <strong>Story Points:</strong>
                    </div>
                    <div class="col-md-7 col-xs-7">
                      <?php $storyPoints = !empty($ticket['Ticket']['story_points']) ? $ticket['Ticket']['story_points'] : '-'; ?>
                      <?php echo __($storyPoints); ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="static-data">
                    <div class="col-md-5 col-xs-5">
                      <strong>Start Date:</strong>
                    </div>
                    <div class="col-md-7 col-xs-7">
                      <?php echo __(date('d/m/Y', strtotime($ticket['Ticket']['start_date']))); ?>
                    </div>
                  </div>
                  <div class="static-data">
                    <div class="col-md-5 col-xs-5">
                      <strong>End Date:</strong>
                    </div>
                    <div class="col-md-7 col-xs-7">
                      <?php echo __(date('d/m/Y', strtotime($ticket['Ticket']['end_date']))); ?>
                    </div>
                  </div>
                  <div class="static-data">
                    <div class="col-md-5 col-xs-5">
                      <strong>% Done:</strong>
                    </div>
                    <div class="col-md-7 col-xs-7">
                      <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $ticket['Ticket']['done'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $ticket['Ticket']['done'] ?>%;">
                          <?php echo __($ticket['Ticket']['done']); ?>%
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr />
            <div class="ticket-description">
              <p><strong>Description</strong></p>
              <p><?php echo __($ticket['Ticket']['description']) ?></p>
              <?php if ( ! empty($ticket['TicketAttachment'])): ?>
                <?php foreach ($ticket['TicketAttachment'] as $attachment): ?>
                  <div class="attachment">
                    <?php echo $this->Html->link(__('<span class="fa fa-unlink"></span>' . ' ' . $attachment['name']), $attachment['url'], array('escape' => false, 'target' => '_blank')); ?>
                  </div>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
            <hr class="custom-separator"/>
            <?php if ( ! empty($ticket['TicketUpdate'])): ?>
              <div class="ticket-updates">
                <h4>Updates</h4>
                <?php foreach ($ticket['TicketUpdate'] as $id => $update): ?>
                  <div class="update">
                    <?php 
                      $commentNumber = '#' . ++$id;
                      $currentUrl =  Router::url( null, true );
                      $commentUrl = $currentUrl . $commentNumber; 
                    ?>
                    <?php echo $this->Html->link(__($commentNumber),  "$commentUrl", array('class' => 'pull-right')); ?>
                    <p class="updated-by">Updated By <?php echo $this->Html->link(__($update['User']['Profile']['name']), array('controller' => 'profiles', 'action' => 'view',$id), array('class' => 'font-green-sharp')); ?></p>
                    <p class="comment"><?php echo __($update['comment']); ?></p>
                  </div>
                <?php endforeach; ?>
              </div>
            <?php endif; ?>
            <hr class="custom-separator" />
            <div id="ticket-edit-form">
              <div class="form-wrapper">
                <div class="form-title">
                  <h5 class="font-red-sharp">Edit Ticket</h5>
                </div>
                <hr class="custom-separator" />
                <div class="row">
                  <div class="col-md-10">
                    <?php 
                      echo $this->Form->create('Ticket', array(
                        'class' => 'form-horizontal form-without-legend new-ticket-form',
                        'role' => 'form',
                        'type' => 'file',
                        'url' => array('controller' => 'tickets', 'action' => 'edit', $ticket['Ticket']['id'])
                      )); 
                    ?>
                    <?php echo $this->Form->input('category', array('class' => 'form-control', 'wrap' => 'col-sm-3', 'afterInput' => '<span class="fa fa-asterisk required-sign required-select"></span>')); ?>
                    <?php echo $this->Form->input('title', array('class' => 'form-control', 'afterInput' => '<span class="fa fa-asterisk required-sign required-text"></span>', 'value' => $ticket['Ticket']['title'])); ?>
                    <?php echo $this->Form->input('description', array('class' => 'form-control', 'type' => 'textarea',  'afterInput' => '<span class="fa fa-asterisk required-sign required-textarea"></span>', 'value' => str_replace('<br />', '', $ticket['Ticket']['description']))); ?>
                    <div class="row inner-row">
                      <div class="col-md-6">
                        <?php echo $this->Form->input('project_id', array('class' => 'form-control', 'wrap' => 'col-sm-7', 'value' => $ticket['Ticket']['project_id'])); ?>
                      </div>
                      <div class="col-md-6">
                        <?php echo $this->Form->input('assignee_id', array('class' => 'form-control', 'wrap' => 'col-sm-7', 'value' => $ticket['Ticket']['assignee_id'])); ?>
                      </div>
                    </div>
                    <div class="row inner-row">
                      <div class="col-md-6">
                        <?php echo $this->Form->input('status', array('class' => 'form-control', 'wrap' => 'col-sm-7', 'value' => $ticket['Ticket']['status'])); ?>
                      </div>
                      <div class="col-md-6">
                        <?php echo $this->Form->input('priority', array('class' => 'form-control', 'wrap' => 'col-sm-7', 'value' => $ticket['Ticket']['priority'])); ?>
                      </div>
                    </div>
                    <div class="row inner-row">
                      <div class="col-md-6">
                        <?php echo $this->Form->input('start_date', array('class' => 'form-control', 'wrap' => 'col-sm-10 custom-date', 'selected' => date('Y-m-d', strtotime($ticket['Ticket']['start_date'])))); ?>
                      </div>
                      <div class="col-md-6">
                        <?php echo $this->Form->input('end_date', array('class' => 'form-control', 'wrap' => 'col-sm-10 custom-date', 'selected' => date('Y-m-d', strtotime($ticket['Ticket']['end_date'])))); ?>
                      </div>
                    </div>
                    <div class="row inner-row">
                      <div class="col-md-6">
                        <?php echo $this->Form->input('done', array('class' => 'form-control', 'wrap' => 'col-sm-7', 'label' => '% Done', 'type' => 'select', 'options' => $doneOptions, 'value' => $ticket['Ticket']['done'])); ?>
                      </div>
                      <div class="col-md-6">
                        <?php echo $this->Form->input('story_points', array('class' => 'form-control', 'wrap' => 'col-sm-7', 'help' => 'Difficulty level 1(lowest) to 10(highest)', 'value' => $ticket['Ticket']['story_points'])); ?>
                      </div>
                    </div>
                    <div class="row ">
                      <div class="col-md-12">
                        <?php echo $this->Form->input('TicketUpdate.0.comment',array('class' => 'form-control', 'label' => 'Comment', 'required' => false)); ?>
                      </div>
                    <div class="row inner-row">
                      <div class="col-md-6">
                        <?php echo $this->Form->input('attachments.', array('class' => 'form-control', 'type' => 'file', 'multiple', 'label' => 'Files', 'wrap' => 'col-sm-7', 'required' => false)); ?>
                      </div>
                    </div>
                    </div>
                    <div class="row inner-row">
                      <div class="col-md-3">
                        <?php echo $this->Form->submit(__('Update'), array('class' => 'btn btn-success btn-block')); ?>
                      </div>
                    </div>
                    <?php echo $this->Form->end(); ?>
                  </div>  
                </div>
              </div>
            </div>
          </div>
            
        </div>
      </div>
    </div>
  </div>
</div>