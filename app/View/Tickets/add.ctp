<?php echo $this->Html->css('chosen.min', array('inline' => false)); ?>
<?php echo $this->Html->css('add_ticket', array('inline' => false)); ?>
<?php echo $this->Html->script('chosen.jquery.min', array('inline' => false)); ?>
<?php echo $this->Html->script('add_ticket', array('inline' => false)); ?>

<div class="page-container">
  <div class="page-head">
    <div class="container">
      <!-- BEGIN PAGE TITLE -->
      <div class="page-title">
        <h1>New Ticket <small>Tickets &amp; task management</small></h1>
      </div>
      <!-- END PAGE TITLE -->
    </div>
  </div>
  <div class="page-content">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="form-wrapper">
            <div class="form-title">
              <h3 class="font-green-sharp">Create Ticket</h3>
            </div>
            <hr />
            <div class="row">
              <div class="col-md-10">
                <?php 
                  echo $this->Form->create('Ticket', array(
                    'class' => 'form-horizontal form-without-legend new-ticket-form',
                    'role' => 'form',
                    'type' => 'file'
                  )); 
                ?>
                <?php echo $this->Form->input('category', array('class' => 'form-control', 'wrap' => 'col-sm-3', 'afterInput' => '<span class="fa fa-asterisk required-sign required-select"></span>')); ?>
                <?php echo $this->Form->input('title', array('class' => 'form-control', 'afterInput' => '<span class="fa fa-asterisk required-sign required-text"></span>')); ?>
                <?php echo $this->Form->input('description', array('class' => 'form-control', 'type' => 'textarea',  'afterInput' => '<span class="fa fa-asterisk required-sign required-textarea"></span>')); ?>
                <div class="row inner-row">
                  <div class="col-md-6">
                    <?php echo $this->Form->input('project_id', array('class' => 'form-control', 'wrap' => 'col-sm-7')); ?>
                  </div>
                  <div class="col-md-6">
                    <?php echo $this->Form->input('assignee_id', array('class' => 'form-control', 'wrap' => 'col-sm-7')); ?>
                  </div>
                </div>
                <div class="row inner-row">
                  <div class="col-md-6">
                    <?php echo $this->Form->input('status', array('class' => 'form-control', 'wrap' => 'col-sm-7')); ?>
                  </div>
                  <div class="col-md-6">
                    <?php echo $this->Form->input('priority', array('class' => 'form-control', 'wrap' => 'col-sm-7')); ?>
                  </div>
                </div>
                <div class="row inner-row">
                  <div class="col-md-6">
                    <?php echo $this->Form->input('start_date', array('class' => 'form-control', 'wrap' => 'col-sm-10 custom-date')); ?>
                  </div>
                  <div class="col-md-6">
                    <?php echo $this->Form->input('end_date', array('class' => 'form-control', 'wrap' => 'col-sm-10 custom-date')); ?>
                  </div>
                </div>
                <div class="row inner-row">
                  <div class="col-md-6">
                    <?php echo $this->Form->input('done', array('class' => 'form-control', 'wrap' => 'col-sm-7', 'label' => '% Done', 'type' => 'select', 'options' => $doneOptions)); ?>
                  </div>
                  <div class="col-md-6">
                    <?php echo $this->Form->input('story_points', array('class' => 'form-control', 'wrap' => 'col-sm-7', 'help' => 'Difficulty level 1(lowest) to 10(highest)')); ?>
                  </div>
                </div>
                <div class="row inner-row">
                  <div class="col-md-6">
                    <?php echo $this->Form->input('TicketWatcher.user_id', array('class' => 'form-control chosen-select', 'data-placeholder' => 'Choose Multiple users', 'type' => 'select', 'label' => 'Watchers', 'multiple' => true, 'required' => false, 'options' => $assignees, 'wrap' => 'col-sm-7'));  ?>
                  </div>
                  <div class="col-md-6">
                    <?php echo $this->Form->input('attachments.', array('class' => 'form-control', 'type' => 'file', 'multiple', 'label' => 'Files', 'wrap' => 'col-sm-7', 'required' => false)); ?>
                  </div>
                </div>
                <div class="row inner-row">
                  <div class="col-md-3">
                    <?php echo $this->Form->submit(__('Create'), array('class' => 'btn btn-success btn-block')); ?>
                  </div>
                </div>
                <?php echo $this->Form->end(); ?>
              </div>  
            </div>
          </div>
            
        </div>
      </div>
    </div>
  </div>
</div>