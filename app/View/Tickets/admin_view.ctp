	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Ticket'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Project'); ?></th>
		<td>
			<?php echo $this->Html->link($ticket['Project']['name'], array('controller' => 'projects', 'action' => 'view', $ticket['Project']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Creator'); ?></th>
		<td>
			<?php echo $this->Html->link($ticket['Creator']['email'], array('controller' => 'users', 'action' => 'view', $ticket['Creator']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Assignee'); ?></th>
		<td>
			<?php echo $this->Html->link($ticket['Assignee']['email'], array('controller' => 'users', 'action' => 'view', $ticket['Assignee']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Category'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['category']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Title'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['title']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Description'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['description']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Status'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['status']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Start Date'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['start_date']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('End Date'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['end_date']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Done'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['done']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Story Points'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['story_points']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Priority'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['priority']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Is Postponed'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['is_postponed']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($ticket['Ticket']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>