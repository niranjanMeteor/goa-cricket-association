	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Tickets'); ?></h1>
		</div><!-- end col md 12 -->
	</div><!-- end row -->


	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<thead>
			<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('project_id'); ?></th>
		<th><?php echo $this->Paginator->sort('creator_id'); ?></th>
		<th><?php echo $this->Paginator->sort('assignee_id'); ?></th>
		<th><?php echo $this->Paginator->sort('category'); ?></th>
		<th><?php echo $this->Paginator->sort('title'); ?></th>
		<th><?php echo $this->Paginator->sort('description'); ?></th>
		<th><?php echo $this->Paginator->sort('status'); ?></th>
		<th><?php echo $this->Paginator->sort('start_date'); ?></th>
		<th><?php echo $this->Paginator->sort('end_date'); ?></th>
		<th><?php echo $this->Paginator->sort('done'); ?></th>
		<th><?php echo $this->Paginator->sort('story_points'); ?></th>
		<th><?php echo $this->Paginator->sort('priority'); ?></th>
		<th><?php echo $this->Paginator->sort('is_postponed'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('modified'); ?></th>
		<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($tickets as $ticket): ?>
					<tr>
						<td><?php echo h($ticket['Ticket']['id']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($ticket['Project']['name'], array('controller' => 'projects', 'action' => 'view', $ticket['Project']['id'])); ?>
		</td>
								<td>
			<?php echo $this->Html->link($ticket['Creator']['email'], array('controller' => 'users', 'action' => 'view', $ticket['Creator']['id'])); ?>
		</td>
								<td>
			<?php echo $this->Html->link($ticket['Assignee']['email'], array('controller' => 'users', 'action' => 'view', $ticket['Assignee']['id'])); ?>
		</td>
						<td><?php echo h($ticket['Ticket']['category']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['title']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['description']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['status']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['start_date']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['end_date']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['done']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['story_points']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['priority']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['is_postponed']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['created']); ?>&nbsp;</td>
						<td><?php echo h($ticket['Ticket']['modified']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $ticket['Ticket']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $ticket['Ticket']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $ticket['Ticket']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $ticket['Ticket']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
		</tbody>
	</table>

	<p>
		<small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
	</p>

	<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
	<ul class="pagination pagination-sm">
		<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
	</ul>
	<?php } ?>
