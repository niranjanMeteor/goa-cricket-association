	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Ticket Watcher'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($ticketWatcher['TicketWatcher']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Ticket'); ?></th>
		<td>
			<?php echo $this->Html->link($ticketWatcher['Ticket']['title'], array('controller' => 'tickets', 'action' => 'view', $ticketWatcher['Ticket']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('User'); ?></th>
		<td>
			<?php echo $this->Html->link($ticketWatcher['User']['email'], array('controller' => 'users', 'action' => 'view', $ticketWatcher['User']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($ticketWatcher['TicketWatcher']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($ticketWatcher['TicketWatcher']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>