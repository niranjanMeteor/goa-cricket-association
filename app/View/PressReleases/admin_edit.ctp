	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Press Release'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('PressRelease', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('subject', array('class' => 'form-control', 'placeholder' => 'Subject'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('body', array('class' => 'form-control', 'placeholder' => 'Body'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('date', array('class' => 'form-control', 'placeholder' => 'Date'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('authority_designation', array('class' => 'form-control', 'placeholder' => 'Authority Designation', 'type' => 'select', 'options' => $authorityDesignations));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('authority_name', array('class' => 'form-control', 'placeholder' => 'Authority Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

