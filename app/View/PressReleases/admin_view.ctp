	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Press Release'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($pressRelease['PressRelease']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Subject'); ?></th>
		<td>
			<?php echo h($pressRelease['PressRelease']['subject']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Body'); ?></th>
		<td>
			<?php echo h($pressRelease['PressRelease']['body']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Date'); ?></th>
		<td>
			<?php echo h($pressRelease['PressRelease']['date']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Authority Designation'); ?></th>
		<td>
			<?php echo h($pressRelease['PressRelease']['authority_designation']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Authority Name'); ?></th>
		<td>
			<?php echo h($pressRelease['PressRelease']['authority_name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($pressRelease['PressRelease']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($pressRelease['PressRelease']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>