<?php echo $this->Html->css('view_press_release', array('inline' => false)); ?>

<div class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-3 text-center">
        <?php echo $this->Html->image(__('combined_logo.png'), array()); ?>
      </div>
      <div class="col-md-6 text-center center-row">
        <h1><strong>Goa Cricket Association</strong></h1>
        <div class="affiliated">
          <p>Affiliated To</p>
          <hr class="affiliated-separator" />
        </div>
        <h3>The Board of Control for Cricket in India</h3>
        <p>(Society Regd. No. 585)</p>
      </div>
      <div class="col-md-3 text-center">
        <?php echo $this->Html->image(__('bcci.png'), array('class' => 'bcci-logo')); ?>
      </div>
      <hr class="black" />
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="subject text-center">
          <h5><strong>Subject: </strong><?php echo __($pressRelease['PressRelease']['subject']); ?></h5>
        </div>
        <div class="body">
          <p><?php echo __(nl2br($pressRelease['PressRelease']['body'])); ?></p>
        </div>
        <div class="release-footer">
          <div class="authority pull-right">
            <p>(<?php echo __(Designations::stringValue($pressRelease['PressRelease']['authority_designation'])) ?>)</p>
            <p><strong><?php echo __($pressRelease['PressRelease']['authority_name']); ?></strong></p>
          </div>
          <div clas="date">
            <p><strong>Date:</strong> <?php echo date('F d, Y', strtotime($pressRelease['PressRelease']['date'])) ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>