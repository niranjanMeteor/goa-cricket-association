
<?php echo $this->Html->css('search', array('inline' => false)); ?>
  <div class="row">
    <div class="col-md-9 col-md-offset-1">
      <div class="search-form">
        <?php echo $this->Form->create('PressRelease', array('type' => 'get', 'role' => 'form')) ?>
        <div class="form-group">
          <?php echo $this->Form->input('q', array('class' => 'form-control', 'label' => false, 'afterIn')); ?>
        </div>
        <?php echo $this->Form->button(__('Search <span class="fa fa-search"></span>'), array('class' => 'btn btn-success', 'escape' => false)) ?>
        <?php $this->Form->end(); ?>
        <form>
        </form>
      </div>
    </div>
  </div>
<?php if ( ! empty($pressReleases)): ?>
  <div clas="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="result-list table-responsive">
        <table class="table table-bordered table-hover search-table">

          <thead>
            <tr>
              <th><?php echo $this->Paginator->sort('date'); ?></th>
              <th><?php echo $this->Paginator->sort('title'); ?></th>
              <th><?php echo $this->Paginator->sort('authority_name'); ?></th>
              <th></th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($pressReleases as $pressRelease): ?>
              <tr>
                <td>
                  <?php echo date('F d, Y', strtotime($pressRelease['PressRelease']['date'])); ?>
                </td>
                <td>
                  <?php echo __($pressRelease['PressRelease']['subject']); ?>
                </td>
                <td>
                  <?php echo __($pressRelease['PressRelease']['authority_name']); ?>
                </td>
                <td>
                  <?php echo $this->Html->link(__('View'), array('controller' => 'press_releases', 'action' => 'view', $pressRelease['PressRelease']['id']), array('class' => 'btn btn-xs btn-default red-stripe')); ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>

        </table>
      </div>
      <p>
        <small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
      </p>

      <?php
          $params = $this->Paginator->params();
          if ($params['pageCount'] > 1) {
          ?>
      <ul class="pagination pagination-sm">
        <?php
              echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
              echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
              echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
            ?>
      </ul>
      <?php } ?>
    </div>
  </div>
<?php else: ?>

  <h3 class="text-center">No Results to display</h3>

<?php endif; ?>