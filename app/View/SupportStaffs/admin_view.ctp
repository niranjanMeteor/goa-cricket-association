	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Support Staff'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($supportStaff['SupportStaff']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Role'); ?></th>
		<td>
			<?php echo h($supportStaff['SupportStaff']['role']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('First Name'); ?></th>
		<td>
			<?php echo h($supportStaff['SupportStaff']['first_name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Last Name'); ?></th>
		<td>
			<?php echo h($supportStaff['SupportStaff']['last_name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Email'); ?></th>
		<td>
			<?php echo h($supportStaff['SupportStaff']['email']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Phone'); ?></th>
		<td>
			<?php echo h($supportStaff['SupportStaff']['phone']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Image'); ?></th>
		<td>
			<?php echo h($supportStaff['SupportStaff']['image']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Dir'); ?></th>
		<td>
			<?php echo h($supportStaff['SupportStaff']['dir']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($supportStaff['SupportStaff']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($supportStaff['SupportStaff']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>