	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Add Ticket Attachment'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('TicketAttachment', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('ticket_id', array('class' => 'form-control', 'placeholder' => 'Ticket Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('name', array('class' => 'form-control', 'placeholder' => 'Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('mime_type', array('class' => 'form-control', 'placeholder' => 'Mime Type'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('url', array('class' => 'form-control', 'placeholder' => 'Url'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('size', array('class' => 'form-control', 'placeholder' => 'Size'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

