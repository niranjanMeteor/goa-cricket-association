	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Ticket Attachment'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($ticketAttachment['TicketAttachment']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Ticket'); ?></th>
		<td>
			<?php echo $this->Html->link($ticketAttachment['Ticket']['title'], array('controller' => 'tickets', 'action' => 'view', $ticketAttachment['Ticket']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Name'); ?></th>
		<td>
			<?php echo h($ticketAttachment['TicketAttachment']['name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Mime Type'); ?></th>
		<td>
			<?php echo h($ticketAttachment['TicketAttachment']['mime_type']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Url'); ?></th>
		<td>
			<?php echo h($ticketAttachment['TicketAttachment']['url']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Size'); ?></th>
		<td>
			<?php echo h($ticketAttachment['TicketAttachment']['size']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($ticketAttachment['TicketAttachment']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($ticketAttachment['TicketAttachment']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>