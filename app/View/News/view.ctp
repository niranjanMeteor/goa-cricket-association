<?php echo $this->Html->css('view_news', array('inline' => false)); ?>

<div class="container">
  <div class="row">
    <div class="col-md-9">
      <div class="main-news card">
        <div class="title">
          <span><?php echo __($news['News']['title']) ?></span>
        </div>
        <div class="meta-data">

          <a href="#"><span class="fa fa-map-marker"></span> <?php echo __($news['News']['location']); ?></a>, 
          <a href="#"><span class="fa fa-calendar"></span> <?php echo date('dS M Y', strtotime($news['News']['date'])) ?></a>

        </div>
          <?php if (!empty($news['News']['image'])): ?>
            <div class="news-cover">
              <?php echo $this->Html->image("{$news['News']['dir']}/{$news['News']['image']}", array('pathPrefix' => 'files/news/image/')); ?>
            </div>
          <?php endif; ?>
        <div class="news-description">
          <p><?php echo __(nl2br($news['News']['body'])); ?></p>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div class="recent-news card">
        <div class="title">
          <span><?php echo __('Recent News'); ?></span>
        </div>
        <?php if (!empty($recentNews)): ?>
        <ul class="list-unstyled recent-news-list">
          <?php foreach ($recentNews as $id => $recentNewsItem): ?>
            <li class="recent-news-item">
              <?php echo $this->Html->link(__($recentNewsItem), array('controller' => 'news', 'action' => 'view', $id), array('escape' => false)); ?>
            </li>
          <?php endforeach; ?>
        </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>