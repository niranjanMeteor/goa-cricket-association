	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Team Player'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($teamPlayer['TeamPlayer']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Team'); ?></th>
		<td>
			<?php echo $this->Html->link($teamPlayer['Team']['name'], array('controller' => 'teams', 'action' => 'view', $teamPlayer['Team']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Player'); ?></th>
		<td>
			<?php echo $this->Html->link($teamPlayer['Player']['id'], array('controller' => 'players', 'action' => 'view', $teamPlayer['Player']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Status'); ?></th>
		<td>
			<?php echo h($teamPlayer['TeamPlayer']['status']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($teamPlayer['TeamPlayer']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($teamPlayer['TeamPlayer']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>