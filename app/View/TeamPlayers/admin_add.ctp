	<div class="row">
		<div class="col-md-6">
			<h1><?php echo __('Admin Add Team Player'); ?></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6">
			<?php echo $this->Form->create('TeamPlayer', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('team_id', array('class' => 'form-control', 'placeholder' => 'Team Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('player_id', array('class' => 'form-control', 'placeholder' => 'Player Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('status', array('class' => 'form-control', 'placeholder' => 'Status'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>
		</div>
	</div>

