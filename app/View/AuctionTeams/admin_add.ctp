	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Add Auction Team'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('AuctionTeam', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('team_id', array('class' => 'form-control', 'placeholder' => 'Team Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('budget', array('class' => 'form-control', 'placeholder' => 'Budget'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('spent', array('class' => 'form-control', 'placeholder' => 'Spent'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('required', array('class' => 'form-control', 'placeholder' => 'Required'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

