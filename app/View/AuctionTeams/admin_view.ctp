	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Auction Team'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($auctionTeam['AuctionTeam']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Team'); ?></th>
		<td>
			<?php echo $this->Html->link($auctionTeam['Team']['name'], array('controller' => 'teams', 'action' => 'view', $auctionTeam['Team']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Budget'); ?></th>
		<td>
			<?php echo h($auctionTeam['AuctionTeam']['budget']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Spent'); ?></th>
		<td>
			<?php echo h($auctionTeam['AuctionTeam']['spent']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Required'); ?></th>
		<td>
			<?php echo h($auctionTeam['AuctionTeam']['required']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($auctionTeam['AuctionTeam']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($auctionTeam['AuctionTeam']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>