	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Player Bowling Record'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Player'); ?></th>
		<td>
			<?php echo $this->Html->link($playerBowlingRecord['Player']['id'], array('controller' => 'players', 'action' => 'view', $playerBowlingRecord['Player']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Level'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['level']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Match Count'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['match_count']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Innings Count'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['innings_count']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Balls Bowled'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['balls_bowled']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Runs Given'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['runs_given']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Wickets Taken'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['wickets_taken']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Bowling Average'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['bowling_average']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Best Bowling Innings'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['best_bowling_innings']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Best Bowling Match'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['best_bowling_match']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Bowling Economy'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['bowling_economy']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Strike Rate'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['strike_rate']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Four Wicket Innings'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['four_wicket_innings']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Five Wicket Innings'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['five_wicket_innings']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Ten Wicket Matches'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['ten_wicket_matches']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($playerBowlingRecord['PlayerBowlingRecord']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>