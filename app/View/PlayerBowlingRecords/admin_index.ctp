	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Player Bowling Records'); ?></h1>
		</div><!-- end col md 12 -->
	</div><!-- end row -->


	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<thead>
			<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('player_id'); ?></th>
		<th><?php echo $this->Paginator->sort('level'); ?></th>
		<th><?php echo $this->Paginator->sort('match_count'); ?></th>
		<th><?php echo $this->Paginator->sort('innings_count'); ?></th>
		<th><?php echo $this->Paginator->sort('balls_bowled'); ?></th>
		<th><?php echo $this->Paginator->sort('runs_given'); ?></th>
		<th><?php echo $this->Paginator->sort('wickets_taken'); ?></th>
		<th><?php echo $this->Paginator->sort('bowling_average'); ?></th>
		<th><?php echo $this->Paginator->sort('best_bowling_innings'); ?></th>
		<th><?php echo $this->Paginator->sort('best_bowling_match'); ?></th>
		<th><?php echo $this->Paginator->sort('bowling_economy'); ?></th>
		<th><?php echo $this->Paginator->sort('strike_rate'); ?></th>
		<th><?php echo $this->Paginator->sort('four_wicket_innings'); ?></th>
		<th><?php echo $this->Paginator->sort('five_wicket_innings'); ?></th>
		<th><?php echo $this->Paginator->sort('ten_wicket_matches'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('modified'); ?></th>
		<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($playerBowlingRecords as $playerBowlingRecord): ?>
					<tr>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['id']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($playerBowlingRecord['Player']['id'], array('controller' => 'players', 'action' => 'view', $playerBowlingRecord['Player']['id'])); ?>
		</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['level']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['match_count']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['innings_count']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['balls_bowled']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['runs_given']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['wickets_taken']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['bowling_average']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['best_bowling_innings']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['best_bowling_match']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['bowling_economy']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['strike_rate']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['four_wicket_innings']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['five_wicket_innings']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['ten_wicket_matches']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['created']); ?>&nbsp;</td>
						<td><?php echo h($playerBowlingRecord['PlayerBowlingRecord']['modified']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $playerBowlingRecord['PlayerBowlingRecord']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $playerBowlingRecord['PlayerBowlingRecord']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $playerBowlingRecord['PlayerBowlingRecord']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $playerBowlingRecord['PlayerBowlingRecord']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
		</tbody>
	</table>

	<p>
		<small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
	</p>

	<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
	<ul class="pagination pagination-sm">
		<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
	</ul>
	<?php } ?>
