	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Add Player Bowling Record'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('PlayerBowlingRecord', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('player_id', array('class' => 'form-control', 'placeholder' => 'Player Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('level', array('class' => 'form-control', 'placeholder' => 'Level'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('match_count', array('class' => 'form-control', 'placeholder' => 'Match Count'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('innings_count', array('class' => 'form-control', 'placeholder' => 'Innings Count'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('balls_bowled', array('class' => 'form-control', 'placeholder' => 'Balls Bowled'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('runs_given', array('class' => 'form-control', 'placeholder' => 'Runs Given'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('wickets_taken', array('class' => 'form-control', 'placeholder' => 'Wickets Taken'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('bowling_average', array('class' => 'form-control', 'placeholder' => 'Bowling Average'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('best_bowling_innings', array('class' => 'form-control', 'placeholder' => 'Best Bowling Innings'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('best_bowling_match', array('class' => 'form-control', 'placeholder' => 'Best Bowling Match'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('bowling_economy', array('class' => 'form-control', 'placeholder' => 'Bowling Economy'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('strike_rate', array('class' => 'form-control', 'placeholder' => 'Strike Rate'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('four_wicket_innings', array('class' => 'form-control', 'placeholder' => 'Four Wicket Innings'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('five_wicket_innings', array('class' => 'form-control', 'placeholder' => 'Five Wicket Innings'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('ten_wicket_matches', array('class' => 'form-control', 'placeholder' => 'Ten Wicket Matches'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

