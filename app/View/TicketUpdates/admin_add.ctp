	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Add Ticket Update'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('TicketUpdate', array('role' => 'form')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('ticket_id', array('class' => 'form-control', 'placeholder' => 'Ticket Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('user_id', array('class' => 'form-control', 'placeholder' => 'User Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('activity', array('class' => 'form-control', 'placeholder' => 'Activity'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('comment', array('class' => 'form-control', 'placeholder' => 'Comment'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

