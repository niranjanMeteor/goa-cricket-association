	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Ticket Update'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($ticketUpdate['TicketUpdate']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Ticket'); ?></th>
		<td>
			<?php echo $this->Html->link($ticketUpdate['Ticket']['title'], array('controller' => 'tickets', 'action' => 'view', $ticketUpdate['Ticket']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('User'); ?></th>
		<td>
			<?php echo $this->Html->link($ticketUpdate['User']['email'], array('controller' => 'users', 'action' => 'view', $ticketUpdate['User']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Activity'); ?></th>
		<td>
			<?php echo h($ticketUpdate['TicketUpdate']['activity']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Comment'); ?></th>
		<td>
			<?php echo h($ticketUpdate['TicketUpdate']['comment']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($ticketUpdate['TicketUpdate']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($ticketUpdate['TicketUpdate']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>