	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Admin Edit Player'); ?></h1>
		</div>
	</div>

			<?php echo $this->Form->create('Player', array('role' => 'form', 'type' => 'file')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('id', array('class' => 'form-control', 'placeholder' => 'Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('first_name', array('class' => 'form-control', 'placeholder' => 'First Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('last_name', array('class' => 'form-control', 'placeholder' => 'Last Name'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('dob', array('class' => 'form-control', 'placeholder' => 'Dob', 'minYear' => date('Y') - 70, 'maxYear' => date('Y')));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('role', array('class' => 'form-control', 'placeholder' => 'Player Role'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('bowling_arm', array('class' => 'form-control', 'placeholder' => 'Bowling Arm', 'type' => 'select', 'options' => $playerArms, 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('batting_arm', array('class' => 'form-control', 'placeholder' => 'Batting Arm', 'type' => 'select', 'options' => $battingHands, 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('bowling_style', array('class' => 'form-control', 'placeholder' => 'Bowling Style', 'empty' => true));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('address', array('class' => 'form-control', 'placeholder' => 'Address'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('pincode', array('class' => 'form-control', 'placeholder' => 'Pincode'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('phone', array('class' => 'form-control', 'placeholder' => 'Phone'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('city_id', array('class' => 'form-control', 'placeholder' => 'City Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('image', array('class' => 'form-control', 'placeholder' => 'Image', 'type' => 'file'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('dir', array('class' => 'form-control', 'placeholder' => 'Dir', 'type' => 'hidden'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-default')); ?>
				</div>

			<?php echo $this->Form->end() ?>

