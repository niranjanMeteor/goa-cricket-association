	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Player'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($player['Player']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('First Name'); ?></th>
		<td>
			<?php echo h($player['Player']['first_name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Last Name'); ?></th>
		<td>
			<?php echo h($player['Player']['last_name']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Dob'); ?></th>
		<td>
			<?php echo h($player['Player']['dob']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Bowling Arm'); ?></th>
		<td>
			<?php echo h($player['Player']['bowling_arm']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Batting Arm'); ?></th>
		<td>
			<?php echo h($player['Player']['batting_arm']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Bowling Style'); ?></th>
		<td>
			<?php echo h($player['Player']['bowling_style']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Address'); ?></th>
		<td>
			<?php echo h($player['Player']['address']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Pincode'); ?></th>
		<td>
			<?php echo h($player['Player']['pincode']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Phone'); ?></th>
		<td>
			<?php echo h($player['Player']['phone']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('City'); ?></th>
		<td>
			<?php echo $this->Html->link($player['City']['name'], array('controller' => 'cities', 'action' => 'view', $player['City']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Image'); ?></th>
		<td>
			<?php echo h($player['Player']['image']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Dir'); ?></th>
		<td>
			<?php echo h($player['Player']['dir']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($player['Player']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($player['Player']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>