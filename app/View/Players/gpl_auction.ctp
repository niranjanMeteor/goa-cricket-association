<?php echo $this->Html->css('gpl_auction', array('inline' => false)) ?>
<?php echo $this->Html->script('build/react', array('inline' => false)); ?>
<?php echo $this->Html->script('moment.min', array('inline' => false)); ?>
<?php echo $this->Html->script('jquery.backstretch.min', array('inline' => false)); ?>
<?php echo $this->Html->script('jquery.countdown.min', array('inline' => false)); ?>
<?php echo $this->Html->script('jquery.slimscroll.min', array('inline' => false)); ?>
<?php echo $this->Html->script('jQuery.print', array('inline' => false)); ?>

<div id="reactId"></div>
  <script src="js/build/auction.react.js"></script>
