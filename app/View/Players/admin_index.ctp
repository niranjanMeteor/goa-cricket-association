	<div class="row">
		<div class="col-md-12">
			<h1><?php echo __('Players'); ?></h1>
		</div><!-- end col md 12 -->
	</div><!-- end row -->


	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<thead>
			<tr>
		<th><?php echo $this->Paginator->sort('id'); ?></th>
		<th><?php echo $this->Paginator->sort('first_name'); ?></th>
		<th><?php echo $this->Paginator->sort('last_name'); ?></th>
		<th><?php echo $this->Paginator->sort('dob'); ?></th>
		<th><?php echo $this->Paginator->sort('bowling_arm'); ?></th>
		<th><?php echo $this->Paginator->sort('batting_arm'); ?></th>
		<th><?php echo $this->Paginator->sort('bowling_style'); ?></th>
		<th><?php echo $this->Paginator->sort('address'); ?></th>
		<th><?php echo $this->Paginator->sort('pincode'); ?></th>
		<th><?php echo $this->Paginator->sort('phone'); ?></th>
		<th><?php echo $this->Paginator->sort('city_id'); ?></th>
		<th><?php echo $this->Paginator->sort('image'); ?></th>
		<th><?php echo $this->Paginator->sort('dir'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('modified'); ?></th>
		<th class="actions"></th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($players as $player): ?>
					<tr>
						<td><?php echo h($player['Player']['id']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['first_name']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['last_name']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['dob']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['bowling_arm']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['batting_arm']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['bowling_style']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['address']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['pincode']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['phone']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($player['City']['name'], array('controller' => 'cities', 'action' => 'view', $player['City']['id'])); ?>
		</td>
						<td><?php echo h($player['Player']['image']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['dir']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['created']); ?>&nbsp;</td>
						<td><?php echo h($player['Player']['modified']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $player['Player']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $player['Player']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $player['Player']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $player['Player']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
		</tbody>
	</table>

	<p>
		<small><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?></small>
	</p>

	<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
	<ul class="pagination pagination-sm">
		<?php
					echo $this->Paginator->prev('&larr; Previous', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Previous</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Next &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Next &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
	</ul>
	<?php } ?>
