<?php echo $this->Html->css('view_player', array('inline' => false)); ?>
<div class="container">
  <div class="row">
    <div class="col-md-3">
      <div class="sidebar">
        <div class="player-info card">
          <div class="player-pic text-center">
            <?php echo $this->Html->image("{$player['Player']['dir']}/profile_{$player['Player']['image']}", array('pathPrefix' => 'files/player/image/', 'class' => 'img-circle player-pic')); ?>
          </div>
          <div class="player-name text-center">
            <?php echo __($player['Player']['name']); ?>
          </div>
          <div class="player-role text-center">
            <?php echo __(PlayerRole::stringValue($player['Player']['role'])) ?>
          </div>
          <?php if (!empty($player['Player']['city_id'])): ?>
            <div class="player-city text-center">
              <?php echo __($player['City']['name']); ?>
            </div>
          <?php endif; ?>
        </div>

        <div class="personal-info card">
          <div class="title">
            About
          </div>
          <div class="major-teams">
            <?php 
              $teams = array();
              if (!empty($player['TeamPlayer'])) {
                foreach ($player['TeamPlayer'] as $team) {
                  $teams[] = $team['Team']['name'];
                }
              }
            ?>
            <p><strong>Major Teams:-</strong> <span><?php echo __(join(', ', $teams));  ?></span></p>
          </div>

          <div class="player-dob">
            <p><strong>Born:-</strong> <span><?php echo date('F d, Y', strtotime($player['Player']['dob'])); ?></span></p>
          </div>
          <div class="player-age">
            <?php 
              function get_age($birth_date){
                return floor((time() - strtotime($birth_date))/31556926);
              }
            ?>
            <p><strong>Age:-</strong> <span><?php echo _(get_age($player['Player']['dob']) . ' years')  ?></span></p>
          </div>
          <div class="batting-style">
            <p><strong>Batting Style:-</strong> <span><?php echo __(BattingHand::stringValue($player['Player']['batting_arm']) . ' Bat'); ?></span></p>
          </div>
          <div class="bowling-style">
            <p><strong>Bowling Style:-</strong> <span><?php echo __(PlayerArm::stringValue($player['Player']['bowling_arm']) . ' ' . BowlingStyle::stringValue($player['Player']['bowling_style'])); ?></span></p>
          </div>
        </div>
        
      </div>
      
      
    </div>
    <div class="col-md-9">
      <div class="batting-stats card">
        <div class="title">
          Batting and Fielding Statistics
        </div>
        <div class="table-responsive">
          <table class="table table-hover custom-table">
            <thead>
              <tr>
                <th>Level</th>
                <th>Matches</th>
                <th>Innings</th>
                <th>Not Outs</th>
                <th>Runs</th>
                <th>High Score</th>
                <th>Average</th>
                <th>Balls Faced</th>
                <th>Strike Rate</th>
                <th>100</th>
                <th>50</th>
                <th>4s</th>
                <th>6s</th>
                <th>Catches</th>
                <th>Stumpings</th>
              </tr>
            </thead>
            <tbody>
              <?php if ( ! empty($player['PlayerBattingRecord'])): ?>
                <?php foreach ($player['PlayerBattingRecord'] as $record): ?>
                  <tr>
                    <td class="match-level"><?php echo __(MatchLevel::stringValue($record['level'])) ?></td>
                    <td><?php echo __(!empty($record['match_count']) ? $record['match_count'] : '-') ?></td>
                    <td><?php echo __(!empty($record['innings_count']) ? $record['innings_count'] : '-') ?></td>
                    <td><?php echo __(!empty($record['not_outs']) ? $record['not_outs'] : '-') ?></td>
                    <td><?php echo __(!empty($record['runs']) ? $record['runs'] : '-') ?></td>
                    <td><?php echo __(!empty($record['high_score']) ? $record['high_score'] : '-') ?></td>
                    <td><?php echo __(!empty($record['batting_average']) ? $record['batting_average'] : '-') ?></td>
                    <td><?php echo __(!empty($record['ball_faced']) ? $record['ball_faced'] : '-') ?></td>
                    <td><?php echo __(!empty($record['strike_rate']) ? $record['strike_rate'] : '-') ?></td>
                    <td><?php echo __(!empty($record['hundreds']) ? $record['hundreds'] : '-') ?></td>
                    <td><?php echo __(!empty($record['fifties']) ? $record['fifties'] : '-') ?></td>
                    <td><?php echo __(!empty($record['fours']) ? $record['fours'] : '-') ?></td>
                    <td><?php echo __(!empty($record['sixes']) ? $record['sixes'] : '-') ?></td>
                    <td><?php echo __(!empty($record['catches_taken']) ? $record['catches_taken'] : '-') ?></td>
                    <td><?php echo __(!empty($record['stumpings_made']) ? $record['stumpings_made'] : '-') ?></td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="bowling-stats card">
        <div class="title">
          Bowling Statistics
        </div>
        <div class="table-responsive">
          <table class="table table-hover custom-table">
            <thead>
              <tr>
                <th>Level</th>
                <th>Matches</th>
                <th>Innings</th>
                <th>Balls</th>
                <th>Runs</th>
                <th>Wickets</th>
                <th>average</th>
                <th>Best Innings</th>
                <th>Best Match</th>
                <th>Economy</th>
                <th>Strike Rate</th>
                <th>4w</th>
                <th>5w</th>
                <th>10</th>
              </tr>
            </thead>
            <tbody>
              <?php if ( ! empty($player['PlayerBowlingRecord'])): ?>
                <?php foreach ($player['PlayerBowlingRecord'] as $record): ?>
                  <tr>
                    <td class="match-level"><?php echo __(MatchLevel::stringValue($record['level'])) ?></td>
                    <td><?php echo __(!empty($record['match_count']) ? $record['match_count'] : '-') ?></td>
                    <td><?php echo __(!empty($record['innings_count']) ? $record['innings_count'] : '-') ?></td>
                    <td><?php echo __(!empty($record['balls_bowled']) ? $record['balls_bowled'] : '-') ?></td>
                    <td><?php echo __(!empty($record['runs_given']) ? $record['runs_given'] : '-') ?></td>
                    <td><?php echo __(!empty($record['wickets_taken']) ? $record['wickets_taken'] : '-') ?></td>
                    <td><?php echo __(!empty($record['bowling_average']) ? $record['bowling_average'] : '-') ?></td>
                    <td><?php echo __(!empty($record['best_bowling_innings']) ? $record['best_bowling_innings'] : '-') ?></td>
                    <td><?php echo __(!empty($record['best_bowling_match']) ? $record['best_bowling_match'] : '-') ?></td>
                    <td><?php echo __(!empty($record['bowling_economy']) ? $record['bowling_economy'] : '-') ?></td>
                    <td><?php echo __(!empty($record['strike_rate']) ? $record['strike_rate'] : '-') ?></td>
                    <td><?php echo __(!empty($record['four_wicket_innings']) ? $record['four_wicket_innings'] : '-') ?></td>
                    <td><?php echo __(!empty($record['five_wicket_innings']) ? $record['five_wicket_innings'] : '-') ?></td>
                    <td><?php echo __(!empty($record['ten_wicket_matches']) ? $record['ten_wicket_matches'] : '-') ?></td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>