	<div class="row">
		<div class="col-md-12">
				<h1><?php echo __('Bid History'); ?></h1>
		</div>
	</div>
	<table cellpadding="0" cellspacing="0" class="table table-striped">
		<tbody>
		<tr>
		<th><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($bidHistory['BidHistory']['id']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Bid'); ?></th>
		<td>
			<?php echo $this->Html->link($bidHistory['Bid']['id'], array('controller' => 'bids', 'action' => 'view', $bidHistory['Bid']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Team'); ?></th>
		<td>
			<?php echo $this->Html->link($bidHistory['Team']['name'], array('controller' => 'teams', 'action' => 'view', $bidHistory['Team']['id'])); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Amount'); ?></th>
		<td>
			<?php echo h($bidHistory['BidHistory']['amount']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($bidHistory['BidHistory']['created']); ?>
			&nbsp;
		</td>
</tr>
<tr>
		<th><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($bidHistory['BidHistory']['modified']); ?>
			&nbsp;
		</td>
</tr>
		</tbody>
	</table>