<?php
/**
 * RoundFixture
 *
 */
class RoundFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'auction_group_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'in_progress' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'is_completed' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'auction_group_id' => 1,
			'in_progress' => 1,
			'is_completed' => 1,
			'created' => '2015-02-04 03:27:09',
			'modified' => '2015-02-04 03:27:09'
		),
	);

}
