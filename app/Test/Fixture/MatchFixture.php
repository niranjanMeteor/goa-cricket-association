<?php
/**
 * MatchFixture
 *
 */
class MatchFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'series_name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'start_date_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'end_date_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'location' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'level' => array('type' => 'integer', 'null' => true, 'default' => '0', 'length' => 3, 'unsigned' => false),
		'type' => array('type' => 'integer', 'null' => true, 'default' => '0', 'length' => 3, 'unsigned' => false),
		'overs_per_innings' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'first_team_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'second_team_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'toss_winning_team_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'toss_decision' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 3, 'unsigned' => false),
		'result_type' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 3, 'unsigned' => false),
		'winning_team_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_bin', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'series_name' => 'Lorem ipsum dolor sit amet',
			'start_date_time' => '2015-02-12 05:11:04',
			'end_date_time' => '2015-02-12 05:11:04',
			'location' => 'Lorem ipsum dolor sit amet',
			'level' => 1,
			'type' => 1,
			'overs_per_innings' => 1,
			'first_team_id' => 1,
			'second_team_id' => 1,
			'toss_winning_team_id' => 1,
			'toss_decision' => 1,
			'result_type' => 1,
			'winning_team_id' => 1,
			'created' => '2015-02-12 05:11:04',
			'modified' => '2015-02-12 05:11:04'
		),
	);

}
