<?php
/**
 * TicketFixture
 *
 */
class TicketFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'project_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'creator_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'assignee_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'category' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'title' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'description' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'status' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'start_date' => array('type' => 'date', 'null' => true, 'default' => null),
		'end_date' => array('type' => 'date', 'null' => true, 'default' => null),
		'done' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'story_points' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'priority' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'is_postponed' => array('type' => 'boolean', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'project_id' => 1,
			'creator_id' => 1,
			'assignee_id' => 1,
			'category' => 1,
			'title' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet',
			'status' => 1,
			'start_date' => '2014-12-20',
			'end_date' => '2014-12-20',
			'done' => 1,
			'story_points' => 1,
			'priority' => 1,
			'is_postponed' => 1,
			'created' => '2014-12-20 12:35:29',
			'modified' => '2014-12-20 12:35:29'
		),
	);

}
