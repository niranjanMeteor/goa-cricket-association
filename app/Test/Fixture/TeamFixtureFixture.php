<?php
/**
 * TeamFixtureFixture
 *
 */
class TeamFixtureFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'team_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'against' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'start_date_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'end_date_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'venue' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'team_id' => 1,
			'against' => 'Lorem ipsum dolor sit amet',
			'start_date_time' => '2014-12-31 18:47:39',
			'end_date_time' => '2014-12-31 18:47:39',
			'venue' => 1,
			'created' => '2014-12-31 18:47:39',
			'modified' => '2014-12-31 18:47:39'
		),
	);

}
