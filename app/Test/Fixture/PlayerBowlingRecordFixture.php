<?php
/**
 * PlayerBowlingRecordFixture
 *
 */
class PlayerBowlingRecordFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'player_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'level' => array('type' => 'integer', 'null' => false, 'default' => '1', 'unsigned' => false),
		'match_count' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'innings_count' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'balls_bowled' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'runs_given' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'wickets_taken' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'bowling_average' => array('type' => 'string', 'null' => true, 'default' => '0.0', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'best_bowling_innings' => array('type' => 'string', 'null' => true, 'default' => 'NA', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'best_bowling_match' => array('type' => 'string', 'null' => true, 'default' => 'NA', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'bowling_economy' => array('type' => 'string', 'null' => true, 'default' => '0.0', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'strike_rate' => array('type' => 'string', 'null' => true, 'default' => '0.0', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'four_wicket_innings' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'five_wicket_innings' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'ten_wicket_matches' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'player_id' => 1,
			'level' => 1,
			'match_count' => 1,
			'innings_count' => 1,
			'balls_bowled' => 1,
			'runs_given' => 1,
			'wickets_taken' => 1,
			'bowling_average' => 'Lorem ipsum dolor sit amet',
			'best_bowling_innings' => 'Lorem ipsum dolor sit amet',
			'best_bowling_match' => 'Lorem ipsum dolor sit amet',
			'bowling_economy' => 'Lorem ipsum dolor sit amet',
			'strike_rate' => 'Lorem ipsum dolor sit amet',
			'four_wicket_innings' => 1,
			'five_wicket_innings' => 1,
			'ten_wicket_matches' => 1,
			'created' => '2014-12-31 18:48:17',
			'modified' => '2014-12-31 18:48:17'
		),
	);

}
