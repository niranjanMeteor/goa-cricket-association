<?php
/**
 * TeamSupportStaffFixture
 *
 */
class TeamSupportStaffFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'team_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'support_staff_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'from_date' => array('type' => 'date', 'null' => true, 'default' => null),
		'to_date' => array('type' => 'date', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'team_id' => 1,
			'support_staff_id' => 1,
			'from_date' => '2015-01-03',
			'to_date' => '2015-01-03',
			'created' => '2015-01-03 04:50:09',
			'modified' => '2015-01-03 04:50:09'
		),
	);

}
