<?php
/**
 * UserFixture
 *
 */
class UserFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'employeeID' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'email' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'password' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'password_reset' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'is_verified' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'level' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'role' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'last_logged_in_time' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'last_logged_in_ip' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'employeeID' => 'Lorem ipsum dolor sit amet',
			'email' => 'Lorem ipsum dolor sit amet',
			'password' => 'Lorem ipsum dolor sit amet',
			'password_reset' => 'Lorem ipsum dolor sit amet',
			'is_verified' => 1,
			'level' => 1,
			'role' => 1,
			'last_logged_in_time' => '2014-12-20 12:37:41',
			'last_logged_in_ip' => 'Lorem ipsum dolor sit amet',
			'created' => '2014-12-20 12:37:41',
			'modified' => '2014-12-20 12:37:41'
		),
	);

}
