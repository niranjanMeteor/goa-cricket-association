<?php
/**
 * PlayerAuctionGroupFixture
 *
 */
class PlayerAuctionGroupFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'player_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'auction_group_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'is_bought' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'is_deleted' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'player_id' => 1,
			'auction_group_id' => 1,
			'is_bought' => 1,
			'is_deleted' => 1,
			'created' => '2015-02-04 03:20:51',
			'modified' => '2015-02-04 03:20:51'
		),
	);

}
