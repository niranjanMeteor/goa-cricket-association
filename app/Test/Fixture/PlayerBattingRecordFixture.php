<?php
/**
 * PlayerBattingRecordFixture
 *
 */
class PlayerBattingRecordFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'player_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'level' => array('type' => 'integer', 'null' => false, 'default' => '1', 'unsigned' => false),
		'match_count' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'innings_count' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'not_outs' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'runs' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'high_score' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'batting_average' => array('type' => 'string', 'null' => true, 'default' => '0.0', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'ball_faced' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'strike_rate' => array('type' => 'string', 'null' => true, 'default' => '0.0', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'hundreds' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'fifties' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'fours' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'sixes' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'catches_taken' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'stumpings_made' => array('type' => 'integer', 'null' => true, 'default' => '0', 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'player_id' => 1,
			'level' => 1,
			'match_count' => 1,
			'innings_count' => 1,
			'not_outs' => 1,
			'runs' => 1,
			'high_score' => 1,
			'batting_average' => 'Lorem ipsum dolor sit amet',
			'ball_faced' => 1,
			'strike_rate' => 'Lorem ipsum dolor sit amet',
			'hundreds' => 1,
			'fifties' => 1,
			'fours' => 1,
			'sixes' => 1,
			'catches_taken' => 1,
			'stumpings_made' => 1,
			'created' => '2014-12-31 18:48:01',
			'modified' => '2014-12-31 18:48:01'
		),
	);

}
