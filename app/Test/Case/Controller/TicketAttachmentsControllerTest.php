<?php
App::uses('TicketAttachmentsController', 'Controller');

/**
 * TicketAttachmentsController Test Case
 *
 */
class TicketAttachmentsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ticket_attachment',
		'app.ticket',
		'app.project',
		'app.user_project',
		'app.user',
		'app.profile',
		'app.designation',
		'app.image',
		'app.album',
		'app.city',
		'app.state',
		'app.country',
		'app.ticket_update',
		'app.ticket_watcher',
		'app.image_comment',
		'app.ticket_relation'
	);

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
		$this->markTestIncomplete('testAdminIndex not implemented.');
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
		$this->markTestIncomplete('testAdminView not implemented.');
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
		$this->markTestIncomplete('testAdminAdd not implemented.');
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
		$this->markTestIncomplete('testAdminEdit not implemented.');
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
		$this->markTestIncomplete('testAdminDelete not implemented.');
	}

}
