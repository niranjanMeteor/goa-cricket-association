<?php
App::uses('AuctionTeam', 'Model');

/**
 * AuctionTeam Test Case
 *
 */
class AuctionTeamTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.auction_team',
		'app.team',
		'app.team_fixture',
		'app.team_player',
		'app.player',
		'app.city',
		'app.state',
		'app.country',
		'app.profile',
		'app.user',
		'app.ticket_update',
		'app.ticket',
		'app.project',
		'app.user_project',
		'app.ticket_attachment',
		'app.ticket_relation',
		'app.ticket_watcher',
		'app.album',
		'app.image',
		'app.image_comment',
		'app.designation',
		'app.player_batting_record',
		'app.player_bowling_record',
		'app.team_support_staff',
		'app.support_staff'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AuctionTeam = ClassRegistry::init('AuctionTeam');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AuctionTeam);

		parent::tearDown();
	}

}
