<?php
App::uses('AuctionGroup', 'Model');

/**
 * AuctionGroup Test Case
 *
 */
class AuctionGroupTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.auction_group'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AuctionGroup = ClassRegistry::init('AuctionGroup');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AuctionGroup);

		parent::tearDown();
	}

}
