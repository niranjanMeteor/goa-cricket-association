<?php
App::uses('MatchBowlerScore', 'Model');

/**
 * MatchBowlerScore Test Case
 *
 */
class MatchBowlerScoreTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.match_bowler_score',
		'app.match_inning_score',
		'app.match',
		'app.match_player',
		'app.team',
		'app.auction_team',
		'app.team_fixture',
		'app.team_player',
		'app.player',
		'app.city',
		'app.state',
		'app.country',
		'app.profile',
		'app.user',
		'app.ticket_update',
		'app.ticket',
		'app.project',
		'app.user_project',
		'app.ticket_attachment',
		'app.ticket_relation',
		'app.ticket_watcher',
		'app.album',
		'app.image',
		'app.image_comment',
		'app.designation',
		'app.player_batting_record',
		'app.player_bowling_record',
		'app.player_auction_group',
		'app.auction_group',
		'app.round',
		'app.bid',
		'app.bid_history',
		'app.team_support_staff',
		'app.support_staff',
		'app.match_ball_score',
		'app.match_batsman_score'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MatchBowlerScore = ClassRegistry::init('MatchBowlerScore');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MatchBowlerScore);

		parent::tearDown();
	}

}
