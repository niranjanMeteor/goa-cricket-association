<?php
App::uses('PressRelease', 'Model');

/**
 * PressRelease Test Case
 *
 */
class PressReleaseTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.press_release'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PressRelease = ClassRegistry::init('PressRelease');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PressRelease);

		parent::tearDown();
	}

}
