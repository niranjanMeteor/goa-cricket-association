<?php
App::uses('TicketWatcher', 'Model');

/**
 * TicketWatcher Test Case
 *
 */
class TicketWatcherTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ticket_watcher',
		'app.ticket',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TicketWatcher = ClassRegistry::init('TicketWatcher');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TicketWatcher);

		parent::tearDown();
	}

}
