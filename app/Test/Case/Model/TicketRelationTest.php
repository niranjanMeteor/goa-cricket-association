<?php
App::uses('TicketRelation', 'Model');

/**
 * TicketRelation Test Case
 *
 */
class TicketRelationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ticket_relation',
		'app.ticket'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TicketRelation = ClassRegistry::init('TicketRelation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TicketRelation);

		parent::tearDown();
	}

}
