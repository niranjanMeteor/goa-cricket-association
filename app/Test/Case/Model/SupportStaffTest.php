<?php
App::uses('SupportStaff', 'Model');

/**
 * SupportStaff Test Case
 *
 */
class SupportStaffTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.support_staff',
		'app.team_support_staff'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SupportStaff = ClassRegistry::init('SupportStaff');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SupportStaff);

		parent::tearDown();
	}

}
