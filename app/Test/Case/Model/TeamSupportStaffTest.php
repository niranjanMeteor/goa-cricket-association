<?php
App::uses('TeamSupportStaff', 'Model');

/**
 * TeamSupportStaff Test Case
 *
 */
class TeamSupportStaffTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.team_support_staff',
		'app.team',
		'app.team_fixture',
		'app.team_player',
		'app.player',
		'app.city',
		'app.state',
		'app.country',
		'app.profile',
		'app.user',
		'app.ticket_update',
		'app.ticket',
		'app.project',
		'app.user_project',
		'app.ticket_attachment',
		'app.ticket_relation',
		'app.ticket_watcher',
		'app.album',
		'app.image',
		'app.image_comment',
		'app.designation',
		'app.player_batting_record',
		'app.player_bowling_record',
		'app.support_staff'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TeamSupportStaff = ClassRegistry::init('TeamSupportStaff');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TeamSupportStaff);

		parent::tearDown();
	}

}
