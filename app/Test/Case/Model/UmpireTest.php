<?php
App::uses('Umpire', 'Model');

/**
 * Umpire Test Case
 *
 */
class UmpireTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.umpire'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Umpire = ClassRegistry::init('Umpire');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Umpire);

		parent::tearDown();
	}

}
