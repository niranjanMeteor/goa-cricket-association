<?php
App::uses('TicketUpdate', 'Model');

/**
 * TicketUpdate Test Case
 *
 */
class TicketUpdateTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ticket_update',
		'app.ticket',
		'app.user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TicketUpdate = ClassRegistry::init('TicketUpdate');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TicketUpdate);

		parent::tearDown();
	}

}
