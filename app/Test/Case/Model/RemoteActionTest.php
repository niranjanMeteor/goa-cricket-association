<?php
App::uses('RemoteAction', 'Model');

/**
 * RemoteAction Test Case
 *
 */
class RemoteActionTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.remote_action'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->RemoteAction = ClassRegistry::init('RemoteAction');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->RemoteAction);

		parent::tearDown();
	}

}
