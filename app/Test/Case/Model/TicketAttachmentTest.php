<?php
App::uses('TicketAttachment', 'Model');

/**
 * TicketAttachment Test Case
 *
 */
class TicketAttachmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ticket_attachment',
		'app.ticket'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TicketAttachment = ClassRegistry::init('TicketAttachment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TicketAttachment);

		parent::tearDown();
	}

}
