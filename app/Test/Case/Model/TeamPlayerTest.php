<?php
App::uses('TeamPlayer', 'Model');

/**
 * TeamPlayer Test Case
 *
 */
class TeamPlayerTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.team_player',
		'app.team',
		'app.team_fixture',
		'app.player',
		'app.city',
		'app.state',
		'app.country',
		'app.profile',
		'app.user',
		'app.ticket_update',
		'app.ticket',
		'app.project',
		'app.user_project',
		'app.ticket_attachment',
		'app.ticket_relation',
		'app.ticket_watcher',
		'app.album',
		'app.image',
		'app.image_comment',
		'app.designation',
		'app.player_batting_record',
		'app.player_bowling_record'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TeamPlayer = ClassRegistry::init('TeamPlayer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TeamPlayer);

		parent::tearDown();
	}

}
