<?php
App::uses('Album', 'Model');

/**
 * Album Test Case
 *
 */
class AlbumTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.album',
		'app.user',
		'app.profile',
		'app.designation',
		'app.image',
		'app.city',
		'app.state',
		'app.country',
		'app.ticket_update',
		'app.ticket',
		'app.project',
		'app.user_project',
		'app.ticket_attachment',
		'app.ticket_relation',
		'app.ticket_watcher'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Album = ClassRegistry::init('Album');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Album);

		parent::tearDown();
	}

}
