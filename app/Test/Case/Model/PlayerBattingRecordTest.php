<?php
App::uses('PlayerBattingRecord', 'Model');

/**
 * PlayerBattingRecord Test Case
 *
 */
class PlayerBattingRecordTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.player_batting_record',
		'app.player',
		'app.city',
		'app.state',
		'app.country',
		'app.profile',
		'app.user',
		'app.ticket_update',
		'app.ticket',
		'app.project',
		'app.user_project',
		'app.ticket_attachment',
		'app.ticket_relation',
		'app.ticket_watcher',
		'app.album',
		'app.image',
		'app.image_comment',
		'app.designation',
		'app.player_bowling_record',
		'app.team_player',
		'app.team',
		'app.team_fixture'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PlayerBattingRecord = ClassRegistry::init('PlayerBattingRecord');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PlayerBattingRecord);

		parent::tearDown();
	}

}
